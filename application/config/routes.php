<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = 'Dashboard';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
//$route['admin/login'] = 'Auth/login';
$route['admin'] = 'Auth/admin_login';
$route['admin/forgot_password'] = 'Auth/admin_forgot_pass';
$route['admin/change_password'] = 'Auth/change_password';
$route['admin/reset_password/(:any)'] = 'Auth/reset_password/$1';
$route['admin/dashboard'] = 'AdminDashboard';
$route['admin/logout'] = 'Auth/logout';

// Dindi Management
$route['admin/dindi'] = 'Dindi';
$route['admin/dindi/add'] = 'Dindi/add_dindi';
$route['admin/dindi/update/(:num)'] = 'Dindi/update_dindi/$1';

//Seva Samiti Management
$route['admin/samiti'] = 'Sevasamiti';
$route['admin/samiti/add'] = 'Sevasamiti/add_Sevasamiti';
$route['admin/samiti/update/(:num)'] = 'Sevasamiti/update_Sevasamiti/$1';

//Sevekari
$route['admin/sevekari/(:num)'] = 'Sevekari/sevekari_list/$1';

//Manage Supplier
$route['admin/supplier/(:num)'] = "Supplier/resource_list/$1";
$route['admin/supplier/add/(:num)'] = "Supplier/add_resource/$1";
$route['admin/supplier/update/(:num)/(:num)'] = "Supplier/update_resource/$1/$2";

//Vehicle Management
$route['admin/vehicle'] = "Vehicle";
$route['admin/vehicle/add'] = "Vehicle/add_vehicle";
$route['admin/vehicle/update/(:num)'] = "Vehicle/update_vehicle/$1";

//Devotee Management
$route['admin/devotee'] = "Devotees";
$route['admin/devotee/add'] = "Devotees/add_devotee";
$route['admin/devotee/update/(:num)'] = "Devotees/update_devotee/$1";
$route['admin/donation/pavti'] = "Donation/SendDonationReceipt";

//Padyatri Management
$route['admin/padyatri'] = "Padyatri";
$route['admin/padyatri/add'] = "Padyatri/add_padyatri";
$route['admin/padyatri/update/(:num)'] = "Padyatri/update_padyatri/$1";
//Donation report
$route['admin/donorlist'] = "Donation/Donor_list";

//----------------------------- User Route --------------------------------

$route['login'] = 'Auth/login';
$route['logout'] = 'Auth/logout';
$route['forgot_password'] = 'Auth/user_forgot_password';
$route['changepassword'] = 'Devotees/change_password';
$route['home'] = 'Dashboard';
$route['AccountDetails'] = "Devotees/account_details";
$route['enrollment'] = "Devotees/enrollment";
$route['terms'] = "Devotees/terms_and_condition";
$route['signup'] = "Devotees/signup";
$route['register'] = "Devotees/create_devotee";
$route['registersuccess'] = "Devotees/register_success";
$route['ccavRequestHandler'] = "Payment/ccavRequestHandler";
$route['enrollment-response'] = "Payment/ccavResponseHandler";
$route['PaymentSuccess'] = "Payment/payment_success";
$route['donation'] = "Donation/index";
$route['donation-response'] = "Donation/ccavResponseHandler";
$route['thankyou'] = "Donation/donation_success";
//--------------------- event galary---------------------------------------
$route['admin/album'] = 'Photogallery';
$route['admin/add_album'] = 'Photogallery/create_album';
$route['admin/add_album_details'] = 'Photogallery/add_album_details';
$route['admin/add_album_details/(:num)'] = 'Photogallery/add_album_details/$1';
$route['admin/update_album_details/(:num)'] = 'Photogallery/update_album_details/$1';
$route['admin/update_image_detail'] = 'Photogallery/change_imagetitle';
$route['admin/delete_album_image'] = 'Photogallery/delete_album_image';
//----------------------------------------------USER SIDE ----------------------------------------------------
$route['photo-gallery'] = 'Photogallery/photo_gallery';
$route['photo-gallery-inner/(:num)'] = 'Photogallery/photo_gallery_inner/$1';
//------------------- Frontend Content Pages--------------------------
$route['aboutus'] = "Frontend/about_us";
$route['assurance-to-his-devotee'] = "Frontend/devotee_assurance";
$route['contact'] = "Frontend/contact_us";
$route['daily-routine-in-palkhi'] = "Frontend/palkhi_routine";
$route['history-of-palkhi'] = "Frontend/palakhi_history";
$route['schedule-of-palkhi'] = "Frontend/palakhi_schedule";
$route['privacy-policy'] = "Frontend/privacy_policy";
$route['refund-policy'] = "Frontend/refund_policy";
$route['rules-regulations'] = "Frontend/rules_regulations";
$route['schedule-of-palkhi-bhavan'] = "Frontend/palkhi_bhavan_schedule";
$route['shirdi'] = "Frontend/shirdi";
$route['trustee'] = "Frontend/trustee";
$route['upcoming-events'] = "Frontend/upcoming_events";
$route['video-gallery'] = "Frontend/video_gallery";
$route['vision-and-mission'] = "Frontend/vision_mission";
$route['why-palkhi'] = "Frontend/why_palkhi";

$route['admin/reports'] = "Reports/index";
$route['admin/DindiReport'] = "Reports/Dindi_report";
$route['admin/dindipadyatrireport/(:num)'] = "Reports/Dindi_report/DindiWisePadyatriReport/$1";
$route['admin/DevoteeEnrollment'] = "Reports/Devotee_enrollment";

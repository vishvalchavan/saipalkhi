<!-- Start header -->
<header class="top-navbar">
    <nav class="navbar navbar-expand-lg navbar-light p-0">
        <div class="container-fluid">
            <a class="navbar-brand" href="<?php echo base_url() . 'home' ?>">
                <img src="<?php echo base_url(); ?>assets/images/logo-new.png" alt="" class="img-responsive header-logo" />
            </a>
            <button class="navbar-toggler mr-4" type="button" data-toggle="collapse" data-target="#navbars-host" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbars-host">
                <ul class="navbar-nav ml-auto">
                    <!-- <li class="nav-item"><a class="nav-link" href="<?php echo base_url() . 'home' ?>">Home</a></li> -->
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="javascript:void();">About</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown-a">
                            <a class="dropdown-item" href="<?php echo base_url() . 'trustee' ?>">Trustee </a>
                            <a class="dropdown-item" href="<?php echo base_url() . 'vision-and-mission' ?>">Mission &amp; Vision</a>
                            <a class="dropdown-item" href="<?php echo base_url() . 'aboutus' ?>">About Us</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="javascript:void();">Sai Palkhi</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown-a">
                            <a class="dropdown-item" href="<?php echo base_url() . 'history-of-palkhi' ?>">History of Palkhi</a>
                            <a class="dropdown-item" href="<?php echo base_url() . 'why-palkhi' ?>">Palkhi and Varkari</a>
                            <a class="dropdown-item" href="<?php echo base_url() . 'rules-regulations' ?>">Rules and Regulations </a>
                            <a class="dropdown-item" href="<?php echo base_url() . 'daily-routine-in-palkhi' ?>">Daily routine in Palkhi</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="javascript:void();" id="dropdown-a" 
                           >Palkhi Bhavan</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown-a">
                            <a class="dropdown-item" href="<?php echo base_url() . 'schedule-of-palkhi-bhavan' ?>">Pune</a>
                            <a class="dropdown-item" href="<?php echo base_url() . 'shirdi' ?>">Shirdi</a>
                            <a class="dropdown-item" href="<?php echo base_url() . 'upcoming-events' ?>">Upcoming Events</a>
                        </div>
                    </li>
                    <?php if (!empty($this->session->userdata('user_id'))) { ?>
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url() . 'donation' ?>">Donate Now</a></li>
                    <?php } ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void();" id="dropdown-a" data-toggle="dropdown">Gallery</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown-a">
                            <a class="dropdown-item" href="<?php echo base_url() . 'photo-gallery' ?>">Photo</a>
                            <a class="dropdown-item" href="<?php echo base_url() . 'video-gallery' ?>">Video</a>
                        </div>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url() . 'contact' ?>">Contact</a></li>
                    <?php if (!empty($this->session->userdata('user_id'))) { ?>
                        <li class="nav-item dropdown u-pro">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>assets/images/icons8-contacts.png" alt="user" class="border-radius-100" >
                                <span class="hidden-md-down"><?php echo $this->session->userdata('firstname') . ' ' . $this->session->userdata('lastname') ?><i class="fa fa-angle-down"></i>
                                </span>
                            </a>
                            <div class="dropdown-menu mt-100">
                                <a href="<?php echo base_url() . 'AccountDetails' ?>" class="dropdown-item"><i class="fa fa-user"></i> Profile</a>
                                <a href="<?php echo base_url() . 'changepassword' ?>" class="dropdown-item"><i class="fa fa-cog"></i> Change Password</a>
                                <a href="<?php echo base_url() . 'logout' ?>" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>
                            </div>
                        </li>
                    <?php } else {
                        ?>
                        <!--<li class="nav-item"><a class="nav-link" href="<?php echo base_url() . 'signup' ?>">New Registration <i class="fa fa-user-plus fa-2x"></i></a></li>-->
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url() . 'login' ?>"><!-- <i class="fa fa-sign-in fa-2x"></i> -->Login</a></li>
                            <?php
                        }
                        ?>
                </ul>
            </div>
        </div>
    </nav>
</header>
<!-- End header -->
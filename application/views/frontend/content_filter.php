<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-xs-12">
                <div class="widget clearfix">
                    <div class="widget-title">
                        <h3>About US</h3>
                    </div>
                    <p>With the blessing of Shri Sainath maharaj & with immense support from sai devotees the “Shri Saibaba Palkhi Sohla” is moving towards 30 year of completion. </p>
                    <div class="footer-right">
                        <ul class="footer-links-soi">
                            <li><a href="https://www.facebook.com/SaiPalkhiPune"><i class="fa fa-facebook mt-1"></i></a></li>
                            <li><a href="https://www.youtube.com/channel/UC0QuzR-qXCpsTvKHv-eTCuQ"><i class="fa fa-youtube mt-1"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-xs-12">
                <div class="widget clearfix">
                    <div class="widget-title">
                        <h3>Information Link</h3>
                    </div>
                    <ul class="footer-links">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="about-us.html">About Us</a></li>
                        <li><a href="donation.html">Donate Now</a></li>
                        <li><a href="termsandconditions.html">Terms and Conditions</a></li>
                        <li><a href="privacy-policy.html">Privacy Policy</a></li>
                        <li><a href="refundpolicy.html">Refund and Cancellation</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-xs-12">
                <div class="widget clearfix">
                    <div class="widget-title">
                        <h3>Contact Details</h3>
                    </div>
                    <ul class="footer-links">
                        <li><strong class=""> Address </strong>:<br>
                            Shri Saibaba Palakhi Bhavan, 25,Kasaba Peth,Gangotri Building Fadake Houda Chowk,Pune-411011</li>
                        <li>Phone: 020-2457 2122</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="copyrights">
    <div class="container">
        <div class="footer-distributed">
            <div class="footer-center">
                <p class="footer-company-name">© 2019 Shri Saibaba Palakhi Solhala Samiti, Pune.</p>
            </div>
        </div>
    </div>
</div> 
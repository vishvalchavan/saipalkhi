<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" >
      <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?php echo isset($dataHeader['title']) ? $dataHeader['title'] : ''; ?></title>
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
      <meta name="keywords" content="Shri Saibaba Palakhi Sohala Pune">
      <meta name="keywords" content="Saibaba Palakhi,Pune">
      <meta name="keywords" content="Sai Palakhi">
      <meta name="keywords" content="Pune to Shirdi">
      <meta name="keywords" content="Padyatra">
      <meta name="keywords" content="Gurupournima Saibaba festival">
      <meta name="keywords" content="Pune Palkhi Sohla,Palkhi Bhavan">
      <meta name="keywords" content="Saibabapalkhisohla">
      <meta name="keywords" content="Sai baba palkhi samiti">
      <meta name="keywords" content="Shirdi Palkhi Sohla">
      <meta name="keywords" content="Pune to Shirdipadyatra">
      <meta name="keywords" content="Padyatri Registration">
      <meta name="keywords" content="Vastunidhi">
      <meta name="keywords" content="Varkari">
      <meta name="keywords" content="Kasbapeth">
      <meta name="keywords" content="Pune Festival">
      <meta name="keywords" content="Shri Saibaba Palakhi Bhavan, 25,Kasaba Peth,Gangotri Building Fadake Houda Chowk,Pune-411011">
      <meta name="description" content="Shri Saibaba Palakhi Sohala Samiti Pune">
      <meta name="author" content="Shri Saibaba Palakhi Sohala Pune, Phadke Houd Near Kasaba Ganapati Pune">
      <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/images/favicon.png">
      <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/images/favicon.png">
      <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/images/favicon.png">
      <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/images/favicon.png">
      <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/images/favicon.png">
      <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/images/favicon.png">
      <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/images/favicon.png">
      <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/images/favicon.png">
      <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/images/favicon.png">
      <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>assets/images/favicon.png">
      <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/images/favicon.png">
      <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/images/favicon.png">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css') ?>">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom.css') ?>">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/css/bootstrap4-table.css') ?>">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/css/table.css') ?>">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/css/jquery.dataTables.min.css') ?>">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/poppins-font.css') ?>">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/perfect-scrollbar.css') ?>">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datepicker/css/jquery-ui.css') ?>">
      <!--         <link href="<?php echo base_url('assets/datepicker/css/datepicker.css'); ?>" rel="stylesheet" type="text/css" media="screen"/>
         -->
      <!-- Core JS files -->
      <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>   
      <!--          <script type="text/javascript" src="<?php echo base_url('assets/js/modernizer.js') ?>"></script>    
         -->        <script type="text/javascript" src="<?php echo base_url('assets/js/upload2-nes.js') ?>"></script>   
      <!--         <script type="text/javascript" src="<?php echo base_url('assets/js/sidbar-toggle.js') ?>"></script>  
         -->        <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/popper.min.js') ?>"></script>
      <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
      <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/perfect-scrollbar.jquery.min.js') ?>"></script>  
      <script type="text/javascript" src="<?php echo base_url('assets/datepicker/js/jqueryui.js') ?>"></script>  
      <script type="text/javascript" src="<?php echo base_url('assets/js/sidebarmenu.js') ?>"></script>
      <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/custom.js') ?>"></script> 
      <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/baguetteBox.min.js') ?>"></script>
      <!--        <script type="text/javascript" src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap4.min.js') ?>"></script>-->
      <script type="text/javascript" src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js') ?>"></script>
      <script type="text/javascript" src="<?php echo base_url('assets/datatables/js/dataTables.buttons.min.js') ?>"></script>
      <script type="text/javascript" src="<?php echo base_url('assets/datatables/js/buttons.flash.min.js') ?>"></script>
      <script type="text/javascript" src="<?php echo base_url('assets/datatables/js/jszip.min.js') ?>"></script>
      <script type="text/javascript" src="<?php echo base_url('assets/datatables/js/pdfmake.min.js') ?>"></script>
      <script type="text/javascript" src="<?php echo base_url('assets/datatables/js/vfs_fonts.js') ?>"></script>
      <script type="text/javascript" src="<?php echo base_url('assets/datatables/js/buttons.html5.min.js') ?>"></script>
      <script type="text/javascript" src="<?php echo base_url('assets/datatables/js/buttons.print.min.js') ?>"></script>
      <!--<script type="text/javascript" src="<?php echo base_url('assets/datatables/js/buttons.colVis.min.js') ?>"></script>-->
      <!--messenger-->
      <link href="<?php echo base_url('assets/plugins/messenger/css/messenger.css'); ?>" rel="stylesheet" type="text/css" media="screen"/>
      <link href="<?php echo base_url('assets/plugins/messenger/css/messenger-theme-future.css'); ?>" rel="stylesheet" type="text/css" media="screen"/>
      <link href="<?php echo base_url('assets/plugins/messenger/css/messenger-theme-flat.css'); ?>" rel="stylesheet" type="text/css" media="screen"/>
      <!--messenger-->
   </head>
   <!-- END HEAD -->
   <!-- BEGIN BODY -->
   <!-- <body class="sidebar-collapse"> -->
   <body >
      <div class="fix-header card-no-border fix-sidebar sidebar-toggled">
         <div id="main-wrapper">
            <div class="loadding-page" style="display:none">
               <div class="loader">
                  <div class="loader__bar"></div>
                  <div class="loader__bar"></div>
                  <div class="loader__bar"></div>
                  <div class="loader__bar"></div>
                  <div class="loader__bar"></div>
                  <div class="loader__ball"></div>
               </div>
            </div>
            {header}
            {sidebar}
            <div class="page-wrapper">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="alert alert-success fade in alert-dismissible show" id="success_msg" style="display: none;">
                           <a href="#" class="close" data-dismiss="alert">&times;</a>
                           <strong>Success!</strong> <?php
                              if ($this->session->flashdata('success_msg')) {
                                  echo $this->session->flashdata('success_msg');
                              }
                              ?>
                        </div>
                        <div class="alert alert-danger fade in alert-dismissible show" id="error_msg" style="display: none;">
                           <a href="#" class="close" data-dismiss="alert">&times;</a>
                           <strong>Error !</strong> <?php
                              if ($this->session->flashdata('error_msg')) {
                                  echo $this->session->flashdata('error_msg');
                              }
                              ?>
                        </div>
                        <div class="alert alert-warning fade in alert-dismissible show" id="warning_msg" style="display:none;">
                           <a href="#" class="close" data-dismiss="alert">&times;</a>
                           <strong>Warning!</strong> There was a problem with your network connection.
                        </div>
                        <div class="alert alert-info fade in alert-dismissible show" id="note_msg" style="display: none;">
                           <a href="#" class="close" data-dismiss="alert">&times;</a>
                           <strong>Note!</strong> <?php
                              if ($this->session->flashdata('note_msg')) {
                                  echo $this->session->flashdata('note_msg');
                              }
                              ?>
                        </div>
                     </div>
                  </div>
                  {content}
                  <!---------- Delete Confirmation --------->
                  <div class="modal" id="confirm-delete">
                     <div class="modal-dialog">
                        <div class="modal-content">
                           <!-- Modal Header -->
                           <div class="modal-header">
                              <h4 class="modal-title">Are you sure ?</h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                           </div>
                           <!-- Modal body -->
                           <div class="modal-body">
                              Do you really want to delete this record ?
                           </div>
                           <!-- Modal footer -->
                           <div class="modal-footer">
                              <a class="btn btn-info add-button btn-ok">Yes</a>
                              <a class="btn btn-danger delete-button" data-dismiss="modal">No</a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!---------- Delete Confirmation --------->
                  <div class="modal" id="confirm-img-delete">
                     <div class="modal-dialog">
                        <div class="modal-content">
                           <!-- Modal Header -->
                           <div class="modal-header">
                              <h4 class="modal-title">Are you sure ?</h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                           </div>
                           <!-- Modal body -->
                           <div class="modal-body">
                              Do you really want to delete this image ?
                           </div>
                           <!-- Modal footer -->
                           <div class="modal-footer">
                              <a class="btn btn-info add-button btn-ok">Yes</a>
                              <a class="btn btn-danger delete-button" data-dismiss="modal">No</a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!---------- Activate Confirmation --------->
                  <div class="modal" id="confirm-active">
                     <div class="modal-dialog">
                        <div class="modal-content">
                           <!-- Modal Header -->
                           <div class="modal-header">
                              <h4 class="modal-title">Are you sure ?</h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                           </div>
                           <!-- Modal body -->
                           <div class="modal-body">
                              Do you really want to active this record ?
                           </div>
                           <!-- Modal footer -->
                           <div class="modal-footer">
                              <a class="btn btn-info add-button btn-ok">Yes</a>
                              <a class="btn btn-danger delete-button" data-dismiss="modal">No</a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-------- Delete Confirmation -------->
                  <div class="modal" id="confirm-pay">
                     <div class="modal-dialog">
                        <div class="modal-content">
                           <!-- Modal Header -->
                           <div class="modal-header">
                              <h4 class="modal-title">Are you sure?</h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                           </div>
                           <!-- Modal body -->
                           <div class="modal-body">
                              Do you really want to make payment?
                           </div>
                           <!-- Modal footer -->
                           <div class="modal-footer">
                              <a class="btn btn-info add-button btn-ok">Yes</a>
                              <a class="btn btn-danger delete-button" data-dismiss="modal">No</a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!---------------------------------------payment modal--------------------------------------------------------------------->
                  <div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                     <div class="modal-dialog" role="document">
                        <div class="modal-content">
                           <?php echo form_open('admin/devotee', array('id' => 'submit_amount', 'name' => 'submit_amount', 'class' => 'form_horizontal')); ?>
                           <div class="modal-header text-center">
                              <h4 class="modal-title w-100 font-weight-bold">Payment Method</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                           </div>
                           <?php
                              //           
                                                                  ?>
                           <!--<form>-->
                           <input type="hidden" name="devoteeid" id="devoteeid" value="<?php echo set_value('devoteeid'); ?>">
                           <input type="hidden" name="padyatriid" id="padyatriid" value="<?php echo set_value('padyatriid'); ?>">
                           <input type="hidden" name="db_year" id="db_year" value="<?php echo set_value('db_year'); ?>">
                           <input type="hidden" name="palki_year" id="palki_year" value="<?php echo set_value('palki_year'); ?>">
                           <input type="hidden" name="dev_address" id="dev_address" value="<?php echo set_value('dev_address'); ?>">
                           <input type="hidden" name="dev_dpnno" id="dev_dpnno" value="<?php echo set_value('dev_dpnno'); ?>">                                    
                           <div class="modal-body">
                              <div class="row mx-0" >
                                 <!--/span-->
                                 <div class="col-lg-12">
                                    <div class="form-group required row">
                                       <label class="control-label text-left col-lg-3">Your name:</label>
                                       <div class="col-lg-9">
                                          <input type="text" class="modal-form-control validate" placeholder="Name" name="devoteename" id="devoteename" data-error=".dtamount_Erorr1" value="" readonly>
                                          <div class="input-field">
                                             <div class="dtamount_Erorr1 error-msg"></div>
                                             <?php echo form_error('devoteename'); ?>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-12">
                                    <div class="form-group required row">
                                       <label class="control-label text-left col-lg-3">Dindi Year:</label>
                                       <div class="col-lg-9">
                                          <select class="modal-form-control custom-select" name="dindiyear" id="dindiyear" data-error=".dtamount_Erorr2">
                                             <option value="" disabled>Select Year</option>
                                          </select>
                                          <div class="input-field">
                                             <div class="dtamount_Erorr2 error-msg"></div>
                                             <?php echo form_error('dindiyear'); ?>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-12">
                                    <div class="form-group row">
                                       <label class="control-label text-left col-lg-3">Seva Samiti:</label>
                                       <div class="col-lg-9">
                                          <!--<input type="text" class="modal-form-control validate" placeholder="Seva Samiti">-->
                                          <select class="modal-form-control custom-select" name="sevasamithi" id="sevasamithi" data-error=".dtamount_Erorr3">
                                             <option value="">Select Samithi</option>
                                             <?php
                                                if (isset($samiti_list) && !empty($samiti_list)) {
                                                    foreach ($samiti_list as $samiti) {
                                                        $selected = '';
                                                        $selected_samithi = set_value('sevasamithi');
                                                        //                                                   ,$devotee_list['']
                                                //                                                                    if ($selected_samithi && $selected_samithi == $samiti['id']) {
                                                //                                                                        $selected = "selected";
                                                //                                                                    }
                                                        ?>
                                             <option value="<?php echo $samiti['id'] ?>" <?php echo $selected; ?>><?php echo $samiti['name']; ?></option>
                                             <?php
                                                }
                                                }
                                                ?>
                                          </select>
                                          <div class="input-field">
                                             <div class="dtamount_Erorr3 error-msg"></div>
                                             <?php echo form_error('sevasamithi'); ?>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-12">
                                    <div class="form-group required row">
                                       <label class="control-label text-left col-lg-3">Dindi:</label>
                                       <div class="col-lg-9">
                                          <!--<input type="text" class="modal-form-control validate" placeholder="Seva Samiti">-->
                                          <select class="modal-form-control custom-select" name="dindiname" id="dindiname" data-error=".dtamount_Erorr4">
                                             <option value="">Select Dindi</option>
                                             <?php
                                                if (isset($dindi_list) && !empty($dindi_list)) {
                                                    foreach ($dindi_list as $dindi) {
                                                        $selected = '';
                                                        $selected_samithi = set_value('sevasamithi');
                                                        //                                                   ,$devotee_list['']
                                                //                                                                    if ($selected_samithi && $selected_samithi == $dindi['id']) {
                                                //                                                                        $selected = "selected";
                                                //                                                                    }
                                                        ?>
                                             <option value="<?php echo $dindi['id'] ?>" <?php echo $selected; ?>><?php echo $dindi['name']; ?></option>
                                             <?php
                                                }
                                                }
                                                ?>
                                          </select>
                                          <div class="input-field">
                                             <div class="dtamount_Erorr4 error-msg"></div>
                                             <?php echo form_error('dindiname'); ?>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-12">
                                    <div class="form-group required row">
                                       <label class="control-label text-left col-lg-3">Transport:</label>
                                       <div class="col-lg-9">
                                          <select class="modal-form-control custom-select" name="vehicletype" id="vehicletype" data-error=".dtamount_Erorr5">
                                             <?php $selected_trnsport = set_value('vehicletype'); ?>
                                             <option value="">Select Transport</option>
                                             <option value="Self">Self</option>
                                             <option value="Samiti">Samiti Transport</option>
                                          </select>
                                          <div class="input-field">
                                             <div class="dtamount_Erorr5 error-msg"></div>
                                             <?php echo form_error('vehicletype'); ?>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-12">
                                    <div class="form-group required row">
                                       <label class="control-label text-left col-lg-3">Amount:</label>
                                       <div class="col-lg-9">
                                          <input type="text" class="modal-form-control validate" placeholder="Enter Amount" name="amountpaid" id="amountpaid" data-error=".dtamount_Erorr6" value="1200" style="cursor: not-allowed;" onkeydown="return false;" readonly>
                                          <div class="input-field">
                                             <div class="dtamount_Erorr6 error-msg"></div>
                                             <?php echo form_error('amountpaid'); ?>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!--                                            <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label class="control-label  text-left col-lg-3">Payment:</label>
                                        <div class="col-lg-9">
                                            <span class="switch switch-lg">
                                                <label for="switch-lg" class="font-14">Cash</label> 
                                                <input type="checkbox" class="switch" id="switch-lg" name="switch-lg">
                                                <label for="switch-lg" class="font-14">Cheque</label>
                                            </span>                                             
                                        </div>
                                    </div>
                                    </div>-->
                              </div>
                              <!--onclick="confirm_payment('this');"-->
                              <div class="modal-footer col text-center">
                                 <button type="submit" id="payamount" name="payamount" class="btn btn-info payamount">Confirm</button>
                              </div>
                           </div>
                           <script type="text/javascript">   
                              $('body').on('click', '.payamount', function (e) {                                   
                                  setTimeout(function () {                                        
                                  if($('.error-msg').text().trim() == ""){
                                      $(".loadding-page").css('display', 'block');
                                      $("#modalContactForm").modal('hide');
                                      $("#modalContactForm").remove();
                                      $(".payamount").css('disabled','true');
                                  }                                            
                              },10);
                              //                                    e.preventDefault(e);
                              });
                           </script>
                           <?php echo form_close(); ?>
                        </div>
                     </div>
                     <!--  -->
                  </div>
                  <!---------------------------------------------------------------------------->
               </div>
            </div>
         </div>
         <div class="clearfix"></div>
         <div class="col-md-12 footer">
            {footer}
         </div>
      </div>
      <!--</div>-->
      <!--         <script src="<?php echo base_url('assets/datepicker/js/datepicker.js') ?>" type="text/javascript"></script>
         -->        <!-- CORE JS Jquery Validation - START -->    
      <script src="<?php echo base_url('assets/validation/jquery.validate.min.js'); ?>" type="text/javascript"></script>
      <script type="text/javascript" src="<?php echo base_url('assets/validation/additional-methods.min.js'); ?>"></script>
      <script src="<?php echo base_url('assets/validation/jquery.custom_validate.js'); ?>"></script>
      <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
      <script src="<?php echo base_url('assets/plugins/messenger/js/messenger.min.js'); ?>" type="text/javascript"></script>
      <script src="<?php echo base_url('assets/plugins/messenger/js/messenger-theme-future.js'); ?>" type="text/javascript"></script>
      <script src="<?php echo base_url('assets/plugins/messenger/js/messenger-theme-flat.js'); ?>" type="text/javascript"></script>
      <script src="<?php echo base_url('assets/js/messenger.js'); ?>" type="text/javascript"></script>
      <script src="<?php echo base_url('assets/js/frontend/jquery.shorten.1.0.js'); ?>"></script>
      <script src="<?php echo base_url('assets/js/datatables.js'); ?>"></script>
      <script type="text/javascript" src="<?php echo base_url('assets/jquery/scrollbar.js') ?>"></script> 
      <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->  
      <script>
         $(document).ready(function () {
             $("#error_msg").css("display", "none");
         <?php if ($this->session->flashdata('error_msg')) { ?>
                 $("#error_msg").css("display", "block");
                 setTimeout(function () {
                     $('#error_msg').fadeOut('fast');
                 }, 3000);
         <?php } ?>
         
             $("#success_msg").css("display", "none");
         <?php if ($this->session->flashdata('success_msg')) { ?>
                 $("#success_msg").css("display", "block");
                 setTimeout(function () {
                     $('#success_msg').fadeOut('fast');
                 }, 3000);
         <?php } ?>
         
             $("#note_msg").css("display", "none");
         <?php if ($this->session->flashdata('note_msg')) { ?>
                 $("#note_msg").css("display", "block");
                 setTimeout(function () {
                     $('#note_msg').fadeOut('fast');
                 }, 3000);
         <?php } ?>
         
             $('#resource_list,#resource_list1').DataTable({
                 "bFilter": false,
                 "bLengthChange": false
             });
             $('.buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
         
             $('.modal').on('show.bs.modal', function (e) {
                 $('body').addClass('test');
             })
         });
         
         $('.datepicker').datepicker({
             dateFormat: "dd MM yy",
             changeMonth: true,
             changeYear: true,
             yearRange: "-100:+nn",
             maxDate: 0
         });
         
         $(".rounded-circle").click(function () {
             setTimeout(function () {
                 $(".sidebar-dark").toggle("ul .sidebar-dark");
             }, 100);
         
         });
         
         //      $('.navbar-nav li a').click(function(e) {
         //     $('.navbar-nav li.active').removeClass('active');
         //     var $parent = $(this).parent();
         //     $parent.addClass('active');
         //     e.preventDefault();
         // });
         
         $(".description-list").shorten({
             "showChars": 25,
             "moreText": "More",
             "lessText": "Less",
         });
         $(".address_length").shorten({
             "showChars": 50,
             "moreText": "More",
             "lessText": "Less",
         });
         $('.dataTables_paginate ').click(function () {
             $(".address_length").shorten({
                 "showChars": 50,
                 "moreText": "More",
                 "lessText": "Less",
             });
         });
         $("#year_list").change(function () {
             $(".address_length").shorten({
                 "showChars": 50,
                 "moreText": "More",
                 "lessText": "Less",
             });
         });
         
         $("#aadharno").keyup(function () {
             document.getElementById('aadharno').addEventListener('input', function (e) {
                 e.target.value = e.target.value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim();
             });
         });
      </script>
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-139993206-1"></script>
      <script>
         window.dataLayer = window.dataLayer || [];
         function gtag() {
             dataLayer.push(arguments);
         }
         gtag('js', new Date());
         
         gtag('config', 'UA-139993206-1');
      </script>
   </body>
</html>
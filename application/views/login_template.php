<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" >
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" /> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>
        <meta name="keywords" content="Shri Saibaba Palakhi Sohala Pune"/>
        <meta name="keywords" content="Saibaba Palakhi,Pune">
        <meta name="keywords" content="Sai Palakhi">
        <meta name="keywords" content="Pune to Shirdi">
        <meta name="keywords" content="Padyatra">
        <meta name="keywords" content="Gurupournima Saibaba festival">
        <meta name="keywords" content="Pune Palkhi Sohla,Palkhi Bhavan">
        <meta name="keywords" content="Saibabapalkhisohla">
        <meta name="keywords" content="Sai baba palkhi samiti">
        <meta name="keywords" content="Shirdi Palkhi Sohla">
        <meta name="keywords" content="Pune to Shirdipadyatra">
        <meta name="keywords" content="Padyatri Registration">
        <meta name="keywords" content="Vastunidhi">
        <meta name="keywords" content="Varkari">
        <meta name="keywords" content="Kasbapeth">
        <meta name="keywords" content="Pune Festival">
        <meta name="keywords" content="Shri Saibaba Palakhi Bhavan, 25,Kasaba Peth,Gangotri Building Fadake Houda Chowk,Pune-411011">
        <meta name="description" content="Shri Saibaba Palakhi Sohala Samiti Pune"/>
        <meta name="author" content="Shri Saibaba Palakhi Sohala Pune, Phadke Houd Near Kasaba Ganapati Pune"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/images/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/poppins-font.css') ?>">

        <!-- Core JS files -->
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/popper.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/perfect-scrollbar.jquery.min.js') ?>"></script>  
    </head>
    <!-- END HEAD -->

    <!-- BEGIN BODY -->
    <!-- <body class="sidebar-collapse"> -->
    <body>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">


                <div class="alert alert-warning fade in alert-dismissible show" id="warning_message" style="display:none;">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Warning!</strong> There was a problem with your network connection.
                </div>  

            </div>
        </div>
        {content}

        <!-- CORE JS Jquery Validation - START -->
        <script src="<?php echo base_url('assets/validation/jquery.validate.min.js'); ?>" type="text/javascript"></script> 
        <script type="text/javascript" src="<?php echo base_url('assets/validation/additional-methods.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/validation/jquery.custom_validate.js'); ?>"></script>
        <script>
            $(document).ready(function () {
                $("#error_message").css("display", "none");
<?php if ($this->session->flashdata('error_msg')) { ?>
                    $("#error_message").css("display", "block");
                    setTimeout(function () {
                        $('#error_message').fadeOut('fast');
                    }, 3000);
                    setTimeout(function () {
                        $('.error-msg').fadeOut('fast');
                    }, 9000);
<?php } ?>

                $("#success_message").css("display", "none");
<?php if ($this->session->flashdata('success_msg')) { ?>
                    $("#success_message").css("display", "block");
                    setTimeout(function () {
                        $('#success_message').fadeOut('fast');
                    }, 9000);
                    setTimeout(function () {
                        $('.success-msg').fadeOut('fast');
                    }, 9000);
<?php } ?>

                $("#note_message").css("display", "none");
<?php if ($this->session->flashdata('note_msg')) { ?>
                    $("#note_message").css("display", "block");
                    setTimeout(function () {
                        $('#note_message').fadeOut('fast');
                    }, 9000);
<?php } ?>

            });
        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-139993206-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-139993206-1');
        </script>
    </body>
</html>

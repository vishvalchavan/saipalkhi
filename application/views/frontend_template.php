<html lang="en">
    <head>
        <!-- Basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" >
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />   
        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Site Metas -->
        <title><?php echo isset($dataHeader['title']) ? $dataHeader['title'] : ''; ?></title>  
        <meta name="keywords" content="Shri Saibaba Palakhi Sohala Pune">
        <meta name="keywords" content="Saibaba Palakhi,Pune">
        <meta name="keywords" content="Sai Palakhi">
        <meta name="keywords" content="Pune to Shirdi">
        <meta name="keywords" content="Padyatra">
        <meta name="keywords" content="Gurupournima Saibaba festival">
        <meta name="keywords" content="Pune Palkhi Sohla,Palkhi Bhavan">
        <meta name="keywords" content="Saibabapalkhisohla">
        <meta name="keywords" content="Sai baba palkhi samiti">
        <meta name="keywords" content="Shirdi Palkhi Sohla">
        <meta name="keywords" content="Pune to Shirdipadyatra">
        <meta name="keywords" content="Padyatri Registration">
        <meta name="keywords" content="Vastunidhi">
        <meta name="keywords" content="Varkari">
        <meta name="keywords" content="Kasbapeth">
        <meta name="keywords" content="Pune Festival">
        <meta name="keywords" content="Shri Saibaba Palakhi Bhavan, 25,Kasaba Peth,Gangotri Building Fadake Houda Chowk,Pune-411011">
        <meta name="description" content="Shri Saibaba Palakhi Sohala Samiti Pune">
        <meta name="author" content="Shri Saibaba Palakhi Bhavan Pune, Phadke Houd Near Kasaba Ganapati Pune">

        <!-- Site Icons -->
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <meta name="theme-color" content="#ffffff">
        <link rel="apple-touch-icon" href="<?php echo base_url('assets/images/favicon.png'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datepicker/css/jquery-ui.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/frontend/baguetteBox.min.css') ?>">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/frontend/bootstrap.min.css') ?>"> 
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/frontend/croppie/croppie.css" />
        <!-- Site CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/frontend/style.css') ?>">
        <!-- ALL VERSION CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/frontend/versions.css') ?>">
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/frontend/responsive.css') ?>">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/frontend/custom.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/frontend/main.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/frontend/cards-gallary.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/frontend/photo-gallary.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/frontend/popins-font.css') ?>">

        <!-- Modernizer for Portfolio -->
        <!-- ALL JS FILES -->
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script> 
<!--        <script type="text/javascript" src="<?php echo base_url('assets/js/frontend/all.js') ?>"></script> -->
        <script type="text/javascript" src="<?php echo base_url('assets/js/frontend/bootstrap.min.js') ?>"></script> 
        <script src="<?php echo base_url() ?>assets/js/frontend/croppie/croppie.js"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/frontend/modernizer.js') ?>"></script>
<!--        <script type="text/javascript" src="<?php echo base_url('assets/js/frontend/custom.js') ?>"></script>-->

        <!--messenger-->
        <link href="<?php echo base_url('assets/plugins/messenger/css/messenger.css'); ?>" rel="stylesheet" type="text/css" media="screen"/>
        <link href="<?php echo base_url('assets/plugins/messenger/css/messenger-theme-future.css'); ?>" rel="stylesheet" type="text/css" media="screen"/>
        <link href="<?php echo base_url('assets/plugins/messenger/css/messenger-theme-flat.css'); ?>" rel="stylesheet" type="text/css" media="screen"/>
        <!--messenger-->
    </head>
    <body> 
        {header}

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="alert alert-success fade in alert-dismissible show" id="success_msg" style="display:none;">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Success!</strong> <?php
                    if ($this->session->flashdata('success_msg')) {
                        echo $this->session->flashdata('success_msg');
                    }
                    ?>
                </div>
                <div class="alert alert-danger fade in alert-dismissible show" id="error_msg" style="display: none;">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Error !</strong> <?php
                    if ($this->session->flashdata('error_msg')) {
                        echo $this->session->flashdata('error_msg');
                    }
                    if (isset($message) && !empty($message)) {
                        echo $message;
                    }
//                    if ($this->session->tempdata('error_msg')) {
//                        echo $this->session->tempdata('error_msg');
//                    }
                    ?>
                </div>
                <div class="alert alert-warning fade in alert-dismissible show" id="warning_msg" style="display:none;">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Warning!</strong> There was a problem with your network connection.
                </div>  
                <div class="alert alert-info fade in alert-dismissible show" id="note_msg" style="display: none;">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Note!</strong> <?php
                    if ($this->session->flashdata('note_msg')) {
                        echo $this->session->flashdata('note_msg');
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="modal" id="account-details" style="z-index: 9999999999 !important;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Note</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body accountbody"></div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <a class="btn btn-info add-button btn-ok" data-dismiss="modal">Ok</a>
                    </div>
                </div>
            </div>
        </div>
        {content}
        {footer}
        <a href="#" id="scroll-to-top" class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>


        <!-- CORE JS Jquery Validation - START -->    
        <script type="text/javascript" src="<?php echo base_url('assets/datepicker/js/jqueryui.js') ?>"></script>
        <script src="<?php echo base_url('assets/validation/jquery.validate.min.js'); ?>" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/validation/additional-methods.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/validation/jquery.custom_validate.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/frontend/popper.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/frontend/baguetteBox.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/frontend/video-gallery.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/frontend/jquery.shorten.1.0.js'); ?>"></script>

        <script src="<?php echo base_url('assets/plugins/messenger/js/messenger.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/messenger/js/messenger-theme-future.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/messenger/js/messenger-theme-flat.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/messenger.js'); ?>" type="text/javascript"></script>
<!--     <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        -->
        <script>
            baguetteBox.run('.compact-gallery', {animation: 'slideIn'});
        </script>
        <script>
            $(document).ready(function () {
                $("#error_msg").css("display", "none");
<?php if ($this->session->flashdata('error_msg')) { ?>
                    $("#error_msg").css("display", "block");
                    setTimeout(function () {
                        $('#error_msg').fadeOut('fast');
                    }, 3000);
<?php } ?>
<?php if (isset($message) && !empty($message)) { ?>
                    $("#error_msg").css("display", "block");
                    setTimeout(function () {
                        $('#error_msg').fadeOut('fast');
                    }, 3000);
<?php } ?>
<?php if ($this->session->tempdata('error_msg')) { ?>
                    $("#error_msgtemp").css("display", "block");
                    setTimeout(function () {
                        $('#error_msgtemp').fadeOut('fast');
                    }, 5000);
<?php } ?>
<?php if ($this->session->flashdata('error_msg1')) { ?>
                    setTimeout(function () {
                        $('.error-msg').fadeOut('fast');
                    }, 9000);
<?php } ?>

                $("#success_msg").css("display", "none");
<?php if ($this->session->flashdata('success_msg')) { ?>
                    $("#success_msg").css("display", "block");
                    setTimeout(function () {
                        $('#success_msg').fadeOut('fast');
                    }, 3000);
<?php } ?>
<?php if ($this->session->flashdata('success_msg1')) { ?>
                    setTimeout(function () {
                        $('.success-msg').fadeOut('fast');
                    }, 9000);
<?php } ?>

                $("#note_msg").css("display", "none");
<?php if ($this->session->flashdata('note_msg')) { ?>
                    $("#note_msg").css("display", "block");
                    setTimeout(function () {
                        $('#note_msg').fadeOut('fast');
                    }, 3000);
<?php } ?>
                $("#error_msg1").css("display", "none");
<?php if ($this->session->flashdata('error_msg1')) { ?>
                    $("#error_msg1").css("display", "block");
                    setTimeout(function () {
                        $('#error_msg1').fadeOut('fast');
                    }, 3000);

<?php } ?>

                $('.datepicker').datepicker({
                    dateFormat: "dd MM yy",
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "-100:+nn",
                    maxDate: 0,
                    inline: true,
                });
                $(".datepicker").datepicker({
                    beforeShow: function (input, obj) {
                        $(input).after($(input).datepicker('widget'));
                    }
                });
            });
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();

                $(".album-description-userside").shorten({
                    "showChars": 25,
                    "moreText": "See More",
                    "lessText": "Less",
                });
            });

            $('.carousel').carousel({
                interval: 3000
            });

        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-139993206-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-139993206-1');
        </script>
    </body>
</html>


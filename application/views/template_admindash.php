<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" >
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" /> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo isset($dataHeader['title']) ? $dataHeader['title'] : ''; ?></title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <meta name="keywords" content="Shri Saibaba Palakhi Sohala Pune">
        <meta name="keywords" content="Saibaba Palakhi,Pune">
        <meta name="keywords" content="Sai Palakhi">
        <meta name="keywords" content="Pune to Shirdi">
        <meta name="keywords" content="Padyatra">
        <meta name="keywords" content="Gurupournima Saibaba festival">
        <meta name="keywords" content="Pune Palkhi Sohla,Palkhi Bhavan">
        <meta name="keywords" content="Saibabapalkhisohla">
        <meta name="keywords" content="Sai baba palkhi samiti">
        <meta name="keywords" content="Shirdi Palkhi Sohla">
        <meta name="keywords" content="Pune to Shirdipadyatra">
        <meta name="keywords" content="Padyatri Registration">
        <meta name="keywords" content="Vastunidhi">
        <meta name="keywords" content="Varkari">
        <meta name="keywords" content="Kasbapeth">
        <meta name="keywords" content="Pune Festival">
        <meta name="keywords" content="Shri Saibaba Palakhi Bhavan, 25,Kasaba Peth,Gangotri Building Fadake Houda Chowk,Pune-411011">
        <meta name="description" content="Shri Saibaba Palakhi Sohala Samiti Pune">
        <meta name="author" content="Shri Saibaba Palakhi Sohala Pune, Phadke Houd Near Kasaba Ganapati Pune">
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/images/favicon.png">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/css/bootstrap4-table.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/css/table.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/css/jquery.dataTables.min.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/poppins-font.css') ?>">
        <link href="<?php echo base_url('assets/datepicker/css/datepicker.css'); ?>" rel="stylesheet" type="text/css" media="screen"/>

        <!-- Core JS files -->
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>           

        <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/popper.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/perfect-scrollbar.jquery.min.js') ?>"></script>  
        <!--<script type="text/javascript" src="<?php // echo base_url('assets/bootstrap/js/waves.js')                                    ?>"></script>-->  
        <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/sidebarmenu.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/custom.js') ?>"></script> 
        <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/baguetteBox.min.js') ?>"></script>

<!--        <script type="text/javascript" src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap4.min.js') ?>"></script>-->
        <script type="text/javascript" src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/datatables/js/dataTables.buttons.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/datatables/js/buttons.flash.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/datatables/js/jszip.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/datatables/js/pdfmake.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/datatables/js/vfs_fonts.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/datatables/js/buttons.html5.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/datatables/js/buttons.print.min.js') ?>"></script>



    </head>
    <!-- END HEAD -->

    <!-- BEGIN BODY -->
    <!-- <body class="sidebar-collapse"> -->
    <body>

        <div class="fix-header card-no-border fix-sidebar">
            <div id="main-wrapper">
                <div class="loadding-page" style="display:none">
                    <div class="loader">
                        <div class="loader__bar"></div>
                        <div class="loader__bar"></div>
                        <div class="loader__bar"></div>
                        <div class="loader__bar"></div>
                        <div class="loader__bar"></div>
                        <div class="loader__ball"></div>
                    </div>
                </div>   
                {header}
                {sidebar}
                <!--<div class="page-wrapper">-->
                <!--<div class="container-fluid">-->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="alert alert-success fade in alert-dismissible show" id="success_msg" style="display: block; margin: 0px auto;">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success!</strong> <?php
                            if ($this->session->flashdata('success_msg')) {
                                echo $this->session->flashdata('success_msg');
                            }
                            ?>
                        </div>
                        <div class="alert alert-danger fade in alert-dismissible show" id="error_msg" style="display: none; margin: 0px auto;">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Error !</strong> <?php
                            if ($this->session->flashdata('error_msg')) {
                                echo $this->session->flashdata('error_msg');
                            }
                            ?>
                        </div>
                        <div class="alert alert-warning fade in alert-dismissible show" id="warning_msg" style="display:none; margin: 0px auto;">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Warning!</strong> There was a problem with your network connection.
                        </div>  
                        <div class="alert alert-info fade in alert-dismissible show" id="note_msg" style="display: none;">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Note!</strong> <?php
                            if ($this->session->flashdata('note_msg')) {
                                echo $this->session->flashdata('note_msg');
                            }
                            ?>
                        </div>
                    </div>
                </div>
                {content}
                <div class="modal" id="confirm-delete">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Are you sure?</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                Do you really want to delete these records?
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <a class="btn btn-info add-button btn-ok">Yes</a>
                                <a class="btn btn-danger delete-button" data-dismiss="modal">No</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--</div>-->
                <!--</div>-->
            </div>
            <div class="col-md-12 footer">
                {footer}
            </div>
        </div>
        <!--</div>-->

        <script src="<?php echo base_url('assets/datepicker/js/datepicker.js') ?>" type="text/javascript"></script>
        <!-- CORE JS Jquery Validation - START -->    
        <script src="<?php echo base_url('assets/validation/jquery.validate.min.js'); ?>" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/validation/additional-methods.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/validation/jquery.custom_validate.js'); ?>"></script>


        <script>
            $(document).ready(function () {

                $("#error_msg").css("display", "none");
<?php if ($this->session->flashdata('error_msg')) { ?>
                    $("#error_msg").css("display", "block");
                    setTimeout(function () {
                        $('#error_msg').fadeOut('fast');
                    }, 3000);
<?php } ?>

                $("#success_msg").css("display", "none");
<?php if ($this->session->flashdata('success_msg')) { ?>
                    $("#success_msg").css("display", "block");
                    setTimeout(function () {
                        $('#success_msg').fadeOut('fast');
                    }, 3000);
<?php } ?>

                $("#note_msg").css("display", "none");
<?php if ($this->session->flashdata('note_msg')) { ?>
                    $("#note_msg").css("display", "block");
                    setTimeout(function () {
                        $('#note_msg').fadeOut('fast');
                    }, 3000);
<?php } ?>
                $('.datatable').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'excel', 'pdf', 'print'
                    ]
                });
                $('#resource_list').DataTable({
                    "bFilter": false,
                    "bLengthChange": false
                });
                $('.buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
            });

            $('.datepicker').datepicker({
                yearRange: "-100:+nn",
            });


        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-139993206-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-139993206-1');
        </script>

    </body>
</html>

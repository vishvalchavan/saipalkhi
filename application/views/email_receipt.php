<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>A4</title>
        <!-- Load paper.css for happy printing -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/paper.css') ?>">
        <!-- Set page size here: A5, A4 or A3 -->
        <!-- Set also "landscape" if you need -->
        <style>
            .article{
                font-family: 'Open Sans', sans-serif;
                font-weight: 600;
                color: #000;
                /*background: url(<?php echo base_url(); ?>assets/images/pavtibg.jpg) no-repeat 4px 0px;*/
                background-size: contain;
                min-height: 488px;
                position: relative;
            }
            .outer{
                width: 466px;
                margin-top: 164px;
                font-size: 12px;
                margin-left: 207px;
                position: absolute;
                border: 2px solid #f58344;
                border-radius: 8px;
            }
            .receipt-header{
                text-align: center;
                margin-top: -10px;
                color: #fff;
                font-weight: bold;
            }
        </style>
        <style>@page { size: A4 }</style>
    </head>
    <!-- Set "A5", "A4" or "A3" for class name -->
    <!-- Set also "landscape" if you need -->
    <body class="A4">

        <!-- Each sheet element should have the class "sheet" -->
        <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
        <section class="sheet padding-10mm">

            <!-- Write HTML just like a web page -->
            <article class="article">
                <div class="outer">
                    <div class="receipt-header">देणगी पावती </div>
                    <table cellpadding="4" cellspacing="2" width="100%">
                        <tr>
                            <td><span style="font-weight:bold;color:#7f0907;">पावती क्र. : </span> <?php echo isset($dpn_no) ? $dpn_no : ''; ?>
                            </td>
                            <td style="text-align:right"><span style="font-weight:bold;color:#7f0907;">दिनांक  :</span>  <?php echo isset($pay_date) ? date("d-m-Y", strtotime($pay_date)) : ''; ?></td>
                        </tr>
                    </table>

                    <table cellpadding="4" cellspacing="2" width="100%">
                        <tr>
                            <td colspan="2">
                                <span style="float:left;"><span style="font-weight:bold;color:#7f0907;">श्री. / सौ. / श्रीमती. / मेसर्स  : </span></span>
                                <span id="rfullname_div" style="border-bottom: 2px solid #696868;padding-bottom: 0;float:left;width:65%;margin-left:10px;height:15px;"><?php echo isset($name) ? $name : ''; ?></span></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <span style="float:left;"><span style="font-weight:bold;color:#7f0907;">पत्ता  : </span></span>
                                <span id="raddress_div" style="border-bottom: 2px solid #696868;padding-bottom: 0;float:left;width:85%;margin-left:10px;height:15px;"><?php echo isset($address) ? $address : ''; ?></span></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <span style="float:left;"><span style="font-weight:bold;color:#7f0907;">अक्षरी रुपये : </span></span>
                                <span id="ramount_inword" style="border-bottom: 2px solid #696868;padding-bottom: 0;float:left;width:65%;margin-left:10px;height:15px;"><?php echo isset($amount_in_word) ? $amount_in_word : ''; ?></span></td>
                        </tr>
                    </table>
                    <table cellpadding="4" cellspacing="2" width="100%">
                        <tr>
                            <td colspan="2">
                                <span style="float:left;"><span style="font-weight:bold;color:#7f0907;">पैसे भरणा पद्धत  : </span></span>
                                <span style="border-bottom: 2px solid #696868;padding-bottom: 0;float:left;width:60%;margin-left:10px;height:15px;"> <?php echo isset($payment_mode) ? $payment_mode : ''; ?></span></td>
                        </tr>
                        <tr>
                            <td style="width:55%;"><span style="float:left;"><span style="font-weight:bold;color:#7f0907;">बँक  : </span></span><span style="border-bottom: 2px solid #696868;padding-bottom: 0;float:left;width:82%;margin-left:10px;height:18px;"></span>
                            </td>
                            <td>  <span style="margin-top:15px;float:left;">मिळाले धन्यवाद </span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<span style="float:right;">&nbsp;<span style="padding:1px 3px; border:1px solid #7f0907; font-size:18px; color:#7f0907; float:right;margin-top: -12px;"> &nbsp; रु. <span id="ramount_div"><?php echo isset($amount) ? $amount : ''; ?></span>/-&nbsp;</span></span></td>
                        </tr>
                    </table>
                </div>
            </article>

        </section>
    </body>
</html>
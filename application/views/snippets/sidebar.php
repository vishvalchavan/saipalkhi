<style>
    li.b-bottom {
        border-bottom: 1px solid gainsboro;
    }
</style>
<?php
$sidebarlinktitle = ($this->uri->segment(2)) ? strtolower($this->uri->segment(2)) : '';
?>
<ul class="navbar-nav bg-white sidebar sidebar-dark" id="accordionSidebar" style="display:none;">
    <div class="scroll-sidebar">
        <li class="<?php echo($sidebarlinktitle == "home" || $sidebarlinktitle == "") ? 'active' : ''; ?> b-bottom">
            <a class="has-arrow waves-effect waves-dark" href="<?php echo base_url('admin/dashboard'); ?>" aria-expanded="false">
                <img src="<?php echo base_url(); ?>assets/images/icon1.png" class="image-reduced">
                <span class="hide-menu text-bold">Dashboard</span>
            </a>
        </li>
        <li class="<?php echo($sidebarlinktitle == "devotee") ? 'active' : ''; ?> b-bottom">
            <a class="has-arrow waves-effect waves-dark" href="<?php echo base_url(); ?>admin/devotee" aria-expanded="false">
                <img src="<?php echo base_url(); ?>assets/images/icon2.png" class="image-reduced">
                <span class="hide-menu text-bold">Devotee</span>
            </a>
        </li>
        <li class="<?php echo($sidebarlinktitle == "padyatri") ? 'active' : ''; ?> b-bottom">
            <a class="has-arrow waves-effect waves-dark" href="<?php echo base_url(); ?>admin/padyatri" aria-expanded="false">
                <img src="<?php echo base_url(); ?>assets/images/icon3.png" class="image-reduced">
                <span class="hide-menu text-bold">Padyatri</span>
            </a>
        </li>
        <li class="<?php echo($sidebarlinktitle == "samiti") ? 'active' : ''; ?> b-bottom">
            <a class="has-arrow waves-effect waves-dark" href="<?php echo base_url(); ?>admin/samiti" aria-expanded="false">
                <img src="<?php echo base_url(); ?>assets/images/icon4.png" class="image-reduced">
                <span class="hide-menu text-bold">Sevekari</span>
            </a>
        </li>
        <li class="<?php echo($sidebarlinktitle == "dindi") ? 'active' : ''; ?> b-bottom">
            <a class="has-arrow waves-effect waves-dark" href="<?php echo base_url(); ?>admin/dindi" aria-expanded="false">
                <img src="<?php echo base_url(); ?>assets/images/dindi.png" class="image-reduced set-image-in-mobile mt-2" width="49px" height="46px">
                <span class="hide-menu text-bold ml-2">Dindi&nbsp;&nbsp;&nbsp;</span>
            </a>
        </li>
        <li class="<?php echo($sidebarlinktitle == "vehicle") ? 'active' : ''; ?> b-bottom">
            <a class="has-arrow waves-effect waves-dark" href="<?php echo base_url(); ?>admin/vehicle" aria-expanded="false">
                <img src="<?php echo base_url(); ?>assets/images/truck.png" class="image-reduced mt-2" width="49px" height="46px">&nbsp;&nbsp;&nbsp;
                <span class="hide-menu text-bold">Vehicle</span>
            </a>
        </li>
        <li class="<?php echo($sidebarlinktitle == "album" || $sidebarlinktitle == "update_album_details" || $sidebarlinktitle == "add_album_details") ? 'active' : ''; ?> b-bottom">
            <a class="has-arrow waves-effect waves-dark" href="<?php echo base_url(); ?>admin/album" aria-expanded="false">
                <img src="<?php echo base_url(); ?>assets/images/photogallery.png" class="image-reduced set-image-in-mobile mt-2 ml-3" width="49px" height="46px">&nbsp;&nbsp;&nbsp;
                <span class="hide-menu text-bold">Photo Gallery</span>
            </a>
        </li>
        <li class="<?php echo($sidebarlinktitle == "donorlist") ? 'active' : ''; ?> b-bottom">
            <a class="has-arrow waves-effect waves-dark" href="<?php echo base_url(); ?>admin/donorlist"  aria-expanded="false">
                <img src="<?php echo base_url(); ?>assets/images/donate-4.png" class="image-reduced mt-2" width="49px" height="46px">
                <span class="hide-menu text-bold">Donation</span>
            </a>
        </li>
        <li class="<?php echo($sidebarlinktitle == "reportlist") ? 'active' : ''; ?> b-bottom dropdown-submenu">
            <a href="<?php echo base_url(); ?>admin/reports" tabindex="-1" class="has-arrow waves-effect waves-dark" aria-expanded="false">
                <img src="<?php echo base_url(); ?>assets/images/icon8.png" class="image-reduced ml-3">&nbsp;&nbsp;&nbsp;
                <span class="hide-menu text-bold">Reports</span>
            </a>

           <!--  <ul class="dropdown-menu">
                <li><a tabindex="-1" href="<?php echo base_url(); ?>admin/DindiReport">Dindi Report</a></li>
                <li><a href="<?php echo base_url(); ?>admin/DevoteeEnrollment">Tablet Report</a></li>
            </ul> -->
        </li>
        <li class="<?php echo($sidebarlinktitle == "configList") ? 'active' : ''; ?>border border-0">
            <a class="has-arrow waves-effect waves-dark" aria-expanded="false">
                <img src="<?php echo base_url(); ?>assets/images/icon9.png" class="image-reduced ml-3">&nbsp;&nbsp;&nbsp;
                <span class="hide-menu text-bold">Config</span>
            </a>
        </li>
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>
    </div>
</ul>
<script>
    $('.navbar-nav li a').click(function (e) {
        $('.navbar-nav li.active').removeClass('active');
        var $parent = $(this).parent();
        $parent.addClass('active');
// e.preventDefault();
    });

</script>
<!-- End Sidebar navigation -->

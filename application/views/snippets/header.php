<nav class="navbar navbar-expand navbar-light  topbar static-top shadow">
    <!-- Sidebar Toggle (Topbar) -->
    <?php if ($this->uri->segment(2) !== 'dashboard')
{ ?>
      <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle">
        <i class="fas fa-bars"></i>
    </button>      
<?php }
 
?>    <!-- Topbar Navbar -->

    <div class="col-lg-3 col-4 p-0">
        <a href="<?php echo base_url('admin/dashboard'); ?>"><img src="<?php echo base_url(); ?>assets/images/logo-new.png" class="img-responsive responsive-img d-md-none  d-md-block d-lg-none d-lg-block d-none d-xl-none d-xl-block d-xl-none"></a>
        <img src="<?php echo base_url(); ?>assets/images/logo-mobile-device1.png" class="ml-4 d-block d-sm-none d-none d-sm-block d-md-none" width="40">
    </div>
    <div class="col-lg-9 col-8">
        <ul class="navbar-nav float-right">

            <!-- Nav Item - Alerts -->
          <!--   <li class="nav-item dropdown">
                <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                    <i class="fas fa-bell"></i>
                    <span class="count">4</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
                    <a class="dropdown-item">
                        <p class="mb-0 font-weight-normal float-left">You have 4 new notifications
                        </p>
                        <span class="badge badge-pill badge-warning float-right">View all</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item preview-item">
                        <div class="preview-thumbnail">
                            <div class="preview-icon bg-success">
                                <i class="mdi mdi-alert-circle-outline mx-0"></i>
                            </div>
                        </div>
                        <div class="preview-item-content">
                            <h6 class="preview-subject font-weight-medium text-dark">Application Error</h6>
                            <p class="font-weight-light small-text">
                                Just now
                            </p>
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item preview-item">
                        <div class="preview-thumbnail">
                            <div class="preview-icon bg-warning">
                                <i class="mdi mdi-comment-text-outline mx-0"></i>
                            </div>
                        </div>
                        <div class="preview-item-content">
                            <h6 class="preview-subject font-weight-medium text-dark">Settings</h6>
                            <p class="font-weight-light small-text">
                                Private message
                            </p>
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item preview-item">
                        <div class="preview-thumbnail">
                            <div class="preview-icon bg-info">
                                <i class="mdi mdi-email-outline mx-0"></i>
                            </div>
                        </div>
                        <div class="preview-item-content">
                            <h6 class="preview-subject font-weight-medium text-dark">New user registration</h6>
                            <p class="font-weight-light small-text">
                                2 days ago
                            </p>
                        </div>
                    </a>
                </div>
            </li> -->
            <li class="nav-item dropdown u-pro">
                <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="<?php echo base_url(); ?>assets/images/icons8-contacts.png" alt="user" class="mr-2"> 
                    <span class="hidden-md-down"><?php echo $this->session->userdata('firstname') . ' ' . $this->session->userdata('lastname') ?>&nbsp;<i class="fa fa-angle-down"></i>
                    </span> 
                </a>
                <div class="dropdown-menu dropdown-menu-right animated flipInY">
                    <ul class="dropdown-user">
                        <li><a href="<?php echo base_url('admin/logout') ?>"><i class="fa fa-power-off"></i> Logout</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>   
</nav>
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('send_sms')) {

    function send_sms($mobile, $smstext) {
        $CI = & get_instance();
        $ID = "saipun";
        $Pwd = "PalkhiSMS";
        $sender = "SAIPUN";
        $baseurl = "http://trans.masssms.tk";
        $PhNo = $mobile;
        $Text = urlencode("$smstext");

        //Invoke HTTP Submit url
        $url = "$baseurl/api.php?username=$ID&password=$Pwd&sender=$sender&sendto=$PhNo&message=$Text";
        // do sendmsg call
        $ret = file($url);
        //  echo  $ret[0] ;
        //Process $ret to check whether it contains "Message Submitted"
        //  if( strpos( $ret[0], 'Message Submitted' ) !== false ) echo '<h4>The sms alert has been sent to your mobile no:'.$PhNo;
//        if (strpos($ret[0], 'Authentication Failed.') !== false)
//            echo '<h4>Sorry! SMS Authentication Failed.</h4>';

        return 1;
    }

}
if (!function_exists('send_mail')) {

    function send_mail($name, $email, $subject, $message) {
        $CI = & get_instance();
        $CI->load->library('SMTP');
        $CI->load->library('PHPMailer');
        $company_name = "Shri SaiBaba Palkhi Sohla Samiti";
        $retype = ucfirst(strtolower("SSS"));
        $mail_body = "Shri SaiBaba Palkhi Sohla Samiti,<br><br>" . $message . "<br><br>Regards,<br>Shri SaiBaba Palkhi Sohla Samiti,Pune<br>";
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->Username = "saipalkhipune@gmail.com"; // SMTP username
        $mail->Password = "Palkhi20!@"; // SMTP password
        $mail->Port = "587";
        $mail->Host = 'smtp.gmail.com'; //hostname
        $mail->From = $email;
        $mail->FromName = $name;
        $mail->AddAddress("saipalkhipune@gmail.com");
        $mail->addCC('info@saibabapalkhipune.org');
        $mail->AddReplyTo($email, $name);
        $mail->WordWrap = 50; // set word wrap
        $mail->IsHTML(false); // send as HTML
        $mail->Subject = $subject;
        $mail->MsgHTML($mail_body);
        if (!$mail->Send()) {
            $error = $mail->ErrorInfo;
            echo $error;
            return 'failed';
        } else {
            return 'success';
        }
    }

}

if (!function_exists('compress_image')) {

    function compress_image($src, $dest, $quality) {
        $info = getimagesize($src);
        if ($info['mime'] == 'image/jpeg') {
            $image = imagecreatefromjpeg($src);
        } elseif ($info['mime'] == 'image/gif') {
            $image = imagecreatefromgif($src);
        } elseif ($info['mime'] == 'image/png') {
            $image = imagecreatefrompng($src);
        } else {
            die('Unknown image file format');
        }
        imagejpeg($image, $dest, $quality);
        return $dest;
    }

}
if (!function_exists('IND_money_format')) {

    function IND_money_format($money) {
        $len = strlen($money);
        $m = '';
        $money = strrev($money);
        for ($i = 0; $i < $len; $i++) {
            if (( $i == 3 || ($i > 3 && ($i - 1) % 2 == 0) ) && $i != $len) {
                $m .=',';
            }
            $m .=$money[$i];
        }
        return strrev($m);
    }

}
/* End of file master_helper.php */
/* Location: ./application/helpers/master_helper.php */
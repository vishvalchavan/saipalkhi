<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Payment_model extends MY_Model {

    //put your code here

    public $_table = 'tbl_app_users';
    public $primary_key = 'id';

    public function get_year_id($year) {
        $this->db->select('id');
        $this->db->where('year', $year);
        $year_id = $this->db->get('tbl_year')->result_array();
        if (isset($year_id) && !empty($year_id)) {
            return $year_id[0]['id'];
        } else {
            return false;
        }
    }

    public function get_devotee_id($dpn_no) {
        $result = $this->db->select('id,mobile')->get_where(SBP_DEVOTEE, array('dpn_no' => $dpn_no))->result_array();
        if (isset($result)) {
            return $result;
        }
    }

    public function get_vehicle_details($year) {
        $truck_name = "";
        $year_id = $this->get_year_id($year);
        $vehicle_info = $this->db->select('id,truck_name,truck_capacity,assigned_count')
                        ->where('assigned_count<truck_capacity')
                        ->where(array('participation_year_id' => $year_id, 'isactive' => 1))
                        ->get(SBP_TRUCK_DETAILS)->result_array();
        if (isset($vehicle_info) && !empty($vehicle_info)) {
            $truck_name = $vehicle_info[0]['truck_name'];
            $truck_id = $vehicle_info[0]['id'];
            $this->update_vehicle_assign_count($truck_id);
        }
        return $truck_name;
    }

    public function update_vehicle_assign_count($truck_id) {
        $assign_count = 'assigned_count+1';
        $this->db->set('assigned_count', $assign_count, FALSE);
        $this->db->where('id', $truck_id);
        $this->db->update(SBP_TRUCK_DETAILS);
    }

    public function update_vehicle_details($year, $truk_name) {
        $assign_count = 'assigned_count-1';
        $this->db->set('assigned_count', $assign_count, FALSE);
        $this->db->where(array('truck_name' => $truk_name, 'participation_year_id' => $year));
        $this->db->update(SBP_TRUCK_DETAILS);
    }

    public function update_padyatri_count($year, $amount) {
        $padyatri_count = 'total_padyatri+1';
        $web_count = 'registered_via_web+1';
        $total_amt = 'total_amt+' . $amount;
        $this->db->set('total_padyatri', $padyatri_count, FALSE);
        $this->db->set('registered_via_web', $web_count, FALSE);
        $this->db->set('total_amt', $total_amt, FALSE);
        $this->db->where('enroll_year', $year);
        $this->db->update('tbl_dashboard_data');
    }

    public function insert_padyatri($devotee, $padyatridata) {
        $this->db->insert(SBP_PADYATRI, $padyatridata);
        $padyatri_id = $this->db->insert_id();
        if (isset($padyatri_id)) {
            $this->db->where('id', $devotee)->update(SBP_DEVOTEE, array('last_paymentid' => $padyatri_id));
            return true;
        }
    }

    public function update_padyatri($padyatri, $padyatridata) {
        $this->db->where('id', $padyatri);
        if ($this->db->update(SBP_PADYATRI, $padyatridata)) {
            return true;
        } else {
            return false;
        }
    }

    public function update_devotee($devotee, $devoteedata) {
        $this->db->where('id', $devotee);
        if ($this->db->update(SBP_DEVOTEE, $devoteedata)) {
            return true;
        } else {
            return false;
        }
    }

    public function if_padyatri_exists($devotee, $year) {
        if (isset($devotee) && !empty($devotee)) {
            $this->db->select('id,payment_status,vehical_type,vehicle_details');
            $result = $this->db->get_where(SBP_PADYATRI, array('devotee_id' => $devotee, 'participation_year_id' => $year))->result_array();
            return $result;
        }
    }

    public function get_devotee($devoteeid) {
        $result = $this->db->select('id,email,dpn_no,firstname,middlename,lastname,address,city,pincode')->get_where(SBP_DEVOTEE, array('id' => $devoteeid))->result_array();
        if (isset($result)) {
            return $result[0];
        }
    }

}

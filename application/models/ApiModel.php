<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ApiModel extends MY_Model {

    //put your code here

    public $_table = 'tbl_app_users';
    public $primary_key = 'id';
    public $where_not_grpid_is_one = "group_id !='1'";

    public function validate_token($token) {
        $response = $this->db->select('id,token')->where(array('token' => $token))->get($this->_table)->result_array();
        return $response;
    }

    public function get_dindi_list() {
        $result = $this->db->select('id as dindi_id,name as dindi_name')->where('isactive', '1')->get(SBP_DINDI_LIST)->result_array();
        return $result;
    }

    public function get_dashboard_data() {
        $result = $this->db->select('enroll_year as year,registered_via_app as total_enrollment,payment_via_app')->get('tbl_dashboard_data')->result_array();
        if (isset($result) && !empty($result)) {
            foreach ($result as $key => $val) {
                $total_amt = 1500 * $val['payment_via_app'];
                $result[$key]['total_payment'] = $total_amt;
            }
        }
        return $result;
    }

    public function get_vehicleInfo($year) {
        $year_id = $this->get_year_id($year);
        $result = $this->db->select('truck_name as tampo_name,truck_capacity as tampo_capacity,assigned_count as devotee_assigned')
                        ->where('assigned_count<truck_capacity')
                        ->where(array('participation_year_id' => $year_id, 'isactive' => 1))
                        ->get(SBP_TRUCK_DETAILS)->result_array();
        return $result;
    }

    public function get_year_id($year) {
        $this->db->select('id');
        $this->db->where('year', $year);
        $year_id = $this->db->get('tbl_year')->result_array();
        if (isset($year_id) && !empty($year_id)) {
            return $year_id[0]['id'];
        } else {
            return false;
        }
    }

    public function devotee_list($timestamp = NULL) {
        $year_id = $this->get_year_id(date('Y'));
        $this->db->select('tbl_dt.id,tbl_dt.email,tbl_dt.dpn_no,tbl_dt.date_of_joining,tbl_dt.firstname,tbl_dt.middlename,tbl_dt.lastname,tbl_dt.last_paymentid,
                                tbl_dt.birth_date,tbl_dt.mobile,tbl_dt.address,tbl_dt.city,tbl_dt.pincode,tbl_dt.gender,tbl_dt.blood_group,tbl_dt.relative_name,tbl_dt.relative_mobile,tbl_py.id as padyatriid,
                                tbl_py.vehical_type,tbl_py.participation_year_id,
                                tbl_py.vehicle_details,
                                tbl_py.dindi_id,                               
                                tbl_py.payment_status,tbl_py.badget_print,tbl_py.receipt_print');
        $this->db->from(SBP_DEVOTEE . ' tbl_dt');
        $this->db->join(SBP_PADYATRI . ' tbl_py', 'tbl_py.devotee_id=tbl_dt.id', 'left');
        $this->db->where('tbl_dt.isactive', 1);
        if ($timestamp !== NULL) {
            $this->db->where('tbl_dt.modified_time>=', date('Y-m-d H:i:s', strtotime($timestamp)));
        }
        $this->db->where($this->where_not_grpid_is_one);
        $this->db->order_by('tbl_dt.id', 'asc');
        $devotee_result = $this->db->get()->result_array();

        $padyatri_result = $this->db->select('tbl_py.devotee_id,tbl_py.payment_status')
                        ->from(SBP_PADYATRI . ' tbl_py')->where('tbl_py.participation_year_id', $year_id)->get()->result_array();
        $devotees = array();
        $index = 0;
        $devotee_ids = array();
        if (isset($devotee_result) && !empty($devotee_result)) {
            foreach ($devotee_result as $devotee) {
                if (!in_array($devotee['id'], $devotee_ids)) {
                    $devotee_ids[] = $devotee['id'];
                    $devotees[$index]['id'] = $devotee['id'];
                    $devotees[$index]['dpn_no'] = $devotee['dpn_no'];
                    $devotees[$index]['date_of_joining'] = $devotee['date_of_joining'];
                    $devotees[$index]['first_name'] = $devotee['firstname'];
                    $devotees[$index]['middle_name'] = $devotee['middlename'];
                    $devotees[$index]['last_name'] = $devotee['lastname'];
                    $devotees[$index]['gender'] = $devotee['gender'];
                    $devotees[$index]['blood_group'] = $devotee['blood_group'];
                    $devotees[$index]['address'] = $devotee['address'];
                    $devotees[$index]['mobile_no'] = $devotee['mobile'];
                    $devotees[$index]['email_id'] = $devotee['email'];
                    $devotees[$index]['date_of_birth'] = date("d M Y", strtotime($devotee['birth_date']));
                    $devotees[$index]['city'] = $devotee['city'];
                    $devotees[$index]['pincode'] = $devotee['pincode'];
                    $devotees[$index]['relative_name'] = isset($devotee['relative_name']) ? $devotee['relative_name'] : '';
                    $devotees[$index]['relative_mobile'] = isset($devotee['relative_mobile']) ? $devotee['relative_mobile'] : '';
                    $devotees[$index]['is_badge_print'] = isset($devotee['badget_print']) ? $devotee['badget_print'] : '0';
                    $devotees[$index]['is_receipt_print'] = isset($devotee['receipt_print']) ? $devotee['receipt_print'] : '0';
                    $record_matched = 0;
                    foreach ($devotee_result as $dt) {
                        if ($devotee['id'] == $dt['id']) {
                            if (!empty($dt['last_paymentid']) && $dt['padyatriid'] == $devotee['last_paymentid']) {
                                $record_matched = 1;
                                $devotees[$index]['vehicle_type'] = $dt['vehical_type'];
                                $devotees[$index]['tampo_name'] = $dt['vehicle_details'];
                                $devotees[$index]['dindi_id'] = $dt['dindi_id'];
                            }
                        }
                    }
                    if ($record_matched == 0) {
                        $devotees[$index]['vehicle_type'] = "";
                        $devotees[$index]['tampo_name'] = "";
                        $devotees[$index]['dindi_id'] = "";
                    }
                    if (isset($padyatri_result) && !empty($padyatri_result)) {
                        $precord_matched = 0;
                        foreach ($padyatri_result as $padyatri) {
                            if ($padyatri['devotee_id'] == $devotee['id']) {
                                $precord_matched = 1;
                                if ($padyatri['payment_status'] == 'success') {
                                    $devotees[$index]['payment_status'] = "yes";
                                } else {
                                    $devotees[$index]['payment_status'] = "no";
                                }
                            }
                        }
                        if ($precord_matched == 0) {
                            $devotees[$index]['payment_status'] = "no";
                        }
                    } else {
                        $devotees[$index]['payment_status'] = "no";
                    }
                    $index++;
                }
            }
        }

        return $devotees;
    }

    public function insert_devotees($devotee) {
        if (isset($devotee) && !empty($devotee)) {
            $this->db->insert(SBP_DEVOTEE, $devotee);
            $devotee_id = $this->db->insert_id();

            $this->db->insert('users_groups', array('group_id' => 2, 'user_id' => $devotee_id));
            return $devotee_id;
        }
    }

    public function update_devotee($id, $devotee_data) {
        if (isset($id, $devotee_data)) {
            $this->db->where('id', $id)->update(SBP_DEVOTEE, $devotee_data);
        }
    }

    public function insert_padyatri($padyatri) {
        if (isset($padyatri) && !empty($padyatri)) {
            $this->db->insert(SBP_PADYATRI, $padyatri);
            $padyatri_id = $this->db->insert_id();
            return $padyatri_id;
        }
    }

    public function update_padyatri($id, $padyatri_data) {
        if (isset($id, $padyatri_data)) {
            $this->db->where('id', $id)->update(SBP_PADYATRI, $padyatri_data);
        }
    }

    public function update_devotee_paymentinfo($padytri_id, $devotee_id, $modified = NULL) {
        $data = array('last_paymentid' => $padytri_id);
        if ($modified != NULL) {
            $data['modified_time'] = $modified;
        }
        $this->db->where('id', $devotee_id)->update(SBP_DEVOTEE, $data);
    }

    public function update_dashboardData($total_devotee, $total_padyatri, $sum_amt, $year) {
        $this->db->select('id,total_devotees,total_padyatri,registered_via_app,payment_via_app,total_amt');
        $dashbard_data = $this->db->get_where('tbl_dashboard_data', array('enroll_year' => $year))->result_array();
        if (isset($dashbard_data) && !empty($dashbard_data)) {
            $sum_devotee = $dashbard_data[0]['total_devotees'] + $total_devotee;
            $sum_padyatri = $dashbard_data[0]['total_padyatri'] + $total_padyatri;
            $register_app = $dashbard_data[0]['registered_via_app'] + $total_devotee;
            $payment_app = $dashbard_data[0]['payment_via_app'] + $total_padyatri;
            $sum_amount = $dashbard_data[0]['total_amt'] + $sum_amt;
            $data = array(
                'total_devotees' => $sum_devotee,
                'total_padyatri' => $sum_padyatri,
                'registered_via_app' => $register_app,
                'payment_via_app' => $payment_app,
                'total_amt' => $sum_amount
            );
            $this->db->where('id', $dashbard_data[0]['id'])->update('tbl_dashboard_data', $data);
        }
    }

    public function get_devotee_id($dpn_no) {
        $devotee = $this->db->select('id')->get_where(SBP_DEVOTEE, array('dpn_no' => $dpn_no))->result_array();
        if (isset($devotee) && !empty($devotee)) {
            return $devotee[0]['id'];
        }
    }

    public function get_devotee($dpn_no) {
        $devotee = $this->db->select('id,dpn_no')->get_where(SBP_DEVOTEE, array('dpn_no' => $dpn_no))->result_array();
        return $devotee;
    }

    public function get_devotee_info($devotee_id) {
        $this->db->select('tbl_dt.id,tbl_dt.dpn_no,tbl_dt.mobile,tbl_dt.email,tbl_dt.firstname,tbl_dt.middlename,tbl_dt.lastname,tbl_dt.last_paymentid,
                                tbl_dt.address,tbl_dt.city,tbl_dt.pincode,tbl_py.id as padyatriid,
                                tbl_py.vehical_type,tbl_py.participation_year_id,
                                tbl_py.vehicle_details,
                                tbl_py.dindi_id');
        $this->db->from(SBP_DEVOTEE . ' tbl_dt');
        $this->db->join(SBP_PADYATRI . ' tbl_py', 'tbl_py.devotee_id=tbl_dt.id', 'left');
        $this->db->where(array('tbl_dt.id' => $devotee_id, 'tbl_dt.isactive' => 1));
        $this->db->where($this->where_not_grpid_is_one);
        $devotee_result = $this->db->get()->result_array();
        $devotees = array();
        if (isset($devotee_result) && !empty($devotee_result)) {
            $index = 0;
            foreach ($devotee_result as $devotee) {
                foreach ($devotee_result as $dt) {
                    if (isset($dt['last_paymentid']) && !empty($dt['last_paymentid'])) {
                        if ($dt['last_paymentid'] == $devotee['padyatriid']) {
                            $devotees[$index]['id'] = $dt['id'];
                            $devotees[$index]['dpn_no'] = $dt['dpn_no'];
                            $devotees[$index]['email'] = $dt['email'];
                            $devotees[$index]['first_name'] = $dt['firstname'];
                            $devotees[$index]['middle_name'] = $dt['middlename'];
                            $devotees[$index]['last_name'] = $dt['lastname'];
                            $devotees[$index]['address'] = $dt['address'];
                            $devotees[$index]['city'] = $dt['city'];
                            $devotees[$index]['pincode'] = $dt['pincode'];
                            $devotees[$index]['mobile'] = $dt['mobile'];
                            $devotees[$index]['vehicle_type'] = $dt['vehical_type'];
                            $devotees[$index]['tampo_name'] = $dt['vehicle_details'];
                            $devotees[$index]['dindi_id'] = $dt['dindi_id'];
                            $index++;
                            break;
                        }
                    }
                }
            }
//            if (isset($devotees)) {
//                return $devotees;
//            }
        }
        return $devotees;
    }

    public function get_padyatri($devotee_id, $year) {
        $padyatri = $this->db->select('id,vehical_type as vehicle_type,vehicle_details,payment_status')->get_where(SBP_PADYATRI, array('devotee_id' => $devotee_id, 'participation_year_id' => $year))->result_array();
        return $padyatri;
    }

    public function delete_devotee($devotee_id) {
        $this->db->where('user_id', $devotee_id)->delete('users_groups');
        $this->db->where('id', $devotee_id)->delete(SBP_DEVOTEE);
    }

    public function vehicle_update($truckname, $year) {
        $this->db->select('id,assigned_count');
        $vehicle = $this->db->get_where(SBP_TRUCK_DETAILS, array('truck_name' => $truckname, 'participation_year_id' => $year, 'isactive' => 1))->result_array();
        if (isset($vehicle) && !empty($vehicle)) {
            $assigned_count = $vehicle[0]['assigned_count'] + 1;
            $data = array('assigned_count' => $assigned_count);
            $this->db->where('id', $vehicle[0]['id']);
            $this->db->update(SBP_TRUCK_DETAILS, $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            }
        }
    }

    public function remove_assigned_vehicle($truckname, $year) {
        $this->db->select('id,assigned_count');
        $vehicle = $this->db->get_where(SBP_TRUCK_DETAILS, array('truck_name' => $truckname, 'participation_year_id' => $year, 'isactive' => 1))->result_array();
        if (isset($vehicle) && !empty($vehicle)) {
            $assigned_count = $vehicle[0]['assigned_count'] - 1;
            $data = array('assigned_count' => $assigned_count);
            $this->db->where('id', $vehicle[0]['id']);
            $this->db->update(SBP_TRUCK_DETAILS, $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            }
        }
    }

    public function getDevotees() {
        $result = $this->db->select('id,user_image_path')->get_where(SBP_DEVOTEE, array('isactive' => 1))->result_array();
        return $result;
    }

    public function getexistingprofile($devotee) {
        $result = $this->db->select('id,user_image_path')->get_where(SBP_DEVOTEE, array('id' => $devotee))->result_array();
        if (isset($result) && !empty($result)) {
            return $result[0]['user_image_path'];
        }
    }

    public function get_enrollment_data() {
        $result = $this->db->select('id,token,date,enrollment_count,enrollment_amount')->get('tbl_app_enrollment')->result_array();
        $report = array();
        if (isset($result) && !empty($result)) {
            $index = 0;
            foreach ($result as $r) {
                $report[$index]['token'] = $r['token'];
                $report[$index]['report_date'] = $r['date'];
                $report[$index]['server_payment_count'] = $r['enrollment_count'];
                $report[$index]['payment_amt'] = $r['enrollment_amount'];
                $index++;
            }
        }
        return $report;
    }

    public function enrollment_exists($date, $token) {
        $result = $this->db->select('id')->get_where('tbl_app_enrollment', array('date' => date('Y-m-d', strtotime($date)), 'token' => $token))->result_array();
        return $result;
    }

    public function insert_enrollment($sabhasad_amount, $enroll) {
        if (isset($enroll) && !empty($enroll)) {
            $enroll_data = array(
                'token' => $enroll['token'],
                'date' => date("Y-m-d", strtotime($enroll['report_date'])),
                'enrollment_count' => $enroll['local_payment_count'],
                'enrollment_amount' => ($sabhasad_amount * $enroll['local_payment_count'])
            );
            if ($this->db->insert('tbl_app_enrollment', $enroll_data)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    public function update_enrollment($sabhasad_amount, $enroll) {
        if (isset($enroll) && !empty($enroll)) {
            $enrollment_count = 'enrollment_count+' . $enroll['local_payment_count'];
            $enrollment_amt = 'enrollment_amount+' . ($sabhasad_amount * $enroll['local_payment_count']);
            $this->db->set('enrollment_count', $enrollment_count, FALSE);
            $this->db->set('enrollment_amount', $enrollment_amt, FALSE);
            $this->db->where(array('token' => $enroll['token'], 'date' => $enroll['report_date']));
            if ($this->db->update('tbl_app_enrollment')) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    public function get_devotee_by_id($devoteeid) {
        $result = $this->db->select('id,email,dpn_no,firstname,middlename,lastname,address,city,pincode')->get_where(SBP_DEVOTEE, array('id' => $devoteeid))->result_array();
        if (isset($result)) {
            return $result[0];
        }
    }

}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Menu
 *
 * @author admin
 */
class Devotee extends MY_Model {

    //put your code here

    public $_table = SBP_DEVOTEE;
    public $primary_key = 'id';
    public $where_not_grpid_is_one = "group_id !='1'";

    function get_slug_by_id($id) {
        if ($id != null) {
            $obj = $this->db->select('salt')->get_where('main_users', array('id' => $id))->row();
            if (isset($obj)) {
                return $obj->salt;
            }
        }

        return null;
    }

    public function get_allData($user_id) {
        $this->db->select('*');
        $this->db->from(SBP_DEVOTEE);
        $this->db->where('id', $user_id);
        $query = $this->db->get();
        $objData = $query->result_array();
        return $objData[0];
    }

    public function get_users() {
        $this->db->select(SBP_DEVOTEE . '.*,l.id as languageid,l.language');
        $this->db->join('language as l', SBP_DEVOTEE . '.language_id=l.id');
//        $this->db->where('active', 1);
        $query = $this->db->get(SBP_DEVOTEE);
        return $query->result_array();
    }

    public function get_user_by_id($user_id) {
        $this->db->select(SBP_DEVOTEE . '.*,l.id as languageid,l.language');
        $this->db->join('language as l', SBP_DEVOTEE . '.language_id=l.id');
        $this->db->where(SBP_DEVOTEE . '.id', $user_id);
        $this->db->where('active', 1);
        $query = $this->db->get(SBP_DEVOTEE);
        $objData = $query->result_array();
        return $objData[0];
    }

    public function update_user_group_id($group_data) {
        $this->db->set('group_id', $group_data['group_id']);
        $this->db->where('id', $group_data['user_id']);
        $this->db->update('main_users');
    }

    public function update_user_status_id($id) {

        if (isset($id) && $id != NULL) {
            $this->db->set('isactive', '5');
            $this->db->where('id', $id);
            $this->db->update('main_users');
            return true;
        } else {
            return false;
        }
    }

    public function check_email($email_id) {

        $result = $this->db->select('email')->get_where('main_users', array('email' => $email_id))->row();
        if (isset($result))
            return FALSE;
        else
            return TRUE;
    }

    public function get_group_name($user_id) {
        if (isset($user_id) && $user_id != NULL) {
            $this->db->select('groups.name,groups.id,' . SBP_DEVOTEE . '.id as user_id');
            $this->db->join('users_groups', SBP_DEVOTEE . '.id=users_groups.user_id');
            $this->db->join('groups', 'users_groups.group_id=groups.id');
            $this->db->where(SBP_DEVOTEE . '.id', $user_id);
            $response = $this->db->get(SBP_DEVOTEE)->result_array();
            return $response;
        }
    }

    public function get_devotee_by_id($devoteeid) {
        $this->db->select('tbl_dt.id,
                                tbl_dt.email,
                                tbl_dt.dpn_no,
                                tbl_dt.firstname,
                                tbl_dt.middlename,
                                tbl_dt.lastname,
                                tbl_dt.date_of_joining,
                                tbl_dt.birth_date,
                                tbl_dt.mobile,
                                tbl_dt.mobile1,
                                tbl_dt.address,
                                tbl_dt.city,
                                tbl_dt.profession,
                                tbl_dt.education,
                                tbl_dt.gender,
                                tbl_dt.blood_group,
                                tbl_dt.relative_name`,
                                tbl_dt.relative_mobile`,
                                tbl_dt.sickness`,tbl_dt.pincode,
                                tbl_dt.identity_prof,tbl_dt.adhaar_no');
        $this->db->where('tbl_dt.id', $devoteeid);
        $this->db->where($this->where_not_grpid_is_one);
        $query = $this->db->get(SBP_DEVOTEE . ' tbl_dt')->result_array();
//        echo $this->db->last_query();die;
        return $query;
    }

    public function get_devotee_details($devoteeid) {
        $this->db->select('tbl_dt.id,tbl_dt.email,tbl_dt.dpn_no,tbl_dt.firstname,tbl_dt.middlename,tbl_dt.lastname,
                           tbl_dt.birth_date,tbl_dt.mobile,tbl_dt.mobile1,tbl_dt.address,tbl_dt.city,tbl_dt.profession,
                           tbl_dt.education,tbl_dt.gender,tbl_dt.blood_group,tbl_dt.relative_name`,tbl_dt.relative_mobile`,
                           tbl_dt.sickness`,tbl_dt.identity_prof,tbl_dt.adhaar_no,tbl_dt.active,tbl_dt.date_of_joining,tbl_dt.pincode,tbl_dt.user_image_path');
        $this->db->where('tbl_dt.id', $devoteeid);
        $this->db->where($this->where_not_grpid_is_one);
        $result = $this->db->get(SBP_DEVOTEE . ' tbl_dt')->result_array();
        if (isset($result)) {
            return $result[0];
        } else {
            return false;
        }
    }

    public function get_alldevotees($yearid, $year) {
        $this->load->library('subquery');
        $lastyear = (strtolower($year) != "all") ? $year : date("Y");
//      
        $from = date("Y-m-d", strtotime(date($lastyear . "-m-d")));
        $to = date("Y-m-d");
        $date1 = new DateTime($from);
        $date2 = $date1->diff(new DateTime($to));
        $Year_differnse = 0;
        $increaseYear = '';
        $whereYearQuery = '';
        $increaseYear = $lastyear + $date2->y;
        $whereYearQuery = 'YEAR(' . SBP_PADYATRI . '.payment_date)<=' . $increaseYear;

        $year = (strtolower($year) == "all") ? 'YEAR(tbl_dt.date_of_joining)<=' . date("Y") : 'YEAR(tbl_dt.date_of_joining)=' . $year;

        $this->db->select('tbl_dt.id,tbl_dt.dpn_no,tbl_dt.firstname,tbl_dt.middlename,tbl_dt.lastname,tbl_dt.birth_date,tbl_dt.user_image_path,
                                tbl_dt.mobile,tbl_dt.address,tbl_dt.city,tbl_dt.active,tbl_dt.blood_group,tbl_dt.pincode,tbl_dt.relative_name,
                                tbl_dt.relative_mobile,tbl_py.id as tbl_py_id,tbl_py.devotee_id,tbl_py.participation_year_id,
                                tbl_py.vehical_type,tbl_py.vehicle_details,tbl_py.dindi_id,tbl_py.payment_date,tbl_py.payment_mode,
                                tbl_py.payment_amount,tbl_py.payment_status,tbl_year.id as tbl_year_id,tbl_year.year as tbl_year_year,
                                tbl_py.seva_samiti_id,tbl_py.badget_print,tbl_py.receipt_print,tbl_py.donationreceipt_print,tbl_donation.donor_id,tbl_donation.devotee_id,
                                tbl_donation.donation_for,tbl_donation.donation_amount,tbl_donation.donation_status,
                                tbl_donation.donation_date,tbl_donation.donated_year,tbl_donation.receipt_no,tbl_donation.created_date');

//start---------------- sub query for get last payment dindi id------------------------------------------------------------------------------------------------
//        $inbetween = ($date2->y > 0) ? $whereYearQuery : "YEAR(" . SBP_PADYATRI . ".payment_date) <= " . $lastyear;
//        #$inbetween = "YEAR(".SBP_PADYATRI.".payment_date) != " . $lastyear;        
//        $subdindi = $this->subquery->start_subquery('select');
//        $subdindi->select(SBP_PADYATRI . '.dindi_id')->from(SBP_PADYATRI);
//        $subdindi->where(SBP_PADYATRI . '.`devotee_id` = `tbl_dt`.`id`');
//        $subdindi->where($inbetween);
//        $subdindi->order_by(SBP_PADYATRI . '.id', 'desc');
//        $subdindi->limit(1);
//        $this->subquery->end_subquery('lastpay_dindi_id');
////start 2----------- sub query for get last payment samiti id------------------------------------------------------------------------------------------------
//        $subsamiti = $this->subquery->start_subquery('select');
//        $subsamiti->select(SBP_PADYATRI . '.seva_samiti_id')->from(SBP_PADYATRI);
//        $subsamiti->where(SBP_PADYATRI . '.`devotee_id` = `tbl_dt`.`id`');
//        $subsamiti->where($inbetween);
//        $subsamiti->order_by(SBP_PADYATRI . '.id', 'desc');
//        $subsamiti->limit(1);
//        $this->subquery->end_subquery('lastpay_samiti_id');
//
//        $ifdindi = $this->subquery->start_subquery('select');
//        $ifdindi->select('if(lastpay_dindi_id > 0,lastpay_dindi_id,dindi_id)');
//        $this->subquery->end_subquery('dindi_id');
//
//        $ifsamiti = $this->subquery->start_subquery('select');
//        $ifsamiti->select('if(lastpay_samiti_id > 0,lastpay_samiti_id,seva_samiti_id)');
//        $this->subquery->end_subquery('seva_samiti_id');
//
//        $lastdate_sub = $this->subquery->start_subquery('select');
//        $lastdate_sub->select('max(' . SBP_PADYATRI . '.`payment_date`)')->from(SBP_PADYATRI);
//        $lastdate_sub->where(SBP_PADYATRI . '.`devotee_id` = `tbl_dt`.`id`');
//        $lastdate_sub->where($inbetween);
//        $lastdate_sub->order_by(SBP_PADYATRI . '.id', 'desc');
//        $lastdate_sub->limit(1);
//        $this->subquery->end_subquery('payment_date');
//end--------------  sub query -----------------------------------------------------------------------------------------------------------------------------        
        $this->db->from(SBP_DEVOTEE . ' tbl_dt');
        $this->db->join(SBP_PADYATRI . ' tbl_py', 'tbl_py.devotee_id=tbl_dt.id AND tbl_py.id=tbl_dt.last_paymentid', 'left');
        $this->db->join('tbl_year', 'tbl_year.id=tbl_py.participation_year_id', 'left');
        $this->db->join('tbl_seva_samiti_master tbl_ssm', 'tbl_ssm.id=tbl_py.seva_samiti_id', 'left');
        $this->db->join('tbl_donation', 'tbl_donation.devotee_id=tbl_dt.id', 'left');
        $this->db->where(array('tbl_dt.isactive' => 1));
        $this->db->where($year);
        $this->db->where($this->where_not_grpid_is_one);
        $this->db->group_by('tbl_dt.id', 'asc');
        $query = $this->db->get();
//            echo $this->db->last_query();die;
        $objData = $query->result_array();
        return $objData;
    }

//        ------------------update divotee ---------------------------------------------------------------------
    public function update_divotee($devotee_id, $devoteedata) {

        $this->db->where(array('id' => $devotee_id));
        $devotee_query = $this->db->update(SBP_DEVOTEE, $devoteedata);
        if ($devotee_query) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_devotee($devotee_id) {
        $update = array(
            'isactive' => 0
        );
        $this->db->where('id', $devotee_id);
        $this->db->update(SBP_DEVOTEE, $update);
        $dashboard_py_count = 'total_devotees-1';
        $this->db->set('total_devotees', $dashboard_py_count, FALSE)->update('tbl_dashboard_data');
    }

//    --------------------submit devotee amount----------------------------------

    public function submit_devotee_amount($devotee_id, $padyatri_id, $padyatri_data, $year) {
        $seatcount = "truck_capacity > assigned_count";
        $truckid = '';
        $vehicle_details = '';
        $vehical_type = '';

        if (isset($padyatri_id) && !empty($padyatri_id)) {
            $pad_y_query = $this->db->select('id,vehical_type,vehicle_details')
                    ->from(SBP_PADYATRI)
                    ->where(array('id' => $padyatri_id, 'participation_year_id' => $padyatri_data['participation_year_id']))
                    ->get();
            $py_row = $pad_y_query->row();
            if ($pad_y_query->num_rows() > 0) {
                $vehicle_details = $py_row->vehicle_details;
                $vehical_type = $py_row->vehical_type;
                $dev_to_padyatri = array('id' => $py_row->id,
//                    'devotee_id' => $padyatri_data['devotee_id'],
                    'seva_samiti_id' => $padyatri_data['seva_samiti_id'],
                    'dindi_id' => $padyatri_data['dindi_id'],
                    'vehical_type' => $padyatri_data['vehical_type'],
                    'vehicle_details' => ($padyatri_data['vehical_type'] == 'Self') ? '' : $py_row->vehicle_details,
                    'payment_date' => date("Y-m-d"),
//                    'payment_mode' => $padyatri_data['payment_mode'],
                    'payment_amount' => $padyatri_data['amountpaid'],
                    'payment_status' => 'success');
                $this->db->where('id', $padyatri_id);
                $insert_id = $padyatri_id;
                $devotee_query = $this->db->update(SBP_PADYATRI, $dev_to_padyatri);
            } else {
//                insert padyatri table data=----------
//                $dt_query = $this->db->select('id,dpn_no')->from(SBP_DEVOTEE)->where(array('id' => $devotee_id, 'YEAR(date_of_joining)' => $year))->get()->row();
                $insert_data_py = array('devotee_id' => $padyatri_data['devotee_id'], 'participation_year_id' => $padyatri_data['participation_year_id'],
                    'vehical_type' => $padyatri_data['vehical_type'], 'vehicle_details' => '', 'dindi_id' => $padyatri_data['dindi_id'],
                    'seva_samiti_id' => $padyatri_data['seva_samiti_id'], 'payment_date' => date("Y-m-d"), 'payment_mode' => $padyatri_data['payment_mode'],
                    'payment_amount' => $padyatri_data['amountpaid'], 'payment_status' => 'success', 'enrollment_via' => 'web');
                $devotee_query = $this->db->insert(SBP_PADYATRI, $insert_data_py);
                $insert_id = $this->db->insert_id();
                $this->db->set('last_paymentid', $insert_id, FALSE)->where('id', $devotee_id)->update(SBP_DEVOTEE);
            }
        } else {
            $dt_query = $this->db->select('id,devotee_id')->from(SBP_PADYATRI)->where(array('devotee_id' => $devotee_id, 'participation_year_id' => $padyatri_data['participation_year_id'], 'payment_status' => 'success'))->get()->num_rows();
            if ($dt_query > 0) {
                
            } else {
                $insert_data_py = array('devotee_id' => $padyatri_data['devotee_id'], 'participation_year_id' => $padyatri_data['participation_year_id'],
                    'vehical_type' => $padyatri_data['vehical_type'], 'vehicle_details' => '', 'dindi_id' => $padyatri_data['dindi_id'],
                    'seva_samiti_id' => $padyatri_data['seva_samiti_id'], 'payment_date' => date("Y-m-d"), 'payment_mode' => $padyatri_data['payment_mode'],
                    'payment_amount' => $padyatri_data['amountpaid'], 'payment_status' => 'success', 'enrollment_via' => 'web');
                $devotee_query = $this->db->insert(SBP_PADYATRI, $insert_data_py);
                $insert_id = $this->db->insert_id();
                $this->db->set('last_paymentid', $insert_id, FALSE)->where('id', $devotee_id)->update(SBP_DEVOTEE);
            }
        }

        if ($devotee_query) {
            $dashboard_py_count = 'total_padyatri+1';
            $p_y_amount = $padyatri_data['amountpaid'];
            $total_amount = 'total_amt+"' . $p_y_amount . '"';
            $this->db->set('total_padyatri', $dashboard_py_count, FALSE)->set('cash_cheque', 'cash_cheque+1', FALSE)->set('total_amt', $total_amount, FALSE)->where('enroll_year', date('Y'))->update('tbl_dashboard_data');
            $this->truck_booking($insert_id, $vehicle_details, $vehical_type, $padyatri_data['vehical_type'], $padyatri_data['participation_year_id']);
            return true;
            exit();
        } else {
            return false;
            exit();
        }
    }

    function truck_booking($padyatri_id = NULL, $truckid = NULL, $db_vehical_type = NULL, $vehical_type, $yearid) {
        $seatcount = "truck_capacity > assigned_count";
        $assigned_count = '';
        $padyatridata['vehical_type'] = $vehical_type;
        if ($vehical_type == "Self") {
            $padyatridata['vehicle_details'] = '';
            $assigned_count = 'assigned_count-1';
        } else {
            $vehicleQuery = $this->db->select('id,truck_name,truck_number')
                    ->from(SBP_TRUCK_DETAILS)
                    ->where('isactive', 1)
                    ->where($seatcount)
                    ->where('participation_year_id', $yearid)
                    ->Order_by('id', 'asc')
                    ->limit('1')
                    ->get();
            $vehicleQueryRes = $vehicleQuery->row();

            if ($vehicleQuery->num_rows() > 0) {
                $padyatridata['vehicle_details'] = $vehicleQueryRes->truck_name;
                $truckid = ($truckid) ? $truckid : $vehicleQueryRes->truck_name;
                $assigned_count = 'assigned_count+1';
            }
        }
        if ($db_vehical_type != $vehical_type) {
            $this->db->set('assigned_count', $assigned_count, FALSE)->where(array('truck_name' => $truckid, 'participation_year_id' => $yearid))->update(SBP_TRUCK_DETAILS);
            $this->db->where('id', $padyatri_id)->update(SBP_PADYATRI, $padyatridata);
        }
    }

    public function get_truck_details($yearid) {
        $seatcount = "truck_capacity > assigned_count";
        return $vehicleQuery = $this->db->select('id,truck_name,truck_number')
                        ->from(SBP_TRUCK_DETAILS)
                        ->where('isactive', 1)
                        ->where($seatcount)
                        ->where('participation_year_id', $yearid)
                        ->Order_by('id', 'asc')
                        ->limit('1')
                        ->get()->row();
    }

    public function if_devotee_exists($dob, $mobile) {
        $result = $this->db->select('id')->get_where(SBP_DEVOTEE, array('birth_date' => $dob, 'mobile' => $mobile))->result_array();
        return $result;
    }

    public function if_devoteedpn_exists($first_name, $middle_name, $last_name, $dob, $city) {
        $result = $this->db->select('id')->get_where(SBP_DEVOTEE, array('lower(firstname)' => strtolower($first_name), 'lower(middlename)' => strtolower($middle_name), 'lower(lastname)' => strtolower($last_name), 'birth_date' => date('Y-m-d', strtotime($dob)), 'lower(city)' => strtolower($city)))->result_array();
        return $result;
    }

    public function insert_devotee($devotee) {
        if (isset($devotee) && !empty($devotee)) {
            $this->db->insert(SBP_DEVOTEE, $devotee);
            $devotee_id = $this->db->insert_id();
            // insert usergroup info
            $this->db->insert('users_groups', array('group_id' => 2, 'user_id' => $devotee_id));
            // Update dashboard count table
            $devotee_count = 'total_devotees+1';
            $this->db->set('total_devotees', $devotee_count, FALSE);
            $this->db->where('tbl_dashboard_data.enroll_year', date('Y'));
            $this->db->update('tbl_dashboard_data');

            return $devotee_id;
        }
    }

    public function devotee_payment_details($devotee, $year) {
        if (isset($devotee) && !empty($devotee)) {
            $this->db->select('tbl_dt.relative_name,tbl_dt.relative_mobile,tbl_dt.sickness,tbl_dt.identity_prof,payment_status,dindi_id,seva_samiti_id,payment_mode,vehical_type,payment_date');
            $this->db->join(SBP_DEVOTEE . '  tbl_dt', SBP_PADYATRI . '.devotee_id=tbl_dt.id');
            $result = $this->db->get_where(SBP_PADYATRI, array('devotee_id' => $devotee, 'participation_year_id' => $year))->result_array();
            return $result;
        }
    }

    public function padyatri_lastyear_details($devotee_id) {
        if (isset($devotee_id) && !empty($devotee_id)) {
            $this->db->select('tbl_dt.relative_name,tbl_dt.relative_mobile,tbl_dt.sickness,tbl_dt.identity_prof,payment_status,tbl_dt.dpn_no,dindi_id,seva_samiti_id,payment_mode,vehical_type');
            $this->db->join(SBP_DEVOTEE . ' tbl_dt', SBP_PADYATRI . '.id=tbl_dt.last_paymentid');
            $result = $this->db->get_where(SBP_PADYATRI, array('devotee_id' => $devotee_id))->result_array();
            return $result;
        }
    }

    public function change_password($userid, $data) {
        if (isset($userid, $data)) {
            $this->db->where(SBP_DEVOTEE . '.id', $userid);
            if ($this->db->update(SBP_DEVOTEE, $data)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function get_education_list() {
        $result = $this->db->select('id,education')->get_where('tbl_education', array('isactive' => 1))->result_array();
        return $result;
    }

    public function get_profession_list() {
        $result = $this->db->select('id,occupation')->get_where('tbl_occupation', array('isactive' => 1))->result_array();
        return $result;
    }

    public function get_idproof_list() {
        $result = $this->db->select('id,document_type')->get_where('tbl_identity_proof', array('isactive' => 1))->result_array();
        return $result;
    }

    public function get_city_list() {
        $result = $this->db->select('DISTINCT(`name`)')->order_by('tbl_city.name', 'ASC')->get_where('tbl_city', array('isactive' => 1))->result_array();
        return $result;
    }

    public function getdevotee_mobileno($devoteeid) {
        $result = $this->db->select('mobile,email')->get_where(SBP_DEVOTEE, array('id' => $devoteeid))->result();
        return isset($result) && !empty($result) ? $result[0] : "";
    }

    public function get_profilepic($userid = NULL) {
        $result = $this->db->select('user_image_path')->get_where(SBP_DEVOTEE, array('id' => $userid))->row();
        ($result) ? unlink($result->user_image_path) : '';
        return true;
    }

    public function updateBadgetReceipt($devotee, $year, $badget_data) {
        $this->db->where(array('devotee_id' => $devotee, 'participation_year_id' => $year));
        if ($this->db->update(SBP_PADYATRI, $badget_data)) {
            return true;
        } else {
            return false;
        }
    }

    public function get_tab_users() {
        $result = $this->db->select('id,username,token')->get_where('tbl_app_users', array('isactive' => 1))->result_array();
        return $result;
    }

    public function get_tab_user_work($token = NULL) {
        $this->db->select('tbl_app_users.username,tbl_app_enrollment.token,tbl_app_enrollment.date,tbl_app_enrollment.enrollment_count,tbl_app_enrollment.enrollment_amount');
        $this->db->from('tbl_app_enrollment');
        $this->db->join('tbl_app_users', 'tbl_app_enrollment.token=tbl_app_users.token');
        $this->db->where('YEAR(tbl_app_enrollment.date)', date('Y'));
        if ($token !== NULL && !empty($token)) {
            $this->db->where('tbl_app_enrollment.token', $token);
        }
        $this->db->order_by('tbl_app_enrollment.date', 'desc');
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function tabuser_count() {
        $result = $this->db->select('count(id) as users')->get_where('tbl_app_users', array('isactive' => 1))->result_array();
        return $result;
    }

}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Sevasamiti_model extends MY_Model {

    //put your code here

    public $_table = 'tbl_seva_samiti_master';
    public $primary_key = 'id';

    public function get_samitis($year) {
        $this->db->select('sm.id,sm.name,sm.year_id,sm.isactive');
        $this->db->from('tbl_seva_samiti_master as sm');
        $this->db->where(array('sm.year_id' => $year));
        $this->db->where(array('sm.isactive' => 1));
        $this->db->order_by('sm.id', 'asc');
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function get_samiti_list($year) {
//        $this->db->select('sm.name,sm.year_id,sm.isactive,sm.id as seva_samiti_id,count(sp.id) as suppliercount,count(sd.id) as sevekaricount');
//        $this->db->from('tbl_seva_samiti_master as sm');
//        $this->db->join('SBP_PADYATRI pd', 'sm.id=pd.seva_samiti_id', 'left');
//        $this->db->join('tbl_devotee dt', 'pd.devotee_id=dt.id', 'left');
//        $this->db->join('tbl_sevekari_details as sd', 'pd.id=sd.padyatri_no', 'left');
//        $this->db->join('tbl_supplier_details as sp', 'sm.id=sp.seva_samiti_id', 'left');
//        $this->db->where(array('sm.year_id' => $year, 'sm.isactive' => 1));
//        $this->db->group_by('sm.id');
//        $this->db->order_by('sm.id', 'asc');
//        $samiti_data = $this->db->get()->result_array();
        $this->db->select('sm.id,sm.name,sm.year_id,sm.isactive');
        $this->db->from('tbl_seva_samiti_master as sm');
        $this->db->where(array('sm.year_id' => $year));
        $this->db->where(array('sm.isactive' => 1));
        $this->db->order_by('sm.id', 'asc');
        $data['samiti_data'] = $this->db->get()->result_array();

        if (isset($data['samiti_data']) && !empty($data['samiti_data'])) {
//            foreach ($data['samiti_data'] as $samiti) {
//                $samiti_ids[] = $samiti['id'];
//            }
//            if (isset($samiti_ids) && !empty($samiti_ids)) {
            $this->db->select('sd.seva_samiti_id,count(sd.id) as sevekaricount');
            $this->db->from('tbl_sevekari_details as sd');
            $this->db->where('sd.isactive', 1);
            $this->db->group_by('sd.seva_samiti_id');
            $data['sevekari_data'] = $this->db->get()->result_array();

            $this->db->select('sp.seva_samiti_id,count(sp.id) as suppliercount');
            $this->db->from('tbl_supplier_details as sp');
            $this->db->where('sp.isactive', 1);
            $this->db->group_by('sp.seva_samiti_id');
            $data['supplier_data'] = $this->db->get()->result_array();

            foreach ($data['samiti_data'] as $key => $samiti) {
                if (isset($data['sevekari_data']) && !empty($data['sevekari_data'])) {
                    $sevekariMatched = 0;
                    foreach ($data['sevekari_data'] as $sevekari) {
                        if ($samiti['id'] == $sevekari['seva_samiti_id']) {
                            $sevekariMatched = 1;
                            $data['samiti_data'][$key]['sevekari_count'] = $sevekari['sevekaricount'];
                        }
                    }
                    if ($sevekariMatched == 0) {
                        $data['samiti_data'][$key]['sevekari_count'] = 0;
                    }
                }
                if (isset($data['supplier_data']) && !empty($data['supplier_data'])) {
                    $supplierMatched = 0;
                    foreach ($data['supplier_data'] as $supplier) {
                        if ($samiti['id'] == $supplier['seva_samiti_id']) {
                            $supplierMatched = 1;
                            $data['samiti_data'][$key]['suplier_count'] = $supplier['suppliercount'];
                        }
                    }
                    if ($supplierMatched == 0) {
                        $data['samiti_data'][$key]['suplier_count'] = 0;
                    }
                }
            }
//            }
        }

        $this->db->select('sm.id as seva_samiti_id,dt.firstname,dt.middlename,dt.lastname,dt.mobile,sd.ishead');
        $this->db->from('tbl_seva_samiti_master as sm');
        $this->db->join(SBP_PADYATRI . ' pd', 'sm.id=pd.seva_samiti_id', 'left');
        $this->db->join('tbl_devotee dt', 'pd.devotee_id=dt.id', 'left');
        $this->db->join('tbl_sevekari_details as sd', 'pd.id=sd.padyatri_no', 'left');
        $this->db->where(array('sm.year_id' => $year, 'sm.isactive' => 1, 'sd.ishead' => 1));
        $samiti_head_data = $this->db->get()->result_array();


        if (isset($data['samiti_data'])) {
            foreach ($data['samiti_data'] as $index => $samiti) {
                if (isset($samiti_head_data)) {
                    $match_record = 0;
                    foreach ($samiti_head_data as $head) {
                        if ($head['seva_samiti_id'] == $samiti['id']) {
                            $data['samiti_data'][$index]['samiti_head'] = $head['firstname'] . ' ' . $head['middlename'] . ' ' . $head['lastname'];
                            $data['samiti_data'][$index]['mobile'] = $head['mobile'];
                            $match_record++;
                        }
                    }
                    if ($match_record == 0) {
                        $data['samiti_data'][$index]['samiti_head'] = "";
                        $data['samiti_data'][$index]['mobile'] = "";
                    }
                } else {
                    $data['samiti_data'][$index]['samiti_head'] = "";
                    $data['samiti_data'][$index]['mobile'] = "";
                }
            }
        }

        return $data['samiti_data'];
    }

    public function if_samiti_exists($samiti, $enroll_year) {
        $this->db->select('id');
        $this->db->where(array('name' => $samiti, 'year_id' => $enroll_year));
        $result = $this->db->get($this->_table)->result();
        return $result;
    }

    public function get_allData($samiti_id) {
        $this->db->select('id,name,description,year_id,isactive');
        $this->db->where('id', $samiti_id);
        $result = $this->db->get($this->_table)->result_array();
        if (isset($result)) {
            return $result[0];
        }
    }

    public function delete_samiti($samiti_id) {
        $update = array(
            'isactive' => 0
        );
        $this->db->where('id', $samiti_id);
        if ($this->db->update($this->_table, $update)) {
            return true;
        } else {
            return false;
        }
    }

    public function activate_samiti($samiti_id) {
        $update = array(
            'isactive' => 1
        );
        $this->db->where('id', $samiti_id);
        if ($this->db->update($this->_table, $update)) {
            return true;
        } else {
            return false;
        }
    }

    public function update_samiticount($year) {
        $dashboardData = $this->db->get_where('tbl_dashboard_data', array('enroll_year' => $year))->result();
        if (count($dashboardData) > 0) {
            $samiti_count = $dashboardData[0]->total_seva_samiti + 1;
            $data = array(
                'total_seva_samiti' => $samiti_count
            );
            $this->db->where('id', $dashboardData[0]->id);
            $this->db->update('tbl_dashboard_data', $data);
        } else {
            $data = array(
                'total_seva_samiti' => 1,
                'enroll_year' => date('Y')
            );
            $this->db->insert('tbl_dashboard_data', $data);
        }
    }

}

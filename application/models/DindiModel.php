<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DindiModel extends MY_Model {

    //put your code here

    public $_table = SBP_DINDI_LIST;
    public $primary_key = 'id';

    public function get_dindi_list() {
        $this->db->select('id,name,description,contact_person_name,contact_person_mobile,contact_person_name1,contact_person_mobile1,isactive');
        $this->db->from($this->_table);
//        $this->db->where('isactive', '1');
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function if_dindi_exists($dindi) {
        $this->db->select('id');
        $this->db->where('name', $dindi);
        $result = $this->db->get(SBP_DINDI_LIST)->result();
        return $result;
    }

    public function get_dindi_name($dindi) {
        $this->db->select('id,name');
        $this->db->where('id', $dindi);
        $result = $this->db->get(SBP_DINDI_LIST)->result();
        return $result;
    }

    public function get_allData($dindi_id) {
        $this->db->select('id,name,description,contact_person_name,contact_person_mobile,contact_person_name1,contact_person_mobile1,isactive');
        $this->db->from(SBP_DINDI_LIST);
        $this->db->where(array('id' => $dindi_id));
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function delete_dindi($dindi_id) {
        $update = array(
            'isactive' => 0
        );
        $this->db->where('id', $dindi_id);
        if ($this->db->update(SBP_DINDI_LIST, $update)) {
            return true;
        } else {
            return false;
        }
    }

    public function activate_dindi($dindi_id) {
        $update = array(
            'isactive' => 1
        );
        $this->db->where('id', $dindi_id);
        if ($this->db->update(SBP_DINDI_LIST, $update)) {
            return true;
        } else {
            return false;
        }
    }

    public function update_dashboard_dindicount($year) {
        $dashboardData = $this->db->get_where('tbl_dashboard_data', array('enroll_year' => $year))->result();
        if (count($dashboardData) > 0) {
            $dindi_count = $dashboardData[0]->no_of_dindis + 1;
            $data = array(
                'no_of_dindis' => $dindi_count
            );
            $this->db->where('id', $dashboardData[0]->id);
            $this->db->update('tbl_dashboard_data', $data);
        } else {
            $data = array(
                'no_of_dindis' => 1,
                'enroll_year' => date('Y')
            );
            $this->db->insert('tbl_dashboard_data', $data);
        }
    }

    public function get_dindi_report($year) {
        $this->db->select('id,name,contact_person_name,contact_person_mobile');
        $this->db->from(SBP_DINDI_LIST);
        $this->db->where('isactive', 1);
        $dindi_list = $this->db->get()->result_array();

        $this->db->select('gender,dindi_id,participation_year_id');
        $this->db->from(SBP_DEVOTEE);
        $this->db->join(SBP_PADYATRI, SBP_DEVOTEE . '.id=' . SBP_PADYATRI . '.devotee_id');
        $this->db->where(SBP_PADYATRI . '.participation_year_id', $year);
        $this->db->where('LOWER(' . SBP_PADYATRI . '.payment_status)', 'success');
        $devotee_info = $this->db->get()->result_array();
        if (isset($dindi_list, $devotee_info) && !empty($dindi_list) && !empty($devotee_info)) {
            foreach ($dindi_list as $key => $dindi) {
                $male_cnt = $female_cnt = 0;
                foreach ($devotee_info as $devotee) {
                    if ($devotee['dindi_id'] == $dindi['id']) {
                        if ($devotee['gender'] == "Male") {
                            $male_cnt++;
                        } elseif ($devotee['gender'] == "Female") {
                            $female_cnt++;
                        }
                    }
                }
                $dindi_list[$key]['male_count'] = $male_cnt;
                $dindi_list[$key]['female_count'] = $female_cnt;
            }
        }
        return $dindi_list;
    }

    public function dindi_count() {
        $result = $this->db->select('count(id) as totaldindi')->get_where($this->_table, array('isactive' => 1))->result_array();
        return $result;
    }

    public function get_dindiwise_padyatriReporty($dindi_id, $year) {
        $this->db->select('tbl_dt.firstname,tbl_dt.middlename,tbl_dt.lastname,tbl_dt.mobile,tbl_dt.gender,'
                . 'tbl_dt.blood_group,tbl_dl.name,tbl_dl.description,tbl_dl.contact_person_name,'
                . 'tbl_dl.contact_person_mobile');
        $this->db->from(SBP_DEVOTEE . ' tbl_dt');
        $this->db->join(SBP_PADYATRI . ' tbl_py', 'tbl_py.devotee_id=tbl_dt.id');
        $this->db->join(SBP_DINDI_LIST . ' tbl_dl', 'tbl_py.dindi_id=tbl_dl.id');
        $this->db->where('tbl_py.participation_year_id', $year);
        $this->db->where('tbl_dl.id', $dindi_id);
        $result = $this->db->get()->result_array();
        return $result;
    }

}

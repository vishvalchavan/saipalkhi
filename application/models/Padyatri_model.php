<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Padyatri_model extends MY_Model {

    //put your code here

    public $_table = SBP_PADYATRI;
    public $primary_key = 'id';
    public $where_not_grpid_is_one = "tbl_dt.group_id !='1'";

    public function get_padyatri_by_id($padyatriid) {
        $this->db->select('tbl_dt.id,
                                tbl_dt.email,
                                tbl_dt.dpn_no,
                                tbl_dt.firstname,
                                tbl_dt.middlename,
                                tbl_dt.lastname,
                                tbl_dt.date_of_joining,
                                tbl_dt.birth_date,
                                tbl_dt.mobile,
                                tbl_dt.mobile1,
                                tbl_dt.address,
                                tbl_dt.city,
                                tbl_dt.profession,
                                tbl_dt.education,
                                tbl_dt.gender,                                
                                tbl_dt.identity_prof,tbl_dt.relative_name,tbl_dt.relative_mobile,
                                tbl_dt.adhaar_no,tbl_dt.pincode,                                
                                tbl_py.id as tbl_py_id,                                
                                tbl_py.devotee_id,
                                tbl_py.participation_year_id,
                                tbl_py.vehical_type,
                                tbl_py.vehicle_details,
                                tbl_py.dindi_id,
                                tbl_py.payment_date,
                                tbl_py.payment_mode,                                
                                tbl_py.payment_status,
                                tbl_dl.id as tbl_dl_id,tbl_dl.name as tbl_dl_name,tbl_ssm.id as tbl_ssm_id,tbl_ssm.name as tbl_ssm_name');
        $this->db->from(SBP_DEVOTEE . ' tbl_dt');
        $this->db->join(SBP_PADYATRI . ' tbl_py', 'tbl_py.devotee_id=tbl_dt.id', 'inner');
        $this->db->join(SBP_DINDI_LIST . ' tbl_dl', 'tbl_py.dindi_id=tbl_dl.id', 'left');
        $this->db->join('tbl_seva_samiti_master tbl_ssm', 'tbl_py.seva_samiti_id=tbl_ssm.id', 'left');
        $this->db->where('tbl_py.id', $padyatriid);
        $this->db->where($this->where_not_grpid_is_one);
        $query = $this->db->get()->result_array();

        return $query;
    }

    public function get_allpadyatri($year_id) {
        $this->db->select('tbl_dt.id,tbl_dt.dpn_no,tbl_dt.firstname,tbl_dt.middlename,tbl_dt.lastname,tbl_dt.mobile,
                            tbl_dt.blood_group,tbl_dt.birth_date,tbl_dt.user_image_path,tbl_dt.address,tbl_dt.city,        
                            tbl_dt.pincode,tbl_dt.relative_name,tbl_dt.relative_mobile,tbl_py.id AS tbl_py_id,
                            tbl_py.payment_date,tbl_py.payment_mode,tbl_py.payment_amount,tbl_py.payment_status,
                            tbl_py.badget_print,tbl_py.receipt_print,tbl_py.dindi_id,tbl_dl.id AS tbl_dl_id,
                            tbl_dl.name AS tbl_dl_name');
        $this->db->from(SBP_DEVOTEE . ' tbl_dt');
        $this->db->join(SBP_PADYATRI . ' tbl_py', 'tbl_py.devotee_id=tbl_dt.id', 'left');
        $this->db->join(SBP_DINDI_LIST . ' tbl_dl', 'tbl_py.dindi_id=tbl_dl.id', 'left');
        $this->db->join('tbl_year', 'tbl_year.id=tbl_py.participation_year_id', 'inner');
        $this->db->where(array('tbl_dt.isactive' => 1, 'LOWER(tbl_py.payment_status)' => 'success'));
        if ($year_id != "all") {
            $this->db->where('tbl_py.participation_year_id', $year_id);
        }
        $this->db->where($this->where_not_grpid_is_one);
        $query = $this->db->get()->result_array();
//        echo $this->db->last_query();
//        die;
        return $query;
    }

//        ------------------update divotee and padyatri---------------------------------------------------------------------
    public function update_divtotee_and_padyatri($padyatriid, $padyatridata, $devoteedata, $truckid, $yearid) {
        $vehicle_row = array();
        $seatcount = "truck_capacity > assigned_count";
        if ($padyatridata['vehical_type'] == "Self") {
            $padyatridata['vehicle_details'] = '';
            $assigned_count = 'assigned_count-1';
        } else {
            $vehicleQuery = $this->db->select('id,truck_name,truck_number')
                    ->from(SBP_TRUCK_DETAILS)
                    ->where('isactive', 1)
                    ->where($seatcount)
                    ->where('participation_year_id', $yearid)
                    ->Order_by('id', 'asc')
                    ->limit('1')
                    ->get();
            $vehicle_row = $vehicleQuery->row();

            if ($vehicleQuery->num_rows() > 0) {

                $padyatridata['vehicle_details'] = $vehicle_row->truck_name;
                $truckid = ($truckid) ? $truckid : $vehicle_row->truck_name;
                $assigned_count = 'assigned_count+1';
            }
        }

        $this->db->where('id', $devoteedata['id']);
        $devotee_query = $this->db->update(SBP_DEVOTEE, $devoteedata);
        if ($devotee_query) {
            $vehicletypeQuery = $this->db->select('vehical_type')->from(SBP_PADYATRI)->where('id', $padyatriid)->get()->row();
            $this->db->where('id', $padyatriid);
            $devotee_query = $this->db->update(SBP_PADYATRI, $padyatridata);
            if ($devotee_query) {

                if ($vehicletypeQuery->vehical_type != $padyatridata['vehical_type']) {
                    $this->db->set('assigned_count', $assigned_count, FALSE)->where(array('truck_name' => $truckid, 'participation_year_id' => $yearid))->update(SBP_TRUCK_DETAILS);
                }
            }
        }
        return $devotee_query;
    }

}

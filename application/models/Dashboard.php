<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Dashboard extends MY_Model {

    //put your code here

    public $_table = 'tbl_dashboard_data';
    public $primary_key = 'id';

    public function get_dashboardInfo() {
        $this->db->select('*');
        $this->db->from($this->_table);
        $result = $this->db->get()->result();

        if (isset($result) && !empty($result)) {
            $data['dashboard_year_data'] = array();
            $data['dashboard_data'] = array();
            foreach ($result as $val) {
                $sum_devotee[] = isset($val->total_devotees) ? $val->total_devotees : 0;
                $sum_padyatri[] = isset($val->total_padyatri) ? $val->total_padyatri : 0;
                $sum_web[] = isset($val->registered_via_web) ? $val->registered_via_web : 0;
                $sum_app[] = isset($val->payment_via_app) ? $val->payment_via_app : 0;
                $sum_cash[] = isset($val->cash_cheque) ? $val->cash_cheque : 0;
                $sum_donor[] = isset($val->total_donor) ? $val->total_donor : 0;
                $sum_donation[] = isset($val->total_donation) ? $val->total_donation : 0;
                if ($val->enroll_year == date("Y")) {
                    $data['dashboard_year_data'] = $val;
                }
            }
            $data['dashboard_data']['sum_devotee'] = isset($sum_devotee) ? array_sum($sum_devotee) : 0;
            $data['dashboard_data']['sum_padyatri'] = isset($sum_padyatri) ? array_sum($sum_padyatri) : 0;
            $data['dashboard_data']['sum_web'] = isset($sum_web) ? array_sum($sum_web) : 0;
            $data['dashboard_data']['sum_app'] = isset($sum_app) ? array_sum($sum_app) : 0;
            $data['dashboard_data']['sum_cash'] = isset($sum_cash) ? array_sum($sum_cash) : 0;
            $data['dashboard_data']['sum_donor'] = isset($sum_donor) ? array_sum($sum_donor) : 0;
            $data['dashboard_data']['sum_donation'] = isset($sum_donation) ? array_sum($sum_donation) : 0;
        }

        if (isset($data) && !empty($data)) {
            return $data;
        } else {
            return false;
        }
    }

    public function get_dashboard_year_data($year) {
        $this->db->select('*');
        $this->db->from($this->_table);
        $this->db->where('enroll_year', $year);
        $result = $this->db->get()->result();
        return $result;
    }

}

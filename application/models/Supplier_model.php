<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Supplier_model extends MY_Model {

    //put your code here

    public $_table = 'tbl_supplier_details';
    public $primary_key = 'id';

    public function get_resource_list($samiti_id) {
        $this->db->select('sd.id,sd.supplier_name,sd.resource_name,sd.resource_mobile,sd.blood_group,sd.seva_samiti_id,sd.isactive');
        $this->db->from('tbl_supplier_details sd');
        $this->db->where('seva_samiti_id', $samiti_id);
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function if_resource_exists($resource_name, $supplier_name) {
        $this->db->select('id');
        $this->db->where(array('supplier_name' => $supplier_name, 'resource_name' => $resource_name));
        $result = $this->db->get('tbl_supplier_details')->result();
        return $result;
    }

    public function get_allData($resource_id) {
        $this->db->select('id,supplier_name,blood_group,resource_name,resource_mobile,seva_samiti_id,isactive');
        $this->db->from('tbl_supplier_details');
        $this->db->where(array('id' => $resource_id, 'isactive' => '1'));
        $result = $this->db->get()->result_array();
        if (isset($result)) {
            return $result[0];
        }
    }

    public function update_supplierCount($year) {
        $dashboardData = $this->db->get_where('tbl_dashboard_data', array('enroll_year' => $year))->result();
        if (count($dashboardData) > 0) {
            $supplier_count = $dashboardData[0]->total_resources + 1;
            $data = array(
                'total_resources' => $supplier_count
            );
            $this->db->where('id', $dashboardData[0]->id);
            $this->db->update('tbl_dashboard_data', $data);
        } else {
            $data = array(
                'total_resources' => 1,
                'enroll_year' => date('Y')
            );
            $this->db->insert('tbl_dashboard_data', $data);
        }
    }

    public function delete_supplierCount($year) {
        $dashboardData = $this->db->get_where('tbl_dashboard_data', array('enroll_year' => $year))->result();
        if (count($dashboardData) > 0) {
            $supplier_count = $dashboardData[0]->total_resources - 1;
            $data = array(
                'total_resources' => $supplier_count
            );
            $this->db->where('id', $dashboardData[0]->id);
            $this->db->update('tbl_dashboard_data', $data);
        }
    }

}

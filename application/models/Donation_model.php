<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Donation_model extends MY_Model {

    //put your code here

    public $_table = 'tbl_donation';
    public $primary_key = 'id';

    public function add_donation($donor_info) {
        if (isset($donor_info) && !empty($donor_info)) {
            $this->db->insert($this->_table, $donor_info);
            $donor_id = $this->db->insert_id();
            $this->update_donor_count(date("Y"), $donor_info['donation_amount']);
            return $donor_id;
        }
    }

    public function update_donation($donor, $donation_amount, $update_info) {
        if (isset($donor, $update_info)) {
            if ($this->db->update($this->_table, $update_info)) {
                $this->update_donor_count(date("Y"), $donation_amount);
                return true;
            } else {
                return false;
            }
        }
    }

    public function update_donor_count($year, $amt) {
        $donor_count = 'total_donor+1';
        $donor_amt = 'total_donation+' . $amt;
        $this->db->set('total_donor', $donor_count, FALSE);
        $this->db->set('total_donation', $donor_amt, FALSE);
        $this->db->where('tbl_dashboard_data.enroll_year', $year);
        $this->db->update('tbl_dashboard_data');
    }

    public function country_list() {
        $result = $this->db->select('id,name')->where('isactive', 1)->order_by('name', 'asc')->get('tbl_country')->result_array();
        return $result;
    }

    public function get_state($country) {
        $result = $this->db->select('id,name')->where(array('country_id' => $country, 'isactive' => 1))->order_by('name', 'asc')->get('tbl_state')->result_array();
        return $result;
    }

    public function get_city($state) {
        $result = $this->db->select('id,name')->where(array('state_id' => $state, 'isactive' => 1))->order_by('name', 'asc')->get('tbl_city')->result_array();
        return $result;
    }

    public function get_year_id($year) {
        $this->db->select('id');
        $this->db->where('year', $year);
        $year_id = $this->db->get('tbl_year')->result_array();
        if (isset($year_id) && !empty($year_id)) {
            return $year_id[0]['id'];
        } else {
            return false;
        }
    }

    public function get_receipt_no() {
        $this->db->select('donor_id');
        $this->db->where('donation_status', 'Success');
        $receipt_data = $this->db->get('tbl_donation')->result_array();
        return isset($receipt_data) ? count($receipt_data) : 0;
    }

    public function getDevotee($devoteeid) {
        $this->db->select('id,email,mobile,address,city,pincode,firstname,middlename,lastname');
        $result = $this->db->get_where(SBP_DEVOTEE, array('id' => $devoteeid))->result_array();
        return $result;
    }

    public function donor_list($year = NULL) {
        $this->db->select('dt.firstname,dt.middlename,dt.lastname,dt.dpn_no,dt.email,dt.mobile,donation_for,donation_amount,note,donation_date,receipt_no,transaction_id');
        $this->db->join(SBP_DEVOTEE . ' as dt', 'tbl_donation.devotee_id=dt.id');
        $this->db->where('tbl_donation.donation_status', 'success');
        if ($year !== NULL) {
            $this->db->where('tbl_donation.donated_year', $year);
        }
        $result = $this->db->get('tbl_donation')->result_array();
        return $result;
    }

}

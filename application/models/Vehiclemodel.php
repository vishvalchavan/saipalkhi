<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Vehiclemodel extends MY_Model {

    //put your code here

    public $_table = SBP_TRUCK_DETAILS;
    public $primary_key = 'id';

    public function get_vehicle_list($year) {
        $this->db->select('td.id,td.truck_name,td.truck_number,td.driver_name,td.driver_mobile,td.participation_year_id,td.truck_capacity,yr.year,td.created_date,td.isactive');
        $this->db->from(SBP_TRUCK_DETAILS . ' as td');
        $this->db->join('tbl_year yr', 'td.participation_year_id=yr.id');
        $this->db->where(array('yr.id' => $year));
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function get_vehicle($year) {
        $this->db->select('td.id');
        $this->db->from(SBP_TRUCK_DETAILS . ' as td');
        $this->db->join('tbl_year yr', 'td.participation_year_id=yr.id');
        $this->db->where(array('yr.id' => $year));
        $result = $this->db->get()->result_array();
        return count($result) + 1;
    }

    public function if_vehicle_exists($truck_name, $truck_number, $year) {
        $this->db->select('id');
        $this->db->where(array('truck_name' => $truck_name, 'truck_number' => $truck_number, 'participation_year_id' => $year));
        $result = $this->db->get(SBP_TRUCK_DETAILS)->result();
        return $result;
    }

    public function get_allData($truck_id) {
        $this->db->select('id,truck_name,truck_number,driver_name,driver_mobile,participation_year_id,truck_capacity,created_date,isactive');
        $this->db->from(SBP_TRUCK_DETAILS);
        $this->db->where(array('id' => $truck_id));
        $result = $this->db->get()->result_array();
        if (isset($result)) {
            return $result[0];
        }
    }

    public function delete_vehicle($truck_id) {
        $update = array(
            'isactive' => 0
        );
        $this->db->where('id', $truck_id);
        if ($this->db->update(SBP_TRUCK_DETAILS, $update)) {
            return true;
        } else {
            return false;
        }
    }

    public function activate_vehicle($truck_id) {
        $update = array(
            'isactive' => 1
        );
        $this->db->where('id', $truck_id);
        if ($this->db->update(SBP_TRUCK_DETAILS, $update)) {
            return true;
        } else {
            return false;
        }
    }

}

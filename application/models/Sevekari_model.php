<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Sevekari_model
 *
 * @author ADMIN
 */
class Sevekari_model extends MY_Model {

    //put your code here
    public $_table = SBP_SEVEKARI_DETAILS;
    public $primary_key = 'id';

    public function get_willing_sevekari($samiti_id) {
        $this->db->select('dt.id,dt.firstname,dt.middlename,dt.lastname,dt.mobile,pd.id as padyatriid,sd.ishead,dt.isactive');
        $this->db->from(SBP_DEVOTEE.' as dt');
        $this->db->join(SBP_PADYATRI.' pd', 'dt.id=pd.devotee_id');
        $this->db->join('tbl_seva_samiti_master sm', 'pd.seva_samiti_id=sm.id');
        $this->db->join(SBP_SEVEKARI_DETAILS.' as sd', 'sd.padyatri_no=pd.id', 'left');
        $this->db->where(array('pd.seva_samiti_id' => $samiti_id));
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function get_sevekari_list($samiti_id) {
        $this->db->select('sd.id,sd.padyatri_no,sd.ishead,sd.isactive');
        $this->db->from(SBP_SEVEKARI_DETAILS.' as sd');
        $this->db->where('sd.seva_samiti_id', $samiti_id);
        $this->db->where('sd.isactive', 1);
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function if_sevekari_exists($samiti_id, $padyatri) {
        $this->db->select('id');
        $this->db->where(array('padyatri_no' => $padyatri, 'seva_samiti_id' => $samiti_id));
        $response = $this->db->get($this->_table)->result_array();
        return $response;
    }

    public function delete_sevekari($sevekari_ids, $samiti_id) {
        if (isset($sevekari_ids)) {
            foreach ($sevekari_ids as $id) {
                $this->db->where(array('padyatri_no' => $id, 'seva_samiti_id' => $samiti_id));
                $this->db->delete($this->_table);
            }
        }
    }

    public function update_sevekariCount($count, $year) {
        $dashboardData = $this->db->get_where('tbl_dashboard_data', array('enroll_year' => $year))->result();
        if (count($dashboardData) > 0) {
            $sevekari_count = $dashboardData[0]->total_sevekari + $count;
            $data = array(
                'total_sevekari' => $sevekari_count
            );
            $this->db->where('id', $dashboardData[0]->id);
            $this->db->update('tbl_dashboard_data', $data);
        }
    }

    public function delete_sevekariCount($count, $year) {
        $dashboardData = $this->db->get_where('tbl_dashboard_data', array('enroll_year' => $year))->result();
        if (count($dashboardData) > 0) {
            $sevekari_count = $dashboardData[0]->total_sevekari - $count;
            $data = array(
                'total_sevekari' => $sevekari_count
            );
            $this->db->where('id', $dashboardData[0]->id);
            $this->db->update('tbl_dashboard_data', $data);
        }
    }

    public function sevekari_details($samiti_id) {
        $this->db->select('dt.id,dt.firstname,dt.middlename,dt.lastname,dt.mobile,pd.id as padyatriid,dt.isactive,sd.ishead,sm.id as samitiid');
        $this->db->from(SBP_DEVOTEE.' as dt');
        $this->db->join(SBP_PADYATRI.' pd', 'dt.id=pd.devotee_id');
        $this->db->join('tbl_seva_samiti_master sm', 'pd.seva_samiti_id=sm.id');
        $this->db->join(SBP_SEVEKARI_DETAILS.' as sd', 'sd.padyatri_no=pd.id');
        $this->db->where('pd.seva_samiti_id', $samiti_id);
        $this->db->where('sd.isactive', 1);
        $result = $this->db->get()->result_array();
        return $result;
    }

}

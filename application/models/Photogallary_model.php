<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Photogallary_model extends MY_Model {

    //put your code here

    public $_table = SBP_EVENT_MASTER;
    public $primary_key = 'id';
    public $where_not_grpid_is_one = "tbl_dt.group_id !='1'";

    
        public function add_update_album_title($album_name,$album_desc,$year,$album_id =NULL) {            
//             id,event_name,description,modified_date
            if(empty($album_id)){
             $album_data=array('event_name'=>$album_name,
                                'description'=>$album_desc,
                                'year_id'=>$year,                                
                                'publish_status'=>'0',
                                'created_date'=>date("Y-m-d"),                                
                                'isactive'=>'1');
            $inserted=$this->db->insert($this->_table, $album_data);
            $inserted=($inserted)? $this->db->insert_id():0;            
            } else {
              $album_data=array('event_name'=>$album_name,
                                'description'=>$album_desc,
                                'year_id'=>$year,                                
                                'modified_date'=>date("Y-m-d")
                                ); 
                $this->db->where(array('id'=>$album_id,'year_id'=>$year));
                $inserted = $this->db->update($this->_table, $album_data);
                $inserted=($inserted)?$album_id:$album_id;
            }
            return $inserted;
        }
        
        public function add_album_details($album_id,$album_data,$counter,$decoded_img_data) { 
            $inserted='';
            foreach ($album_data as $key => $album_data_value) {            
                 $this->db->insert(SBP_EVENT_DETAILS, $album_data_value);
                 $inserted=$this->db->insert_id();
                    if(!empty($decoded_img_data[$key]['decoded_img'])){
                    file_put_contents($album_data_value['img_path'], $decoded_img_data[$key]['decoded_img']);
                    }
            }
            return ($inserted>0)? $this->db->insert_id():0;
        }
        
        public function if_album_exists($album_name, $year ) {            
        $this->db->select('id');
        $this->db->where(array('event_name' => $album_name, 'year_id' => $year));
        $result = $this->db->get($this->_table)->row();  
//        echo $this->db->last_query();die;
        return ($result)? $result->id:0;
        }
        
        public function if_image_exists($album_name, $year) {             
        $this->db->select(SBP_EVENT_DETAILS.'.id');
        $this->db->join(SBP_EVENT_MASTER,SBP_EVENT_MASTER.'.id='.SBP_EVENT_DETAILS.'.event_id','inner');    
        $this->db->where(array(SBP_EVENT_DETAILS.'.img_description' => $album_name, SBP_EVENT_MASTER.'.year_id' => $year));
        $result = $this->db->get(SBP_EVENT_DETAILS)->num_rows();  
//        echo $this->db->last_query();
        return $result;
//        exit;
        }
//--------------------------get all album with cover---------------------------------------------------------------------        
        public function get_all_albums($year_id)
        {
            $this->load->library('subquery');
                $user_type = $this->session->userdata('user_type');
                $this->db->select(SBP_EVENT_MASTER.'.id,'.SBP_EVENT_MASTER.'.event_name,'.SBP_EVENT_MASTER.'.publish_status,'.SBP_EVENT_MASTER.'.description,
                                   '.SBP_EVENT_DETAILS.'.id as `tbl_event_det_id`,'.SBP_EVENT_DETAILS.'.img_path,'.SBP_EVENT_DETAILS.'.iscover_pic as imagecover');
//---------------------sub query start-----------------------------------------------------------------------------------
                $sub = $this->subquery->start_subquery('select');
                $sub->select('count('.SBP_EVENT_DETAILS.'.event_id)')->from(SBP_EVENT_DETAILS);
                $sub->where(SBP_EVENT_DETAILS.'.event_id = '.SBP_EVENT_MASTER.'.id');                
                $sub->where(SBP_EVENT_MASTER.'.year_id',$year_id);                
                $this->subquery->end_subquery('total_album_phts');
/////- second sub query for iscover image status----------------------------------
                $sub1 = $this->subquery->start_subquery('select');
                $sub1->select(SBP_EVENT_DETAILS.'.iscover_pic')->from(SBP_EVENT_DETAILS);
                $sub1->where(SBP_EVENT_DETAILS.'.event_id = '.SBP_EVENT_MASTER.'.id');
                $sub1->where(SBP_EVENT_MASTER.'.year_id',$year_id);
                $sub1->where(SBP_EVENT_DETAILS.'.iscover_pic',1);
                $sub1->limit(1);
                $this->subquery->end_subquery('iscover_pic');                
/////- third sub query for iscover image path----------------------------------
                $sub1 = $this->subquery->start_subquery('select');
                $sub1->select(SBP_EVENT_DETAILS.'.img_path')->from(SBP_EVENT_DETAILS);
                $sub1->where(SBP_EVENT_DETAILS.'.event_id = '.SBP_EVENT_MASTER.'.id');
                $sub1->where(SBP_EVENT_MASTER.'.year_id',$year_id);
                $sub1->where(SBP_EVENT_DETAILS.'.iscover_pic',1);
                $sub1->limit(1);
                $this->subquery->end_subquery('cover_image');                
//---------------------sub query                 
                $this->db->from(SBP_EVENT_MASTER);
                $this->db->join(SBP_EVENT_DETAILS,SBP_EVENT_MASTER.'.id='.SBP_EVENT_DETAILS.'.event_id','left');
                $this->db->where(array(SBP_EVENT_MASTER.'.isactive'=> 1,SBP_EVENT_MASTER.'.year_id'=>$year_id));
//                $this->db->where(array(SBP_EVENT_DETAILS.'.iscover_pic'=> 1));
                if($user_type != 'admin'){
                $this->db->where(SBP_EVENT_MASTER.'.publish_status',1);    
                }
                $this->db->group_by(SBP_EVENT_MASTER.'.id');
                $this->db->order_by(SBP_EVENT_MASTER.'.id','desc');
//--------------------------GET MASTER WISE AVAILABLE COVER PAGE---------------------------------------------------------
                $result=$this->db->get()->result_array();
// echo $this->db->last_query();die;  
                    foreach ($result as $key1 => $value) {
//                        if($value['iscover_status'] == 1){
                             if(file_exists($value['cover_image'])){
                                $result[$key1]['cover_image']=$value['cover_image'];                                 
                             }else{
                                $Sqlcover=$this->db->select(SBP_EVENT_MASTER.'.id,'.SBP_EVENT_DETAILS.'.img_path,'.SBP_EVENT_DETAILS.'.iscover_pic');
                                $this->db->from(SBP_EVENT_MASTER);
                                $this->db->join(SBP_EVENT_DETAILS,SBP_EVENT_MASTER.'.id='.SBP_EVENT_DETAILS.'.event_id','left');
                                $this->db->where(array(SBP_EVENT_MASTER.'.year_id'=>$year_id));
                                $this->db->where(SBP_EVENT_DETAILS.'.event_id',$value['id']);
                                $this->db->where(SBP_EVENT_DETAILS.'.id!=',$value['tbl_event_det_id']);                                  
//                                  $this->db->group_by(SBP_EVENT_MASTER.'.id');                                                                 
                                $SqlcoverRes=$this->db->get()->result();
                                
                                 foreach($SqlcoverRes as $SqlcoverRow) {
//                                if($SqlcoverRow->iscover_pic == 0){
                                        if(file_exists($SqlcoverRow->img_path)){                                            
                                            $result[$key1]['cover_image']=$SqlcoverRow->img_path;;
                                        }
                                    }
                             }
                    }        
                
//--------------------------END GET MASTER WISE AVAILABLE COVER PAGE---------------------------------------------------------                
            return $result;   
        }
//--------------------------get all album with cover---------------------------------------------------------------------        
        public function get_albumdetails_by_albumid($album_id,$year_id)
        {
            $this->load->library('subquery');
            $user_type = $this->session->userdata('user_type');
                $this->db->select(SBP_EVENT_MASTER.'.id,'.SBP_EVENT_MASTER.'.event_name,
                                    '.SBP_EVENT_MASTER.'.description,'.SBP_EVENT_MASTER.'.publish_by,'.SBP_EVENT_MASTER.'.publish_status,
                                    '.SBP_EVENT_MASTER.'.isactive,'.SBP_EVENT_DETAILS.'.id as tbl_event_detail_id,
                                    '.SBP_EVENT_DETAILS.'.event_id,'.SBP_EVENT_DETAILS.'.img_path,'.SBP_EVENT_DETAILS.'.iscover_pic,'.SBP_EVENT_DETAILS.'.img_description');                            
                $this->db->from(SBP_EVENT_MASTER);
                $this->db->join(SBP_EVENT_DETAILS,SBP_EVENT_MASTER.'.id='.SBP_EVENT_DETAILS.'.event_id','left');
                $this->db->where(array(SBP_EVENT_MASTER.'.isactive'=> 1,SBP_EVENT_MASTER.'.year_id'=>$year_id,SBP_EVENT_MASTER.'.id'=>$album_id));                 
             if($user_type != 'admin'){
               $this->db->where(SBP_EVENT_MASTER.'.publish_status',1);  
             }   
                $result=$this->db->get()->result_array();
//                echo $this->db->last_query();die;
            return $result;   
        }
        
//        ------------------update divotee and padyatri---------------------------------------------------------------------
        public function update_divtotee_and_padyatri($padyatriid,$padyatridata,$devoteedata,$truckid,$yearid) {
            $seatcount="truck_capacity > assigned_count";
           if($padyatridata['vehical_type'] == "Self")
           {
               $padyatridata['vehicle_details']='';
               $assigned_count='assigned_count-1';
           }else
           {
                    $vehicleQuery=  $this->db->select('id,truck_name,truck_number')
                                             ->from(SBP_TRUCK_DETAILS)
                                             ->where('isactive',1)
                                             ->where($seatcount)
                                             ->where('participation_year_id',$yearid)
                                             ->Order_by('id','asc')
                                             ->limit('1')
                                             ->get()->row();
                    if($vehicleQuery > 0)
                    {
                    $padyatridata['vehicle_details']=$vehicleQuery->id;
                    $truckid = ($truckid)?$truckid:$vehicleQuery->id;
                    $assigned_count='assigned_count+1';
                    }
           }
            $this->db->where('id', $devoteedata['id']);
            $devotee_query=$this->db->update(SBP_DEVOTEE, $devoteedata);
            if($devotee_query)
            {
                $vehicletypeQuery=$this->db->select('vehical_type')->from(SBP_PADYATRI)->where('id',$padyatriid)->get()->row(); 
                $this->db->where('id', $padyatriid);
                $devotee_query=$this->db->update(SBP_PADYATRI, $padyatridata);
                if($devotee_query){
                   
                 if($vehicletypeQuery->vehical_type != $padyatridata['vehical_type']){
                $this->db->set('assigned_count', $assigned_count,FALSE)->where(array('id'=>$truckid,'participation_year_id'=>$yearid))->update(SBP_TRUCK_DETAILS);                 
                } }
            }
            return $devotee_query;

    }
    
//    ---------------------- update old image title and cover ===================================
    function change_img_details($img_id,$title,$action)
    {        
        $url=base_url().$title;
            if($action == 'update'){
                $updatedata=array('img_description'=>$title);
            }else if($action == 'cover'){
                $updatedata=array('iscover_pic'=>'1');
                $this->db->set('iscover_pic','0')->where('event_id',$title)->update(SBP_EVENT_DETAILS);
            }
            $this->db->where('id',$img_id);
                if($action != 'remove'){
                    $update_query=$this->db->update(SBP_EVENT_DETAILS,$updatedata);
                    $returnMsg=array('success' =>'Image successfully updated.','returnstatus'=>'1');
                }else
                {
                    $getlink=$this->db->get_where(SBP_EVENT_DETAILS, array('id' => $img_id))->row();                    
//                    var_dump($getlink->img_path);die;
                    @unlink($getlink->img_path);
                    $this->db->where('id',$img_id);
                    $update_query=$this->db->delete(SBP_EVENT_DETAILS);
                    $returnMsg=array('success' =>'Image removed.','returnstatus'=>'1');
                }
//                die;
                return $returnMsg;
        //exit;
    }
    function change_publish_status($album_id,$status)
    {   
        $Updatestatus=$this->db->set('publish_status',$status)->where('id',$album_id)->update($this->_table);              
        $getUpdateId=$this->db->where('id',$album_id)->limit('1')->get($this->_table)->row();
        return $getUpdateId=($getUpdateId)?$getUpdateId->id:0;
    }
    
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Language extends MY_Model {

    //put your code here
    public $_table = 'language';
    public $primary_key = 'id';

    public function get_allLanguage() {
        $this->db->select('id,language');
        $this->db->from('language');
        $this->db->where(array('isactive' => 1, 'isdeleted' => 0));
        $query = $this->db->get();
        return $query->result_array();
    }

}

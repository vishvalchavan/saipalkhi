<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Year extends MY_Model {

    //put your code here

    public $_table = 'tbl_year';
    public $primary_key = 'id';

    public function get_year_id($year) {
        $this->db->select('id');
        $this->db->where('year', $year);
        $year_id = $this->db->get($this->_table)->result_array();
        if (isset($year_id) && !empty($year_id)) {
            return $year_id[0]['id'];
        } else {
            return false;
        }
    }

    public function get_year_list() {
        $this->db->select('id,year,isactive');
        $this->db->where('isactive', 1);
        $result = $this->db->get($this->_table)->result_array();
        return $result;
    }

    public function get_year_by_id($year_id) {
        $this->db->select('year');
        $this->db->where('id', $year_id);
        $year = $this->db->get($this->_table)->result_array();
        if (isset($year) && !empty($year)) {
            return $year[0]['year'];
        } else {
            return false;
        }
    }

}

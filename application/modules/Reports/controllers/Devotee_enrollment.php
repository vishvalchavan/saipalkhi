<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Devotee_enrollment extends MY_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            redirect('admin', 'refresh');
        }
        if (!$this->ion_auth->is_admin()) {
            redirect('admin', 'refresh');
        }
        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->model(array('Year', 'devotee', 'language'));
        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('Auth');
    }

    public function index() {
        $data['devotee_enroll_list'] = $this->devotee->get_tab_user_work();
        $data['tab_user'] = $this->devotee->get_tab_users();
        $data['dataHeader']['title'] = 'Tab User Report';
        $this->template->set_master_template('template.php');
        $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
        $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
        $this->template->write_view('content', 'devotee_enrollment', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'snippets/footer', '', TRUE);
        $this->template->render();
    }

    public function ajx_devotee_enrollment() {
        if ($this->input->post('userid')) {
            $data['devotee_enroll_list'] = $this->devotee->get_tab_user_work($this->input->post('userid'));
            $view = $this->load->view('ajx_devoteeEnrollment', $data);
            echo $view;
        }
    }

}

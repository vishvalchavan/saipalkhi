<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dindi_report extends MY_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            redirect('admin', 'refresh');
        }
        if (!$this->ion_auth->is_admin()) {
            redirect('admin', 'refresh');
        }
        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->model(array('DindiModel', 'Year', 'devotee', 'language'));
        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('Auth');
    }

    public function index() {
        $year = $this->Year->get_year_id(date('Y'));
        $data['dindi_list'] = $this->DindiModel->get_dindi_report($year);
        $data['year_list'] = $this->Year->get_year_list();
        $data['dataHeader']['title'] = 'Dindi Report';
        $this->template->set_master_template('template.php');
        $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
        $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
        $this->template->write_view('content', 'dindi_report', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'snippets/footer', '', TRUE);
        $this->template->render();
    }

    public function ajx_dindireport() {
        if ($this->input->post('year')) {
            $data['dindi_list'] = $this->DindiModel->get_dindi_report($this->input->post('year'));
            $view = $this->load->view('ajx_dindiReport', $data);
            echo $view;
        }
    }

    public function DindiWisePadyatriReport($dindi_id) {
        if (isset($dindi_id) && !empty($dindi_id)) {
            $year = $this->Year->get_year_id(date('Y'));
            $data['padyatri_data'] = $this->DindiModel->get_dindiwise_padyatriReporty($dindi_id, $year);
            if (isset($data['padyatri_data']) && !empty($data['padyatri_data'])) {
                $data['dataHeader']['title'] = $data['padyatri_data'][0]['name'] . " Report";
            } else {
                $dindi = $this->DindiModel->get_dindi_name($dindi_id);
                $data['dataHeader']['title'] = $dindi[0]->name . " Report";
            }
            $this->template->set_master_template('template.php');
            $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
            $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
            $this->template->write_view('content', 'dindi_wise_padyatriReport', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'snippets/footer', '', TRUE);
            $this->template->render();
        }
    }

}

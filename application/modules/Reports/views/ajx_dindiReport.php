<table id="dindi_report" class=" w-100 dt-bootstrap4  display nowrap table table-hover table-striped table-bordered dataTable datatable" cellspacing="0" role="grid" aria-describedby="example23_info">
    <thead>
        <tr role="row">
            <th>Dindi Name</th>
            <th>Dindi Head</th>
            <th>Contact Number</th>
            <th>No Of Males</th>
            <th>No Of Females</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (isset($dindi_list) && !empty($dindi_list)) {
            foreach ($dindi_list as $dindi) {
                ?>
                <tr>
                    <td><?php echo isset($dindi['name']) ? $dindi['name'] : ''; ?></td>
                    <td><?php echo isset($dindi['contact_person_name']) ? $dindi['contact_person_name'] : ''; ?></td>
                    <td><?php echo isset($dindi['contact_person_mobile']) ? $dindi['contact_person_mobile'] : ''; ?></td>
                    <td><?php echo isset($dindi['male_count']) ? $dindi['male_count'] : 0; ?></td>
                    <td><?php echo isset($dindi['female_count']) ? $dindi['female_count'] : 0; ?></td>
                </tr>                                            
                <?php
            }
        }
        ?>
    </tbody>
</table>
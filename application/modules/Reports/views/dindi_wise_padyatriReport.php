<style>
    .form-control{
        color:black !important;
    }
</style>
<div class="row">
    <div class="col-12">
        <div class="card min-height80">
            <div class="card-body">     
                <div class="row">
                    <div class="col-md-5 align-self-center col-6">
                        <h3 class="card-title">Dindi Wise Padyatri Report</h3>
                    </div>
                    <div class="col-md-7 align-self-center text-right col-6">
                        <a href="<?php echo base_url(); ?>admin/DindiReport">  <button type="button" class="btn btn-info add-button"><i class="fas fa-arrow-left"></i> &nbsp; &nbsp; Back to Dindi Report</button></a>
                    </div>
                </div>
                <hr>
                <div class="row mx-0">
                    <div class="col-12">
                        <form class="form-horizontal p-t-20">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Dindi Name:</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control" id="samitiName" placeholder="Samiti Name" value="<?php echo isset($padyatri_data[0]['name']) ? $padyatri_data[0]['name'] : '' ?>" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group required row" aria-required="true">
                                        <label class="control-label text-left col-md-4">Description:</label>
                                        <div class="col-md-7">
                                            <textarea class="form-control" colspan="8" rows="3" id="comment" readonly><?php echo isset($padyatri_data[0]['description']) ? $padyatri_data[0]['description'] : '' ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Dindi Head:</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control" id="samitiName" placeholder="Samiti Name" value="<?php echo isset($padyatri_data[0]['contact_person_name']) ? $padyatri_data[0]['contact_person_name'] : '' ?>" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group required row" aria-required="true">
                                        <label class="control-label text-left col-md-4">Contact No:</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control" id="comment" value="<?php echo isset($padyatri_data[0]['contact_person_mobile']) ? $padyatri_data[0]['contact_person_mobile'] : '' ?>" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="">
                    <div id="example23_wrapper" class="dataTables_wrapper table-responsive">
                        <table id="dindi_report" class=" w-100 dt-bootstrap4  display nowrap table table-hover table-striped table-bordered dataTable datatable" cellspacing="0" role="grid" aria-describedby="example23_info">
                            <thead>
                                <tr role="row">
                                    <th>Devotee Name</th>
                                    <th>Contact Number</th>
                                    <th>Blood Group</th>
                                    <th>Gender</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($padyatri_data) && !empty($padyatri_data)) {
                                    foreach ($padyatri_data as $devotee) {
                                        ?>
                                        <tr>
                                            <td><?php echo isset($devotee['firstname'], $devotee['lastname']) ? $devotee['firstname'] . " " . $devotee['middlename'] . " " . $devotee['lastname'] : ''; ?></td>
                                            <td><?php echo isset($devotee['mobile']) ? $devotee['mobile'] : ''; ?></td>
                                            <td><?php echo isset($devotee['blood_group']) ? $devotee['blood_group'] : ''; ?></td>
                                            <td><?php echo isset($devotee['gender']) ? $devotee['gender'] : 0; ?></td>
                                        </tr>                                            
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<table id="devotee_enrollment" class=" w-100 dt-bootstrap4  display nowrap table table-hover table-striped table-bordered dataTable datatable" cellspacing="0" role="grid" aria-describedby="example23_info">
    <thead>
        <tr role="row">
            <th>Username</th>
            <th>Date</th>
            <th>Enrollment Count</th>
            <th>Total Amount</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (isset($devotee_enroll_list) && !empty($devotee_enroll_list)) {
            foreach ($devotee_enroll_list as $dt) {
                ?>
                <tr>
                    <td><?php echo isset($dt['username']) ? $dt['username'] : ''; ?></td>
                    <td><?php echo isset($dt['date']) ? date('d M Y', strtotime($dt['date'])) : ''; ?></td>
                    <td><?php echo isset($dt['enrollment_count']) ? $dt['enrollment_count'] : ''; ?></td>
                    <td><?php echo isset($dt['enrollment_amount']) ? IND_money_format($dt['enrollment_amount']) : ''; ?></td>
                </tr> 
                <?php
            }
        }
        ?>

    </tbody>
</table>
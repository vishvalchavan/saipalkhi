<div class="row">
    <div class="col-12">
        <div class="card min-height80">
            <div class="card-body">
                <h3 class="card-title">Reports</h3>
                <hr>
                <div class="border-radius20">
                    <div class="clearfix"></div>
                    <div id="dashboard_section">

                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-4 bg-f5f5f5 Devotee-management mx-4 report-block">
                                <div class="row width-500 mt-2">
                                    <div class="col-lg-5 mt-2 col">
                                        <div class="dashboard-img">
                                            <img src="<?php echo base_url(); ?>assets/images/3.png" class="img-responsive Devotee-img report-page">
                                        </div>
                                        <h3 class="Devotee-head mt-2">Dindi </h3>
                                        <h5 class="dashboard-management">Report</h5>
                                    </div>
                                    <div class="col-lg-7 col">
                                        <div class="row mt-2">
                                            <div class="col-lg-12">
                                                <div class="">
                                                    <a href="<?php echo base_url(); ?>admin/DindiReport" class="">
                                                        <i aria-hidden="true" class="fas fa-pencil-alt report-icon"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 m-top-5 text-left">
                                                <h5 class="total font-14 mleft">Total</h5>
                                                <h3 class="center text-left">#<span class="total-Devotee font-weight-bold"><?php echo isset($total_dindis) ? $total_dindis[0]['totaldindi'] : ''; ?></span></h3>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>               
                            <div class="col-lg-4 col bg-f5f5f5 Dindi-management mx-4 report-block" >

                                <div class="row width-500 mt-2">
                                    <div class="col-lg-5 mt-2 col">
                                        <div class="dashboard-img">
                                            <img src="<?php echo base_url(); ?>assets/images/3.png" class="img-responsive Devotee-img report-page">
                                        </div>
                                        <h3 class="Devotee-head mt-2">Tablet User</h3>
                                        <h5 class="dashboard-management">Report</h5>
                                    </div>
                                    <div class="col-lg-7 col">
                                        <div class="row mt-2">
                                            <div class="col-lg-12 col">
                                                <div class="">
                                                    <a href="<?php echo base_url(); ?>admin/DevoteeEnrollment" class=""><i aria-hidden="true" class="fas fa-pencil-alt report-icon"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 m-top-5 text-left">
                                                <h5 class="total font-14 mleft">Total</h5>
                                                <h3 class="center text-left">#<span class="total-Devotee font-weight-bold"><?php echo isset($total_tabusers) ? $total_tabusers[0]['users'] : ''; ?></span></h3>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-1"></div>

                        </div>
                    </div>


                    <!-- Column -->
                    <!-- Column -->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card min-height80">
            <div class="card-body">     
                <div class="row">
                    <div class="col-md-5 align-self-center col-6">
                        <h3 class="card-title">Dindi Report</h3>
                    </div>
                    <div class="col-md-7 align-self-center text-right col-6">
                        <a href="<?php echo base_url(); ?>admin/reports">  <button type="button" class="btn btn-info add-button"><i class="fas fa-arrow-left"></i> &nbsp; &nbsp; Back to Report</button></a>
                    </div>
                </div>
                <hr>
                <div class="">
                    <div class="row mx-0">
                        <div class="button-table button-table-right">
                            <form class="form-inline">
                                <label class="palkhi-select">Select Year:</label>
                                <select class="form-control ml-2" id="year_list" name="year_list">
                                    <option value="">Select Year</option>
                                    <option value="all">All</option>
                                    <?php
                                    $curr_yr_id = '';
                                    if (isset($year_list) && !empty($year_list)) {
                                        foreach ($year_list as $year) {
                                            $selected = '';
                                            $palkhiyear = ($this->session->userdata('palki_year')) ? $this->session->userdata('palki_year') : date('Y');
                                            if ($palkhiyear == $year['year']) {
                                                $selected = "selected";
                                                $curr_yr_id = $year['id'];
                                            }
                                            ?>
                                            <option value="<?php echo $year['id'] ?>" <?php echo $selected; ?>><?php echo $year['year'] ?></option>
                                            <?php
                                        }
                                    }
                                    $this->session->unset_userdata('palki_year');
                                    ?>
                                </select>

                            </form>
                        </div>
                    </div>
                    <div id="example23_wrapper" class="dataTables_wrapper table-responsive">
                        <table id="dindi_report" class=" w-100 dt-bootstrap4  display nowrap table table-hover table-striped table-bordered dataTable datatable" cellspacing="0" role="grid" aria-describedby="example23_info">
                            <thead>
                                <tr role="row">
                                    <th>Dindi Name</th>
                                    <th>Dindi Head</th>
                                    <th>Contact Number</th>
                                    <th>No Of Males</th>
                                    <th>No Of Females</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($dindi_list) && !empty($dindi_list)) {
                                    foreach ($dindi_list as $dindi) {
                                        ?>
                                        <tr>
                                            <td><a href="<?php echo base_url() . 'admin/dindipadyatrireport/' . $dindi['id']; ?>"><?php echo isset($dindi['name']) ? $dindi['name'] : ''; ?></a></td>
                                            <td><?php echo isset($dindi['contact_person_name']) ? $dindi['contact_person_name'] : ''; ?></td>
                                            <td><?php echo isset($dindi['contact_person_mobile']) ? $dindi['contact_person_mobile'] : ''; ?></td>
                                            <td><?php echo isset($dindi['male_count']) ? $dindi['male_count'] : 0; ?></td>
                                            <td><?php echo isset($dindi['female_count']) ? $dindi['female_count'] : 0; ?></td>
                                        </tr>                                            
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('body').on('change', '#year_list', function () {

        var year = $(this).val();
        if (year.length !== 0) {
            $(".loadding-page").css('display', 'block');
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>Reports/Dindi_report/ajx_dindireport',
                data: {'year': year},
                success: function (data) {
                    if (data) {
                        $("#example23_wrapper").html(data);
                        $('#dindi_report').DataTable({
                            dom: 'Bfrtip',
                            buttons: [
                                'excel', 'pdf', 'print'
                            ],
                        });
                        $(".loadding-page").css('display', 'none');
                    }
                }
            });
        }
    });
</script>
<div class="row">
    <div class="col-12">
        <div class="card min-height80">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-5 align-self-center col-6">
                        <h3 class="card-title">Tablet User Report</h3>
                    </div>
                    <div class="col-md-7 align-self-center text-right col-6">
                        <a href="<?php echo base_url(); ?>admin/reports"> <button type="button" class="btn btn-info add-button"><i class="fas fa-arrow-left"></i> &nbsp; &nbsp; Back to Report</button></a>
                    </div>
                </div>
                <hr>
                <div class="">
                    <div class="row mx-0">
                        <div class="button-table button-table-right">
                            <form class="form-inline">
                                <label class="palkhi-select">User Name:&nbsp;&nbsp;&nbsp;</label>
                                <select class="form-control" id="user_list" name="user_list">
                                    <option value="">Select Username</option>
                                    <?php
                                    if (isset($tab_user) && !empty($tab_user)) {
                                        foreach ($tab_user as $user) {
                                            ?><option value="<?php echo $user['token']; ?>"><?php echo $user['username']; ?></option><?php
                                        }
                                    }
                                    ?>

                                </select>
                            </form>
                        </div>
                    </div>
                    <div id="example23_wrapper" class="dataTables_wrapper table-responsive">
                        <table id="devotee_enrollment" class=" w-100 dt-bootstrap4  display nowrap table table-hover table-striped table-bordered dataTable datatable" cellspacing="0" role="grid" aria-describedby="example23_info">
                            <thead>
                                <tr role="row">
                                    <th>Username</th>
                                    <th>Date</th>
                                    <th>Enrollment Count</th>
                                    <th>Total Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($devotee_enroll_list) && !empty($devotee_enroll_list)) {
                                    foreach ($devotee_enroll_list as $dt) {
                                        ?>
                                        <tr>
                                            <td><?php echo isset($dt['username']) ? $dt['username'] : ''; ?></td>
                                            <td><?php echo isset($dt['date']) ? date('d M Y', strtotime($dt['date'])) : ''; ?></td>
                                            <td><?php echo isset($dt['enrollment_count']) ? $dt['enrollment_count'] : ''; ?></td>
                                            <td><?php echo isset($dt['enrollment_amount']) ? IND_money_format($dt['enrollment_amount']) : ''; ?></td>
                                        </tr> 
                                        <?php
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('body').on('change', '#user_list', function () {

        var userid = $(this).val();
        if (userid.length !== 0) {
            $(".loadding-page").css('display', 'block');
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>Reports/Devotee_enrollment/ajx_devotee_enrollment',
                data: {'userid': userid},
                success: function (data) {
                    if (data) {
                        $("#example23_wrapper").html(data);
                        $('#devotee_enrollment').DataTable({
                            dom: 'Bfrtip',
                            buttons: [
                                'excel', 'pdf', 'print'
                            ],
                        });
                        $(".loadding-page").css('display', 'none');
                    }
                }
            });
        }
    });
</script>

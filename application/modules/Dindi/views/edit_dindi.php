<div class="card min-height80">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <div class="row mx-0">
                    <div class="col-md-5 col-5 align-self-center p-0">
                        <h3 class="card-title">Edit Dindi</h3>
                    </div>
                    <div class="col-md-7 col-7 text-right p-0">
                        <a href="<?php echo base_url() . 'admin/dindi' ?>">  <button type="button" class="btn btn-info add-button"><i class="fas fa-arrow-left"></i> &nbsp; &nbsp; Back to Dindi List</button></a>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <?php echo form_open("Dindi/update_dindi/" . $dindi_details[0]['id'], array('id' => 'add_dindi', 'class' => 'form-horizontal p-t-20')); ?>  

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group required row">
                                    <label class="control-label text-left col-md-4">Dindi Name:</label>
                                    <div class="col-md-7">
                                        <div class="input-group">                            
                                            <input type="text" class="form-control" id="dindi_name" placeholder="Enter Dindi Name" name="dindi_name" data-error=".dindiErorr1" value="<?php echo isset($dindi_details[0]['name']) ? $dindi_details[0]['name'] : ''; ?>">
                                            <div class="dindiErorr1 error-msg"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group required row">
                                    <label class="control-label text-left col-md-4">Description:</label>
                                    <div class="col-md-7">
                                        <div class="input-group">                            
                                            <textarea rows="3" class="form-control" id="dindi_description" placeholder="Enter description" name="dindi_description" data-error=".dindiErorr2"><?php echo isset($dindi_details[0]['description']) ? $dindi_details[0]['description'] : ''; ?></textarea>
                                            <div class="dindiErorr2 error-msg"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group required row">
                                    <label class="control-label text-left col-md-4">Dindi Head:</label>
                                    <div class="col-md-7">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="contact_person" name="contact_person" placeholder="Enter Name" data-error=".dindiErorr3" value="<?php echo isset($dindi_details[0]['contact_person_name']) ? $dindi_details[0]['contact_person_name'] : ''; ?>">
                                            <div class="dindiErorr3 error-msg"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group required row">
                                    <label class="control-label text-left col-md-4">Dindi Head Mobile:</label>
                                    <div class="col-md-7">
                                        <div class="input-group">
                                            <div class="row">
                                                <div class="col-sm-3 col">
                                                    <input type="text" class="form-control mobile-width" id="country_code" name="country_code" readonly="" value="+91">
                                                </div>
                                                <div class="col-sm-9 col">
                                                    <input type="text" class="form-control mobile-width" id="contact_mobile" name="contact_mobile" placeholder="Enter Mobile No" data-error=".dindiErorr4" value="<?php echo isset($dindi_details[0]['contact_person_mobile']) ? substr($dindi_details[0]['contact_person_mobile'], 2) : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="dindiErorr4 error-msg"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group required row">
                                    <label class="control-label text-left col-md-4">Contact Person:</label>
                                    <div class="col-md-7">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="ref_name" name="ref_name" placeholder="Enter Name" data-error=".dindiErorr5" value="<?php echo isset($dindi_details[0]['contact_person_name1']) ? $dindi_details[0]['contact_person_name1'] : ''; ?>">
                                            <div class="dindiErorr5 error-msg"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group required row">
                                    <label class="control-label text-left col-md-4">Contact Person Mobile:</label>
                                    <div class="col-md-7">
                                        <div class="input-group">
                                            <div class="row">
                                                <div class="col-sm-3 col">
                                                    <input type="text" class="form-control mobile-width" id="refcountry_code" name="refcountry_code" readonly="" value="+91">
                                                </div>
                                                <div class="col-sm-9 col">
                                                    <input type="text" class="form-control mobile-width" id="ref_mobile" name="ref_mobile" placeholder="Enter Mobile No" data-error=".dindiErorr6" value="<?php echo isset($dindi_details[0]['contact_person_mobile1']) ? substr($dindi_details[0][ 'contact_person_mobile1'], 2)  : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="dindiErorr6 error-msg"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

<hr class="my-4">
                        <div class="form-group row m-b-0 col">
                            <div class="offset-sm-11 col-sm-1 p-0">
                                <input type="submit" class="btn btn-inverse update-button" value="Update">
                                <!--<button type="button" class="btn btn-success waves-effect waves-light">Cancel</button>-->
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card min-height80">
            <div class="card-body">
                <div class="row mx-0">
                    <div class="col-md-5 col-5 align-self-center p-0">
                        <h3 class="card-title">Dindi List</h3>
                    </div>
                    <div class="col-md-7 col-7 text-right p-0">
                        <a href="<?php echo base_url('admin/dindi/add'); ?>"><button type="button" class="btn btn-info add-button"><i class="fa fa-plus-circle"></i> Add Dindi</button></a>
                    </div>
                </div>
                <hr>
                <div class="">
                    <div class="row mx-0">
                        <div class="float-left"></div>
                        <div class="button-table">

                            <form class="form-inline">


                                <label class="palkhi-select font-14 mr-1 ml-1">Category:</label>
                                <select class="form-control" id="search_by" name="search_by">
                                    <option value="">Search By</option>
                                    <option value="name" selected>Name</option>
                                    <option value="mobile">Mobile No.</option>
                                </select>

                         <div class="form-group has-search ml-2">
                                    <span class="fas fa-search form-control-feedback"></span>
                                    <input type="text" class="form-control" id="column_filter" placeholder="Search">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div id="example23_wrapper" class="dataTables_wrapper dt-bootstrap4 table-responsive">
                        <table id="dindi_list" class="display nowrap table table-hover table-striped table-bordered dataTable datatable" cellspacing="0" width="100%" role="grid" aria-describedby="example23_info">
                            <thead>
                                <tr role="row">
                                    <th>Dindi Name</th>
                                    <th>Description</th>
                                    <th>Dindi Head</th>
                                    <th>Dindi Head Mobile</th>
                                    <th>Contact Person</th>
                                    <th>Contact Person Mobile</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($dindi_list) && !empty($dindi_list)) {
                                    foreach ($dindi_list as $dindi) {
                                        ?>
                                        <tr>
                                            <td><?php echo isset($dindi['name']) ? $dindi['name'] : ''; ?></td>
                                            <td class="description-list"><?php echo isset($dindi['description']) ? $dindi['description'] : ''; ?></td>
                                            <td><?php echo isset($dindi['contact_person_name']) ? $dindi['contact_person_name'] : ''; ?></td>
                                            <td><?php echo isset($dindi['contact_person_mobile']) ? $dindi['contact_person_mobile'] : ''; ?></td>
                                            <td><?php echo isset($dindi['contact_person_name1']) ? $dindi['contact_person_name1'] : ''; ?></td>
                                            <td><?php echo isset($dindi['contact_person_mobile1']) ? $dindi['contact_person_mobile1'] : ''; ?></td>
                                            <td><?php echo isset($dindi['isactive']) ? $dindi['isactive'] == 1 ? 'Active' : 'In-Active' : ''; ?></td>
                                            <td>
                                                <a href="<?php echo base_url() . 'admin/dindi/update/' . $dindi['id']; ?>" class="bg-button" title="Edit"><i class="fas fa-edit"></i>
                                                </a>&nbsp;
                                                <?php if (isset($dindi['isactive']) && $dindi['isactive'] == 1) { ?>
                                                    <a class="bg-button delete" data-dindiid="<?php echo $dindi['id']; ?>" title="Delete"><i class="fas fa-trash"></i>
                                                    </a>&nbsp;
                                                <?php } else { ?>
                                                    <a class="bg-button reactive" data-dindiid="<?php echo $dindi['id']; ?>" title="Activate"><i class="fas fa-check-circle"></i>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function deleteDindi(dId) {
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Dindi/delete',
            data: {'dindi_id': dId},
            success: function (data) {
                if ($.trim(data) == 'success') {
                    location.reload();
                }
            }
        });
        return false;
    }
    function ActivateDindi(dId) {
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Dindi/activate',
            data: {'dindi_id': dId},
            success: function (data) {
                if ($.trim(data) == 'success') {
                    location.reload();
                }
            }
        });
        return false;
    }
    $('body').on('click', '.delete', function () {
        var id = $(this).attr('data-dindiid');
        $('#confirm-delete').modal();
        $(".btn-ok").click(function () {
            deleteDindi(id);
        });
    });
    $('body').on('click', '.reactive', function () {
        var id = $(this).attr('data-dindiid');
        $('#confirm-active').modal();
        $(".btn-ok").click(function () {
            ActivateDindi(id);
        });
    });
    $(document).ready(function () {
        $("#search_by").change(function () {
            var search = $(this).val();
            if (search.length !== 0) {
                $("#column_filter").removeAttr('disabled');
                $("#column_filter").val("");
                if ($.trim(search) == 'name') {
                    filterColumn('3', "");
                } else if ($.trim(search) == 'mobile') {
                    filterColumn('0', "");
                }
            }
        });
        $('#column_filter').on('keyup click', function () {
            var search_txt = $(this).val();
            var search_by = $("#search_by").val();
            if (search_by.length !== 0) {
                if ($.trim(search_by) == 'name') {
                    filterColumn('0', search_txt);
                } else if ($.trim(search_by) == 'mobile') {
                    filterColumn('3', search_txt);
                }
            } else {
                filterColumn('0', search_txt);
            }
        });
    });
    function filterColumn(i, searchVal) {
        $('#dindi_list').DataTable().column(i).search(searchVal, false, true).draw();
    }
</script>

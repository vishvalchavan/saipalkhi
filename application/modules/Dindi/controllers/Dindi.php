<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dindi extends MY_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            redirect('admin', 'refresh');
        }
        if (!$this->ion_auth->is_admin()) {
            redirect('admin', 'refresh');
        }
        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->model(array('DindiModel', 'Year', 'devotee', 'language', 'ApiModel'));
        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('Auth');
    }

    public function index() {
        if (!$this->ion_auth->logged_in()) {
            redirect('admin', 'refresh');
        }

        $user_id = $this->session->userdata('user_id');
        $data['dindi_list'] = $this->DindiModel->get_dindi_list();
        $data['year_list'] = $this->Year->get_year_list();
        $data['dataHeader']['title'] = 'Dindi List';
        $this->template->set_master_template('template.php');
        $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
        $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
        $this->template->write_view('content', 'dindi_list', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'snippets/footer', '', TRUE);
        $this->template->render();
    }

    public function add_dindi() {
        if (!$this->ion_auth->logged_in()) {
            redirect('admin', 'refresh');
        }
        $user_id = $this->session->userdata('user_id');
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->form_validation->set_rules('dindi_name', 'Dindi Name', 'required');
            $this->form_validation->set_rules('contact_person', 'Contact Person', 'required');
            $this->form_validation->set_rules('contact_mobile', 'Contact Person Mobile', 'required');
            $this->form_validation->set_rules('ref_name', 'Ref. Name', 'required');
            $this->form_validation->set_rules('ref_mobile', 'Ref. Mobile', 'required');
            if ($this->form_validation->run() == TRUE) {
                $check = $this->DindiModel->if_dindi_exists($this->input->post('dindi_name'));
                if (count($check) == 0) {
                    $dindi_details = array(
                        'name' => $this->input->post('dindi_name'),
                        'contact_person_name' => $this->input->post('contact_person'),
                        'contact_person_mobile' => '91' . $this->input->post('contact_mobile'),
                        'contact_person_name1' => $this->input->post('ref_name'),
                        'contact_person_mobile1' => '91' . $this->input->post('ref_mobile'),
                        'created_by' => $user_id,
                        'created_date' => date('Y-m-d'),
                        'isactive' => 1
                    );
                    if ($this->input->post('dindi_description')) {
                        $dindi_details['description'] = $this->input->post('dindi_description');
                    }
                    $response = $this->DindiModel->insert($dindi_details);
                    if ($this->db->affected_rows() > 0) {
                        $this->DindiModel->update_dashboard_dindicount(date("Y"));
                        $this->session->set_flashdata('success_msg', 'Dindi Added Successfully.');
                        redirect('admin/dindi');
                    } else {
                        $this->session->set_flashdata('error_msg', 'Failed to add dindi');
                        redirect('admin/dindi');
                    }
                } else {
                    $this->session->set_flashdata('error_msg', 'Dindi already exists.');
                    redirect('admin/dindi');
                }
            }
        } else {
//            $data['dataHeader'] = $this->devotee->get_allData($user_id);
            $data['dataHeader']['title'] = 'Add Dindi';
            $this->template->set_master_template('template.php');
            $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
            $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
            $this->template->write_view('content', 'add_dindi', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'snippets/footer', '', TRUE);
            $this->template->render();
        }
    }

    public function update_dindi($dindi_id) {
        if (!$this->ion_auth->logged_in()) {
            redirect('admin', 'refresh');
        }
        $user_id = $this->session->userdata('user_id');
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $this->form_validation->set_rules('dindi_name', 'Dindi Name', 'required');
            $this->form_validation->set_rules('contact_person', 'Contact Person', 'required');
            $this->form_validation->set_rules('contact_mobile', 'Contact Person Mobile', 'required');
            $this->form_validation->set_rules('ref_name', 'Ref. Name', 'required');
            $this->form_validation->set_rules('ref_mobile', 'Ref. Mobile', 'required');
            if ($this->form_validation->run() == TRUE) {
                $check = $this->DindiModel->if_dindi_exists($this->input->post('dindi_name'));
                if ((count($check) == 1 && $check[0]->id == $dindi_id) || count($check) == 0) {
                    $dindi_details = array(
                        'name' => $this->input->post('dindi_name'),
                        'contact_person_name' => $this->input->post('contact_person'),
                        'contact_person_mobile' => '91' . $this->input->post('contact_mobile'),
                        'contact_person_name1' => $this->input->post('ref_name'),
                        'contact_person_mobile1' => '91' . $this->input->post('ref_mobile'),
                        'modified_date' => date('Y-m-d')
                    );
                    if ($this->input->post('dindi_description')) {
                        $dindi_details['description'] = $this->input->post('dindi_description');
                    }
                    $response = $this->DindiModel->update($dindi_id, $dindi_details);
                    if ($this->db->affected_rows() > 0) {
                        $this->session->set_flashdata('success_msg', 'Dindi updated Successfully.');
                        redirect('admin/dindi');
                    } else {
                        $this->session->set_flashdata('error_msg', 'Failed to Update dindi');
                        redirect('admin/dindi');
                    }
                } else {
                    $this->session->set_flashdata('error_msg', 'Dindi already exists.');
                    redirect('admin/dindi');
                }
            }
        } else {
            $data['dindi_details'] = $this->DindiModel->get_allData($dindi_id);
            $data['dataHeader']['title'] = 'Edit Dindi';
            $this->template->set_master_template('template.php');
            $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
            $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
            $this->template->write_view('content', 'edit_dindi', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'snippets/footer', '', TRUE);
            $this->template->render();
        }
    }

    public function delete() {
        if ($this->input->post('dindi_id')) {
            $updateid = $this->DindiModel->delete_dindi($this->input->post('dindi_id'));
            if ($updateid) {
                $this->session->set_flashdata('success_msg', 'Dindi deleted successfully.');
                echo 'success';
            } else {
                $this->session->set_flashdata('error_msg', 'Failed to delete dindi.');
                echo 'failed';
            }
        }
    }

    public function activate() {
        if ($this->input->post('dindi_id')) {
            $activeid = $this->DindiModel->activate_dindi($this->input->post('dindi_id'));
            if ($activeid) {
                $this->session->set_flashdata('success_msg', 'Dindi activated successfully.');
                echo 'success';
            } else {
                $this->session->set_flashdata('error_msg', 'Failed to activate dindi.');
                echo 'failed';
            }
        }
    }
}
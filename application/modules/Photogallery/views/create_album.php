              <div class="row">
                                     <div class="col-12">
                        <div class="row">
                           <div class="col-md-5 align-self-center">
                              <h3 class="card-title">Create Album</h3>
                           </div>
                           <div class="col-md-7 align-self-center text-right">
                              <a href="photography.html"><button type="button " class="btn btn-info back-button "><i class="fas fa-arrow-left"></i>  Back to Album</button></a>
                           </div>
                        </div>
                        <hr>
                        <div class="row">
                           <div class="col-12">                              
                            <form class="form-horizontal p-t-20" action="<?php echo base_url()?>admin/add_album" method="post" id="create_album">
                                 <div class="form-group required row">
                                    <label for="" class="col-sm-2 control-label">Album Name</label>
                                    <div class="col-sm-4">
                                       <div class="input-group">
                                           <input type="text" class="form-control" id="album_title" name="album_title" placeholder="Album Title">
                                          <div class="dindiErorr1"></div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group required row">
                                    <label for="" class="col-sm-2 control-label">Description</label>
                                    <div class="col-sm-4">
                                       <div class="input-group">
                                           <textarea class="form-control" rows="4" name="description" id="description" placeholder="Description"></textarea>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group required row">
                                    <label for="" class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-4">
                                       <div class="input-group">
                                          <select class="form-control custom-select">
                                             <option value="">Active</option>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group row m-b-0">
                                    <div class="offset-sm-2 col-sm-6 col">
                                       <button type="button" class="btn btn-inverse add-button">Save</button>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                       
                     </div>
                </div> 

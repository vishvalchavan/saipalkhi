<style>
    button#submitFormbtn:hover { 
        cursor: not-allowed;
    }  
</style>
<div class="card min-height80">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <div class="row mx-0">
                    <div class="col-md-5 align-self-center col-12 p-0">
                        <h3 class="card-title">Update Photo Album
                        </h3>
                    </div>
                    <div class="col-md-7 align-self-center text-right col-12 p-0">
                        <a href="<?php echo base_url() ?>admin/album" class="-info"><button type="button " class="btn btn-info back-button "><i class="fa fa-arrow-left"></i> Back to Photo Gallery</button></a>
                    </div>
                </div>
                <hr>
                 <?php  if(isset($album_details) && !empty($album_details)){
                $filelength=array();
                $filecount='';
                $albumcover_imgid='';
                foreach ($album_details as $album_data) {                    
                        if(file_exists($album_data['img_path'])) {
                                $filelength[]=$album_details[0]['img_path'];
                                 if($album_data['iscover_pic'] == '1') {
                                    $albumcover_imgid = $album_data['id'].",".$album_data['tbl_event_detail_id'];
                                 }  
                            }                             
                }                
                $filecount=count($filelength);?>
                <!--<form class="form-horizontal p-t-20">-->
                <form action="<?php echo base_url() ?>admin/update_album_details/<?php echo $album_data['id'] ?>" method="post" enctype="multipart/form-data" name="add_album_details" id="add_album_details"> 
                    <div class="col-12">  



                              <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Album Name:</label>
                                     <div class="col-md-7">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="album_title" name="album_title" value="<?php echo set_value('album_title', (!empty($album_data['event_name'])) ? $album_data['event_name'] : ''); ?>" data-error=".albumErorr1" placeholder="Album Title">
                                </div>
                                <div class="input-field">
                                    <div class="albumErorr1 error-msg"></div>
                                    <?php echo form_error('album_title'); ?>
                                </div> 
                            </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group required row" aria-required="true">
                                        <label class="control-label text-left col-md-4">Description:</label>
                                      <div class="col-md-7">
                                <div class="input-group">
                                    <textarea class="form-control" rows="3" name="description" id="description" data-error=".albumErorr2" placeholder="Description"><?php echo set_value('description', (!empty($album_data['description'])) ? $album_data['description'] : ''); ?></textarea>
                                </div>
                                <div class="input-field">
                                    <div class="albumErorr2 error-msg"></div>
                                    <?php echo form_error('description'); ?>
                                </div> 
                            </div>
                                    </div>
                                </div>
                            </div>

                    <!--     <div class="form-group required row">
                            <label for="" class="col-sm-2 control-label">Album Name</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="album_title" name="album_title" value="<?php echo set_value('album_title', (!empty($album_data['event_name'])) ? $album_data['event_name'] : ''); ?>" data-error=".albumErorr1" placeholder="Album Title">
                                </div>
                                <div class="input-field">
                                    <div class="albumErorr1 error-msg"></div>
                                    <?php echo form_error('album_title'); ?>
                                </div> 
                            </div>
                        </div>
                       
                        <div class="form-group required row">
                            <label for="" class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <textarea class="form-control" rows="4" name="description" id="description" data-error=".albumErorr2" placeholder="Description"><?php echo set_value('description', (!empty($album_data['description'])) ? $album_data['description'] : ''); ?></textarea>
                                </div>
                                <div class="input-field">
                                    <div class="albumErorr2 error-msg"></div>
                                    <?php echo form_error('description'); ?>
                                </div> 
                            </div>                                                         
                        </div> -->
                    </div>
                    <div class="row">               
                        <div class="col-12">                                  
                            <div id="jquery-script-menu">
                                <div class="jquery-script-clear"></div>
                            </div>
                            <div class="container" style="margin-top:20px;">

                                <input type="file" id="abcd" accept="image/*" multiple><br>

                                <div class="float-right ml-4">
                                    <!-- <button type='button' class='btn btn-info'>Upload</button> -->
                                </div>

                            </div>                                  

                        </div>
                    </div>

                    <!--</div>-->
                    <div class="row">
                        <div class="col-12">
                            <div class="container">
                                <div class="row">


                                    <div class="addnew-div"> </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-5 align-self-center">
                            <h3 class="card-title"></h3>
                        </div>
                        <div class="col-md-7 align-self-center text-right">
                            <input type="hidden" id="imglength" name="imglength" data-error=".albumErorr8" value="<?php echo $filecount;?>">
                            <input type="hidden" id="old_imgid" name="old_imgid[]">
                            <input type="hidden" id="old_coverimgid" name="old_coverimgid" value="">
                            <input type="hidden" id="albumid" name="albumid" value="<?php echo $album_data['id'] ?>">
                            <input type="hidden" id="lastcount" name="lastcount">
                            <input type='submit' name="btnSubmit" class='btn btn-info btnSubmit' onclick="" value="Update">
                            <!--</a>-->
                        </div>
                    </div>
                </form>
                <?php } else { ?>
               <div class="form-body">
                        <!--<hr class="m-t-0 m-b-40">-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-center margin-top-10">Data not found</div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function () {
        $('input[type="file"]').imageuploadify();
        $(".addnew-div").insertAfter(".imageuploadify-container");
        var olddiv = '';
        var imgpath = '';
        var img_title = '';
        var cover_img = '';
        var imgid = '';
        var imagepath = '';
        var eventid = '';
<?php
if (isset($album_details) && !empty($album_details)) {

//                $(".imageuploadify-images-list").html('
    foreach ($album_details as $album_images) {
        if (file_exists($album_images['img_path'])) {
            $imppath = base_url() . $album_images['img_path'];
            $img_title = $album_images['img_description'];
            $cover_img = ($album_images['iscover_pic'] == '1') ? 'checked' : '';
            ?>
                    eventid = "<?php echo $album_images['id'] ?>";
                    imgid = "<?php echo $album_images['tbl_event_detail_id'] ?>";
                    imagepath = "<?php echo $album_images['img_path'] ?>";
                    imgpath = "<?php echo $imppath ?>";
                    img_title = "<?php echo str_replace(array('/', '_'), array(' ', ' '), $img_title) ?>";

                    cover_img = "<?php echo $cover_img ?>";
                    olddiv += '<div class="imageuploadify-container" id="img_div_' + imgid + '" style="width: 24%; margin-left: 0px;"><button type="button" data-devotee=' + imgid + ' id=' + imagepath + ' class="btn btn-danger remove" onclick="remove(this,' + imgid + ')">X</button><div class="imageuploadify-details" style="opacity: 0;"><span>5 - Copy.jpg</span><span>image/jpeg</span><span>800332</span></div><input type="text" id="oldtitle_' + imgid + '" name="oldtitle_' + imgid + '" data-title="' + img_title + '" placeholder="Enter photo title" value="' + img_title + '" onchange="checkMaxlength(this,' + imgid + ')" maxlength="51" style="background: gainsboro;position: absolute;z-index: 99999;width: 91%;margin-top: 110px; margin-left: 0%; float: left;left: 5%;padding: 3px;border: navajowhite;border-radius: 5px;" data-error=".albumErorr5"><div class="albumErorr5 error-msg"></div><label class="check-select"><input type="radio" id="chk_' + imgid + '" name="cover_photo" ' + cover_img + ' value="oldchk_' + imgid + '" onclick="checkradio(this,' + eventid + ',' + imgid + ')" title="add to album title" style="position: absolute;z-index: 999999;float: left;margin-top: 3%;margin-left: -41%;" data-error=".showImgMsg"><span class="checkmark"></span></label><img class="upload-img" src="' + imgpath + '" id="img_' + imgid + '"></div>';
                    //                     olddiv+='<div class="imageuploadify-container" style="width: 24%; margin-left: 0px;"><button type="button" class="btn btn-danger">X</button><div class="imageuploadify-details" style="opacity: 0;"><span>5 - Copy.jpg</span><span>image/jpeg</span><span>800332</span></div><input type="text" id="title_2" name="title_2" value="800332" style="background: gainsboro;position: absolute;z-index: 99999;width: 91%;margin-top: 110px; margin-left: 0%; float: left;left: 5%;padding: 3px;border: navajowhite;border-radius: 5px;"><label class="check-select"><input type="radio" id="chk_2" name="cover_photo" value="chk_2" title="add to album title" style="position: absolute;z-index: 999999;float: left;margin-top: 3%;margin-left: -41%;"><span class="checkmark"></span><img src="'+imgpath+'"></div>';
                    $(".imageuploadify-images-list").html(olddiv);
            <?php
        }
    }
    ?>
            var imglength=$("#imglength").val();
              $(".btnSubmit").click(function(e){                  
                  imglength=$("#imglength").val();                  
                if(imglength < 1)
                { 
                        $(".showImgMsg").html("Please upload at least one photo");
                        $(".imagediv").addClass('borderclass');
                         setTimeout(function () {
                              $('.showImgMsg').html('');                        
                              $(".imagediv").removeClass('borderclass');
                          }, 3000);
                e.preventDefault(e);
                }
            });
            
        })
<?php } ?>
    function openExplorer()
    {
        $('#abcd').trigger('click');        
        $(".addnew-div").insertAfter(".imageuploadify-container");
    }
//------------------------------ update img title-----------------------------------------------------------------------------------            
    function updatetitle(input, img_ids) {
        var inputval = input.value;        
        if(inputval.length > 50)
        {  
        }else{
        change_img_details(inputval, img_ids, 'update');
    }
    }
//------------------------------ change cover img -----------------------------------------------------------------------------------            
    function checkradio(input, album_id, img_ids) {
        var inputval = '';
        $("#old_coverimgid").val(album_id+','+img_ids);
        //change_img_details(album_id, img_ids, 'cover');
    }
//------------------------------ remove img -----------------------------------------------------------------------------------            
var old_imgid=[];
    function remove(input, img_ids) {
        var inputval = input.id;  
        $('#img_div_' + img_ids).remove();
//        change_img_details(inputval, img_ids, 'remove');
        old_imgid.push(img_ids);
        $("#old_imgid").val(old_imgid);
        $("#imglength").val($(".upload-img").length);
    }

    function change_img_details(inputval, img_ids, btnaction)
    {
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Photogallery/change_imagetitle',
            data: {'imgid': img_ids, 'title': inputval, action: btnaction},
            success: function (data) {

                var parsed = $.parseJSON(data);
                $(".messenger-message").remove();
                if (parsed.returnstatus == '0')
                {
                    $("#oldtitle_" + img_ids).focus();
                    $("#oldtitle_" + img_ids).css('color', 'red');
                    setTimeout(function () {
                        //                                data-title
                        $("#oldtitle_" + img_ids).val($("#oldtitle_" + img_ids).attr('data-title'));
                        $("#oldtitle_" + img_ids).css('color', 'rgb(0, 0, 0)');
                    }, 4000);

                    showErrorMessage(parsed.failed);
                } else if (parsed.returnstatus == '1') {
                    showSuccess(parsed.success);
                }
            }
        });
    }
    
</script>
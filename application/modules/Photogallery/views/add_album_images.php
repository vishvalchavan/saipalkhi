<div class="card min-height80">
    <div class="card-body">
        <div class="row">
            <div class="col-12">

                <div class="row mx-0">
                    <div class="col-md-5 align-self-center col-12 p-0">
                        <h3 class="card-title">Add Photo Album
                        </h3>
                    </div>
                    <div class="col-md-7 text-right col-12 p-0">                    
                        <!--<div class="col-md-offset-7 col-md-2 align-self-center  d-none d-md-block">-->
                        <a href="<?php echo base_url() ?>admin/album"> <button type="button" class="btn btn-info back-button"><i class="fas fa-arrow-left"></i>  Back to Photo Gallery</button></a>
                    </div>                                        
                </div>
                <hr>
                <!--<div class="row">-->
                <form action="<?php echo base_url() ?>admin/add_album_details/12" method="post" enctype="multipart/form-data" name="add_album_details" id="add_album_details"> 
                    <div class="col-12">                              
                     <!--<form class="form-horizontal p-t-20" action="<?php echo base_url() ?>admin/add_album" method="post" id="create_album">-->
               
                               <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Album Name:</label>
                                      <div class="col-md-7">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="album_title" name="album_title" value="<?php echo set_value('album_title'); ?>" data-error=".albumErorr1" placeholder="Album Title">
                                </div>
                                <div class="input-field">
                                    <div class="albumErorr1 error-msg"></div>
                                    <?php echo form_error('album_title'); ?>
                                </div> 
                            </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group required row" aria-required="true">
                                        <label class="control-label text-left col-md-4">Description:</label>
                                        <div class="col-md-7">
                                <div class="input-group">
                                    <textarea class="form-control" rows="4" name="description" id="description" data-error=".albumErorr2" placeholder="Description"><?php echo set_value('description'); ?></textarea>
                                </div>
                                <div class="input-field">
                                    <div class="albumErorr2 error-msg"></div>
                                    <?php echo form_error('description'); ?>
                                </div> 
                            </div>
                                    </div>
                                </div>
                            </div>

<!-- 
                        <div class="form-group required row">
                            <label for="" class="col-sm-2 control-label">Album Name</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="album_title" name="album_title" value="<?php echo set_value('album_title'); ?>" data-error=".albumErorr1" placeholder="Album Title">
                                </div>
                                <div class="input-field">
                                    <div class="albumErorr1 error-msg"></div>
                                    <?php echo form_error('album_title'); ?>
                                </div> 
                            </div>
                        </div>
                        <div class="form-group required row">
                            <label for="" class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <textarea class="form-control" rows="4" name="description" id="description" data-error=".albumErorr2" placeholder="Description"><?php echo set_value('description'); ?></textarea>
                                </div>
                                <div class="input-field">
                                    <div class="albumErorr2 error-msg"></div>
                                    <?php echo form_error('description'); ?>
                                </div> 
                            </div>
                        </div> -->

                    </div>
                    <!--</div>-->
                    <div class="col-12">
                        <div id="jquery-script-menu">
                            <div class="jquery-script-clear"></div>
                        </div>
                        <div class="container" style="margin-top:20px;">

                            <input type="file" id="abcd" name="files" accept="image/*" multiple><br>

                            <div class="float-right ml-4">
                                <input type="hidden" id="imglength" name="imglength" data-error=".albumErorr8">
                                <input type="hidden" id="albumid" name="albumid" value="1">
                                <input type="hidden" id="lastcount" name="lastcount">
                                <input type='submit' name="btnSubmit" class='btn btn-info btnSubmit' value="Save">
                            </div>

                        </div>


                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function () {
        $('input[type="file"]').imageuploadify();
        var imglength = $("#imglength").val();
        var upload_imglength = $(".upload-img");

        $(".btnSubmit").click(function (e) {
            imglength = $("#imglength").val();
            upload_imglength = $(".upload-img").length;

            if (imglength < 1)
            {
                $(".showImgMsg").html("Please upload at least one photo");
                $(".imagediv").addClass('borderclass');
                setTimeout(function () {
                    $('.showImgMsg').html('');
                    $(".imagediv").removeClass('borderclass');
                }, 3000);
                e.preventDefault(e);
            }
//                if(upload_imglength > 10)
//                {
//                        $(".showImgMsg").html("Please upload at least max 10 photos");
//                        $(".imagediv").addClass('borderclass');
//                         setTimeout(function () {
//                              $('.showImgMsg').html('');                        
//                              $(".imagediv").removeClass('borderclass');
//                          }, 3000);
//                e.preventDefault(e);  
//                }
        });
    });
    function openExplorer()
    {
        $('#abcd').trigger('click');
    }

</script>

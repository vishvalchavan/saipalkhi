<section class="gallery-block cards-gallery ">
    <div class="container">
        <div id="overviews" class="section">

            <div class="heading section-header">
                <h2 class="section-title text-center wow fadeInDown animated font-weight-bold">Photo Gallery</h2>
            </div>
            <div class="row photogalleryrow">
                <?php foreach ($album_list as $albums) { ?> 
                    <div class="col-md-6 col-lg-3 col-sm-6">
                        <a class="lightbox" href="<?php echo base_url(); ?>photo-gallery-inner/<?php echo $albums['id'] ?>">
                            <div class="card border-0 transform-on-hover">
                                <?php
                                if (file_exists($albums['cover_image'])) {
                                    $album_cover = ($albums['cover_image']) ? $albums['cover_image'] : 'assets/images/img-not-found.png';
                                } else {
                                    $album_cover = 'assets/images/img-not-found.png';
                                }
                                ?>

                                <?php // if(file_exists($albums['img_path'])){  ?>
                                <img src="<?php echo base_url() . $album_cover; ?>" alt="Card Image" class="card-img-top" width="326px" height="190px">
                                <?php // }  ?>        
                                <div class="card-body">
                                    <h6><?php echo $albums['event_name']; ?></h6>
                                    <p class="text-muted card-text album-description-userside"><?php echo $albums['description']; ?></p>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
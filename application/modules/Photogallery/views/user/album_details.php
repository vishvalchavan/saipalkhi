<!-- <div class="all-title-box">
  </div> -->
<section class="gallery-block compact-gallery">
    <div class="container mt-4">
        <div id="overviews" class="">

            <div class="heading section-header">
                <h2 class="section-title text-center wow fadeInDown animated font-weight-bold mb-4"><?php echo!empty($album_details) ? $album_details[0]['event_name'] : 'Photo Gallery' ?></h2>                                
                <a href="<?php echo base_url() ?>photo-gallery" class="button-photogallery"><button type="button" class="btn btn-info back-button float-right mb-4"><i class="fa fa-arrow-left"></i> Back to Photo Gallery</button></a>
            </div>            

            <?php if (isset($album_details) && !empty($album_details)) { ?>
                <div class="row"> <?php
                    $album_names = array();
                    foreach ($album_details as $album_name) {
                        $album_names['event_name'] = $album_name['event_name'];
                        $album_names['description'] = $album_name['description'];
                    }
                    ?>                 


                </div>
                <div class="row no-gutters col-12">                
                    <?php
                    $album_cover;
                    $album_cove;
                    foreach ($album_details as $album_data) {
                        if (file_exists($album_data['img_path'])) {
                            $album_cover = ($album_data['img_path']) ? $album_data['img_path'] : 'assets/images/bg.png';
                            $album_cover = ($album_data['img_path']) ? $album_data['img_path'] : 'assets/images/bg.png';
                        } else {
                            $album_cover = 'assets/images/bg.png';
                        }
                        ?>
                        <div class="col-md-6 col-lg-3 col-sm-6 item zoom-on-hover">
                            <a class="lightbox" href="<?php echo base_url() . $album_cover ?>">                       

                                <?php // if(file_exists($album_data['img_path'])){  ?>   
                                <img class="img-fluid image" src="<?php echo base_url() . $album_cover ?>" >
                                <?php // }   ?> </a>   
                            <span class="description">
                                <span class="description-heading"><?php echo str_replace(array('/', '_'), array(' ', ' '), $album_data['img_description']) ?></span>
                            </span>

                        </div>
                    <?php } ?>    
                </div>
            <?php } else { ?>
                <div class="form-body">
                    <!--<hr class="m-t-0 m-b-40">-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center margin-top-10">Data not found</div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<div class="card min-height80">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <div class="row mx-0">
                    <div class="col-md-5 align-self-center col-6 p-0">
                        <h3 class="card-title">Photo Gallery</h3>
                    </div>
                    <div class="col-md-7 align-self-center text-right col-6 p-0">
                        <a href="<?php echo base_url() ?>admin/add_album_details" ><button type="button" title="Add New Album" class="btn btn-info back-button "><i class="fa fa-plus-circle"></i>  Create Album</button></a>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <div class="container">
                            <div class="row">
                                <!--<div class="row">-->
                                <?php #var_dump($album_list);die;
                                $album_cover = '';
                                if (isset($album_list) && !empty($album_list)) {
                                    foreach ($album_list as $albums) {
                                        if (file_exists($albums['cover_image'])) {
                                            $album_cover = ($albums['cover_image']) ? $albums['cover_image'] : 'assets/images/img-not-found.png';
                                        } else {
                                            $album_cover = 'assets/images/img-not-found.png';
                                        }
                                        ?> 
                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 thumb">
                                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="<?php echo $albums['event_name'] ?>"
                                               data-image="<?php echo base_url() . $album_cover ?>"
                                               data-target="">
        
                                                <img class="img-thumbnail"
                                                     src="<?php echo base_url() . $album_cover ?>"
                                                     alt="Album cover not set">
        
                                            </a>
                                            <div class="text-center mt-2 ">
                                                <div class="row ">
                                                    <h4 class="text-center col-md-10 pr-0 font-weight-bold "><?php echo $albums['event_name'] ?></h4>

                                                    <span class="float-right col-md-2 font-14 ">
                                                        <a href="<?php echo base_url() . 'admin/update_album_details/' . $albums['id'] ?>" data-target="tooltip " title="Edit "><i class="fa fa-pencil-alt "></i></a>
                                                    </span>
                                                </div>
                                                <h5 class="text-center mr-4"><?php echo $albums['total_album_phts'] ?> Photos</h5>
                                                <label class="jtoggler-wrapper" title="">
                                                    <span >Unpublish</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <input type="checkbox" id="<?php echo $albums['id'] ?>" <?php echo ($albums['publish_status'] == 1) ? 'checked' : ''; ?> class="jtoggler">
                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                    <span>Publish</span>

                                                </label>
                                            </div>
                                        </div>

    <?php } ?>     
                                    <!--</div>-->
                                    <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="image-gallery-title"></h4>
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <img id="image-gallery-image" class="img-responsive col-md-12" src="">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i class="fa fa-arrow-left"></i>
                                                    </button>
                                                    <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i class="fa fa-arrow-right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                       
<?php } else { ?>
                                <div class="form-body">
                                    <!--<hr class="m-t-0 m-b-40">-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="text-center margin-top-10 ">Album not exist</div>
                                        </div>
                                    </div>
                                </div>
<?php } ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script>



    let modalId = $('#image-gallery');

    $(document)
            .ready(function () {

                loadGallery(true, 'a.thumbnail');

                //This function disables buttons when needed
                function disableButtons(counter_max, counter_current) {
                    $('#show-previous-image, #show-next-image')
                            .show();
                    if (counter_max === counter_current) {
                        $('#show-next-image')
                                .hide();
                    } else if (counter_current === 1) {
                        $('#show-previous-image')
                                .hide();
                    }
                }

                function loadGallery(setIDs, setClickAttr) {
                    let current_image,
                            selector,
                            counter = 0;

                    $('#show-next-image, #show-previous-image')
                            .click(function () {
                                if ($(this)
                                        .attr('id') === 'show-previous-image') {
                                    current_image--;
                                } else {
                                    current_image++;
                                }

                                selector = $('[data-image-id="' + current_image + '"]');
                                updateGallery(selector);
                            });

                    function updateGallery(selector) {
                        let $sel = selector;
                        current_image = $sel.data('image-id');
                        $('#image-gallery-title')
                                .text($sel.data('title'));
                        $('#image-gallery-image')
                                .attr('src', $sel.data('image'));
                        disableButtons(counter, $sel.data('image-id'));
                    }

                    if (setIDs == true) {
                        $('[data-image-id]')
                                .each(function () {
                                    counter++;
                                    $(this)
                                            .attr('data-image-id', counter);
                                });
                    }
                    $(setClickAttr)
                            .on('click', function () {
                                updateGallery($(this));
                            });
                }
            });

    $(document)
            .keydown(function (e) {
                switch (e.which) {
                    case 37: // left
                        if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
                            $('#show-previous-image')
                                    .click();
                        }
                        break;

                    case 39: // right
                        if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
                            $('#show-next-image')
                                    .click();
                        }
                        break;

                    default:
                        return;
                }
                e.preventDefault();
            });

    $(function () {
        $('.jtoggler').jtoggler();

    });
    $(document).on('jt:toggled', function (event, target) {

        console.log(event, target);
        console.info($(target).prop('checked'))
//        alert($('input[type="checkbox"]').attr('id'))
        publish($(target).attr('id'), $(target).prop('checked'));
    });

    $(document).on('jt:toggled:multi', function (event, target) {
        console.log(event, target);
        console.info($(target).parent().index())
    });

// toggle button
    ;
    (function ($, window, document, undefined) {

        "use strict";

        var pluginName = "jtoggler",
                defaults = {
                    className: "",
                };

        function Toggler(element, options) {
            this.element = element;

            this.settings = $.extend({}, defaults, options);
            this._defaults = defaults;
            this._name = pluginName;
            this.init();
            this.events();
        }

        $.extend(Toggler.prototype, {
            init: function () {
                var $element = $(this.element);
                if ($element.data('jtmulti-state') != null) {
                    this.generateThreeStateHTML();
                } else {
                    this.generateTwoStateHTML();
                }
            },
            events: function () {
                var $element = $(this.element);

                $element.on('change', this, function (event) {
                    $(document).trigger('jt:toggled', [event.target]);
                })

                if (!$element.prop('disabled')) {
                    var $control = $element.next('.jtoggler-control');
                    $control
                            .find('.jtoggler-radio')
                            .on('click', this, function (event) {
                                $(this)
                                        .parents('.jtoggler-control')
                                        .find('.jtoggler-btn-wrapper')
                                        .removeClass('is-active');

                                $(this)
                                        .parent()
                                        .addClass('is-active');

                                if ($(event.currentTarget).parent().index() === 2) {
                                    $control.addClass('is-fully-active');
                                } else {
                                    $control.removeClass('is-fully-active');
                                }

                                $(document).trigger('jt:toggled:multi', [event.target]);
                            });
                }
            },
            generateTwoStateHTML: function () {
                var $element = $(this.element);

                var $wrapper = $('<label />', {
                    class: $.trim("jtoggler-wrapper " + this._defaults.className),
                });
                var $control = $('<div />', {
                    class: 'jtoggler-control',
                });
                var $handle = $('<div />', {
                    class: 'jtoggler-handle',
                });

                $control.prepend($handle);
                $element.wrap($wrapper).after($control);

                if ($element.data('jtlabel')) {
                    var $label = $('<div />', {
                        class: 'jtoggler-label',
                        text: $element.data('jtlabel'),
                    });
                    $control.after($label);
                }

            },
            generateThreeStateHTML: function () {
                var $element = $(this.element);

                var $wrapper = $('<div />', {
                    class: $.trim("jtoggler-wrapper jtoggler-wrapper-multistate " + this._defaults.className),
                });
                var $control = $('<div />', {
                    class: 'jtoggler-control',
                });
                var $handle = $('<div />', {
                    class: 'jtoggler-handle',
                });
                for (var i = 0; i < 3; i++) {
                    var $label = $('<label />', {
                        class: 'jtoggler-btn-wrapper',
                    });
                    var $btn = $('<input />', {
                        type: 'radio',
                        name: 'options',
                        class: 'jtoggler-radio',
                    });

                    $label.append($btn);
                    $control.prepend($label);
                }
                $control.append($handle);
                $element.wrap($wrapper).after($control);
                $control.find('.jtoggler-btn-wrapper:first').addClass('is-active');

            },
        });

        $.fn[ pluginName ] = function (options) {
            return this.each(function () {
                if (!$.data(this, "plugin_" + pluginName)) {
                    $.data(this, "plugin_" +
                            pluginName, new Toggler(this, options));
                }
            });
        };

    })(jQuery, window, document);
//----------------------- publish album---------------------------------------------------
    function publish(albumid, status)
    {
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Photogallery/publish_album',
            data: {'album_id': albumid, 'status': status},
            success: function (data) {

                var parsed = $.parseJSON(data);
                $(".messenger-message").remove();
                if (parsed.returnstatus == '0')
                {
                    showSuccess(parsed.failed);
                } else if (parsed.returnstatus == '1') {
                    showSuccess(parsed.success);
                }
            }
        });
    }
</script>
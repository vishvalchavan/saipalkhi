<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Photogallery extends MY_Controller {

    public function __construct() {
        parent::__construct();

//        if (!$this->ion_auth->logged_in()) {
//            redirect('admin', 'refresh');
//        }
        // if ($this->session->userdata('user_type') != 'admin') {
        //     redirect($_SERVER['HTTP_REFERER']);
        // }
//        var_dump(method_exists($this, $method));die;

        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation', 'subquery', 'user_agent'));
        $this->load->model(array('Sevasamiti_model', 'DindiModel', 'Padyatri_model', 'Year', 'Devotee', 'Photogallary_model', 'language'));
        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('Auth');
    }

    public function _remap($method, $params = array()) {
        $method = $method;
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else {
            if ($this->agent->is_referral()) {
                $last_Url = $this->agent->referrer();
            } else {
                $last_Url = $this->session->userdata('previous_url');
            }
            redirect($last_Url);
//                   show_404();                   
        }
    }

    public function index() {
         if (!$this->ion_auth->logged_in()) {
             redirect('admin', 'refresh');
         }
         if (!$this->ion_auth->is_admin()) {             
             redirect('home', 'refresh');
         }
        $this->session->set_userdata('previous_url', current_url());
        $user_id = $this->session->userdata('user_id');
        $year = $this->Year->get_year_id(date('Y'));
        $data['album_list'] = $this->Photogallary_model->get_all_albums($year);
        //var_dump($data);die;

        $data['dataHeader']['title'] = 'Albums';
//        var_dump($data);
        $this->template->set_master_template('template.php');
        $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
        $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
        $this->template->write_view('content', 'Photogallery/photogallery', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'snippets/footer', '', TRUE);
        $this->template->render();
    }

    public function create_album() {
         if (!$this->ion_auth->logged_in()) {
             redirect('admin', 'refresh');
         }
         if (!$this->ion_auth->is_admin()) {
             redirect('home', 'refresh');
         }
        $this->session->set_userdata('previous_url', current_url());
        $user_id = $this->session->userdata('user_id');

        $data['dataHeader']['title'] = 'Add Album';
//        echo "13132";
        $this->template->set_master_template('template.php');
        $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
        $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
        $this->template->write_view('content', 'Photogallery/create_album', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'snippets/footer', '', TRUE);
        $this->template->render();
    }

    public function add_album_details() {
         if (!$this->ion_auth->logged_in()) {
             redirect('admin', 'refresh');
         }
         if (!$this->ion_auth->is_admin()) {
             redirect('home', 'refresh');
         }
        $this->session->set_userdata('previous_url', current_url());
        $user_id = $this->session->userdata('user_id');
        $year = $this->Year->get_year_id(date('Y'));

        $data['dataHeader']['title'] = 'Add Album Images';
        $this->template->set_master_template('template.php');

        if (isset($_POST["btnSubmit"])) {

            $counter = 0;
            $album_name = $this->input->post('album_title');
            $album_desc = $this->input->post('description');
            $album_status = 1;
            $decoded_img_data = array();
            $counter = $this->input->post('lastcount');
            $this->form_validation->set_rules('album_title', 'Album Title', 'required');
            $this->form_validation->set_rules('description', 'Description', 'required');
//            $this->form_validation->set_rules('enroll_year', 'Enrollment year', 'required');
            if ($this->form_validation->run() == TRUE) {
                //------$data=$this->input->post('hiddeninput_1');
                $directory_name = '';
                $album_dir_name = str_replace(array('/', ' '), array('_', '_'), $album_name);
                $img_name = '';
                //--------------------------------------If not exist create dir-----------------------------------------------------------
                if (is_dir(ALBUM_PATH)) {
                    if (!is_dir(ALBUM_PATH . '/' . $album_dir_name)) {
                        mkdir(ALBUM_PATH . '/' . $album_dir_name, 0777, TRUE);
                    }
                } else {
                    mkdir(ALBUM_PATH . '/' . $album_dir_name, 0777, TRUE);
                }
                //--------------------------------Create image and save-----------------------------------------------------------------                                    
                $album_dir_path = ALBUM_PATH . "/" . $album_dir_name;
                //                  $album_data['iscover_pic']=$this->input->post('cover_photo');
                $explode = '';
                if (!empty($this->input->post('cover_photo'))) {
                    $explode = explode("_", $this->input->post('cover_photo'));
                }

                $album_data = array();
                $checked = '';
                //--------------------------------add album title-----------------------------------------------------------------   
                $check = $this->Photogallary_model->if_album_exists($album_name, $year);
                if ($check != 0) {
                    $this->session->set_flashdata('error_msg', 'Album title already exists.');
//                    redirect('admin/add_album_details/1');
                    $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
                    $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
                    $this->template->write_view('content', 'Photogallery/add_album_images', (isset($this->data) ? $this->data : NULL), TRUE);
                } else {
                    $album_id = $this->Photogallary_model->add_update_album_title($album_name, $album_desc, $year);
//                    echo $album_id ;
                    for ($img = 1; $img <= $counter; $img++) {

                        if (!empty($this->input->post('hiddeninput_' . $img))) {

                            $imagedata = $this->input->post('hiddeninput_' . $img);

                            if (preg_match('/^data:image\/(\w+);base64,/', $imagedata, $type)) {
                                $imagedata = substr($imagedata, strpos($imagedata, ',') + 1);
                                $type = strtolower($type[1]); // jpg, png, gif 
                                if (in_array($type, ['jpg', 'jpeg', 'gif', 'png'])) {
                                    //                      throw new \Exception('valid image type');

                                    $img_name = str_replace(array('/', ' '), array('_', '_'), $this->input->post('title_' . $img));
                                    $album_data[$img]['img_description'] = $img_name;
                                    $img_name = ($img_name) ? $img_name : $img . "_" . strtotime(date("Y-m-d H:i:s"));
                                    $album_data[$img]['img_path'] = $album_dir_path . "/" . "$img_name.{$type}";
                                    if (!empty($explode)) {
                                        $checked = ($explode[1] == $img) ? 1 : 0;
                                    } else {
                                        $checked = '0';
                                    }
                                    $album_data[$img]['iscover_pic'] = $checked;

                                    $album_data[$img]['event_id'] = $album_id;
                                    $album_data[$img]['created_date'] = date("Y-m-d");
                                    $image_decode_data = base64_decode($imagedata);
                                    $decoded_img_data[$img]['decoded_img'] = $image_decode_data;
                                    if ($image_decode_data === false) {
//----------------------------------------------------throw new \Exception('base64_decode failed');
                                    }

//----------------------------------------------------------throw new \Exception('invalid image type'); --else
                                }
//-------------------------------------------------- ---preg match----------------------------------else
                            } else {
                                //                      throw new \Exception('did not match data URI with image data');
                            }
//----------------------------- ---put images----------------------------------else                        
                            // file_put_contents($album_dir_path . "/" . "$img_name.{$type}", $image_decode_data);
                        }
                    }

                    //--------------------------------add album details and images-----------------------------------------------------------------  

                    if ($album_id > 0) {
                        $add_album_images = $this->Photogallary_model->add_album_details($album_id, $album_data, $counter, $decoded_img_data);
                        $this->session->set_flashdata('success_msg', 'Album Successfully added.');
                        redirect('admin/album');
                        //                        file_put_contents($album_dir_path."/". "$img_name.{$type}", $data);
                        //                      $last_image=file_get_contents(base_url().$album_dir_path."/",$img_name.".".$type);
                    } else {
                        $this->session->set_flashdata('error_msg', 'Failed to add album.');
//                    redirect('admin/add_album_details/1');
                        $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
                        $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
                        $this->template->write_view('content', 'Photogallery/add_album_images', (isset($this->data) ? $this->data : NULL), TRUE);
                    }
                    //----------------------------- alreary exist -----------------------------------
                }
                //----------------------------- validtion else -----------------------------------
            } else {
                $this->session->set_flashdata('error_msg', 'Something went wrong! Please try again.');
                redirect('admin/add_album_details');
            }
            //----------------------------- submit action else -----------------------------------            
        } else {
            $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
            $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
            $this->template->write_view('content', 'Photogallery/add_album_images', (isset($this->data) ? $this->data : NULL), TRUE);
        }
        $this->template->write_view('footer', 'snippets/footer', '', TRUE);
        $this->template->render();
    }

    public function update_album_details($album_id) {
         if (!$this->ion_auth->logged_in()) {
             redirect('admin', 'refresh');
         }
         if (!$this->ion_auth->is_admin()) {
             redirect('home', 'refresh');
         }

        $user_id = $this->session->userdata('user_id');
        $year = $this->Year->get_year_id(date('Y'));
        //$check = $this->Photogallary_model->if_album_exists($album_name, $year);
        $data['album_details'] = $this->Photogallary_model->get_albumdetails_by_albumid($album_id, $year);

        $data['dataHeader']['title'] = 'Update Album';
        //        echo "13132";
        //        --=====-=-=------------------------------===================================================================================/
        $this->template->set_master_template('template.php');

        if (isset($_POST["btnSubmit"])) {
//                    var_dump($_POST);die;
            $counter = 0;
            $old_imgid = $this->input->post('old_imgid');
            $album_id = ($album_id) ? $album_id : $this->input->post('albumid');
            $album_name = $this->input->post('album_title');
            $album_desc = $this->input->post('description');
            $album_status = 1;
            $decoded_img_data = array();
            $counter = $this->input->post('lastcount');
            $this->form_validation->set_rules('album_title', 'Album Title', 'required');
            $this->form_validation->set_rules('description', 'Description', 'required');
            if (!empty($this->input->post('cover_photo'))) {
                $explode = explode("_", $this->input->post('cover_photo'));
            }
            if (isset($explode[0]) && $explode[0] == "oldchk") {
                if (!empty($this->input->post('old_coverimgid'))) {
                    $old_coverimgid = explode(",", $this->input->post('old_coverimgid'));
                }
            } else {
                $old_coverimgid[0] = $album_id;
                $old_coverimgid[1] = '0';
            }
            $setcover = $this->Photogallary_model->change_img_details($old_coverimgid[1], $old_coverimgid[0], 'cover');
            if ($this->form_validation->run() == TRUE) {
                $directory_name = '';
                $album_dir_name = str_replace(array('/', ' '), array('_', '_'), $album_name);
                $img_name = '';

                //--------------------------------------If not exist create dir-----------------------------------------------------------
                if (is_dir(ALBUM_PATH)) {
                    if (!is_dir(ALBUM_PATH . '/' . $album_dir_name)) {
                        mkdir(ALBUM_PATH . '/' . $album_dir_name, 0777, TRUE);
                    }
                } else {
                    mkdir(ALBUM_PATH . '/' . $album_dir_name, 0777, TRUE);
                }

                //--------------------------------Create image and save-----------------------------------------------------------------                                    
                $album_dir_path = ALBUM_PATH . "/" . $album_dir_name;
                //                  $album_data['iscover_pic']=$this->input->post('cover_photo');
                $explode = '';
                if (!empty($this->input->post('cover_photo'))) {
                    $explode = explode("_", $this->input->post('cover_photo'));
                }


                $album_data = array();
                $checked = '';
                //--------------------------------add album title-----------------------------------------------------------------   
                $check = $this->Photogallary_model->if_album_exists($album_name, $year);

                if ($check != 0 && $check != $album_id) {

                    $this->session->set_flashdata('error_msg', 'Album title already exists.');
                    //                    redirect('admin/add_album_details/1');
                    $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
                    $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
                    $this->template->write_view('content', 'Photogallery/update_album_details', (isset($this->data) ? $this->data : NULL), TRUE);
                } else {
                    $album_id = $this->Photogallary_model->add_update_album_title($album_name, $album_desc, $year, $album_id);

                    for ($img = 1; $img <= $counter; $img++) {

                        if (!empty($this->input->post('hiddeninput_' . $img))) {

                            $imagedata = $this->input->post('hiddeninput_' . $img);

                            if (preg_match('/^data:image\/(\w+);base64,/', $imagedata, $type)) {
                                $imagedata = substr($imagedata, strpos($imagedata, ',') + 1);
                                $type = strtolower($type[1]); // jpg, png, gif 
                                if (in_array($type, ['jpg', 'jpeg', 'gif', 'png'])) {
                                    //                      throw new \Exception('valid image type');

                                    $img_name = str_replace(array('/', ' '), array('_', '_'), $this->input->post('title_' . $img));
                                    $album_data[$img]['img_description'] = $img_name;
                                    $img_name = ($img_name) ? $img_name : $img . "_" . strtotime(date("Y-m-d H:i:s"));
                                    $album_data[$img]['img_path'] = $album_dir_path . "/" . "$img_name.{$type}";
                                    if (!empty($explode) && $explode[0] == "chk") {
                                        $checked = ($explode[1] == $img) ? 1 : 0;
                                    } else {
                                        $checked = '0';
                                    }
                                    $album_data[$img]['iscover_pic'] = $checked;

                                    $album_data[$img]['event_id'] = $album_id;
                                    $album_data[$img]['created_date'] = date("Y-m-d");
                                    $image_decode_data = base64_decode($imagedata);
                                    $decoded_img_data[$img]['decoded_img'] = $image_decode_data;
                                    if ($image_decode_data === false) {
                                        //----------------------------------------------------throw new \Exception('base64_decode failed');
                                    }

                                    //----------------------------------------------------------throw new \Exception('invalid image type'); --else
                                }
                                //-------------------------------------------------- ---preg match----------------------------------else
                            } else {
                                //                      throw new \Exception('did not match data URI with image data');
                            }
                            //----------------------------- ---put images----------------------------------else                        
                            // file_put_contents($album_dir_path . "/" . "$img_name.{$type}", $image_decode_data);
                        }
                    }

                    //--------------------------------add album details and images-----------------------------------------------------------------  
//                        var_dump($album_data);die;
                    if ($album_id > 0) {
                        $add_album_images = $this->Photogallary_model->add_album_details($album_id, $album_data, $counter, $decoded_img_data);
//                           var_dump($old_imgid[0]);
                        if (!empty($old_imgid)) {
                            $old_imgid = explode(",", $old_imgid[0]);
                            $title = '';
                            for ($old = 0; $old < count($old_imgid); $old++) {
                                $remove = $this->Photogallary_model->change_img_details($old_imgid[$old], $title, 'remove');
                            }
                        }
                        $this->session->set_flashdata('success_msg', 'Album Successfully updated.');
                        redirect('admin/update_album_details/' . $album_id);
                        //                        file_put_contents($album_dir_path."/". "$img_name.{$type}", $data);
                        //                      $last_image=file_get_contents(base_url().$album_dir_path."/",$img_name.".".$type);
                    } else {
                        $this->session->set_flashdata('error_msg', 'Failed to update album.');
                        //                    redirect('admin/add_album_details/1');
                        $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
                        $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
                        $this->template->write_view('content', 'Photogallery/update_album_details', (isset($this->data) ? $this->data : NULL), TRUE);
                    }
                    //----------------------------- alreary exist -----------------------------------
                }
                //----------------------------- validtion else -----------------------------------
            } else {
                redirect('admin/update_album_details/' . $album_id);
            }
            //----------------------------- submit action else -----------------------------------            
        } else {
            $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
            $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
            $this->template->write_view('content', 'Photogallery/update_album_details', (isset($this->data) ? $this->data : NULL), TRUE);
        }

        $this->template->write_view('footer', 'snippets/footer', '', TRUE);
        $this->template->render();
        //        -------------------------------------------------------------/////////////////////////////////////////////////////////////
    }

//    ------------------------update old image title----------------------------------------/
    public function change_imagetitle() {
        $check = '';
        $img_id = $this->input->post('imgid');
        $action = $this->input->post('action');
        $title = $this->input->post('title');
        $year = $this->Year->get_year_id(date('Y'));
        if ($action == "update") {
            $check = $this->Photogallary_model->if_image_exists($title, $year);
        }
        if ($check != 0) {
//                    $this->session->set_flashdata('error_msg', 'Album title already exists.');
            $returnMsg = array('failed' => 'Image title already exists.', 'returnstatus' => '0');
            echo json_encode($returnMsg);
            exit;
        } else {
            $returnStatus = $this->Photogallary_model->change_img_details($img_id, $title, $action);
            echo json_encode($returnStatus);
            exit;
        }
    }

//    ------------------------ delete images----------------------------------------/
    public function delete_album_image() {
        if ($this->input->post('dindi_id')) {
            $this->DindiModel->delete_dindi($this->input->post('dindi_id'));
            if ($this->db->affected_rows() > 0) {
                $this->session->set_flashdata('success_msg', 'Dindi deleted successfully.');
                echo 'success';
            } else {
                $this->session->set_flashdata('error_msg', 'Failed to delete dindi.');
                echo 'failed';
            }
        }
    }

//    ------------------------ delete images----------------------------------------/    
    function publish_album() {
        $resonseStatus = '';
        $publish_id = $this->input->post('album_id');
        $status = ($this->input->post('status') == 'true') ? 1 : 0;
        $response = $this->Photogallary_model->change_publish_status($publish_id, $status);
        if ($response != 0 && $status == 1) {
            $resonseStatus = array('success' => 'Album successfully published.', 'returnstatus' => '1');
        } else if ($response != 0 && $status == 0) {
            $resonseStatus = array('failed' => 'Album unpublished successfully.', 'returnstatus' => '0');
        } else if ($response == 0) {
            $resonseStatus = array('failed' => 'Album failed to publish.', 'returnstatus' => '0');
        }
        echo json_encode($resonseStatus);
        exit;
    }

//------------------------------[[[[[[[[[[[[[[[[[[[[[[[[[ user side view ]]]]]]]]]]]]]]]]]]]]]]]]]------------------------------------
    public function photo_gallery() {
        if ($this->ion_auth->is_admin()) {
            $this->ion_auth->logout();
            $this->session->unset_userdata('user_id');
        }
//        $user_id = $this->session->userdata('user_id');
        $year = $this->Year->get_year_id(date('Y'));
        $data['album_list'] = $this->Photogallary_model->get_all_albums($year);
        $data['dataHeader']['title'] = 'Albums';
        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'Photogallery/user/album', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }

    public function photo_gallery_inner($album_id) {
        if ($this->ion_auth->is_admin()) {
            $this->ion_auth->logout();
            $this->session->unset_userdata('user_id');
        }
//        $user_id = $this->session->userdata('user_id');
        $year = $this->Year->get_year_id(date('Y'));
        //$check = $this->Photogallary_model->if_album_exists($album_name, $year);
        $data['album_details'] = $this->Photogallary_model->get_albumdetails_by_albumid($album_id, $year);

        $data['dataHeader']['title'] = 'Inner-Galary';
        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'Photogallery/user/album_details', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }

}
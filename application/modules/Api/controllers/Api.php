<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Api extends REST_Controller {

    function __construct() {

        parent::__construct();
        $this->load->database();
        $this->load->helper('string');
        $this->load->model('language');
        $this->load->library(array('numbertowords', 'm_pdf'));
// Configure limits on our controller methods
// Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 500; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function index_get() {
        $message = [
            'message' => 'Invalid method',
            'status' => 'Failed',
            'code' => '0',
        ];
        $this->set_response($message, REST_Controller::HTTP_OK);
    }

    public function index_getww() {
        $this->$message = [
            'message' => 'Invalid method',
            'status' => 'Failed',
            'code' => '0',
        ];
        $this->set_response($message, REST_Controller::HTTP_OK);
    }

    public function validate_token($token) {
        $is_user = $this->ApiModel->validate_token($token);
        if (count($is_user) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function create_user_post() {
        $username = $this->input->post('username');
        $password = base64_encode($this->input->post('password'));
        if (isset($username, $password)) {
            $token = random_string('alnum', 20);
            $data = array(
                'username' => $username,
                'password' => $password,
                'token' => $token,
                'created_date' => date("Y-m-d"),
                'isactive' => 1
            );
            $this->ApiModel->insert($data);
            if ($this->db->affected_rows() > 0) {
                $response = [
                    'token' => $token,
                    'message' => 'User created successfully',
                    'status' => 'success',
                    'code' => '200',
                ];
            } else {
                $response = [
                    'message' => 'Failed to create user',
                    'status' => 'failed',
                    'code' => '500',
                ];
            }
        } else {
            $response = [
                'message' => 'Incorrect arguments',
                'status' => 'failed',
                'code' => '500',
            ];
        }
        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function login_post() {
        $data = $this->post();
        $username = $data['username'];
        $password = $data['password'];
        if (isset($username, $password)) {
            if ($username == "atul" && $password == "atul123") {
                $response = [
                    'message' => 'Login successfully',
                    'status' => 'success',
                    'code' => '200',
                ];
            } else {
                $response = [
                    'message' => 'Invalid Credentials',
                    'status' => 'failed',
                    'code' => '500',
                ];
            }
        } else {
            $response = [
                'message' => 'Unauthorised request',
                'status' => 'failed',
                'code' => '500',
            ];
        }
        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function get_masterSyncData_post() {
        $token = $_SERVER['HTTP_TOKEN'];
//        $token = $this->head('token');
        $post = json_decode(trim(file_get_contents('php://input')), TRUE);
//        $post = $this->post();
        if (isset($token)) {
            $year = date('Y');
            $check_token = $this->validate_token($token);
            if ($check_token) {
                $data['dindi_details'] = $this->ApiModel->get_dindi_list();
                $data['dashboard_info'] = $this->ApiModel->get_dashboard_data();
                $data['config']['amount'] = '1500';
                $data['vehicle_info'] = $this->ApiModel->get_vehicleInfo($year);
                if (isset($post['timestamp']) && !empty($post['timestamp']) && strtolower($post['timestamp']) !== 'null') {
                    $data['devotee'] = $this->ApiModel->devotee_list($post['timestamp']);
                } else {
                    $data['devotee'] = $this->ApiModel->devotee_list();
                }
                $data['enroll_report_data'] = $this->ApiModel->get_enrollment_data();
                $response = [
                    'data' => $data,
                    'message' => 'Master Sync successfully',
                    'status' => 'success',
                    'code' => '200',
                ];
            } else {
                $response = [
                    'data' => $token,
                    'message' => 'Unauthorized user',
                    'status' => 'failed',
                    'code' => '500',
                ];
            }
        } else {
            $response = [
                'message' => 'Invalid Request',
                'status' => 'failed',
                'code' => '400',
            ];
        }
        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function sync_app_data_post() {
        $token = $_SERVER['HTTP_TOKEN'];
//        $token = $this->head('token');
        if (isset($token)) {
            $year = date('Y');
            $sabhasad_amount = 1500;
            $check_token = $this->validate_token($token);

            if ($check_token) {
                $data = json_decode(trim(file_get_contents('php://input')), TRUE);

                $createdindex = $updatedindex = $paymentindex = 0;
                $total_devotee = $total_padyatri = $sum_amt = 0;
                $responseData['created'] = array();
                $responseData['updated'] = array();
                $responseData['payment'] = array();
                $responseData['enroll_report_data'] = array();
                $year_id = $this->ApiModel->get_year_id(date('Y'));
//If New Devotee registered
                if (isset($data['created_records']) && !empty($data['created_records'])) {
                    foreach ($data['created_records'] as $new_devotee) {
                        $devotee_mobile = trim(str_replace('+', '', $new_devotee['mobile_no']));
                        $dpn = $new_devotee['dpn_no'];
                        $devotee_exist = $this->ApiModel->get_devotee($new_devotee['dpn_no']);
                        if (count($devotee_exist) == 0) {
                            $password = $this->ion_auth->hash_password('sairam');
                            $devotee_insert = array(
                                'group_id' => 2,
                                'ip_address' => $_SERVER['REMOTE_ADDR'],
                                'password' => $password,
                                'email' => $new_devotee['email_id'],
                                'created_on' => time(),
                                'active' => 1,
                                'date_of_joining' => date('Y-m-d', strtotime($new_devotee['date_of_joining'])),
                                'dpn_no' => $new_devotee['dpn_no'],
                                'firstname' => $new_devotee['first_name'],
                                'middlename' => $new_devotee['middle_name'],
                                'lastname' => $new_devotee['last_name'],
                                'birth_date' => date('Y-m-d', strtotime($new_devotee['date_of_birth'])),
                                'mobile' => trim(str_replace('+', '', $new_devotee['mobile_no'])),
                                'address' => $new_devotee['address'],
                                'city' => $new_devotee['city'],
                                'pincode' => $new_devotee['pincode'],
                                'gender' => $new_devotee['gender'],
                                'blood_group' => $new_devotee['blood_group'],
                                'relative_name' => $new_devotee['relative_name'],
                                'relative_mobile' => $new_devotee['relative_mobile'],
                                'registered_via' => 'app',
                                'isactive' => 1,
                                'modified_time' => date("Y-m-d H:i:s")
                            );
                            //Insert new Devotee 
                            $devotee_id = $this->ApiModel->insert_devotees($devotee_insert);
                            if (isset($devotee_id) && !empty($devotee_id)) {
                                $smstext = "Om Sai Ram!!\nThanks for registration!\nYour default password is - sairam\n- Sai Palkhi Samiti, Pune\nCall: 9860003639 / 7057451648 / 8600258390";
                                $sms_send_status = send_sms($devotee_mobile, $smstext);
                                $responseData['created'][$createdindex]['id'] = $devotee_id;
                                $responseData['created'][$createdindex]['devotee_id'] = $new_devotee['id'];
                                $createdindex++;
                                $total_devotee = $total_devotee + 1;
                                $pay_matched = 0;
                                if (isset($data['payment']) && !empty($data['payment'])) {
                                    foreach ($data['payment'] as $key => $payment) {
                                        if ($new_devotee['id'] == $payment['id']) {
                                            //If Devotee payment is accepted
                                            $pay_matched = 1;
                                            $padyatri_insert = array(
//                                                'dpn_no' => $new_devotee['dpn_no'],
                                                'devotee_id' => $devotee_id,
                                                'participation_year_id' => $year_id,
                                                'vehical_type' => $new_devotee['vehicle_type'],
                                                'vehicle_details' => $new_devotee['tampo_name'],
                                                'dindi_id' => $new_devotee['dindi_id'],
                                                'payment_date' => date('Y-m-d', strtotime($payment['payment_date'])),
                                                'payment_mode' => 'palakhibhavan',
                                                'payment_amount' => 1500,
                                                'payment_status' => 'success',
                                                'enrollment_via' => 'app',
                                                'badget_print' => (int) $new_devotee['is_badge_print'],
                                                'receipt_print' => (int) $new_devotee['is_receipt_print']
                                            );
                                            //Insert New padyatri
                                            $padytri_id = $this->ApiModel->insert_padyatri($padyatri_insert);
                                            if (isset($padytri_id) && !empty($padytri_id)) {
                                                $responseData['payment'][$paymentindex]['id'] = $devotee_id;
                                                $paymentindex++;
                                                $total_padyatri = $total_padyatri + 1;
                                                $sum_amt = $sum_amt + 1500;
                                                $this->ApiModel->update_devotee_paymentinfo($padytri_id, $devotee_id);
                                                if ($new_devotee['vehicle_type'] == 'Samiti') {
                                                    $this->ApiModel->vehicle_update($new_devotee['tampo_name'], $year_id);
                                                }
                                                $smstext = 'Om Sai Ram!!\nThanks for Sai Palkhi -' . date('Y') . ' registration\nYour DPN no is "' . $dpn . '"\nPlease collect your ID card from samiti members.\nSai Palkhi Samiti, Pune';
//                                                $smstext = "Om Sai Ram!!\nThanks for Sai-Palakhi " . date('Y') . " registration\nYour DPN no is $dpn\n-Sai Palakhi Samiti,Pune\nCall: 9860003639 / 7057451648 / 8600258390";
                                                $sms_send_status = send_sms($devotee_mobile, $smstext);
                                                if (isset($new_devotee['email_id']) && !empty($new_devotee['email_id'])) {
                                                    $this->send_receipt($new_devotee['first_name'], $new_devotee['middle_name'], $new_devotee['last_name'], $new_devotee['address'], $new_devotee['city'], $new_devotee['pincode'], $new_devotee['dpn_no'], $payment['payment_date'], $new_devotee['email_id']);
                                                }
                                            } else {
                                                $this->ApiModel->delete_devotee($devotee_id);
                                            }
                                            //remove dpn from payment array
                                            unset($data['payment'][$key]);
                                        }
                                    }
                                }
                                if ($pay_matched == 0) {
                                    //If payment not accepted then insert padyatri & make payment as failed
                                    $padyatri_insert = array(
//                                        'dpn_no' => $new_devotee['dpn_no'],
                                        'devotee_id' => $devotee_id,
                                        'participation_year_id' => $year_id,
                                        'vehical_type' => $new_devotee['vehicle_type'],
                                        'vehicle_details' => $new_devotee['tampo_name'],
                                        'dindi_id' => $new_devotee['dindi_id'],
                                        'payment_status' => 'failed',
                                        'enrollment_via' => 'app',
                                        'badget_print' => (int) $new_devotee['is_badge_print'],
                                        'receipt_print' => (int) $new_devotee['is_receipt_print']
                                    );
                                    //Insert New padyatri
                                    $padytri_id = $this->ApiModel->insert_padyatri($padyatri_insert);
                                    if (isset($padytri_id) && !empty($padytri_id)) {
                                        $this->ApiModel->update_devotee_paymentinfo($padytri_id, $devotee_id);
                                    } else {
                                        $this->ApiModel->delete_devotee($devotee_id);
                                    }
                                }
                            }
                        } else {
                            //If Devotee already exists
                            $responseData['created'][$createdindex]['id'] = $devotee_exist[0]['id'];
                            $responseData['created'][$createdindex]['devotee_id'] = $new_devotee['id'];
                            $createdindex++;
                            if (isset($data['payment']) && !empty($data['payment'])) {
                                foreach ($data['payment'] as $key => $payment) {
                                    if ($new_devotee['id'] == $payment['id']) {
                                        $responseData['payment'][$paymentindex]['id'] = $devotee_exist[0]['id'];
                                        $paymentindex++;
                                    }
                                }
                            }
                        }
                    }
                }
                //If Devotee record updated
                if (isset($data['updated_records']) && !empty($data['updated_records'])) {
                    foreach ($data['updated_records'] as $devotee) {
                        $devotees_id = $devotee['id'];
                        $dpn = $devotee['dpn_no'];
                        $devotee_mobile = trim(str_replace('+', '', $devotee['mobile_no']));
                        //Update Devotee details
                        $devotee_update = array(
                            'email' => $devotee['email_id'],
                            'dpn_no' => $devotee['dpn_no'],
                            'firstname' => $devotee['first_name'],
                            'middlename' => $devotee['middle_name'],
                            'lastname' => $devotee['last_name'],
                            'birth_date' => date('Y-m-d', strtotime($devotee['date_of_birth'])),
                            'mobile' => trim(str_replace('+', '', $devotee['mobile_no'])),
                            'address' => $devotee['address'],
                            'city' => $devotee['city'],
                            'pincode' => $devotee['pincode'],
                            'gender' => $devotee['gender'],
                            'blood_group' => $devotee['blood_group'],
                            'relative_name' => $devotee['relative_name'],
                            'relative_mobile' => $devotee['relative_mobile'],
                            'modified_time' => date("Y-m-d H:i:s")
                        );
                        $this->ApiModel->update_devotee($devotees_id, $devotee_update);
                        $responseData['updated'][$updatedindex]['id'] = $devotees_id;
                        $updatedindex++;
                        $payment_matched = 0;
                        if (isset($data['payment']) && !empty($data['payment'])) {
                            foreach ($data['payment'] as $key => $payment) {
                                if ($devotee['id'] == $payment['id']) {
                                    //If Devotee's payment is accepted
                                    $padyatri_exists = $this->ApiModel->get_padyatri($devotee['id'], $year_id);
                                    if (count($padyatri_exists) == 0) {
                                        $payment_matched = 1;
                                        $padyatri_insert = array(
//                                            'dpn_no' => $devotee['dpn_no'],
                                            'devotee_id' => $devotees_id,
                                            'participation_year_id' => $year_id,
                                            'vehical_type' => $devotee['vehicle_type'],
                                            'vehicle_details' => $devotee['tampo_name'],
                                            'dindi_id' => $devotee['dindi_id'],
                                            'payment_date' => date('Y-m-d', strtotime($payment['payment_date'])),
                                            'payment_mode' => 'palakhibhavan',
                                            'payment_amount' => 1500,
                                            'payment_status' => 'success',
                                            'enrollment_via' => 'app',
                                            'badget_print' => (int) $devotee['is_badge_print'],
                                            'receipt_print' => (int) $devotee['is_receipt_print']
                                        );
                                        $padyatri_id = $this->ApiModel->insert_padyatri($padyatri_insert);
                                        if (isset($padyatri_id)) {
                                            $responseData['payment'][$paymentindex]['id'] = $devotee['id'];
                                            $paymentindex++;
                                            $total_padyatri = $total_padyatri + 1;
                                            $sum_amt = $sum_amt + 1500;
                                            $this->ApiModel->update_devotee_paymentinfo($padyatri_id, $devotees_id);
                                            if ($devotee['vehicle_type'] == 'Samiti') {
                                                $this->ApiModel->vehicle_update($devotee['tampo_name'], $year_id);
                                            }
                                            $smstext = 'Om Sai Ram!!\nThanks for Sai Palkhi -' . date('Y') . ' registration\nYour DPN no is "' . $dpn . '"\nPlease collect your ID card from samiti members.\nSai Palkhi Samiti, Pune';
//                                            $smstext = "Om Sai Ram!!\nThanks for Sai-Palakhi " . date('Y') . " registration\nYour DPN no is $dpn\n-Sai Palakhi Samiti,Pune\nCall: 9860003639 / 7057451648 / 8600258390";
                                            $sms_send_status = send_sms($devotee_mobile, $smstext);
                                            if (isset($devotee['email_id']) && !empty($devotee['email_id'])) {
                                                $this->send_receipt($devotee['first_name'], $devotee['middle_name'], $devotee['last_name'], $devotee['address'], $devotee['city'], $devotee['pincode'], $devotee['dpn_no'], $payment['payment_date'], $devotee['email_id']);
                                            }
                                            unset($data['payment'][$key]);
                                        }
                                    } else {
                                        //If Padyatri record already exists then update the record with payment info.
                                        $payment_matched = 1;
                                        $padyatri_update = array(
                                            'vehical_type' => $devotee['vehicle_type'],
                                            'vehicle_details' => $devotee['tampo_name'],
                                            'dindi_id' => $devotee['dindi_id'],
                                            'payment_date' => date('Y-m-d', strtotime($payment['payment_date'])),
                                            'payment_mode' => 'palakhibhavan',
                                            'payment_amount' => 1500,
                                            'payment_status' => 'success',
                                            'enrollment_via' => 'app',
                                            'badget_print' => (int) $devotee['is_badge_print'],
                                            'receipt_print' => (int) $devotee['is_receipt_print']
                                        );
                                        $this->ApiModel->update_padyatri($padyatri_exists[0]['id'], $padyatri_update);
                                        if ($this->db->affected_rows() > 0) {
                                            $responseData['payment'][$paymentindex]['id'] = $devotee['id'];
                                            $paymentindex++;
                                            $total_padyatri = $total_padyatri + 1;
                                            $sum_amt = $sum_amt + 1500;
                                            $this->ApiModel->update_devotee_paymentinfo($padyatri_exists[0]['id'], $devotees_id);
                                            if ($devotee['vehicle_type'] == 'Samiti') {
                                                $this->ApiModel->vehicle_update($devotee['tampo_name'], $year_id);
                                            }
                                            $smstext = 'Om Sai Ram!!\nThanks for Sai Palkhi -' . date('Y') . ' registration\nYour DPN no is "' . $dpn . '"\nPlease collect your ID card from samiti members.\nSai Palkhi Samiti, Pune';
//                                            $smstext = "Om Sai Ram!!\nThanks for Sai-Palakhi " . date('Y') . " registration\nYour DPN no is $dpn\n-Sai Palakhi Samiti,Pune\nCall: 9860003639 / 7057451648 / 8600258390";
                                            $sms_send_status = send_sms($devotee_mobile, $smstext);
                                            if (isset($devotee['email_id']) && !empty($devotee['email_id'])) {
                                                $this->send_receipt($devotee['first_name'], $devotee['middle_name'], $devotee['last_name'], $devotee['address'], $devotee['city'], $devotee['pincode'], $devotee['dpn_no'], $payment['payment_date'], $devotee['email_id']);
                                            }
                                            unset($data['payment'][$key]);
                                        }
                                    }
                                }
                            }
                        }
                        if ($payment_matched == 0) {
                            //If Devotees payment is not accepted then just update the padyatri record.
                            $padyatri_info = $this->ApiModel->get_padyatri($devotee['id'], $year_id);
                            if (isset($padyatri_info) && !empty($padyatri_info)) {
                                $padyatri_update = array(
                                    'vehical_type' => $devotee['vehicle_type'],
                                    'vehicle_details' => $devotee['tampo_name'],
                                    'dindi_id' => $devotee['dindi_id'],
                                    'badget_print' => (int) $devotee['is_badge_print'],
                                    'receipt_print' => (int) $devotee['is_receipt_print']
                                );
                                $this->ApiModel->update_padyatri($padyatri_info[0]['id'], $padyatri_update);

                                $upmatched = 0;
                                foreach ($responseData['updated'] as $up) {
                                    if ($up['id'] == $devotee['id']) {
                                        $upmatched = 1;
                                    }
                                }
                                if ($upmatched == 0) {
                                    $responseData['updated'][$updatedindex]['id'] = $devotee['id'];
                                    $updatedindex++;
                                }
                                if ($padyatri_info[0]['payment_status'] == 'success') {
                                    if ($padyatri_info[0]['vehicle_type'] != $devotee['vehicle_type']) {
                                        if ($padyatri_info[0]['vehicle_type'] == 'Self' && $devotee['vehicle_type'] == 'Samiti') {
                                            $this->ApiModel->vehicle_update($devotee['tampo_name'], $year_id);
                                        } else {
                                            $this->ApiModel->remove_assigned_vehicle($padyatri_info[0]['vehicle_details'], $year_id);
                                        }
                                    }
                                }
                            } else {
                                $padyatri_insert = array(
                                    'devotee_id' => $devotees_id,
                                    'participation_year_id' => $year_id,
                                    'vehical_type' => $devotee['vehicle_type'],
                                    'vehicle_details' => $devotee['tampo_name'],
                                    'dindi_id' => $devotee['dindi_id'],
//                                    'payment_date' => date('Y-m-d', strtotime($payment['payment_date'])),
                                    'payment_date' => date('Y-m-d'),
                                    'payment_mode' => 'palakhibhavan',
                                    'payment_status' => 'failed',
                                    'enrollment_via' => 'app',
                                    'badget_print' => (int) $devotee['is_badge_print'],
                                    'receipt_print' => (int) $devotee['is_receipt_print']
                                );
                                $padyatri_id = $this->ApiModel->insert_padyatri($padyatri_insert);
                                if (isset($padyatri_id)) {
                                    $this->ApiModel->update_devotee_paymentinfo($padyatri_id, $devotees_id);
                                }
                            }
                        }
                    }
                }
                //If only Payment accepted for Devotee then update Payment info.
                if (isset($data['payment']) && !empty($data['payment'])) {
                    foreach ($data['payment'] as $key => $payment) {
                        $padyatri_exists = $this->ApiModel->get_padyatri($payment['id'], $year_id);
                        if (count($padyatri_exists) == 0) {
                            //If Padyatri record not exists the insert new record with payment info.
                            $devotee_info = $this->ApiModel->get_devotee_info($payment['id']);
                            if (isset($devotee_info) && !empty($devotee_info)) {
                                $dpn = $devotee_info[0]['dpn_no'];
                                $devotee_mobile = $devotee_info[0]['mobile'];
                                $payment_matched = 1;
                                $padyatri_insert = array(
//                                    'dpn_no' => $devotee_info[0]['dpn_no'],
                                    'devotee_id' => $devotee_info[0]['id'],
                                    'participation_year_id' => $year_id,
                                    'vehical_type' => $devotee_info[0]['vehicle_type'],
                                    'vehicle_details' => $devotee_info[0]['tampo_name'],
                                    'dindi_id' => $devotee_info[0]['dindi_id'],
                                    'payment_date' => date('Y-m-d', strtotime($payment['payment_date'])),
                                    'payment_mode' => 'palakhibhavan',
                                    'payment_amount' => 1500,
                                    'payment_status' => 'success',
                                    'enrollment_via' => 'app',
//                                    'badget_print' => (int) $payment['is_badge_print'],
//                                    'receipt_print' => (int) $payment['is_receipt_print']
                                );
                                $padyatri_id = $this->ApiModel->insert_padyatri($padyatri_insert);
                                if (isset($padyatri_id)) {
                                    $responseData['payment'][$paymentindex]['id'] = $payment['id'];
                                    $paymentindex++;
                                    $total_padyatri = $total_padyatri + 1;
                                    $sum_amt = $sum_amt + 1500;
                                    $this->ApiModel->update_devotee_paymentinfo($padyatri_id, $devotee_info[0]['id'], date('Y-m-d H:i:s'));
                                    if ($devotee_info[0]['vehicle_type'] == 'Samiti') {
                                        $this->ApiModel->vehicle_update($devotee_info[0]['tampo_name'], $year_id);
                                    }
                                    $smstext = 'Om Sai Ram!!\nThanks for Sai Palkhi -' . date('Y') . ' registration\nYour DPN no is "' . $dpn . '"\nPlease collect your ID card from samiti members.\nSai Palkhi Samiti, Pune';
//                                    $smstext = "Om Sai Ram!!\nThanks for Sai-Palakhi " . date('Y') . " registration\nYour DPN no is $dpn\n-Sai Palakhi Samiti,Pune\nCall: 9860003639 / 7057451648 / 8600258390";
                                    $sms_send_status = send_sms($devotee_mobile, $smstext);
                                    if (isset($devotee_info[0]['email']) && !empty($devotee_info[0]['email'])) {
                                        $this->send_receipt($devotee_info[0]['first_name'], $devotee_info[0]['middle_name'], $devotee_info[0]['last_name'], $devotee_info[0]['address'], $devotee_info[0]['city'], $devotee_info[0]['pincode'], $devotee_info[0]['dpn_no'], $payment['payment_date'], $devotee_info[0]['email']);
                                    }
                                }
                            }
                        } else {
                            if (strtolower($padyatri_exists[0]['payment_status']) == 'success') {
                                //If Record already exists & its payment is accepted then send synced status to app.
                                $responseData['payment'][$paymentindex]['id'] = $payment['id'];
                                $paymentindex++;
                            } else {
                                //If Record already exists & payment is pending then Update payment info.
                                $devotee_info = $this->ApiModel->get_devotee_info($payment['id']);

                                if (isset($devotee_info) && !empty($devotee_info)) {
                                    $dpn = $devotee_info[0]['dpn_no'];
                                    $devotee_mobile = $devotee_info[0]['mobile'];
                                    $padyatri_update = array(
                                        'payment_date' => date('Y-m-d', strtotime($payment['payment_date'])),
                                        'payment_mode' => 'palakhibhavan',
                                        'payment_amount' => 1500,
                                        'payment_status' => 'success',
                                        'enrollment_via' => 'app',
//                                        'badget_print' => (int) $payment['is_badge_print'],
//                                        'receipt_print' => (int) $payment['is_receipt_print']
                                    );
                                    $this->ApiModel->update_padyatri($padyatri_exists[0]['id'], $padyatri_update);
                                    if ($this->db->affected_rows() > 0) {
                                        $responseData['payment'][$paymentindex]['id'] = $payment['id'];
                                        $paymentindex++;
                                        $total_padyatri = $total_padyatri + 1;
                                        $sum_amt = $sum_amt + 1500;
                                        $this->ApiModel->update_devotee_paymentinfo($padyatri_exists[0]['id'], $devotee_info[0]['id'], date('Y-m-d H:i:s'));
                                        if (strtolower($padyatri_exists[0]['vehicle_type']) == 'samiti') {
                                            $this->ApiModel->vehicle_update($padyatri_exists[0]['vehicle_details'], $year_id);
                                        }
                                        $smstext = 'Om Sai Ram!!\nThanks for Sai Palkhi -' . date('Y') . ' registration\nYour DPN no is "' . $dpn . '"\nPlease collect your ID card from samiti members.\nSai Palkhi Samiti, Pune';
//                                        $smstext = "Om Sai Ram!!\nThanks for Sai-Palakhi " . date('Y') . " registration\nYour DPN no is $dpn\n-Sai Palakhi Samiti,Pune\nCall: 9860003639 / 7057451648 / 8600258390";
                                        $sms_send_status = send_sms($devotee_mobile, $smstext);
                                        if (isset($devotee_info[0]['email']) && !empty($devotee_info[0]['email'])) {
                                            $this->send_receipt($devotee_info[0]['first_name'], $devotee_info[0]['middle_name'], $devotee_info[0]['last_name'], $devotee_info[0]['address'], $devotee_info[0]['city'], $devotee_info[0]['pincode'], $devotee_info[0]['dpn_no'], $payment['payment_date'], $devotee_info[0]['email']);
                                        }
//                                    unset($data['payment'][$key]);
                                    }
                                }
                            }
                        }
                    }
                }
                if (isset($data['enroll_report_data']) && !empty($data['enroll_report_data'])) {
                    $enrollment_index = 0;
                    foreach ($data['enroll_report_data'] as $enroll) {
                        $if_exists = $this->ApiModel->enrollment_exists($enroll['report_date'], $enroll['token']);
                        if (count($if_exists) == 0) {
                            if ($this->ApiModel->insert_enrollment($sabhasad_amount, $enroll)) {
                                $responseData['enroll_report_data'][$enrollment_index]['token'] = $enroll['token'];
                                $responseData['enroll_report_data'][$enrollment_index]['report_date'] = $enroll['report_date'];
                                $enrollment_index++;
                            }
                        } else {
                            if ($this->ApiModel->update_enrollment($sabhasad_amount, $enroll)) {
                                $responseData['enroll_report_data'][$enrollment_index]['token'] = $enroll['token'];
                                $responseData['enroll_report_data'][$enrollment_index]['report_date'] = $enroll['report_date'];
                                $enrollment_index++;
                            }
                        }
                    }
                }
                if ($total_devotee > 0 || $total_padyatri > 0 || $sum_amt > 0) {
                    $this->ApiModel->update_dashboardData($total_devotee, $total_padyatri, $sum_amt, $year);
                }
                if ((count($responseData['created']) > 0) || (count($responseData['updated']) > 0) || (count($responseData['payment']) > 0) || (count($responseData['enroll_report_data']) > 0)) {
                    $response = [
                        'data' => $responseData,
                        'message' => 'Data Sync successfully',
                        'status' => 'success',
                        'code' => '200',
                    ];
                } else {
                    $response = [
                        'message' => 'Failed to Sync data',
                        'status' => 'failed',
                        'code' => '500',
                    ];
                }
            } else {
                $response = [
                    'data' => 'Error',
                    'message' => 'Unauthorized user',
                    'status' => 'failed',
                    'code' => '400',
                ];
            }
        } else {
            $response = [
                'message' => 'Invalid Request',
                'status' => 'failed',
                'code' => '400',
            ];
        }
        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function getUserProfileImages_post() {
        $token = $_SERVER['HTTP_TOKEN'];
//        $token = $this->head('token');
        if (isset($token)) {
            $check_token = $this->validate_token($token);
            if ($check_token) {
                $devoteeinfo = $this->ApiModel->getDevotees();
                if (isset($devoteeinfo) && !empty($devoteeinfo)) {
                    $data = array();
                    $index = 0;
                    foreach ($devoteeinfo as $devotee) {
                        if (!empty($devotee['user_image_path'])) {
                            if (file_exists($devotee['user_image_path'])) {
                                $data[$index]['id'] = $devotee['id'];
                                $image_name = explode('/', $devotee['user_image_path']);
                                $data[$index]['name'] = $image_name[4];
                                $data[$index]['image'] = base64_encode(file_get_contents(base_url() . $devotee['user_image_path']));
                                $index++;
                            }
                        }
                    }
                    $response = [
                        'data' => $data,
                        'message' => "successfully sync profile image",
                        'status' => 'success',
                        'code' => '200',
                    ];
                }
            } else {
                $response = [
                    'data' => 'Error',
                    'message' => 'Unauthorized user',
                    'status' => 'failed',
                    'code' => '400',
                ];
            }
        } else {
            $response = [
                'message' => 'Invalid Request',
                'status' => 'failed',
                'code' => '400',
            ];
        }
        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function syncUserImages_post() {
        $token = $_SERVER['HTTP_TOKEN'];
//        $token = $this->head('token');
        if (isset($token)) {
            $check_token = $this->validate_token($token);
            if ($check_token) {
                $responseData = array();
                if (isset($_FILES)) {
                    $index = 0;
                    foreach ($_FILES as $key => $val) {
                        $profile_tmp = $_FILES[$key]['tmp_name'];
                        $new_profile = $_FILES[$key]['name'];

                        if (!file_exists("assets/uploads/users/profile")) {
                            mkdir("assets/uploads/users/profile", 0777, true);
                        }
//                        $image = $this->ApiModel->getexistingprofile($key);
//                        if (!empty($image)) {
//                            unlink($image);
//                        }
                        compress_image($profile_tmp, "assets/uploads/users/profile/" . $new_profile, 0);

                        if (move_uploaded_file($profile_tmp, "assets/uploads/users/profile/" . $new_profile)) {
                            $profileimage = "assets/uploads/users/profile/" . $new_profile;
                            $data = array(
                                'user_image_path' => $profileimage,
                                'modified_time' => date("Y-m-d H:i:s")
                            );
                            $this->ApiModel->update_devotee($key, $data);
                            if ($this->db->affected_rows() > 0) {
                                $responseData[$index]['id'] = $key;
                            }
                            $index++;
                        }
                    }
                    if (isset($responseData) && count($responseData) > 0) {
//                    $file = file_get_contents(base_url() . "assets/uploads/users/profile/-profile-pooja.png", true);
                        $response = [
                            'data' => $responseData,
                            'message' => "Devotee Profile Synced successfully",
                            'status' => 'success',
                            'code' => '200',
                        ];
                    } else {
                        $response = [
                            'data' => $responseData,
                            'message' => "Failed to Sync Devotee Profile",
                            'status' => 'success',
                            'code' => '200',
                        ];
                    }
                } else {
                    $response = [
                        'data' => 'Error',
                        'message' => 'Invalid Input parameters',
                        'status' => 'failed',
                        'code' => '500',
                    ];
                }
            } else {
                $response = [
                    'data' => 'Error',
                    'message' => 'Unauthorized user',
                    'status' => 'failed',
                    'code' => '400',
                ];
            }
        } else {
            $response = [
                'message' => 'Invalid Request',
                'status' => 'failed',
                'code' => '400',
            ];
        }
        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function send_receipt($first, $middle, $last, $address, $city, $pincode, $dpn, $paydate, $email) {
//        error_reporting(E_ALL ^ E_WARNING);
//        error_reporting(0);
        if (isset($first, $middle, $last, $address, $city, $pincode, $dpn, $paydate, $email)) {
            $data['name'] = $first . " " . $middle . " " . $last;
            $data['amount'] = 1500;
            $data['pay_date'] = date("d-m-Y", strtotime($paydate));
            $data['address'] = $address . ", " . $city . ", " . $pincode;
            $data['dpn_no'] = $dpn;
            $data['amount_in_word'] = $this->numbertowords->convert_number(1500) . " only";
            $data['payment_mode'] = "Cash";

            $html = $this->load->view("Auth/email/email_receipt", $data, true);
            $pdfFilePath = 'RECEIPT_' . md5(date('Y-m-d H:i:s')) . '.PDF';
            $pdf = $this->m_pdf->load();
            $pdf->allow_charset_conversion = true;
            $pdf->charset_in = 'UTF-8';
            $pdf->SetDirectionality('rtl');
            $pdf->autoLangToFont = true;
            $pdf->AddPage('P');
            $pdf->WriteHTML($html);
            $pdf->Output($pdfFilePath, "F");
            $enquirer_name = "Shri Saibaba Palkhi Sohala Samiti, Pune";
            $enquirer_email = "saipalkhipune@gmail.com";

            $mail_body = "Om Sai Ram!!,<br><br>Please find the attached document of payment receipt.<br><br><br>Regards,<br>Shri Palkhi Samiti, Pune<br>Call: 9860003639 / 7057451648 / 8600258390";
            $mail = new PHPMailer();
            //$mail->IsSendmail(); // send via SMTP
            $mail->IsSMTP();
            $mail->SMTPDebug = 0;
            $mail->Debugoutput = 'html';
            $mail->SMTPAuth = true; // turn on SMTP authentication
            $mail->Username = "saipalkhipune@gmail.com"; // SMTP username
            $mail->Password = "Palkhi20!@"; // SMTP password
            //$mail->SMTPSecure = 'ssl';
            $mail->Port = "587";
            $mail->Host = 'smtp.gmail.com'; //hostname
            $mail->From = $enquirer_email;
            $mail->FromName = $enquirer_name;
            $mail->AddAddress($email);
            $mail->AddReplyTo($enquirer_email, $enquirer_name);
            $mail->AddAttachment($pdfFilePath);
            $mail->WordWrap = 50; // set word wrap
            $mail->IsHTML(false); // send as HTML
            $mail->Subject = "Sabhasad Nondani Pavti";
            $mail->MsgHTML($mail_body);
            $mail->AltBody = ""; //Text Body
            $mail->Send();
            unlink($pdfFilePath);
        }
    }

}

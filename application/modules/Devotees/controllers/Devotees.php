<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Devotees extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();

        $this->load->library(array('ion_auth', 'form_validation', 'session', 'm_pdf', 'numbertowords'));
        $this->load->model(array('DindiModel', 'Year', 'Devotee', 'Sevasamiti_model', 'language', 'Ion_auth_model', 'ApiModel'));
        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('Auth');
        error_reporting(E_ALL ^ E_WARNING);
        error_reporting(0);
    }

    function donation_receipt() {
        $this->load->view('Devotees/donation_receipt');
    }

    public function index() {
        if (!$this->ion_auth->logged_in()) {
            redirect('admin', 'refresh');
        }
        if (!$this->ion_auth->is_admin()) {
            redirect('admin', 'refresh');
        }

        $user_id = $this->session->userdata('user_id');
        $year = $this->Year->get_year_id(date('Y'));
        $yearcurrent = ($this->session->userdata('palki_year')) ? $this->session->userdata('palki_year') : date('Y');
        $data['year_list'] = $this->Year->get_year_list();
        $data['dindi_list'] = $this->DindiModel->get_dindi_list();
        $data['samiti_list'] = $this->Sevasamiti_model->get_samiti_list($year);

        $data['dataHeader']['title'] = 'Devotee List';
        $this->template->set_master_template('template.php');

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $firstname = '';
            $middlename = '';
            $lastname = '';

            if (!empty($this->input->post('devoteename'))) {
                $devoteename = explode(" ", $this->input->post('devoteename'));
                $firstname = $devoteename[0];
                $middlename = $devoteename[1];
                $lastname = $devoteename[2];
            }

            $devotee_id = $this->input->post('devoteeid');
            $padyatri_id = $this->input->post('padyatriid');
            $year = $this->input->post('db_year');
            $this->form_validation->set_rules('devoteename', 'Name', 'trim|required');
            $this->form_validation->set_rules('dindiyear', 'Dindi Year', 'trim|required');
            $this->form_validation->set_rules('sevasamithi', 'Seva Samithi', 'trim');
            $this->form_validation->set_rules('dindiname', 'Dindi Name', 'trim|required');
            $this->form_validation->set_rules('vehicletype', 'Transport', 'trim|required');
            $this->form_validation->set_rules('amountpaid', 'Amount', 'trim|required|numeric|min_length[1]|max_length[10]');



            if ($this->form_validation->run() == TRUE && isset($devotee_id)) {
                $padyatri_data = array('devotee_id' => $this->input->post('devoteeid'),
                    'id' => $this->input->post('padyatriid'),
                    'firstname' => $firstname,
                    'middlename' => $middlename,
                    'lastname' => $lastname,
                    'participation_year_id' => $this->input->post('dindiyear'),
                    'seva_samiti_id' => $this->input->post('sevasamithi'),
                    'dindi_id' => $this->input->post('dindiname'),
                    'vehical_type' => $this->input->post('vehicletype'),
                    'amountpaid' => $this->input->post('amountpaid'),
//                    'payment_mode' => ($this->input->post('switch-lg') == 'on') ? 'palakhibhavan' : 'palakhibhavan'
                    'payment_mode' => 'palakhibhavan'
                );
//                var_dump($padyatri_data);die;
                $this->session->set_userdata('palki_year', $this->input->post('palki_year'));
                $palkhiyear = $this->session->userdata('palki_year');
//             $data['alldevotee_list'] = $this->Devotee->get_alldevotees($year,$palkhiyear); 
                $data['palkiyear'] = $palkhiyear;
//                $response=1;
                $response = $this->Devotee->submit_devotee_amount($devotee_id, $padyatri_id, $padyatri_data, $year);
                $mobile_no = array();

                if ($response) {
                    $dt['name'] = $firstname . " " . $middlename . " " . $lastname;
                    $dt['amount'] = $padyatri_data['amountpaid'];
                    $dt['pay_date'] = date("Y-m-d");
                    $dt['address'] = $this->input->post('dev_address');
                    $dt['dpn_no'] = $this->input->post('dev_dpnno');
                    $dt['amount_in_word'] = $this->numbertowords->convert_number($this->input->post('amountpaid')) . " only";
                    $dt['payment_mode'] = 'Cash';

//                    print_r($dt);die;
                    $mobile_no = $this->Devotee->getdevotee_mobileno($devotee_id);
                    $dt['email'] = $mobile_no->email;
                    $createPdf = $this->createPdf($dt);

                    $devotee_phone_no = $mobile_no->mobile;
                    $dpn = $dt['dpn_no'];
                    $smstext = 'Om Sai Ram!!\nThanks for Sai Palkhi -' . $palkhiyear . ' registration\nYour DPN no is "' . $dpn . '"\nPlease collect your ID card from samiti members.\nSai Palkhi Samiti, Pune';
                    $sms_send_status = send_sms($devotee_phone_no, $smstext);

                    $this->session->set_flashdata('success_msg', 'Devotee payment made successfully .');
                    redirect('admin/devotee');
                } else {
                    $this->session->set_flashdata('error_msg', 'Failed to make payment.');
                    redirect('admin/devotee');
                }
                redirect('admin/devotee');
            } else {
                $data['alldevotee_list'] = $this->Devotee->get_alldevotees($year, $yearcurrent);
                redirect('admin/devotee');
            }
        } else {
            $data['alldevotee_list'] = $this->Devotee->get_alldevotees($year, $yearcurrent);
        }
        $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
        $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
        $this->template->write_view('content', 'Devotees/devotee_list', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'snippets/footer', '', TRUE);
        $this->template->render();
    }

    /*   public function pavti_receipt(){     
      $data['name'] = "Anna sopan mane";
      $data['amount'] = 1500;
      $data['pay_date'] = date("Y-m-d");
      $data['address'] = '"Sai Krupa" , Plot No. 508/1 Near Nagari Suraksha Prashikhshan Kendra ,
      Pune 411009, Pune, 411009';
      $data['dpn_no'] = 'ASM06052019PA2019';
      $data['amount_in_word'] = $this->numbertowords->convert_number(1500) . " only";
      $data['payment_mode'] = 'Cash';

      $data['dataHeader']['title'] = 'Devotee List';
      $this->template->set_master_template('template.php');
      $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
      $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
      $this->template->write_view('content', 'Devotees/email_receipt', (isset($this->data) ? $this->data : NULL), TRUE);
      $this->template->write_view('footer', 'snippets/footer', '', TRUE);
      $this->template->render();
      } */

    public function update_devotee($devotee_id) {
        if (!$this->ion_auth->logged_in()) {
            redirect('admin', 'refresh');
        }
        if (!$this->ion_auth->is_admin()) {
            redirect('admin', 'refresh');
        }
        $user_id = $this->session->userdata('user_id');

        $data['dataHeader']['title'] = 'Edit Devotee';
        $year = $this->Year->get_year_id(date('Y'));
        $devotee_id = ($devotee_id) ? $devotee_id : $this->input->post('devotee_id');
        $data['devotee_list'] = $this->Devotee->get_devotee_by_id($devotee_id);

//        var_dump($_POST);die;    
        $this->template->set_master_template('template.php');

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $joining = (!empty($data['devotee_list'][0]['date_of_joining'])) ? date("Y", strtotime($data['devotee_list'][0]['date_of_joining'])) : '';

            $this->form_validation->set_rules('devoteefirstname', 'First Name', 'trim|required|alpha');
            $this->form_validation->set_rules('devoteemiddlename', 'Middle Name', 'trim|required|alpha');
            $this->form_validation->set_rules('devoteelastname', 'Last Name', 'trim|required|alpha');
            $this->form_validation->set_rules('devoteedob', 'D.O.B', 'trim|required');
            $this->form_validation->set_rules('devoteemobileno', 'Mobile No.', 'trim|required|numeric|min_length[10]|max_length[10]');
            $this->form_validation->set_rules('devoteebloodgroup', 'Blood Group', 'trim|required');
            $this->form_validation->set_rules('devoteegender', 'Gender', 'trim|required');
            $this->form_validation->set_rules('devoteecity', 'City', 'trim|required');
            $this->form_validation->set_rules('devoteeaddress', 'Address', 'trim|required');
            // $this->form_validation->set_rules('devoteerelativename', 'Relative Name', 'trim|required');            
            $this->form_validation->set_rules('devoteemailid', 'Email ID', 'trim');
//            $this->form_validation->set_rules('devoteefestivalcommitee', 'Festival Commitee', 'trim|required');
            $this->form_validation->set_rules('devoteeprofession', 'Profession', 'trim|required');
            // $this->form_validation->set_rules('devoteeidproof', 'ID Proof', 'trim|required');
//            $this->form_validation->set_rules('devoteeeducation', 'Education', 'trim|required');
            $this->form_validation->set_rules('aadharno', 'Aadhaar Card No.', 'trim|min_length[14]|max_length[14]');
            // $this->form_validation->set_rules('devoteesickness', 'Sickness', 'trim');
            $this->form_validation->set_rules('devoteepincode', 'Pin Code', 'trim|required|numeric|min_length[6]|max_length[6]');


            if ($this->form_validation->run() == TRUE) {
                $first_name = $this->input->post('devoteefirstname');
                $middle_name = $this->input->post('devoteemiddlename');
                $last_name = $this->input->post('devoteelastname');
                $dob = $this->input->post('devoteedob');
                $city = $this->input->post('devoteecity');
                $year = $joining;

                $dpn_no = $this->create_dpn_no($first_name, $middle_name, $last_name, $dob, $city, $year);
//                $dpnno = $firstChar_fname . "" . $firstChar_lname . "" . $dob . "" . $firsttwoChar_city . "" . $dt_of_joining;
//                var_dump($dpnno);die;
                $devoteedata = array('id' => ($this->input->post('devotee_id')) ? $this->input->post('devotee_id') : '',
                    'email' => ($this->input->post('devoteemailid')) ? $this->input->post('devoteemailid') : '',
                    'dpn_no' => $dpn_no,
                    'firstname' => ($this->input->post('devoteefirstname')) ? $this->input->post('devoteefirstname') : '',
                    'middlename' => ($this->input->post('devoteemiddlename')) ? $this->input->post('devoteemiddlename') : '',
                    'lastname' => ($this->input->post('devoteelastname')) ? $this->input->post('devoteelastname') : '',
                    'birth_date' => ($this->input->post('devoteedob')) ? date("Y-m-d", strtotime($this->input->post('devoteedob'))) : '',
                    'mobile' => ($this->input->post('devoteemobileno')) ? '91' . $this->input->post('devoteemobileno') : '',
                    'blood_group' => ($this->input->post('devoteebloodgroup')) ? $this->input->post('devoteebloodgroup') : '',
                    'mobile1' => '91' . $this->input->post('devoteesecmobileno'),
                    'relative_name' => ($this->input->post('devoteerelativename')) ? $this->input->post('devoteerelativename') : '',
                    'relative_mobile' => '91' . $this->input->post('devoteeralativeno'),
                    'address' => ($this->input->post('devoteeaddress')) ? $this->input->post('devoteeaddress') : '',
                    'city' => ($this->input->post('devoteecity')) ? $this->input->post('devoteecity') : '',
                    'profession' => ($this->input->post('devoteeprofession')) ? $this->input->post('devoteeprofession') : '',
//                    'education' => ($this->input->post('devoteeeducation')) ? $this->input->post('devoteeeducation') : '',
                    'gender' => ($this->input->post('devoteegender')) ? $this->input->post('devoteegender') : '',
                    'identity_prof' => ($this->input->post('devoteeidproof')) ? $this->input->post('devoteeidproof') : '',
                    'adhaar_no' => ($this->input->post('aadharno')) ? $this->input->post('aadharno') : '',
                    'sickness' => ($this->input->post('devoteesickness')) ? $this->input->post('devoteesickness') : '',
                    'pincode' => ($this->input->post('devoteepincode')) ? $this->input->post('devoteepincode') : ''
                );

                $response = $this->Devotee->update_divotee($devotee_id, $devoteedata);
                if ($response) {
                    $this->session->set_flashdata('success_msg', 'Devotee details Successfully updated.');
                    redirect('admin/devotee');
                } else {
                    $this->session->set_flashdata('error_msg', 'Failed to Update Devotee details');
                    redirect('admin/devotee/' . $devotee_id);
                }
            } else {
//                $data['dindi_details'] = $this->DindiModel->get_allData($devotee_id);
                $data['samiti_list'] = $this->Sevasamiti_model->get_samiti_list($year);
                $data['id_proof'] = $this->Devotee->get_idproof_list();
                $data['occupation_list'] = $this->Devotee->get_profession_list();
//                $data['education_list'] = $this->Devotee->get_education_list();
                $data['city_list'] = $this->Devotee->get_city_list();
                $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
                $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
                $this->template->write_view('content', 'Devotees/devotee_details', (isset($this->data) ? $this->data : NULL), TRUE);
            }
        } else {
            $data['samiti_list'] = $this->Sevasamiti_model->get_samiti_list($year);
            $data['id_proof'] = $this->Devotee->get_idproof_list();
            $data['occupation_list'] = $this->Devotee->get_profession_list();
//            $data['education_list'] = $this->Devotee->get_education_list();
            $data['city_list'] = $this->Devotee->get_city_list();

//            $data['dindi_details'] = $this->DindiModel->get_allData($devotee_id);
            $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
            $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
            $this->template->write_view('content', 'Devotees/devotee_details', (isset($this->data) ? $this->data : NULL), TRUE);
        }
        $this->template->write_view('footer', 'snippets/footer', '', TRUE);
        $this->template->render();
    }

    public function delete() {
        if ($this->input->post('devotee_id')) {
            $this->Devotee->delete_devotee($this->input->post('devotee_id'));
            if ($this->db->affected_rows() > 0) {
                $this->session->set_flashdata('success_msg', 'Devotee details successfully deleted .');
                echo 'success';
            } else {
                $this->session->set_flashdata('error_msg', 'Devotee details failed to delete.');
                echo 'failed';
            }
        }
    }

    public function devotee_list() {
        $yearcurrent = '';
        $yearcurrentid = '';
        if ($this->input->post('year')) {
            if ($this->input->post('year') != "all") {
                $yearcurrent = $this->Year->get_year_by_id($this->input->post('year'));
                $yearcurrentid = $this->input->post('year');
            } else {
                $yearcurrent = $this->input->post('year');
                $yearcurrentid = $this->input->post('year');
            }

            $data['curr_yr_id'] = $yearcurrent;
            $this->session->set_userdata('devotee_year', $yearcurrent);
            $data['year_list'] = $this->Year->get_year_list();
            $data['alldevotee_list'] = $this->Devotee->get_alldevotees($yearcurrentid, $yearcurrent);
            $view = $this->load->view('ajax/devotee_list', $data);
            echo $view;
        }
    }

    public function pay_amount() {
        if ($this->input->post('devotee_id')) {
//            $this->DindiModel->delete_dindi($this->input->post('devotee_id'),$this->input->post('padyatri_id'));
//            if ($this->db->affected_rows() > 0) {
//                $this->session->set_flashdata('success_msg', 'Dindi deleted successfully.');
//                echo 'success';
//            } else {
//                $this->session->set_flashdata('error_msg', 'Failed to delete dindi.');
//                echo 'failed';
//            }
        }
    }

    public function signup() {

        if ($this->ion_auth->is_admin()) {
            $this->ion_auth->logout();
            $this->session->unset_userdata('user_id');
        }


        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->form_validation->set_rules('dob', 'Date of birth', 'trim|required');
            $this->form_validation->set_rules('mobileno', 'Mobile No.', 'trim|required|numeric|min_length[10]|max_length[10]');
            if ($this->form_validation->run() == TRUE) {
                $data['birth_date'] = date("Y-m-d", strtotime($this->input->post('dob')));
                $data['mobile_no'] = $this->input->post('mobileno');
                $mobileno = "91" . $this->input->post('mobileno');
                $devotee = $this->Devotee->if_devotee_exists($data['birth_date'], $mobileno);
                if (count($devotee) == 0) {
//                    $data['education_list'] = $this->Devotee->get_education_list();
                    $data['profession_list'] = $this->Devotee->get_profession_list();
                    $data['idproof_list'] = $this->Devotee->get_idproof_list();
                    $data['city_list'] = $this->Devotee->get_city_list();
                    $this->template->set_master_template('frontend_template.php');
                    $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
                    $this->template->write_view('content', 'user/devotee_register', (isset($this->data) ? $this->data : NULL), TRUE);
                    $this->template->write_view('footer', 'frontend/footer', '', TRUE);
                    $this->template->render();
                } else {
                    //Devotee already exists
                    $this->template->set_master_template('frontend_template.php');
                    $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
                    $this->template->write_view('content', 'Auth/login', (isset($this->data) ? $this->data : NULL), TRUE);
                    $this->template->write_view('footer', 'frontend/footer', '', TRUE);
                    $this->template->render();
//                    $this->session->set_flashdata('signupmsg', 'You already have an account, please go to login');
//                    redirect('login');
                }
            } else {
                //Validation failed
                $this->template->set_master_template('frontend_template.php');
                $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
                $this->template->write_view('content', 'user/signup', (isset($this->data) ? $this->data : NULL), TRUE);
                $this->template->write_view('footer', 'frontend/footer', '', TRUE);
                $this->template->render();
            }
        } else {
            $this->template->set_master_template('frontend_template.php');
            $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
            $this->template->write_view('content', 'user/signup', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'frontend/footer', '', TRUE);
            $this->template->render();
        }
    }

    public function create_devotee() {
        if ($this->ion_auth->is_admin()) {
            $this->ion_auth->logout();
            $this->session->unset_userdata('user_id');
        }
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->form_validation->set_rules('firstname', 'First Name', 'trim|required|alpha');
            $this->form_validation->set_rules('middlename', 'Middle Name', 'trim|required|alpha');
            $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required|alpha');
            $this->form_validation->set_rules('dob', 'Date of Birth', 'trim|required');
            $this->form_validation->set_rules('mobileno', 'Mobile No.', 'trim|required|numeric|min_length[10]|max_length[10]');
            $this->form_validation->set_rules('secmobileno', 'Mobile No.', 'trim|numeric|min_length[10]|max_length[10]');
            $this->form_validation->set_rules('bloodgroup', 'Blood Group', 'trim|required');
            $this->form_validation->set_rules('gender', 'Gender', 'trim|required');
            $this->form_validation->set_rules('city', 'City', 'trim|required');
            $this->form_validation->set_rules('address', 'Address', 'trim|required');
            $this->form_validation->set_rules('email', 'Email ID', 'trim');
            $this->form_validation->set_rules('profession', 'Profession', 'trim|required');
//            $this->form_validation->set_rules('education', 'Education', 'trim|required');
            $this->form_validation->set_rules('pincode', 'Pin Code', 'trim|required|numeric|min_length[6]|max_length[6]');

            $imagedata = array();
            if ($this->form_validation->run() == TRUE) {
                $first_name = $this->input->post('firstname');
                $middle_name = $this->input->post('middlename');
                $last_name = $this->input->post('lastname');
                $dob = $this->input->post('dob');
                $city = $this->input->post('city');
                $year = date("Y");
                $ifexistsdevotee = $this->Devotee->if_devoteedpn_exists($first_name, $middle_name, $last_name, $dob, $city);
//                echo $this->db->last_query();
//                echo "<pre>";
//                echo print_r($ifexistsdevotee);
//                exit();
                if (count($ifexistsdevotee) == 0) {
                    $dpn_no = $this->create_dpn_no($first_name, $middle_name, $last_name, $dob, $city, $year);
                    //encoaded password
                    $password = $this->ion_auth->hash_password('sairam');
                    $upload_responce = array('message' => '', 'status' => '');
                    $devotee_data = array(
                        'group_id' => 2,
                        'ip_address' => $_SERVER['REMOTE_ADDR'],
                        'password' => $password,
                        'created_on' => time(),
                        'active' => 1,
                        'date_of_joining' => date('Y-m-d'),
                        'dpn_no' => $dpn_no,
                        'firstname' => $first_name,
                        'middlename' => $middle_name,
                        'lastname' => $last_name,
                        'birth_date' => date('Y-m-d', strtotime($dob)),
                        'mobile' => '91' . $this->input->post('mobileno'),
                        'address' => $this->input->post('address'),
                        'city' => $this->input->post('city'),
                        'profession' => $this->input->post('profession'),
//                    'education' => $this->input->post('education'),
                        'gender' => $this->input->post('gender'),
                        'blood_group' => $this->input->post('bloodgroup'),
                        'registered_via' => 'web',
                        'isactive' => 1,
                        'pincode' => $this->input->post('pincode'),
                    );
                    if ($this->input->post('email')) {
                        $devotee_data['email'] = $this->input->post('email');
                    }
                    if ($this->input->post('secmobileno')) {
                        $devotee_data['mobile1'] = '91' . $this->input->post('secmobileno');
                    }

//                $profile_tmp = $_FILES['profile_image']['tmp_name'];                
                    $imagedata['hiddenImgTxt'] = $_POST['hiddenImgTxt'];
                    $imagedata['firstname'] = $devotee_data['firstname'];
                    $imagedata['lastname'] = $devotee_data['lastname'];
//                if (isset($profile_tmp) && !empty($profile_tmp)) {
//                    $upload_responce = $this->upload_profile_pic($_FILES);
//                } else {
//                    $upload_responce = array('message' => '', 'status' => '1');
//                }

                    if (isset($imagedata['hiddenImgTxt']) && !empty($imagedata['hiddenImgTxt'])) {
                        $up_responce = $this->upload_profile_pic($imagedata);
                        $devotee_data['user_image_path'] = $up_responce; #$_FILES['profile_image']['tmp_name'];
                    }

                    ($upload_responce['message']) ? $devotee_data['user_image_path'] = ($upload_responce['message']) ? $upload_responce['message'] : '' : '';
                    $devoteeid = $this->Devotee->insert_devotee($devotee_data);
                    if (isset($devoteeid) && !empty($devoteeid)) {
                        //send password on devotees mobile.
                        $devotee_phone_no = '91' . $this->input->post('mobileno');
                        $smstext = "Om Sai Ram!!\nThanks for registration!\nYour default password is - sairam\n- Sai Palkhi Samiti, Pune\nCall: 9860003639 / 7057451648 / 8600258390";
                        $sms_send_status = send_sms($devotee_phone_no, $smstext);
                        $this->session->set_userdata('user_type', 'user');
                        $remember = (bool) $this->input->post('remember');
                        $this->session->set_userdata('user_type', 'user');
                        if ($this->ion_auth->login($this->input->post('mobileno'), 'sairam', date('Y-m-d', strtotime($dob)), $remember)) {
                            if ($this->input->post('enroll') == 'no') {
                                $this->session->set_flashdata('success_msg', 'Devotee Registration Successfully');
                                redirect('AccountDetails');
                            } elseif ($this->input->post('enroll') == 'yes') {
                                redirect('enrollment');
                            }
                        } else {
                            redirect('registersuccess');
                        }
                    } else {
                        unlink($imagedata['firstname'] . "" . $imagedata['lastname'] . "." . $type);
                        $this->session->set_flashdata('success_msg', 'Failed to register devotee');
                        redirect('register');
                    }
                } else {
                    $this->session->set_tempdata('error_msg', 'You already have an account with the given information', 10);
//                    $data['message'] = "You already have an account with the given information";
                    $data['profession_list'] = $this->Devotee->get_profession_list();
                    $data['city_list'] = $this->Devotee->get_city_list();
                    $data['dataHeader']['title'] = 'Devotee Registration';
                    $this->template->set_master_template('frontend_template.php');
                    $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
                    $this->template->write_view('content', 'user/devotee_register', (isset($this->data) ? $this->data : NULL), TRUE);
                    $this->template->write_view('footer', 'frontend/footer', '', TRUE);
                    $this->template->render();
                }
            } else {
//                $data['education_list'] = $this->Devotee->get_education_list();
                $data['profession_list'] = $this->Devotee->get_profession_list();
//                $data['idproof_list'] = $this->Devotee->get_idproof_list();
                $data['city_list'] = $this->Devotee->get_city_list();
                $data['dataHeader']['title'] = 'Devotee Registration';
                $this->template->set_master_template('frontend_template.php');
                $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
                $this->template->write_view('content', 'user/devotee_register', (isset($this->data) ? $this->data : NULL), TRUE);
                $this->template->write_view('footer', 'frontend/footer', '', TRUE);
                $this->template->render();
            }
        } else {
            redirect('signup');
        }
    }

    public function account_details() {
        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }
        if ($this->ion_auth->is_admin()) {
            redirect('login', 'refresh');
        }
        $user_id = $this->session->userdata('user_id');
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $this->form_validation->set_rules('firstname', 'First Name', 'trim|required|alpha');
            $this->form_validation->set_rules('middlename', 'Middle Name', 'trim|required|alpha');
            $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required|alpha');
            $this->form_validation->set_rules('dob', 'Date of Birth', 'trim|required');
            $this->form_validation->set_rules('mobileno', 'Mobile No.', 'trim|required|numeric|min_length[10]|max_length[10]');
            $this->form_validation->set_rules('secmobileno', 'Mobile No.', 'trim|numeric|min_length[10]|max_length[10]');
            $this->form_validation->set_rules('bloodgroup', 'Blood Group', 'trim|required');
            $this->form_validation->set_rules('gender', 'Gender', 'trim|required');
            $this->form_validation->set_rules('city', 'City', 'trim|required');
            $this->form_validation->set_rules('address', 'Address', 'trim|required');
            $this->form_validation->set_rules('email', 'Email ID', 'trim');
            $this->form_validation->set_rules('profession', 'Profession', 'trim|required');
//            $this->form_validation->set_rules('education', 'Education', 'trim|required');
//            $this->form_validation->set_rules('profile_image', 'Profile Image', 'trim|required');
            $this->form_validation->set_rules('pincode', 'Pin Code', 'trim|required|numeric|min_length[6]|max_length[6]');
            $imagedata = array();
            if ($this->form_validation->run() == TRUE) {
                $first_name = $this->input->post('firstname');
                $middle_name = $this->input->post('middlename');
                $last_name = $this->input->post('lastname');
                $dob = $this->input->post('dob');
                $city = $this->input->post('city');
                $year = $this->input->post('joinyear');
                $old_dpn = $this->input->post('dpn');
                $dpn_no = $this->create_dpn_no($first_name, $middle_name, $last_name, $dob, $city, $year);
                $devotee_data = array(
                    'firstname' => $this->input->post('firstname'),
                    'middlename' => $this->input->post('middlename'),
                    'lastname' => $this->input->post('lastname'),
                    'birth_date' => date('Y-m-d', strtotime($this->input->post('dob'))),
                    'mobile' => '91' . $this->input->post('mobileno'),
                    'address' => $this->input->post('address'),
                    'city' => $this->input->post('city'),
                    'profession' => $this->input->post('profession'),
//                    'education' => $this->input->post('education'),
                    'gender' => $this->input->post('gender'),
                    'blood_group' => $this->input->post('bloodgroup'),
//                    'relative_name' => $this->input->post('relativename'),
//                    'relative_mobile' => '91' . $this->input->post('relativeno'),
//                    'sickness' => $this->input->post('sickness'),
//                    'identity_prof' => $this->input->post('idproof'),
                    'pincode' => $this->input->post('pincode'),
                );
                $upload_responce = array('message' => '', 'status' => '');
                if ($old_dpn != $dpn_no) {
                    $devotee_data['dpn_no'] = $dpn_no;
                }
                if ($this->input->post('secmobileno')) {
                    $devotee_data['mobile1'] = '91' . $this->input->post('secmobileno');
                }
                if ($this->input->post('email')) {
                    $devotee_data['email'] = $this->input->post('email');
                }

                $imagedata['hiddenImgTxt'] = $_POST['hiddenImgTxt'];
                $imagedata['firstname'] = $devotee_data['firstname'];
                $imagedata['lastname'] = $devotee_data['lastname'];
                if (isset($imagedata['hiddenImgTxt']) && !empty($imagedata['hiddenImgTxt'])) {
                    $up_responce = $this->upload_profile_pic($imagedata);
                    ($up_responce) ? $devotee_data['user_image_path'] = $up_responce : '';
                    #$_FILES['profile_image']['tmp_name'];
                }
                ($upload_responce['message']) ? $devotee_data['user_image_path'] = ($upload_responce['message']) ? $upload_responce['message'] : '' : '';
                if ($this->Devotee->update_divotee($user_id, $devotee_data)) {
                    $this->session->set_flashdata('success_msg', 'Account Details updated Successfully');
                    redirect('AccountDetails');
                } else {
                    $this->session->set_flashdata('error_msg', 'Failed to update account details');
                    redirect('AccountDetails');
                }

//                }
            } else {
                $data['devotee_details'] = $this->Devotee->get_devotee_details($user_id);
                $year = $this->Year->get_year_id(date('Y'));
                $data['payment_info'] = $this->Devotee->devotee_payment_details($user_id, $year);
//                $data['education_list'] = $this->Devotee->get_education_list();
                $data['profession_list'] = $this->Devotee->get_profession_list();
                $data['city_list'] = $this->Devotee->get_city_list();
                $data['dataHeader']['title'] = 'Account Details';
                $this->template->set_master_template('frontend_template.php');
                $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
                $this->template->write_view('content', 'user/account_details', (isset($this->data) ? $this->data : NULL), TRUE);
                $this->template->write_view('footer', 'frontend/footer', '', TRUE);
                $this->template->render();
            }
        } else {
//            $year = $this->Year->get_year_id(date('Y'));
            $data['devotee_details'] = $this->Devotee->get_devotee_details($user_id);
            $year = $this->Year->get_year_id(date('Y'));
            $data['payment_info'] = $this->Devotee->devotee_payment_details($user_id, $year);
//            $data['education_list'] = $this->Devotee->get_education_list();
            $data['profession_list'] = $this->Devotee->get_profession_list();
//            $data['idproof_list'] = $this->Devotee->get_idproof_list();
            $data['city_list'] = $this->Devotee->get_city_list();
            $data['dataHeader']['title'] = 'Account Details';

            $this->template->set_master_template('frontend_template.php');
            $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
            $this->template->write_view('content', 'user/account_details', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'frontend/footer', '', TRUE);
            $this->template->render();
        }
    }

    public function create_dpn_no($firstname, $middle_name, $lastname, $dob, $city, $year) {
        $firstChar_fname = strtoupper(substr($firstname, 0, 1));
        $firstChar_mname = strtoupper(substr($middle_name, 0, 1));
        $firstChar_lname = strtoupper(substr($lastname, 0, 1));
        $entiredob = date("dmY", strtotime($dob));
        $firsttwoChar_city = strtoupper(substr($city, 0, 2));
        $dpnno = $firstChar_fname . "" . $firstChar_mname . "" . $firstChar_lname . "" . $entiredob . "" . $firsttwoChar_city . "" . $year;
        return $dpnno;
    }

    public function enrollment() {
        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }
        if ($this->ion_auth->is_admin()) {
            redirect('login', 'refresh');
        }
        $user_id = $this->session->userdata('user_id');
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->form_validation->set_rules('relativename', 'Relative Name', 'trim|required');
            $this->form_validation->set_rules('relativeno', 'Relative No.', 'trim|required|numeric|min_length[10]|max_length[10]');
            $this->form_validation->set_rules('idproof', 'ID Proof', 'trim|required');
            $this->form_validation->set_rules('payment_mode', 'Payment Option', 'trim|required');
            $this->form_validation->set_rules('vehicle_type', 'Vehicle Type', 'trim|required');
            $this->form_validation->set_rules('dindi', 'Dindi', 'trim|required');
            $this->form_validation->set_rules('amount', 'Amount', 'trim|numeric|required');
            if ($this->form_validation->run() == TRUE) {
                $data['relativename'] = $this->input->post('relativename');
                $data['relativeno'] = $this->input->post('relativeno');
                $data['idproof'] = $this->input->post('idproof');
                $data['adharnumber'] = $this->input->post('adharnumber');
                $data['sickness'] = $this->input->post('sickness');
                $data['payment_mode'] = $this->input->post('payment_mode');
                $data['samiti'] = $this->input->post('samiti');
                $data['vehicle_type'] = $this->input->post('vehicle_type');
                $data['dindi'] = $this->input->post('dindi');
                $data['amount'] = $this->input->post('amount');
                $data['devotee'] = $this->Devotee->get_devotee_details($user_id);
                $data['dataHeader']['title'] = 'Terms and Conditions';
                $this->template->set_master_template('frontend_template.php');
                $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
                $this->template->write_view('content', 'user/terms_and_condition', (isset($this->data) ? $this->data : NULL), TRUE);
                $this->template->write_view('footer', 'frontend/footer', '', TRUE);
                $this->template->render();
            } else {
                $year = $this->Year->get_year_id(date('Y'));
                $data['samiti_list'] = $this->Sevasamiti_model->get_samitis($year);
                $data['dindi_list'] = $this->DindiModel->get_dindi_list();
                $data['idproof_list'] = $this->Devotee->get_idproof_list();
                $data['dataHeader']['title'] = 'Palakhi Enrollment';
                $year = $this->Year->get_year_id(date('Y'));
                $data['padyatri_details'] = $this->Devotee->devotee_payment_details($user_id, $year);
                if (empty($data['padyatri_details']) && count($data['padyatri_details']) == 0) {
                    $data['padyatri_details'] = $this->Devotee->padyatri_lastyear_details($user_id);
                }
                $this->template->set_master_template('frontend_template.php');
                $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
                $this->template->write_view('content', 'user/enrollment', (isset($this->data) ? $this->data : NULL), TRUE);
                $this->template->write_view('footer', 'frontend/footer', '', TRUE);
                $this->template->render();
            }
        } else {
            $year = $this->Year->get_year_id(date('Y'));
            $data['samiti_list'] = $this->Sevasamiti_model->get_samitis($year);
            $data['dindi_list'] = $this->DindiModel->get_dindi_list();
            $data['idproof_list'] = $this->Devotee->get_idproof_list();
            $data['dataHeader']['title'] = 'Palakhi Enrollment';
            $year = $this->Year->get_year_id(date('Y'));
            $data['padyatri_details'] = $this->Devotee->devotee_payment_details($user_id, $year);
            if (empty($data['padyatri_details']) && count($data['padyatri_details']) == 0) {
                $data['padyatri_details'] = $this->Devotee->padyatri_lastyear_details($user_id);
            } else {
                if (strtolower($data['padyatri_details'][0]['payment_status']) == 'success') {
                    redirect('AccountDetails');
                }
            }
            $this->template->set_master_template('frontend_template.php');
            $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
            $this->template->write_view('content', 'user/enrollment', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'frontend/footer', '', TRUE);
            $this->template->render();
        }
    }

    public function change_password() {
        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }
        if ($this->ion_auth->is_admin()) {
            redirect('login', 'refresh');
        }
        $user_id = $this->session->userdata('user_id');
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->form_validation->set_rules('old_password', "Old Password", 'required');
            $this->form_validation->set_rules('new_password', "New Password", 'required|min_length[8]|max_length[20]|matches[confirm_password]');
            $this->form_validation->set_rules('confirm_password', "Confirm Password", 'required|min_length[8]|max_length[20]');
            if ($this->form_validation->run() == TRUE) {
                $hashed_new_password = $this->ion_auth->hash_password($this->input->post('confirm_password'));
                $data = array(
                    'password' => $hashed_new_password,
                    'remember_code' => NULL
                );
                if ($this->Devotee->change_password($user_id, $data)) {
                    $this->ion_auth->logout();
                    $this->session->set_flashdata('success_msg', 'Password has beed changed successfully.');
                    redirect('login');
                } else {
                    $this->session->set_flashdata('error_msg', 'Failed to change password');
                    redirect('changepassword');
                }
            } else {
                $this->template->set_master_template('frontend_template.php');
                $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
                $this->template->write_view('content', 'user/change_password', (isset($this->data) ? $this->data : NULL), TRUE);
                $this->template->write_view('footer', 'frontend/footer', '', TRUE);
                $this->template->render();
            }
        } else {
            $this->template->set_master_template('frontend_template.php');
            $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
            $this->template->write_view('content', 'user/change_password', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'frontend/footer', '', TRUE);
            $this->template->render();
        }
    }

    public function register_success() {
        $data['dataHeader']['title'] = 'Devotee Registration';
        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'user/register_success', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }

    public function getCity_autocomplete_c() {
        $keyword = $this->input->post('term');
        $this->Devotee->getCity_autocomplete($keyword);
    }

    public function upload_profile_pic($FILE = NULL, $user_id = NULL) {
        $profile_data = '';
        $imagedata = $FILE['hiddenImgTxt'];
        $image_decode_data = '';
        $decoded_img_data = '';
        if (isset($imagedata) && !empty($imagedata)) {
            if (preg_match('/^data:image\/(\w+);base64,/', $imagedata, $type)) {
                $imagedata = substr($imagedata, strpos($imagedata, ',') + 1);
                $type = strtolower($type[1]); // jpg, png, gif 

                if (in_array($type, ['jpg', 'jpeg', 'gif', 'png'])) {
                    $decoded_img_data = base64_decode($imagedata);
                }
                $profile_data = PROFILE_PATH . $FILE['firstname'] . "" . $FILE['lastname'] . "." . $type;
                file_put_contents($profile_data, $decoded_img_data);
            }
        }
        return ($profile_data) ? $profile_data : '';
    }

    public function createPdf($data = NULL) {
        $pdffile_name = '';
        $webpage = $this->load->view("Auth/email/email_receipt", $data, true);
        $pdffile_name = $data['dpn_no'] . md5(date('Y-m-d H')) . '.PDF';

        $pdf = $this->m_pdf->load();
        $pdf->allow_charset_conversion = true;
        $pdf->charset_in = 'UTF-8';
        $pdf->SetDirectionality('rtl');
        $pdf->autoLangToFont = false;
        $pdf->SetAutoFont(AUTOFONT_INDIC);
        $pdf->AddPage('P');
        $pdf->WriteHTML($webpage);
//<<<<<<< HEAD
//        $pdfdoc = $pdf->Output($pdffile_name, "D");DIE;
//        file_put_contents(RECEIPT_PATH . $pdffile_name, $pdfdoc);
//  
//         $fileImg_Pdf =($pdffile_name)? base_url().RECEIPT_PATH.$pdffile_name:base_url().RECEIPT_PATH.$imgfile_name;
//        //$fileData = base64_encode(file_get_contents($fileImg_Pdf));
//        $form_name = 'Sai Baba Palkhi Team';

        $pdf->Output($pdffile_name, "F");
        $enquirer_name = "Sai Baba Palkhi Team";
        $enquirer_email = "saipalkhipune@gmail.com";
        $subject_title = "Sabhasad Nondani Pavti";

        $mail_body = "Dear Associate,<br><br>Please find the attached document of payment receipt.<br><br><br>Regards,<br>Shri Saibaba Palkhi Sohala Samiti, Pune";
        $mail = new PHPMailer();
        //$mail->IsSendmail(); // send via SMTP
        $mail->IsSMTP();
        $mail->SMTPDebug = 0;
        //Ask for HTML-friendly debug output
        $mail->Debugoutput = 'html';
        //Set the hostname of the mail server
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->Username = "saipalkhipune@gmail.com"; // SMTP username
        $mail->Password = "Palkhi20!@"; // SMTP password
        $webmaster_email = "saipalkhipune@gmail.com"; //Reply to this email ID
        //$mail->SMTPSecure = 'ssl';
        $mail->Port = "587";
        $mail->Host = 'smtp.gmail.com'; //hostname

        $receiver_email = $data['email']; #'padamasing.m@mindworx.in';
        $email = $receiver_email; // Recipients email ID //inquiry@mindworx.in

        $mail->From = $enquirer_email;
        $mail->FromName = $enquirer_name;
        $mail->AddAddress($email);
        $mail->AddReplyTo($enquirer_email, $enquirer_name);
        $mail->AddAttachment($pdffile_name);
        $mail->WordWrap = 50; // set word wrap
        $mail->IsHTML(false); // send as HTML
        $mail->Subject = $subject_title;
        $mail->MsgHTML($mail_body);
        $mail->AltBody = "This is the body when user views in plain text format"; //Text Body
        if (!$mail->Send()) {
            // echo "Mailer Error: " . $mail->ErrorInfo;
            $a = 0;
        } else {
            if (file_exists($pdffile_name)) {
                unlink($pdffile_name);
            }
            $a = 1;
        }
    }

    public function update_badget() {
        if ($this->input->post('devotee_id')) {
            $badgetdata = array(
                'badget_print' => 1
            );
            $year = $this->Year->get_year_id(date('Y'));
            $response = $this->Devotee->updateBadgetReceipt($this->input->post('devotee_id'), $year, $badgetdata);
            if ($response == true) {
                echo "success";
            } else {
                echo "failed";
            }
        }
    }

    public function update_receipt() {
        if ($this->input->post('devotee_id')) {
            $badgetdata = array(
                'receipt_print' => 1
            );
            $year = $this->Year->get_year_id(date('Y'));
            $response = $this->Devotee->updateBadgetReceipt($this->input->post('devotee_id'), $year, $badgetdata);
            if ($response == true) {
                echo "success";
            } else {
                echo "failed";
            }
        }
    }

    public function update_donation_receipt() {
        if ($this->input->post('devotee_id')) {
            $donationdata = array(
                'donationreceipt_print' => 1
            );
            $year = $this->Year->get_year_id(date('Y'));
            $response = $this->Devotee->updateBadgetReceipt($this->input->post('devotee_id'), $year, $donationdata);
            if ($response == true) {
                echo "success";
            } else {
                echo "failed";
            }
        }
    }

}

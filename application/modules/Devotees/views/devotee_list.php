<div class="row d-print-none">
    <div class="col-12">
        <div class="card min-height80 d-print-none">
            <div class="card-body d-print-none">
                <h3 class="card-title d-print-none">Devotee List</h3>
                <hr>
                <div class="">
                    <div class="row mx-0">
                        <div class="button-table">
                            <form class="form-inline">
                                <label class="palkhi-select" for="email"><?php echo $this->session->userdata('palki_year'); ?>Palkhi Sohala:</label>
                                <select class="form-control ml-2" id="year_list" name="year_list">
                                    <option value="">Select Year</option>
                                    <option value="all" selected>All</option>
                                    <?php
                                    $curr_yr_id = '';
                                    if (isset($year_list) && !empty($year_list)) {
                                        foreach ($year_list as $year) {
                                            $selected = '';
                                            $palkhiyear = ($this->session->userdata('palki_year')) ? $this->session->userdata('palki_year') : date('Y');
                                            if ($palkhiyear == $year['year']) {
                                                $selected = "selected";
                                                $curr_yr_id = $year['id'];
                                            }
                                            ?>
                                            <option value="<?php echo $year['id'] ?>" <?php echo $selected; ?>><?php echo $year['year'] ?></option>
                                            <?php
                                        }
                                    }
                                    $this->session->unset_userdata('palki_year');
                                    ?>
                                </select>

                                <label class="palkhi-select font-14 mr-1 ml-1">Category:</label>
                                <select class="form-control" id="search_by" name="search_by">
                                    <option value="">Search By</option>                                    
                                    <option value="name" selected>Name</option>
                                    <option value="mobile">Mobile No.</option>
                                    <option value="dpn">DPN No.</option>
                                </select>
                                <div class="form-group has-search ml-2">
                                    <span class="fas fa-search form-control-feedback"></span>
                                    <input type="text" class="form-control" id="column_filter" placeholder="Search">
                                </div>
                            </form>
                        </div>
                        <div class="float-left"></div>

                    </div>
                    <div id="example23_wrapper" class="dataTables_wrapper dt-bootstrap4 table-responsive">
                        <table id="devotee_list" class="display table table-hover table-striped table-bordered dataTable datatable" cellspacing="0" width="100%" role="grid" aria-describedby="example23_info" >
                            <thead>
                                <tr class="nowrap"> 
                                    <th style="width: 15%;">DPN No.</th>
                                    <th style="width: 20%;">Devotee Name </th>
                                    <th style="width: 30%;">Address</th>
                                    <th style="width: 10%">Mobile No.</th>
                                    <th style="width: 10%;">Status</th>
                                    <!--<th>Payment</th>-->
                                    <th style="width: 10%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($alldevotee_list) && !empty($alldevotee_list)) {
                                    foreach ($alldevotee_list as $devotee_list) {
                                        ?>
                                        <tr id="row_<?php echo $devotee_list['id'] ?>">
                                            <td><?php echo ($devotee_list['dpn_no']) ? $devotee_list['dpn_no'] : ''; ?></td>
                                            <td><?php echo ($devotee_list['firstname']) ? $devotee_list['firstname'] . " " . $devotee_list['middlename'] . " " . $devotee_list['lastname'] : ''; ?></td>
                                            <td style="word-break: break-word !important;" class="address_length" title="<?php echo $devotee_list['address'] . ", " . $devotee_list['city'] ?>"><?php echo ($devotee_list['address']) ? $devotee_list['address'] . ", " . $devotee_list['city'] : ''; ?></td>
                                            <td><?php echo ($devotee_list['mobile']) ? substr($devotee_list['mobile'], 2) : ''; ?></td>
                                            <td><?php echo ($devotee_list['active']) ? 'Active' : 'Inactive'; ?></td>                                
                                            <td>                                                
                                                <a href="<?php echo base_url() . 'admin/devotee/update/' . $devotee_list['id']; ?>" class="bg-button" title="Edit"><i class="fas fa-edit"></i>
                                                </a>&nbsp;                                                
                                               <!--  <a class="bg-button delete" data-devotee="<?php // echo $devotee_list['id'];           ?>" title="Delete"><i class="fas fa-trash"></i>
                                                </a> -->
                                                &nbsp;
                                                <?php
                                                $payment_status = '';
                                                $onclick = '';
                                                $age = '';
                                                $image_path = '';
                                                $confirm_pay = (strtolower($devotee_list['payment_status']) == 'success') ? '' : 'confirm_pay';
                                                $elements_data = '';
                                                $dbvehical_type = ($devotee_list['vehical_type']) ? $devotee_list['vehical_type'] : '';
                                                $db_vehicle_details = ($devotee_list['vehicle_details']) ? $devotee_list['vehicle_details'] : '';
                                                $db_seva_samiti_id = ($devotee_list['seva_samiti_id']) ? $devotee_list['seva_samiti_id'] : '';
                                                $db_dindi_id = ($devotee_list['dindi_id']) ? $devotee_list['dindi_id'] : '26';


                                                if (file_exists($devotee_list['user_image_path'])) {
                                                    $image_path = base_url() . $devotee_list['user_image_path'];
                                                } else {
                                                    $image_path = base_url('assets/images/') . "default-profile.png";
                                                }
                                                $from = new DateTime($devotee_list['birth_date']);
                                                $to = new DateTime('today');
                                                $age = $from->diff($to)->y;
                                                $amountinword = $this->numbertowords->convert_number($devotee_list['donation_amount']);

//                                                if (strtolower($devotee_list['payment_status']) != 'success') {
//                                                    $elements_data = "id='pay_" . $devotee_list['id'] . "' data-devotee='" . $devotee_list['id'] . "/" . $devotee_list['tbl_py_id'] . "' data-dtname='" . $devotee_list['firstname'] . " " . $devotee_list['middlename'] . " " . $devotee_list['lastname'] . "' data-dindi_id='" . $db_dindi_id . "' data-vehical_type='" . $dbvehical_type . "' data-vehicle_details='" . $db_vehicle_details . "' data-seva_samiti_id='" . $db_seva_samiti_id . "' data-address='" . $devotee_list['address'] . ", " . $devotee_list['city'] . " - " . $devotee_list['pincode'] . "' data-dt-dpnno='" . $devotee_list['dpn_no'] . "'";
                                                ?> 
        <!--                                                    <a href="#" class="<?php echo $confirm_pay ?> rupees-icons" data-toggle="modal" <?php echo $elements_data ?>
                                                       data-dtyear_id="<?php echo ($devotee_list['tbl_year_id']) ? $devotee_list['tbl_year_id'] : $curr_yr_id; ?>"
                                                       data-dtyear="<?php echo ($devotee_list['tbl_year_year']) ? $devotee_list['tbl_year_year'] : date('Y'); ?>"                                                       
                                                       data-payment_amount="<?php echo ($devotee_list['payment_amount']) ? $devotee_list['payment_amount'] : '1500'; ?>" 
                                                       title="<?php echo (strtolower($devotee_list['payment_status']) == 'success') ? 'Already Paid' : 'Accept Payment'; ?>">
                                                        <i class="fas fa-rupee-sign"></i></a>&nbsp;&nbsp;&nbsp;                                                         -->
                                                <?php
//                                                } else
                                                if (strtolower($devotee_list['payment_status']) != 'success' || strtolower($devotee_list['payment_status']) == 'success' && date("Y", strtotime($devotee_list['payment_date'])) != date("Y")) {
                                                    $elements_data = "id='pay_" . $devotee_list['id'] . "' data-devotee='" . $devotee_list['id'] . "/" . $devotee_list['tbl_py_id'] . "' data-dtname='" . $devotee_list['firstname'] . " " . $devotee_list['middlename'] . " " . $devotee_list['lastname'] . "' data-dindi_id='" . $db_dindi_id . "' data-vehical_type='" . $dbvehical_type . "' data-vehicle_details='" . $db_vehicle_details . "' data-seva_samiti_id='" . $db_seva_samiti_id . "' data-address='" . $devotee_list['address'] . ", " . $devotee_list['city'] . " - " . $devotee_list['pincode'] . "' data-dt-dpnno='" . $devotee_list['dpn_no'] . "'";
                                                    ?>
                                                    <a href="#" class="<?php echo 'confirm_pay' ?> rupees-icons" data-toggle="modal" <?php echo $elements_data ?>
                                                       data-dtyear_id="<?php echo $curr_yr_id ?>"
                                                       data-dtyear="<?php echo date('Y'); ?>"                                                       
                                                       data-payment_amount="1500" 
                                                       title="<?php echo (strtolower($devotee_list['payment_status']) == 'success') ? 'Already Paid' : 'Accept Payment'; ?>">
                                                        <i class="fas fa-rupee-sign"></i></a>&nbsp;&nbsp;&nbsp;                                                        

                                                    <?php
                                                }
                                                $donation_for = "";
                                                if (isset($devotee_list['donation_for']) && $devotee_list['donation_for'] == 'vastunidhishirdi') {
                                                    $donation_for = "Vastunidhi - Shirdi";
                                                } elseif (isset($devotee_list['donation_for']) && $devotee_list['donation_for'] == 'palkhi') {
                                                    $donation_for = "Palkhi Sohla";
                                                }
                                                $receipt_data = "data-pr-receiptno='" . $devotee_list['receipt_no'] . "' data-pr-dtname='" . $devotee_list['firstname'] . " " . $devotee_list['middlename'] . " " . $devotee_list['lastname'] . "' data-pr-address='" . $devotee_list['address'] . ", " . $devotee_list['city'] . " - " . $devotee_list['pincode'] . "' data-pr-amount='" . $devotee_list['donation_amount'] . "' data-pr-payment-date='" . date('d-m-Y', strtotime($devotee_list['donation_date'])) . "' data-pr-amountinword='" . $amountinword . "' data-pr-devotee='" . $devotee_list['id'] . "' data-pr-donationfor='" . $donation_for . "'";

                                                if (strtolower($devotee_list['donation_status']) == 'success' && date("Y", strtotime($devotee_list['donation_date'])) == date("Y")) {
                                                    ?>
                                                    <i class="fas fa-donate cursor-pointer <?php echo ($devotee_list['donationreceipt_print'] == 1) ? 'fahover' : '' ?>" <?php echo ($devotee_list['donationreceipt_print'] == 1) ? 'title="Receipt Already Printed"' : 'title="Print Donation Receipt"' ?> onclick="printDonationReceipt(this, 'donationReceipt')" <?php echo $receipt_data ?>></i>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>  

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('donation_receipt.php'); ?>
<?php // include('print_form.php');     ?>
<!----------------------- slip print ------------------------------------------------>
<script>
    function printDonationReceipt(click_Eve, divName) {
        $("#" + divName).css("display", 'block');
        $("#rdpnno_div").html($(click_Eve).attr('data-pr-receiptno'));
        $("#rfullname_div").html($(click_Eve).attr('data-pr-dtname'));
        $("#rpaydate_div").html($(click_Eve).attr('data-pr-payment-date'));
        $("#ramount_div").html($(click_Eve).attr('data-pr-amount'));
        $("#ramount_inword").html($(click_Eve).attr('data-pr-amountinword') + ' only');
        $("#raddress_div").html($(click_Eve).attr('data-pr-address'));
        $("#rdonationfor_div").html($(click_Eve).attr('data-pr-donationfor'));
        var devotee = $(click_Eve).attr('data-pr-devotee');
        var receiptContents = document.getElementById(divName).innerHTML;
        console.log(receiptContents);
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = receiptContents;
        window.print();
        document.body.innerHTML = originalContents;
        $("#" + divName).css("display", 'none');
        updateReceipt(devotee);
    }
    function updateReceipt(devotee) {
        window.onafterprint = function () {
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>Devotees/update_donation_receipt',
                data: {'devotee_id': devotee},
                success: function (data) {
//                    location.reload();
                }
            });
        }
    }

</script>
<script>
    function deleteDevotee(dId) {
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Devotees/delete',
            data: {'devotee_id': dId},
            success: function (data) {
                if ($.trim(data) == 'success') {
//                    $("#row_"+dId).remove();
                    location.reload();
                }
            }
        });
        return false;
    }
    $('body').on('click', '.delete', function () {
        var id = $(this).attr('data-devotee');
        $('#confirm-delete').modal();
        $(".btn-ok").click(function () {
//            $("#row_"+id).remove();
            deleteDevotee(id);
        });
    });
    $('body').on('change', '#year_list', function () {

        var year = $(this).val();
        if (year.length !== 0) {
            $(".loadding-page").css('display', 'block');
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>Devotees/devotee_list',
                data: {'year': year},
                success: function (data) {
                    if (data) {
                        $("#example23_wrapper").html(data);
                        $('#devotee_list').DataTable({
                            dom: 'Bfrtip',
                            buttons: [{
                                    extend: 'excel',
                                    exportOptions: {
                                        columns: [0, 1, 2, 3, 4]
                                    }
                                }, {
                                    extend: 'pdf',
                                    exportOptions: {
                                        columns: [0, 1, 2, 3, 4]
                                    }
                                }, {
                                    extend: 'print',
                                    exportOptions: {
                                        columns: [0, 1, 2, 3, 4]
                                    }
                                }]
                        });
                        $(".loadding-page").css('display', 'none');
                    }
                }
            });
        }
    });

    $(document).ready(function () {
        $("#search_by").change(function () {
            var search = $(this).val();
            if (search.length !== 0) {
                $("#column_filter").removeAttr('disabled');
                $("#column_filter").val("");
                if ($.trim(search) == 'name') {
                    filterColumn('3', "");
                } else if ($.trim(search) == 'mobile') {
                    filterColumn('1', "");
                } else if ($.trim(search) == 'dpn') {
                    filterColumn('3', "");
                }
            }
        });
        $('#column_filter').on('keyup click', function () {
            var search_txt = $(this).val();
            var search_by = $("#search_by").val();
            if (search_by.length !== 0) {
                if ($.trim(search_by) == 'name') {
                    filterColumn('1', search_txt);
                } else if ($.trim(search_by) == 'mobile') {
                    filterColumn('3', search_txt);
                } else if ($.trim(search_by) == 'dpn') {
                    filterColumn('0', search_txt);
                }
            } else {
                filterColumn('1', search_txt);
            }
        });
    });
    function filterColumn(i, searchVal) {
        $('#devotee_list').DataTable().column(i).search(searchVal, false, true).draw();
    }

    $('body').on('click', '.confirm_pay', function () {
        $("#submit_amount")[0].reset();
        $(".dtamount_Erorr3").html('');
        $(".dtamount_Erorr4").html('');
        $(".dtamount_Erorr5").html('');
//        alert($(this).attr('data-dindi_id'));
        var id = $(this).attr('data-devotee').split('/');
        var name = $(this).attr('data-dtname');
        //set current year id ,removed db year-id and year
        var yearid = $(this).attr('data-dtyear_id');
        var year = $(this).attr('data-dtyear');
        var palki_year = $("#year_list option:selected").text();
        var dindi_id = $(this).attr('data-dindi_id');
        var vehical_type = $(this).attr('data-vehical_type');
        var seva_samiti_id = $(this).attr('data-seva_samiti_id');
        var dtaddress = $(this).attr('data-address');
        var dtdpnno = $(this).attr('data-dt-dpnno');
        var payment_amount = '';
        if ($(this).attr('data-payment_amount') != "")
        {
            payment_amount = $(this).attr('data-payment_amount');
        } else {
            payment_amount = '1500';
        }
        var vehicleOptNo = '';
        $("#devoteeid").val(id[0]);
        $("#padyatriid").val(id[1]);
        $("#devoteename").val(name);
        $("#devoteename").val(name);
        $("#dindiyear").html('<option value="" disabled>Select Year</option><option value="' + yearid + '" selected>' + year + '</option>');
        $("#db_year").val(year);
        $("#palki_year").val(palki_year);
//       24 Sai Palkhi Dindi No1 
//       Sai Palkhi Dindi No1
//$('#myOption').val($('#myOption').text());
        var text2 = "Sai Palkhi Dindi No1";
        if (dindi_id == '') {
//            $("#dindiname option").filter(function() {
//                    return this.text == text2; 
//                }).attr('selected', true);
            $("#dindiname").val(26).trigger("chosen:updated");
        } else {
            $("#dindiname").val(dindi_id).trigger("chosen:updated");
        }

        if (vehical_type == "Self") {
            vehicleOptNo = "Self";
        } else if (vehical_type != "Self" && vehical_type != "") {
            vehicleOptNo = "Samiti";
        }
        $("#vehicletype").val(vehicleOptNo).trigger("chosen:updated");
        $("#sevasamithi").val(seva_samiti_id).trigger("chosen:updated");
        $("#amountpaid").val(payment_amount);
        $("#dev_address").val(dtaddress);
        $("#dev_dpnno").val(dtdpnno);
        $('#modalContactForm').modal();
        $(".payamount").click(function () {
//            alert($(".error").length);
//            $('#modalContactForm').remove();
//            $('#modalContactForm').modal('hide');
//            submit_amount.form
//            pay_amount(id[0],id[1]);
//            alert(id[1]);
        });
    });
//start-----------------------------------------confirm payment---------------------------------------

    function confirm_payment(ele) {
//        var year = $(this).val();
        var devoteeid = $("#devoteeid").val();
        var padyatriid = $("#padyatriid").val();
        var devoteename = $("#devoteename").val();
        var dindiyear = $("#dindiyear").val();
        var sevasamithi = $("#sevasamithi").val();
        var dindiname = $("#dindiname").val();
        var vehicletype = $("#vehicletype").val();
        var amountpaid = $("#amountpaid").val();
        var htmlpages = '';
        if (devoteeid !== 0) {
            //$(".loadding-page").css('display', 'block');
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>Devotees/confirm_payment',
                data: {'devoteeid': devoteeid, 'padyatriid': padyatriid, 'devoteename': devoteename, 'dindiyear': dindiyear,
                    'sevasamithi': sevasamithi, 'dindiname': dindiname, 'vehicletype': vehicletype, 'amountpaid': amountpaid},
                success: function (data) {
                    if (data) {
                        $("#example23_wrapper").html(data);
//                        $("#pytreceipt").css('display','none');
                        //        ------------------------------------------
                        $("#paymentreceipt").attr('display', 'none');
//        ------------------------------------------
                        $(".loadding-page").css('display', 'none');
                    }
                }
            });
        }
    }

    function post_data(imageURL) {
        //console.log(imageURL);
        var dpnno = "<?php echo isset($dpn_no) ? $dpn_no : ''; ?>";
        $.ajax({
            url: "<?php echo base_url(); ?>Devotees/capture_receipt",
            type: "POST",
            data: {'image': imageURL, 'dpnno': dpnno},
            dataType: "png",
            success: function () {
                //alert('Success!!');				
                //location.reload();
            }
        });
    }
//end-----------------------------------------confirm payment---------------------------------------
// $('body').load(function () {
//$(function() {
// callsortfun();
// });

//$('body').on('click', '.dataTables_paginate', function () {
//alert("click");
//});
</script>
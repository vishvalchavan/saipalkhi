<div class="row print_div" style="display:none;" id="donationReceipt"> 
    <html lang="en"> 
        <head>
            <meta charset="utf-8">
            <title>A4</title>
            <style>
                table td {
                    padding:4px;
                    padding-left:0;
                }
                * {
                    margin:0px;
                    padding:0px;
                }
            </style>
        </head>
        <body style="font-family: 'Open Sans', sans-serif;" onafterprint="myFunction()">
            <div style="diplay:block;margin-left:335px;margin-top:190px;width:100px;height:100px;">
                <img id="image_div" src="<?php echo base_url('assets/images/') ?>default-profile.png" style="width:100px;height:100px; object-fit: cover; border:1px solid #000;">
            </div>

            <div style="font-size:16px !important;transform: rotate(180deg); margin-top:90px;diplay:block;margin-left:20px;width:400px;display: block;">
                <table cellpadding="2" cellspacing="2" width="100%">
                    <tr>
                        <td colspan="2"><span style="font-weight: 600;color:#7f0907;">नाव: </span> <span style="#000000" id="fullname_div"></span>
                        </td>

                    </tr>
                    <tr>
                        <td><span style="font-weight: 600;color:#7f0907;">वय: </span><span style="#000000" id="age_div"></span> वर्ष</td>
                        <td style="text-align:cener"><span style="font-weight: 600;color:#7f0907;">रक्त गट : </span><span style="#000000" id="bldgrp_div"></span></td>

                    </tr>
                    <tr>
                        <td colspan="2" style="word-wrap: break-word;"><span style="font-weight: 600;color:#7f0907;">पत्ता: </span><span style="#000000" id="address_div"></span>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="2"><span style="font-weight: 600;color:#7f0907;">संपर्क क्र. :</span> <span style="#000000" id="mobile_div"></span>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2"><span style="font-weight: 600;color:#7f0907;">नातेवाईकाचे नाव : </span><span style="#000000" id="relative_div"></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><span style="font-weight: 600;color:#7f0907;">नातेवाईकाचा संपर्क क्र. : </span><span style="#000000" id="relmobile_div"></span></td>
                    </tr>

                    <tr>
                        <td colspan="2"><div style="width:400px; float:left; padding:5px;border:2px solid #7f0907; text-align:center; color:#7f0907; font-weight: bold; font-size:18px;"><strong>DPN No.: </strong><span id="dpnno_div"></span></div></td>
                    </tr>

                </table>


            </div>


        </body>  
    </html>
</div>
<script>
    function myFunction() {
        $("#printMe").attr("display", 'none');
        $("#printMe").css("display", 'none');
    }
</script>
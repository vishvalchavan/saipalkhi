<div class="row print_div" style="display:none;" id="donationReceipt">  
    <html lang="en"> 
        <head>
            <meta charset="utf-8">
            <title>Donation Receipt</title>            
            <style>
                table td {
                    padding:4px;
                    padding-left:0;
                }
                * {
                    margin:0px;
                    padding:0px;
                }
                .main-div{
                    border: 2px solid #FF4500;
                    border-radius: 8px;
                    width: 640px;
                    margin-left: 245px;
                    margin-top: 195px;
                    padding: 20px;
                }
                .second-div{
                    width: auto;
                    /* margin-left: 275px; */
                    padding: 51px 30px 5px 0px; 
                }
                .inner-div{
                    text-align: center;
                    margin-top: -61px;
                    color: #fff;
                    font-weight: bold;
                    width: 100%;
                    margin-top: -82px;
                    margin-bottom: 17px;
                }  
                span.print-pavti {
                    background: #FF4500;
                    border-radius: 50px; 
                    font-size: 15px;
                    padding: 8px 25px 7px 25px;
                    font-weight: bold;
                }

                @media print { 

                    .print-pavti{
                        background-color: #FF4500 !important;
                        color: white !important;
                        -webkit-print-color-adjust: exact !important;  
                        padding: 10px !important;
                        border-radius: 8px !important;
                    }
                    span.print-pavti {                        
                        border-radius: 50px !important; 
                        font-size: 15px !important;
                        padding: 8px 25px 7px 25px !important;
                        font-weight: bold;
                    }
                }
                @page{
                    padding: 100px;
                }
                .main-div{
                    font-family: 'Open Sans', sans-serif;font-weight: normal;color:#000;  
                }
            </style>
        </head>
        <body class="donation_body" onafterprint="myFunction()">
            <div class="main-div">
                <div class="second-div">
                    <div class="inner-div"><span class="print-pavti"> देणगी पावती </span> </div>
                    <!--<div style="width: 575px;margin-top: 210px;font-size: 14px;margin-left: 275px;">-->
                    <table cellpadding="4" cellspacing="2" width="100%">
                        <tr>
                            <td><span style="font-weight:bold;color:#6f1512;font-size: 16px;">पावती क्र. : </span> <span id="rdpnno_div" style="font-size:16px"></span>
                            </td>
                            <td style="text-align:right"><span style="font-weight:bold;color:#7f0907;font-size: 16px; ">दिनांक  :</span>  <span id="rpaydate_div" style="font-size:17px"></span></td>
                        </tr>
                    </table>
                    <div style="height:10px;display: block;"></div>
                    <table cellpadding="4" cellspacing="2" width="100%">
                        <tr>
                            <td colspan="2">
                                <span style="float:left;"><span style="font-weight:bold;color:#6f1512;font-size: 16px; ">श्री. / सौ. / श्रीमती. : </span></span>
                                <span id="rfullname_div" style="padding-bottom: 0;float:left;width:65%;margin-left:10px;height:24px;font-size: 16.5px;"></span></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <span style="float:left;"><span style="font-weight:bold;color:#6f1512;font-size: 16px;">पत्ता  : </span></span>
                                <span id="raddress_div" style="padding-bottom: 0;float:left;width:85%;margin-left:10px;font-size: 16.5px;"></span></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <span style="float:left;"><span style="font-weight:bold;color:#6f1512;font-size: 16px;">अक्षरी रुपये : </span></span>
                                <span id="ramount_inword" style="padding-bottom: 0;float:left;width:65%;margin-left:10px;height:24px;font-size: 16.5px;"></span></td>
                        </tr>
                    </table>
                    <table cellpadding="4" cellspacing="2" width="100%">
                        <tr>
                            <td style="width:60%">
                                <span style="float:left;"><span style="font-weight:bold;color:#6f1512;font-size: 16px;">देणगी : </span>
                                    <span id="rdonationfor_div" style="padding-bottom: 0;width:60%;margin-left:10px;height:24px;font-size: 16px;"></span>
                                    <span style="font-weight:bold;color:#6f1512;font-size: 16px;margin-left:10px;">साठी </span></span></td>

                            <td style="width:40%"><span style="float:right;">&nbsp;<span style="padding:1px 3px; border:1px solid #7f0907; font-size:24px; color:#7f0907; float:right"> &nbsp; रु. <span id="ramount_div"></span>/-&nbsp;</span></span></td>
                        </tr>
                    </table>
                    <!--</div>-->
                </div>
            </div>
        </body>  
    </html>
</div>
<script>
    function myFunction() {
        $("#donationReceipt").attr("display", 'none');
        $("#donationReceipt").css("display", 'none');
    }
</script>
<div class="page-wrapper">
    <div class="container mt-4">
        <div id="overviews" class="section">
            <div class="row">            
                <div class="col-lg-12">
                    <div class="card card-signin flex-row">
                        <div class="card-body align-left">
                            <h5 class="card-title text-center">Enrollment for Palkhi 2019</h5>
                            <hr class="my-4">
                            <h3 class="card-title heading">साई पालखीला येणाऱ्या पदयात्रींनसाठी काही महत्वाच्या अटी व सूचना</h3>

                            <?php echo form_open('ccavRequestHandler', array('id' => 'termsandcondition', 'class' => 'form-signin')); ?>
                            <input type="hidden" name="param1" id="param1" value="<?php echo isset($samiti) ? $samiti : '' ?>">
                            <input type="hidden" name="param2" id="param2" value="<?php echo isset($vehicle_type) ? $vehicle_type : '' ?>">
                            <input type="hidden" name="param3" id="param3" value="<?php echo isset($dindi) ? $dindi : '' ?>">
                            <input type="hidden" name="param4" id="param4" value="<?php echo isset($devotee['dpn_no']) ? $devotee['dpn_no'] : '' ?>">
                            <input type="hidden" name="param5" id="param5" value="<?php echo isset($payment_mode) ? $payment_mode : '' ?>">
                            <input type="hidden" name="param6" id="param6" value="<?php echo isset($relativename) ? $relativename : '' ?>">
                            <input type="hidden" name="param7" id="param7" value="<?php echo isset($relativeno) ? $relativeno : '' ?>">
                            <input type="hidden" name="param8" id="param8" value="<?php echo isset($idproof) ? $idproof : '' ?>">
                            <input type="hidden" name="param9" id="param9" value="<?php echo isset($adharnumber) ? $adharnumber : '' ?>">
                            <input type="hidden" name="param10" id="param10" value="<?php echo isset($sickness) ? $sickness : '' ?>">
                            <input type="hidden" name="amount" id="amount" value="<?php echo isset($amount) ? $amount : '1500' ?>">
                            <!--<input type="hidden" name="amount" id="amount" value="1">-->
                            <input type="hidden" name="order_id" id="tid" value="" />
                            <input type="hidden" name="merchant_id" value="134572"/>
                            <input type="hidden" name="currency" value="INR"/>
                            <input type="hidden" name="billing_country" value="India"/>
                            <input type="hidden" name="redirect_url" value="<?php echo base_url() . 'enrollment-response' ?>"/>
                            <input type="hidden" name="cancel_url" value="<?php echo base_url() . 'enrollment-response' ?>"/>
                            <input type="hidden" name="language" value="EN"/>
                            <input type="hidden" name="billing_name" value="<?php echo $devotee['firstname'] . ' ' . $devotee['middlename'] . ' ' . $devotee['lastname'] ?>">
                            <input type="hidden" name="billing_address" value="<?php echo $devotee['address']; ?>">
                            <input type="hidden" name="AtPost" value="<?php echo $devotee['city']; ?>">
                            <input type="hidden" name="FullName" value="<?php echo $devotee['firstname'] . ' ' . $devotee['middlename'] . ' ' . $devotee['lastname'] ?>">     
                            <input type="hidden" name="billing_tel" value="<?php echo $devotee['mobile']; ?>">
                            <input type="hidden" name="billing_email" value="<?php echo isset($devotee['email']) ? $devotee['email'] : '' ?>">
                            <input type="hidden" name="billing_zip" value="">
                            <input type="hidden" name="billing_state" value="">
                            <input type="hidden" name="billing_city" value="<?php echo $devotee['city']; ?>">
                            <p><b>1.&nbsp;&nbsp;</b>पालखीचे सर्व मुक्काम हे दुसऱ्याच्या जागेत असून गावकर्यांना त्रास होईल असे वर्तन करू नये व त्या जागा अस्वच्छ करू नये.</p> 
                            <p><b>2.&nbsp;&nbsp;</b>पालखीत मादक पदार्थांचे सेवन करू नये, तसेच असे कोणतेही वर्तन करू नये जेणे करून पालखीला शोभणार नाही, अन्यथा कडक कारवाई करण्यात येईल. <p>
                            <p><b>3.&nbsp;&nbsp;</b>पालखीमध्ये मौल्यवान वस्तू, दागिने वैगरे आणू नये व आणल्यास त्याची जबाबदारी आमच्यावर राहणार नाही. </p>
                            <p><b>4.&nbsp;&nbsp;</b>पालखी सोहळा हा पायी वारीकरणाऱ्यांसाठी आहे, गाडीत बसून येणाऱ्यांसाठी नव्हे.</p>
                            <p><b>5.&nbsp;&nbsp;</b>भक्तांनी पालखी सोहळ्या मध्ये, पालखी निशाणाच्या पुढे जाऊ नये. </p>
                            <p><b>6.&nbsp;&nbsp;</b>साईभक्तांनी पालखीला सहलीचे स्वरूप येईल असे वर्तन करू नये.</p>

                            <div id="remember" class="checkbox float-left mb-2 font-weight-bold">
                                <label>
                                    <input type="checkbox" name="remember" id="remember" class="font-weight-bold" data-error=".error1">&nbsp;&nbsp; वरील सर्व नियम मी वाचले असून मला ते मान्य आहेत.
                                </label>
                                <div class="input-field">
                                    <div class="error1 error-msg"></div>
                                    <?php echo form_error('remember'); ?>
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                            <div class="form-group float-left">
                                <button type="submit" class="btn btn-info border-0">Next</button>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    window.onload = function () {
        var d = new Date().getTime();
        document.getElementById("tid").value = d;
    };
</script>
<div class="page-wrapper">
    <div class="container-fluid mt-4">
        <div id="overviews" class="section">
            <div class="wp-caption alignnone">
                <h3>Thank you for showing interest to join Shri Sai Palkhi Sohla – <?php echo date("Y"); ?>.</h3>
                <p>Please check your mobile message for further steps to login to the account and complete registration process</p>
                <div class="row">
                    <div class="col-12"><a href='<?php echo base_url() . 'login' ?>' class="lgn-link">Click here to login</a></div>
                </div>
            </div>
        </div>
    </div>
</div>

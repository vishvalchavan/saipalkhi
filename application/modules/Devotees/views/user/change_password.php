<div class="bg-image no-wrapper">
  <div class="">
    <div id="overviews" class="section">
        <div class="container-fluid mt-4">
            <div class="float-right">
            <div class="row">
                <div class="col-lg-12 mt-5">
                    <div class="col-lg-12 mt-5">
                        <div class="card card-signin flex-row mt-5">
                            <div class="card-body">
                                <h5 class="card-title text-center Register-text">Change Your Password</h5>
                                <hr class="mb-2">
                                <form class="form-signin" autocomplete="off" method="post" action="<?php echo base_url('changepassword'); ?>" id="devotee_change_password">
                                    <div class="row mb-2 mt-4">
                                        <div class="col-md-12">
                                            <div class="form-group required row mb-0">
                                                <div class="col-md-12">
                                                    <input type="password" class="form-control ht-40" placeholder="Enter your Current Password" name="old_password" id="old_password" data-error=".error1">
                                                    <div class="input-field error-msg">
                                                        <div class="error1 error-msg"></div>
                                                        <?php echo form_error('old_password'); ?>
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <div class="col-md-12">

                                            <div class="form-group required row mb-0">
                                                <div class="col-md-12">
                                                    <input type="password" class="form-control ht-40 " placeholder="New Password" name="new_password" id="new_password" data-error=".error2">
                                                    <div class="input-field error-msg">
                                                        <div class="error2 error-msg"></div>
                                                        <?php echo form_error('new_password'); ?>
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-4">
                                        <div class="col-md-12">

                                            <div class="form-group required row mb-0">
                                                <div class="col-md-12">
                                                    <input type="password" class="form-control ht-40" placeholder="Confirm Password" name="confirm_password" id="confirm_password" data-error=".error3">
                                                    <div class="input-field error-msg">
                                                        <div class="error3 error-msg"></div>
                                                        <?php echo form_error('confirm_password'); ?>
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-lg btn-primary btn-block btn-signin">Change Password</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
</div>
<div class="page-wrapper1">
    <div class="container">
        <div id="overviews" class="section-container">
            <div class="card">
                <div class="card-body">
                    <div class="row page-titles">
                        <div class="col-md-3 align-self-center">
                            <h2 class="card-title heading">Account Details</h2>                           
                        </div>
                        <!--                        <div class="col-md-5 align-self-center">
                                                </div>-->
                        <div class="col-md-9 align-self-center text-right">
                            <?php
                            if (!empty($devotee_details['firstname']) && !empty($devotee_details['middlename']) && !empty($devotee_details['lastname']) && !empty($devotee_details['birth_date']) && !empty($devotee_details['mobile']) && !empty($devotee_details['address']) && !empty($devotee_details['city']) && !empty($devotee_details['profession']) && !empty($devotee_details['gender']) && !empty($devotee_details['blood_group']) && !empty($devotee_details['pincode'])) {
                                if (date('Y-m-d') <= date('Y-m-d', strtotime('17 July 2019')) && date('Y-m-d') >= date('Y-m-d', strtotime('-3 months 5 july 2019'))) {
                                    if (isset($payment_info) && !empty($payment_info)) {
                                        if (strtolower($payment_info[0]['payment_status']) == "success") {
                                            ?>
                                            <a><button type="button" class="btn btn-info buttonacceptpayment" disabled> <i class="fa fa-check">&nbsp;</i>Enrollment for Palakhi <?php echo date('Y') ?></button></a>
                                            <p class="pay-msg">Payment paid on <?php echo date('d F Y', strtotime($payment_info[0]['payment_date'])); ?></p>
                                            <?php
                                        } else {
                                            ?>
                                            <a href="<?php echo base_url() . 'enrollment' ?>"><button type="button" class="btn btn-info">Enrollment for Palakhi <?php echo date('Y') ?></button></a>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <a href="<?php echo base_url() . 'enrollment' ?>"><button type="button" class="btn btn-info">Enrollment for Palakhi <?php echo date('Y') ?></button></a>
                                        <?php
                                    }
                                }
                            } else {
                                ?>
                                <a id="enroll"><button type="button" class="btn btn-info">Enrollment for Palakhi <?php echo date('Y') ?></button></a>
                            <?php }
                            ?>
                        </div>
                        <span class="text-danger ml-3" style="font-size:12px;">Note:  &nbsp;Profile Image Height and Width should be in between 50px to 100px.</span>
                    </div>
                    <hr>
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <?php echo form_open_multipart('AccountDetails', array('id' => 'register_devotee', 'class' => 'form-horizontal')); ?>
                                <!--<form action="#" class="form-horizontal">-->
                                <input type="hidden" name="dpn" value="<?php echo isset($devotee_details['dpn_no']) ? $devotee_details['dpn_no'] : ''; ?>">
                                <input type="hidden" name="joinyear" value="<?php echo isset($devotee_details['date_of_joining']) ? date("Y", strtotime($devotee_details['date_of_joining'])) : date("Y"); ?>">
                                <div class="form-body">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group required row">
                                                <label class="control-label col-md-4"> Name:</label>
                                                <div class="col-md-8 row pr-0">
                                                    <div class="col-md-4 col-md-15 pr-2">
                                                        <input type="text" placeholder="First Name" class="form-control" name="firstname" id="firstname" value="<?php echo set_value('firstname', $devotee_details['firstname']); ?>" data-error=".Erorr1">
                                                        <div class="input-field">
                                                            <div class="Erorr1 error-msg"></div>
                                                            <?php echo form_error('firstname'); ?>
                                                        </div> 
                                                    </div>
                                                    <div class="col-md-4 col-md-15 pl-0 pr-0">
                                                        <input type="text" class="form-control" placeholder="Middle Name" name="middlename" id="middlename" value="<?php echo set_value('middlename', $devotee_details['middlename']); ?>" data-error=".Erorr2">
                                                        <div class="input-field">
                                                            <div class="Erorr2 error-msg"></div>
                                                            <?php echo form_error('middlename'); ?>
                                                        </div> 
                                                    </div>
                                                    <div class="col-md-4 col-md-15 pr-0 pl-2">
                                                        <input type="text" class="form-control" placeholder="Last Name" name="lastname" id="lastname" value="<?php echo set_value('lastname', $devotee_details['lastname']); ?>" data-error=".Erorr3">
                                                        <div class="input-field">
                                                            <div class="Erorr3 error-msg"></div>
                                                            <?php echo form_error('lastname'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--/span-->

                                            <div class="form-group required row">
                                                <label class="control-label col-md-4">Date of Birth:</label>
                                                <div class="col-md-8">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" placeholder="dd-mm-yyyy" name="dob" id="dob" value="<?php echo set_value('dob', date("d F Y", strtotime($devotee_details['birth_date']))); ?>" data-error=".Erorr4" readonly>
                                                    </div>
                                                    <div class="input-field">
                                                        <div class="Erorr4 error-msg"></div>
                                                        <?php echo form_error('dob'); ?>
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 ">
                                            <div class="form-group row">
                                                <label class="control-label col-md-4">Profile Image:</label>
                                                <div class="col-md-8 bg-f9f9f9">
                                                    <div class="input-group my-2">

                                                        <!--<span class="input-group-btn" style="width: 100%;">-->
                                                        <?php
                                                        $user_image_path = '';
                                                        $hiddenclass = "";
                                                        if (file_exists($devotee_details['user_image_path'])) {
                                                            $user_image_path = $devotee_details['user_image_path'];
                                                            $hiddenclass = "disabled";
                                                        } else {
                                                            $user_image_path = base_url('assets/images/default-profile.png');
                                                            $hiddenclass = "";
                                                        }
                                                        ?>
                                                        <img id='img-upload' name="img_upload" class="img-thumbnail" src="<?php echo $user_image_path; ?>" style="width:80px;height:80px;"/>
                                                        <div id="uploaded_image"></div>   
                                                        <span class="btn btn-default btn-file" id="select_image">
                                                            Browse
                                                        </span>
                                                        <input type="file" id="profile_pic" class="pull-right" name="profile_image" value="<?php echo $user_image_path; ?>" style="display:none;opacity: 0;
                                                               position: absolute; z-index: 9999;">
                                                        <!--</span>-->
                                                    </div>
                                                    <div class="input-field">
                                                        <div class="errorprofilepic error-msg"></div>
                                                        <?php echo form_error('profile_image'); ?>
                                                    </div> 
                                                </div>                                                
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group required row">
                                                <label class="control-label col-md-4">Mobile No:</label>
                                                <div class="col-md-8">
                                                    <div class="">
                                                        <div class="row">
                                                            <div class="col-sm-3 col-4">
                                                                <input type="text" class="form-control" placeholder="+91" value="+91" name="countrycode" readonly>
                                                            </div>
                                                            <div class="col-sm-9 col pl-0">
                                                                <input type="text" class="form-control" placeholder="Enter Mobile no" name="mobileno" id="mobileno" value="<?php echo set_value('mobileno', substr($devotee_details['mobile'], 2)); ?>" data-error=".Erorr5" readonly>
                                                                <div class="input-field">
                                                                    <div class="Erorr5 error-msg"></div>
                                                                    <?php echo form_error('mobileno'); ?>
                                                                </div> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group required row">
                                                <label class="control-label col-md-4">Blood Group:</label>
                                                <div class="col-md-8">
                                                    <!--<input type="text" class="form-control form-control-danger" placeholder="Blood Group">-->
                                                    <select class="form-control custom-select" name="bloodgroup" id="bloodgroup" data-error=".Erorr6">
                                                        <option value="">Select Blood Group</option>
                                                        <?php $selected_bldgrp = set_value('bloodgroup', $devotee_details['blood_group']); ?>
                                                        <option value="A+" <?php echo ($selected_bldgrp == "A+") ? 'selected' : ''; ?> >A+</option>
                                                        <option value="A-" <?php echo ($selected_bldgrp == "A-") ? 'selected' : ''; ?> >A-</option>
                                                        <option value="B+" <?php echo ($selected_bldgrp == "B+") ? 'selected' : ''; ?> >B+</option>
                                                        <option value="B-" <?php echo ($selected_bldgrp == "B-") ? 'selected' : ''; ?> >B-</option>
                                                        <option value="O+" <?php echo ($selected_bldgrp == "O+") ? 'selected' : ''; ?> >O+</option>
                                                        <option value="O-" <?php echo ($selected_bldgrp == "O-") ? 'selected' : ''; ?> >O-</option>
                                                        <option value="AB+" <?php echo ($selected_bldgrp == "AB+") ? 'selected' : ''; ?> >AB+</option>
                                                        <option value="AB-" <?php echo ($selected_bldgrp == "AB-") ? 'selected' : ''; ?> >AB-</option>
                                                        <option value="unknown" <?php echo ($selected_bldgrp == "unknown") ? 'selected' : ''; ?> >Unknown</option>
                                                    </select>
                                                    <div class="input-field">
                                                        <div class="Erorr6 error-msg"></div>
                                                        <?php echo form_error('bloodgroup'); ?>
                                                    </div>  
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label col-md-4">Sec Mobile:</label>
                                                <div class="col-md-8">
                                                    <div class="">
                                                        <div class="row">
                                                            <div class="col-sm-3 col-4">
                                                                <input type="text" class="form-control" placeholder="+91" value="+91">
                                                            </div>
                                                            <div class="col-sm-9 col pl-0">
                                                                <input type="text" class="form-control" placeholder="Enter Mobile no" name="secmobileno" id="secmobileno" value="<?php echo set_value('secmobileno', substr($devotee_details['mobile1'], 2)); ?>" data-error=".Erorr7">
                                                                <div class="input-field">
                                                                    <div class="Erorr7 error-msg"></div>
                                                                    <?php echo form_error('secmobileno'); ?>
                                                                </div> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group required row">
                                                <label class="control-label col-md-4">Gender:</label>
                                                <div class="col-md-8">
                                                    <select class="form-control custom-select" name="gender" data-error=".Erorr8">
                                                        <option value="">Select Gender</option>
                                                        <?php $selected_gender = set_value('gender', $devotee_details['gender']); ?>
                                                        <option value="Male" <?php echo ($selected_gender == "Male") ? 'selected' : ''; ?> >Male</option>
                                                        <option value="Female" <?php echo ($selected_gender == "Female") ? 'selected' : ''; ?>>Female</option>
                                                        <option value="other" <?php echo ($selected_gender == "other") ? 'selected' : ''; ?>>Other</option>
                                                    </select>
                                                    <div class="input-field">
                                                        <div class="Erorr8 error-msg"></div>
                                                        <?php echo form_error('gender'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group required row">
                                                <label class="control-label col-md-4">Address:</label>
                                                <div class="col-md-8">
                                                    <textarea rows="2" class="form-control" placeholder="Enter address" name="address" id="address" data-error=".Erorr9"><?php echo set_value('address', $devotee_details['address']); ?></textarea>
                                                    <div class="input-field">
                                                        <div class="Erorr9 error-msg"></div>
                                                        <?php echo form_error('address'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group required row">
                                                <label class="control-label col-md-4">City:</label>
                                                <div class="col-md-8">
                                                    <!--<input type="text" class="form-control form-control-danger" placeholder="City" name="city" id="city" value="<?php echo set_value('city', $devotee_details['city']); ?>" data-error=".Erorr11">-->
                                                    <select class="form-control custom-select" name="city" id="city" data-error=".Erorr11">
                                                        <option value="">Select City</option>   
                                                        <?php
                                                        if (isset($city_list) && !empty($city_list)) {
                                                            foreach ($city_list as $city) {
                                                                $selected = '';
                                                                if (isset($devotee_details['city']) && $devotee_details['city'] == $city['name']) {
                                                                    $selected = 'selected';
                                                                }
                                                                ?>
                                                                <option value="<?php echo $city['name']; ?>" <?php echo $selected; ?>><?php echo ucfirst($city['name']); ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                    <div class="input-field">
                                                        <div class="Erorr11 error-msg"></div>
                                                        <?php echo form_error('city'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group required row">
                                                <label class="control-label text-left col-md-4">Pin Code:</label>
                                                <div class="col-md-8">
                                                    <input type="text" name="pincode" id="pincode" class="form-control form-control-danger" placeholder="Enter PIN Code here" value="<?php echo set_value('pincode', $devotee_details['pincode']); ?>" data-error=".Erorr19">
                                                    <div class="input-field">
                                                        <div class="Erorr19 error-msg"></div>
                                                        <?php echo form_error('pincode'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label col-md-4">Email:</label>
                                                <div class="col-md-8">
                                                    <input type="email" class="form-control" placeholder="Enter Email" name="email" id="email" value="<?php echo set_value('email', $devotee_details['email']); ?>" data-error=".Erorr13">
                                                    <div class="input-field">
                                                        <div class="Erorr13 error-msg"></div>
                                                        <?php echo form_error('email'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group required row">
                                                <label class="control-label col-md-4">Occupation:</label>
                                                <div class="col-md-8">
                                                    <!--<input type="text" class="form-control" placeholder="Enter Profession" name="profession" id="profession" value="<?php echo set_value('profession', $devotee_details['profession']); ?>" data-error=".Erorr15">-->
                                                    <select class="form-control custom-select" name="profession" id="profession" data-error=".Erorr15">
                                                        <option value="">Select Occupation</option>    
                                                        <?php
                                                        if (isset($profession_list) && !empty($profession_list)) {
                                                            foreach ($profession_list as $profession) {
                                                                $selected = '';
                                                                if (isset($devotee_details['profession']) && $devotee_details['profession'] == $profession['id']) {
                                                                    $selected = 'selected';
                                                                }
                                                                ?>
                                                                <option value="<?php echo $profession['id']; ?>" <?php echo $selected; ?>><?php echo $profession['occupation'] ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>                                                                                                                                                                                                                                   
                                                    </select>
                                                    <div class="input-field">
                                                        <div class="Erorr15 error-msg"></div>
                                                        <?php echo form_error('profession'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--                                        <div class="col-md-6">
                                                                                    <div class="form-group required row">
                                                                                        <label class="control-label col-md-4">Education:</label>
                                                                                        <div class="col-md-8">
                                                                                            <input type="text" class="form-control" placeholder="Education" name="education" id="education" value="<?php echo set_value('education', $devotee_details['education']); ?>" data-error=".Erorr16">
                                                                                            <select class="form-control custom-select" name="education" id="education" data-error=".Erorr16">
                                                                                                <option value="">Select Education</option>    
                                        <?php
                                        if (isset($education_list) && !empty($education_list)) {
                                            foreach ($education_list as $education) {
                                                $selected = '';
                                                if (isset($devotee_details['education']) && $devotee_details['education'] == $education['id']) {
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                                                                                                                                                                                                                                                                                                                        <option value="<?php echo $education['id']; ?>" <?php echo $selected; ?>><?php echo $education['education'] ?></option>
                                                <?php
                                            }
                                        }
                                        ?>                                                                                                                                                                                                                                   
                                                                                            </select>
                                                                                            <div class="input-field">
                                                                                                <div class="Erorr16 error-msg"></div>
                                        <?php echo form_error('education'); ?>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>  -->
                                    </div>                                    

                                    <hr>
                                    <input type="hidden" class="pull-left" id='hiddenImgTxt' name="hiddenImgTxt"  data-imgsrc="<?php echo $user_image_path; ?>" value="<?php echo (file_exists($devotee_details['user_image_path'])) ? $devotee_details['user_image_path'] : ''; ?>" data-error=".errorprofilepic" style="display:block;" readonly />   
                                    <div class="form-actions float-right">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row m-r-14">
                                                    <div class="m-left--40">                                                    
                                                        <button type="submit" class="btn btn-info">Update Record</button>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-6"> </div>
                                        </div>
                                    </div>
                                    <!--</form>-->
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--//-cropie---------------------------------------------------------------------------->

<div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header mod-header">
                <h4 class="modal-title mod-head">Crop & Upload Image</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>                
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div id="image_demo" style="width:100%;"></div>
                    </div>
                </div>   
                <div class="row">
                    <div class="col-md-4 mb-1 float-left ">                                       
                        <button class="vanilla-rotate btn btn-primary font-12" id="rightrotate" data-deg="90">Rotate Left</button> 
                    </div>
                    <div class="col-md-4 mb-1 float-right">
                        <button class="btn btn-success crop_image" id="crop">Crop Image</button>

                    </div>
                    <div class="col-md-4 float-right">     
                        <button class="vanilla-rotate btn btn-primary font-12" id="leftrotate" data-deg="-90">Rotate Right</button>
                    </div>
                </div>
                <!--       <div class="col-md-12">
                             <div class="col-md-4 float-left">
                                  <button class="vanilla-rotate btn btn-info" id="rightrotate" data-deg="90">Rotate Left</button>
                      </div>
                      <div class="col-md-4 float-right">
                          <button class="btn btn-success crop_image" id="crop">Crop Image</button>
                      </div>
                   
                      <div class="col-md-4 float-right">
                      <button class="vanilla-rotate btn btn-info" id="leftrotate" data-deg="-90">Rotate Right</button>
                      </div>
                  </div> -->
                <div style="clear:both;"></div>
                <div class="row mt-3">
                    <div class="col-md-4 pull-left">
                        <img src="#" id="croped_img" style="display:none;">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-sm" id="croppingdone" disabled>Ok</button>
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- <!end--//-cropie----------------------------------------------------------------------------> 


<script>
    $("#idproof").change(function () {
        var selected = $(this).val();
        var text = $("#idproof option:selected").text();
        if (selected == 1 || $.trim(text) == 'Aadhaar card') {
            $("#aadharno").css('display', 'flex');
        } else {
            $("#aadharno").css('display', 'none');
        }
    });


</script>
<!--//-cropie js---------------------------------------------------------------------------->
<script>
    $(document).ready(function () {

        $image_crop = $('#image_demo').croppie({
            enableExif: true,
            viewport: {
                width: 100,
                height: 100,
                type: 'square' //circle
            },
            boundary: {
//                width: 300,
                height: 300
            },
            enableOrientation: true
        });
        $('.vanilla-rotate').on('click', function (ev) {
            $image_crop.croppie('rotate', parseInt($(this).data('deg')));
        });
        $('#profile_pic').on('change', function () {
            //                    console.log(input.files);
            var flag = '';
            if (this.files && this.files[0]) {
                if (this.files[0]['type'] == 'image/jpeg' || this.files[0]['type'] == 'image/jpg' || this.files[0]['type'] == 'image/png') {
                    var reader = new FileReader();
                    //                            reader.readAsDataURL(input.files[0]);
                    reader.onload = function (e) {
                        $image_crop.croppie('bind', {
                            url: event.target.result,
                            orientation: 4
                        }).then(function () {
//                            console.log('jQuery bind complete');
                            $('#profile_pic').val("");
                        });
                        //                        $("#hiddenImgTxt").val(e.target.result);
                        var image = new Image();
                        image.src = e.target.result;
                    }

                    reader.readAsDataURL(this.files[0]);
                    $('#uploadimageModal').modal('show');
                } else {
                    $("#croped_img").css('display', 'none');
                    $(".errorprofilepic").html('Please select an image with valid extension.');
                    $("#hiddenImgTxt").val("");
                }
            } else {
                $(".errorprofilepic").html('');
//                $("#hiddenImgTxt").val("");
//                $('#img-upload').attr('src', '');
//                $('#img-upload').css('display', 'none');
            }
        });

        $("#crop").on("click", function () {
            $("#croppedimage").remove();
            $("#croped_img").css('display', 'block');
            $image_crop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (response) {
                $("#croped_img").attr('src', response);

//                $("#hiddenImgTxt").val(response);
//                $('#img-upload').attr('src', response);
                var myImg = document.querySelector("#croped_img");
                var height = myImg.naturalWidth;
                var width = myImg.naturalHeight;

            });
            $("#croppingdone").removeAttr('disabled');
        });

        $("#croppingdone").click(function (e) {
            var crop_img = document.querySelector("#croped_img");
            var crop_height = crop_img.naturalWidth;
            var crop_width = crop_img.naturalHeight;
            if (parseInt(crop_height) < 50 || parseInt(crop_height) > 100)
            {
                flag = "true";
                $("#hiddenImgTxt").val('Height');
                $(".errorprofilepic").html('Height should be in between 50px to 100px.');
            } else if (parseInt(crop_width) < 50 || parseInt(crop_width) > 100) {
                flag = "true";
                $("#hiddenImgTxt").val('Width');
                $(".errorprofilepic").html('Width should be in between 50px to 100px.');
            } else {
                $(".errorprofilepic").html("");
                $("#hiddenImgTxt").val(crop_img.src);
                $('#uploadimageModal').modal('hide');
                $('#img-upload').attr('src', crop_img.src);
                $("#croppingdone").attr('disabled', 'disabled');
            }
        });
    });
    $(document).mouseup(function (e) {
        var container = $("#uploadimageModal");

        // If the target of the click isn't the container
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            //        container.hide();
            $("#croped_img").removeAttr('src');
        }
    });
</script>
<!-- <!end--//-cropie js----------------------------------------------------------------------------> 
<script type="text/javascript">

    $(document).ready(function () {
        var rot = 0, ratio = 1, CanvasCrop;

        $(document).on('change', '.btn-file :file', function () {
            var input = $(this),
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function (event, label) {
            var input = $(this).parents('.input-group').find(':text'),
                    log = label;
            if (input.length) {
                input.val(log);
            } else {
                if (log)
                    (log);
            }
        });

        $("#select_image").click(function () {
            //            $("#hiddenImgTxt").val('');
            $('#profile_pic').trigger('click');
        });
        $("form").submit(function (e) {
            if ($("#errorprofilepic").val() == "") {
                e.preventDefault();
            }
        });
        $("#enroll").click(function () {
            $(".accountbody").html('Please Fill all the required (<font style="color:red">*</font>) fields and update your details.');
            $("#account-details").modal();
            $(".modal-backdrop").css('display', 'block');
//            alert('Please Fill all the required fields and update your information.');
        });
    });
</script>
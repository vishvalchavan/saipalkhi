<div class="page-wrapper">
    <div class="container-fluid mt-4">
        <div id="overviews" class="section">
            <div class="row">
                <div class="col-lg-5 mobile-p0">
                    <div class="col-lg-12">
                        <div class="card card-signin flex-row ">
                            <div class="card-body p-2">
                                <h5 class="card-title text-center mb-0">Enrollment for Palkhi <?php echo date("Y"); ?></h5>
                                <hr class="my-2">
                                <?php echo form_open('enrollment', array('id' => 'enrollment', 'class' => 'form-horizontal')); ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group required row">
                                            <label class="control-label col-md-4">Relative/Referred Name:</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" placeholder="Enter a Name" name="relativename" id="relativename" value="<?php echo set_value('relativename', isset($padyatri_details[0]['relative_name']) ? $padyatri_details[0]['relative_name'] : ''); ?>" data-error=".Erorr10">
                                                <div class="input-field">
                                                    <div class="Erorr10 error-msg"></div>
                                                    <?php echo form_error('relativename'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group required row">
                                            <label class="control-label col-md-4">Relative/Referred Mobile:</label>
                                            <div class="col-md-8">
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col-sm-3 col-4">
                                                            <input type="text" class="form-control" placeholder="+91" value="+91">
                                                        </div>
                                                        <div class="col-sm-9 col pl-0">
                                                            <input type="text" class="form-control" placeholder="Enter Mobile no" name="relativeno" id="relativeno" value="<?php echo set_value('relativeno', isset($padyatri_details[0]['relative_mobile']) ? substr($padyatri_details[0]['relative_mobile'], 2) : ''); ?>" data-error=".Erorr12">
                                                            <div class="input-field">
                                                                <div class="Erorr12 error-msg"></div>
                                                                <?php echo form_error('relativeno'); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="control-label col-md-4">Sickness:</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" placeholder="Enter sickness" name="sickness" id="sickness" value="<?php echo set_value('sickness', isset($padyatri_details[0]['sickness']) ? $padyatri_details[0]['sickness'] : ''); ?>" data-error=".Erorr14">
                                                <div class="input-field">
                                                    <div class="Erorr14 error-msg"></div>
                                                    <?php echo form_error('sickness'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>   
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group required row">
                                            <label class="control-label col-md-4">Identity Proof:</label>
                                            <div class="col-md-8">
                                                <!--<input type="text" class="form-control" placeholder="PAN">-->
                                                <select class="form-control custom-select" name="idproof" id="idproof" data-error=".Erorr17">
                                                    <option value="">Select ID Proof</option>    
                                                    <?php
                                                    if (isset($idproof_list) && !empty($idproof_list)) {
                                                        foreach ($idproof_list as $id) {
                                                            $selected = '';
                                                            if (isset($padyatri_details[0]['identity_prof']) && $padyatri_details[0]['identity_prof'] == $id['id']) {
                                                                $selected = 'selected';
                                                            }
                                                            ?>
                                                            <option value="<?php echo $id['id']; ?>" <?php echo $selected; ?>><?php echo $id['document_type'] ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>                                                                                                                                                                                                                                   
                                                </select>
                                                <div class="input-field">
                                                    <div class="Erorr17 error-msg"></div>
                                                    <?php echo form_error('idproof'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row" id="aadharno" style="<?php echo isset($padyatri_details[0]['identity_prof']) && $padyatri_details[0]['identity_prof'] == 1 ? "" : "display:none;" ?>">
                                            <label class="control-label col-md-4">Aadhaar Number:</label>
                                            <div class="col-md-8">
                                                <input type="text" name="adharnumber" id="adharnumber" class="form-control aadharcard" placeholder="Enter Aadhar No." value="<?php echo set_value('adharnumber', isset($padyatri_details[0]['adhaar_no']) ? $padyatri_details[0]['adhaar_no'] : ''); ?>" data-error=".Erorr18" maxlength="14">
                                                <div class="input-field">
                                                    <div class="Erorr18 error-msg"></div>
                                                    <?php echo form_error('adharnumber'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group required row">                 
                                            <label class="control-label col-md-4 mb-0 mt-2">Payment Option:</label>
                                            <div class="col-md-8">
                                                <select class="form-control" id="payment_mode" name="payment_mode" data-error=".error5">
                                                    <option value="">Select Payment Mode</option> 
                                                    <option value="online" <?php echo isset($padyatri_details[0]['payment_mode']) && $padyatri_details[0]['payment_mode'] == "online" ? 'selected' : ''; ?>>Online</option>
                                                    <option value="palakhibhavan" <?php echo isset($padyatri_details[0]['payment_mode']) && $padyatri_details[0]['payment_mode'] == "palakhibhavan" ? 'selected' : ''; ?>>At Sai Palkhi Bhavan</option>
                                                    <option value="bank" <?php echo isset($padyatri_details[0]['payment_mode']) && $padyatri_details[0]['payment_mode'] == "bank" ? 'selected' : ''; ?>>In Bank Account</option>
                                                </select>
                                                <div class="input-field">
                                                    <div class="error5 error-msg"></div>
                                                    <?php echo form_error('payment_mode'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group required row">
                                            <label class="control-label col-md-4 mb-0 mt-2">Dindi:</label>
                                            <div class="col-md-8">
                                                <select class="form-control" id="dindi" name="dindi" data-error=".error3">     
                                                    <option value="">Select Dindi</option>
                                                    <?php
                                                    if (isset($dindi_list) && !empty($dindi_list)) {
                                                        foreach ($dindi_list as $dindi) {
                                                            ?>
                                                            <option value="<?php echo $dindi['id']; ?>" <?php echo isset($padyatrii_details[0]['dindi_id']) && $padyatrii_details[0]['dindi_id'] == $dindi['id'] ? 'selected' : $dindi['id'] == 26 || $dindi['name'] == 'Sai Palkhi Samiti Dindi (default)' ? 'selected' : ''; ?>><?php echo $dindi['name']; ?></option> 
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <div class="input-field">
                                                    <div class="error3 error-msg"></div>
                                                    <?php echo form_error('dindi'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group required row">                 
                                            <label class="control-label col-md-4 mb-0 mt-2">Vehicle Type:</label>
                                            <div class="col-md-8">
                                                <select class="form-control" id="vehicle_type" name="vehicle_type" data-error=".error2">
                                                    <option value="">Select Vehicle Type</option> 
                                                    <option value="Self" <?php echo isset($padyatri_details[0]['vehical_type']) && $padyatri_details[0]['vehical_type'] == "Self" ? 'selected' : ''; ?>>Self</option>
                                                    <option value="Samiti" <?php echo isset($padyatri_details[0]['vehical_type']) && $padyatri_details[0]['vehical_type'] == "Samiti" ? 'selected' : ''; ?>>Samiti Transport</option>
                                                </select>
                                                <div class="input-field">
                                                    <div class="error2 error-msg"></div>
                                                    <?php echo form_error('vehicle_type'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">    
                                            <label class="control-label col-md-4 mb-0 mt-2">Seva Samiti:</label>
                                            <div class="col-md-8">
                                                <select class="form-control" id="samiti" name="samiti" data-error=".error1">
                                                    <option value="">Select Seva Smiti</option>
                                                    <?php
                                                    if (isset($samiti_list) && !empty($samiti_list)) {
                                                        foreach ($samiti_list as $samiti) {
                                                            ?>
                                                            <option value="<?php echo $samiti['id']; ?>" <?php echo isset($padyatri_details[0]['seva_samiti_id']) && $padyatri_details[0]['seva_samiti_id'] == $samiti['id'] ? 'selected' : ''; ?>><?php echo $samiti['name']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <div class="input-field">
                                                    <div class="error1 error-msg"></div>
                                                    <?php echo form_error('samiti'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group required row">
                                            <label class="control-label col-md-4 mb-0 mt-2">Amount:</label>
                                            <div class="col-md-8 ">
                                                <input type="text" class="form-control" placeholder="2" name="amount" value="1500" data-error=".error4" readonly>
                                                <div class="input-field">
                                                    <div class="error4 error-msg"></div>
                                                    <?php echo form_error('amount'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <label class="control-label col-md-4"></label>
                                            <div class="col-md-8">
                                                <button type="submit" class="btn btn-info border-0">Next</button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <h2 class="card-title text-left tablet-margin-top4 ml-5">फॉर्म भरण्यासाठी सूचना -</h2>
                    <ul class="list-ul">
                        <li class="bullet-none bullet-none wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold">&#9672;</small>&nbsp;Relative/Referred Name:  आपल्या नातेवाईकाचे अथवा जवळच्या व्यक्तीचे नाव टाका.</li>
                        <li class="bullet-none bullet-none wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold">&#9672;</small>&nbsp;Relative/Referred Mobile: आपल्या नातेवाईकाचे अथवा जवळच्या व्यक्तीचा मोबाईल नंबर इथे भरा.</li>
                        <li class="bullet-none bullet-none wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold">&#9672;</small>&nbsp;Sickness :आपल्यास काही आजार असल्यास (ब्लडप्रेशर, डायबिटीस ) त्याची नोंद इथे करा. </li>
                        <li class="bullet-none wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold">&#9672;</small>&nbsp;Identity Proof: आपण पालखी मध्ये सहभागी होताना कोणते ओळखपत्र सोबत आणणार आहात ते इथे निवडा. </li>
                        <li class="bullet-none bullet-none wow fadeInRight animated"> <small class="font-14-bold">&#9672;</small>&nbsp;Payment Option - पदयात्री नावनोंदणी फी भरण्यासाठी योग्य तो पर्याय निवडा -
                            <br>
                            <div class="col-md-11 col-sm-11 col-lg-11">
                                <p>a.&nbsp;&nbsp;Online - ऑनलाईन फी भरण्यासाठी हा पर्याय निवडा - ऑनलाईन व्यवहारासाठी पालखी समिती "CC Avenue" ची सर्व्हिस वापरते, जी अतिशय सुरक्षित आहे. </p>
                                <p>b.&nbsp;&nbsp;At Sai Palkhi Bhavan - आपण पालखी भवनमध्ये येऊन, रोखीने पैसे भरणार असाल तर हा पर्याय निवडा. </p>
                                <p>c.&nbsp;&nbsp;In Bank Account - पदयात्री फी आपण, समितीच्या बँक ऑफ इंडियाच्या अकाउंट मध्ये सुद्धा जमा करू शकता.  फक्त बँकेत पैसे भरल्यानंतर समितीच्या खालील पैकी एका कार्यकर्त्याला फोन करून आवश्य कळवावे. 
                                    बँक ऑफ इंडिया पुणे शहर शाखा
                                    A/c no - 050610100005667
                                    isfc code – 0000506</p>
                            </div>
                        </li>
                        <li class="bullet-none wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold">&#9672;</small>&nbsp;Dindi: आपण ज्या दिंडी सोबत पालखीमध्ये चालणार आहेत त्या दिंडीचे नाव इथे निवडा. जर आपण कोणत्याही दिंडी मध्ये सहभागी नसाल तर "Sai Palkhi Samiti (default)" हि दिंडी निवडा.</li>
                        <li class="bullet-none wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold">&#9672;</small>&nbsp;Vehicle Type: आपण आपल्या बॅग्स समितीच्या ट्रक मध्ये ठेवणार आहात की आपली स्वतःची गाडी असणार आहे ?</li>
                        <li class="bullet-none wow fadeInRight animated mb-2" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold">&#9672;</small>&nbsp;Seva Samiti: साई पालखी मध्ये सध्या अनेक समित्या कार्यन्वत आहेत, आपण कोणत्या समिती मध्ये सेवा करण्यास इच्छुक आहात ?</li>
                        <li class="bullet-none wow fadeInRight animated mb-2" style="visibility: visible; animation-name: fadeInRight;"> 
                            <div class="tip">
                                टीप - श्री साईबाबा पालखी समितीने ऑनलाईन पेमेंट साठी "CC Avenue" या प्रसिद्ध कंपनी सोबत करार केला आहे, जेणे करून आपल्याला घरबसल्या, कोणत्याही शहरातून,साई पालखीसाठी नाव नोंदणी अथवा ऑनलाईन देणगीचे व्यवहार अतिशय सुरक्षित प्रमाणे करण्यात येईल.
                            </div>
                        </li>
                        <li class="bullet-none wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">पदयात्री नाव नोंदणी करताना कोणतीही अडचण आल्यास, कृपया पुढे दिलेल्या सेवेकऱ्यांना फोन करा.<br><b>रश्मी झपके – ९८६०००३६३९ </b>|<b> अमेय कालेकर - ९९२२७५०९४० </b>|<b> केदार मेथे – ८६००२५८३९०</b> | <b>अक्षय बाजारे - ९७६६८५१५८६</b></li>
                        <!--<li class="bullet-none wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold">&#9672;</small>&nbsp;Identity Proof: आपण पालखी मध्ये सहभागी होताना कोणते ओळखपत्र सोबत आणणार आहात ते इथे निवडा. </li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.getElementById('adharnumber').addEventListener('input', function (e) {
        e.target.value = e.target.value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim();
    });
    $("#idproof").change(function () {
        var selected = $(this).val();
        var text = $("#idproof option:selected").text();
        if (selected == 1 || $.trim(text) == 'Aadhaar card') {
            $("#aadharno").css('display', 'flex');
        } else {
            $("#aadharno").css('display', 'none');
        }
    });
</script>

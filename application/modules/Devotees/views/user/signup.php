<div class="page-wrapper mobile-wrapper">
    <div class="container mt-5 " >
        <div id="overviews" class="section">
            <div class="row">
                <div class="col-lg-5">
                    <div class="col-lg-10 col-xl-9">
                        <?php echo $this->session->flashdata('signupmsg'); ?>
                    </div>
                    <div class="w-100 col-12 middle-0">
                        <div class="card card-signin flex-row  ">
                            <div class="card-body">
                                <h5 class="card-title text-center Register-text">New Devotee Registration</h5>
                                <hr class="mb-2">
                                <?php echo form_open('signup', array('method' => 'post', 'id' => 'signup_devotee', 'class' => 'form-horizontal', 'autocomplete' => 'off')); ?>
                                <div class="row mb-2 mt-4">
                                    <div class="col-md-12">
                                        <label class="control-label text-left col-md-12 mb-2 p-0">Mobile Number:</label>
                                        <div class="form-group required row mb-0">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control ht-40" placeholder="Mobile Number" name="mobileno" id="mobileno" data-error='.error1'>
                                                <div class="input-field">
                                                    <div class="error1 error-msg"></div>
                                                    <?php echo form_error('mobileno'); ?>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-4 userlogin">
                                    <div class="col-md-12">
                                        <label class="control-label text-left col-md-12 mb-2 p-0">Date of Birth:</label>
                                        <div class="form-group required row mb-0">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control ht-40 datepicker dobreadonly"  name='dob' id='dob' data-error='.error2' placeholder='DD-MM-YYYY' readonly>
                                                <div class="input-field">
                                                    <div class="error2 error-msg"></div>
                                                    <?php echo form_error('dob'); ?>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-lg btn-primary btn-block btn-signin">Next</button>
                                </div>
                                <!--</form>-->
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 mobile-margin-top">
                    <h2 class="card-title text-left mb-0">सर्वाना ॐ साईराम !!!</h2>
                    <div class="mt-2">
                        श्री साईबाबांच्या कृपेने व आपल्या सारख्या साईभक्तांच्या प्रेमा मुळे, पुणे ते शिर्डी पालखी सोहळा ३१ व्या वर्षात पर्दापण करत आहे. १९८९ साली अवघ्या पाच साईभक्तांच्या पदयात्रेने सुरु झालेल्या पालखी सोहळा आता महाराष्ट्राचा सांस्कृतिक / धार्मिक जीवनाचा एक अविभाज्य घटक बनत आहे. सालाबाद प्रमाणे, या वर्षी सुद्धा गुरुपौर्णिमे निमित्त श्री साईबाबा पालखी सोहळा समिती आयोजित <b> “श्री साई पालखी सोहळा” दिं. ५ जुलै २०१९ शुक्रवार</b>, रोजी पुण्याचे ग्रामदैवत श्री कसबा गणपतीचा शुभ आशिर्वाद घेऊन शिर्डीकडे मार्गस्थ होणार आहे.
                    </div>

                    <ul class="pl-0 mangal">
                        <li class="bullet-none wow fadeInRight animated mt-4" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold">&#9672;</small>&nbsp;प्रथमच पालखी मध्ये येऊ इच्छिणाऱ्या साईभक्तांनी,या पेजवरून नवीन अकाऊंट बनवावे.</li>
                        <li class="bullet-none wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold">&#9672;</small>&nbsp;आपण, जर २०१६/१७/१८ या वर्षी पालखी मध्ये आले असल्यास आपण <a href="http://saibabapalkhipune.org/login" class="text-danger">http://saibabapalkhipune.org/login</a>  पेज वरून आपले अकाउंट उघडावे. </li>
                        <li class="bullet-none wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold">&#9672;</small>&nbsp;अकाउंट मध्ये प्रवेश केल्यावर, आवश्यक ती सर्वमाहिती भरावी. </li>
                        <li class="bullet-none wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold">&#9672;</small>&nbsp;आपण पालखीमध्ये येण्यास उत्सुकअसाल तर, योग्य तो पर्याय निवडून पुढे पालखी विषयी माहिती भरावी. </li>
                        <li class="bullet-none wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold">&#9672;</small>&nbsp;"Payment Option" मध्ये "Online" निवडून, नाव नोंदणी फी भरावी. </li>
                        <li class="bullet-none wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold">&#9672;</small>&nbsp;आपला व्यवहार यशस्वी झाल्यानंतर, आपणास समितीकडून एक मेसेज मिळेल. </li>
                        <li class="bullet-none wow fadeInRight animated mb-2" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold">&#9672;</small>&nbsp;५ जुलै पासून सुरु होणाऱ्या पालखी सोहळ्यात आपण नाव सांगून आपले आय-कार्ड घ्यावे.</li>
                        <li class="bullet-none wow fadeInRight animated mb-2" style="visibility: visible; animation-name: fadeInRight;"> 
                            <div class="tip">
                                टीप - श्री साईबाबा पालखी समितीने ऑनलाईन पेमेंट साठी "CC Avenue" या प्रसिद्ध कंपनी सोबत करार केला आहे, जेणे करून आपल्याला घरबसल्या, कोणत्याही शहरातून,साई पालखीसाठी नाव नोंदणी अथवा ऑनलाईन देणगीचे व्यवहार अतिशय सुरक्षित प्रमाणे करण्यात येईल.
                            </div>
                        </li>
                        <li class="bullet-none wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">पदयात्री नाव नोंदणी करताना कोणतीही अडचण आल्यास, कृपया पुढे दिलेल्या सेवेकऱ्यांना फोन करा.<br><b>रश्मी झपके – ९८६०००३६३९ </b>|<b> अमेय कालेकर - ९९२२७५०९४० </b>|<b> केदार मेथे – ८६००२५८३९०</b> | <b>अक्षय बाजारे - ९७६६८५१५८६</b></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


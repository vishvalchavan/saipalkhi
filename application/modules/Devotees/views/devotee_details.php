<div class="card">
    <div class="card-body">
        <?php
        if (isset($devotee_list) && !empty($devotee_list)) {
            foreach ($devotee_list as $devotee_list) {
                ?>
                <div class="row mx-0">
                    <div class="col-md-5 align-self-center p-0 col-7">
                        <h3 class="card-title">Devotee Details</h3>

                    </div>
                    <div class="col-md-7 col-5 text-right p-0">
                        <h5 class="font-small"><?php echo ($devotee_list['dpn_no']) ? 'DPN: ' . $devotee_list['dpn_no'] : '' ?></h5>
                    </div>

                </div>

                <div class="row">
                    <div class="col-12">
                        <!--<form action="#" class="form-horizontal">-->                                
                        <?php
                        echo form_open('admin/devotee/update/' . $devotee_list['id'], array('id' => 'update_devotee', 'class' => 'form_horizontal'));
                        echo form_hidden('devotee_id', $devotee_list['id']);
                        echo form_hidden('dpn_no', $devotee_list['dpn_no']);
                        ?>
                        <div class="form-body">
                            <hr class="m-t-0 m-b-40">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Full Name:</label>
                                        <div class="col-md-7 row pr-0">
                                            <div class="col-md-4 col-md-15">
                                                <input type="text" name="devoteefirstname" value="<?php echo set_value('devoteefirstname', $devotee_list['firstname']); ?>" class="form-control1" placeholder="First Name" data-error=".devoteeErorr1">
                                                <div class="input-field">
                                                    <div class="devoteeErorr1 error-msg"></div>
                                                    <?php echo form_error('devoteefirstname'); ?>
                                                </div> 
                                            </div>
                                            <div class="col-md-4 col-md-15">
                                                <input type="text" name="devoteemiddlename" class="form-control1" placeholder="Middle Name" value="<?php echo set_value('devoteemiddlename', $devotee_list['middlename']); ?>" data-error=".devoteeErorr2">
                                                <div class="input-field">
                                                    <div class="devoteeErorr2 error-msg"></div>
                                                    <?php echo form_error('devoteemiddlename'); ?>
                                                </div> 
                                            </div>
                                            <div class="col-md-4 col-md-15">
                                                <input type="text" name="devoteelastname" class="form-control1" placeholder="Last Name" value="<?php echo set_value('devoteelastname', $devotee_list['lastname']); ?>" data-error=".devoteeErorr3">
                                                <div class="input-field">
                                                    <div class="devoteeErorr3 error-msg"></div>
                                                    <?php echo form_error('devoteelastname'); ?>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Date of Birth:</label>
                                        <div class="col-md-7"> 
                                            <div class="input-group">
                                                <?php
                                                $dob = '';
                                                if (!empty($devotee_list['birth_date']) && $devotee_list['birth_date'] != "0000-00-00") {
                                                    $dob = date("d F Y", strtotime($devotee_list['birth_date']));
                                                }
                                                ?>

                                                <input type="text" name="devoteedob" class="form-control datepicker" placeholder="dd-mm-yyyy" value="<?php echo set_value('devoteedob', $dob); ?>" data-error=".devoteeErorr4">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                </div>
                                            </div>
                                            <div class="input-field">
                                                <div class="devoteeErorr4 error-msg"></div>
                                                <?php echo form_error('devoteedob'); ?>
                                            </div> 
                                        </div>
                                    </div>
                                </div>

                                <!--/span-->
                            </div>
                            <!--/row-->

                            <div class="row">                                            
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Mobile No:</label>

                                        <div class="col-md-7">                                          
                                            <div class="row">
                                                <div class="col-sm-3 col">
                                                    <input type="text" class="form-control" id="country_code" name="country_code" value="+91" placeholder="+91" readonly>
                                                </div>
                                                <div class="col-sm-9 col">
                                                    <input type="text" name="devoteemobileno" class="form-control" placeholder="Enter Mobile Number" value="<?php echo set_value('devoteemobileno', substr($devotee_list['mobile'], 2)); ?>" data-error=".devoteeErorr5">
                                                    <div class="input-field">
                                                        <div class="devoteeErorr5 error-msg"></div>
                                                        <?php echo form_error('devoteemobileno'); ?>
                                                    </div> 
                                                </div>
                                            </div>  

                                        </div>    
                                    </div>
                                </div>

                                <!--/span-->

                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Blood Group:</label>
                                        <div class="col-md-7">
                                            <!--<input type="text" name="devoteebloodgroup" class="form-control form-control-danger" placeholder="Blood Group" value="<?php echo set_value('devoteebloodgroup'); ?>" data-error=".devoteeErorr6">-->
                                            <select class="form-control custom-select" name="devoteebloodgroup" id="devoteebloodgroup" data-error=".devoteeErorr6">
                                                <option value="">Select Blood Group</option>
                                                <?php $selected_bldgrp = set_value('devoteebloodgroup', $devotee_list['blood_group']); ?>
                                                <option value="A+" <?php echo ($selected_bldgrp == "A+") ? 'selected' : ''; ?> >A+</option>
                                                <option value="A-" <?php echo ($selected_bldgrp == "A-") ? 'selected' : ''; ?> >A-</option>
                                                <option value="B+" <?php echo ($selected_bldgrp == "B+") ? 'selected' : ''; ?> >B+</option>
                                                <option value="B-" <?php echo ($selected_bldgrp == "B-") ? 'selected' : ''; ?> >B-</option>
                                                <option value="O+" <?php echo ($selected_bldgrp == "O+") ? 'selected' : ''; ?> >O+</option>
                                                <option value="O-" <?php echo ($selected_bldgrp == "O-") ? 'selected' : ''; ?> >O-</option>
                                                <option value="AB+" <?php echo ($selected_bldgrp == "AB+") ? 'selected' : ''; ?> >AB+</option>
                                                <option value="AB-" <?php echo ($selected_bldgrp == "AB-") ? 'selected' : ''; ?> >AB-</option>
                                                <option value="unknown" <?php echo ($selected_bldgrp == "unknown") ? 'selected' : ''; ?> >Unknown</option>
                                            </select>
                                            <div class="input-field">
                                                <div class="devoteeErorr6 error-msg"></div>
                                                <?php echo form_error('devoteebloodgroup'); ?>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-left col-md-4">Sec Mobile No:</label>                                                    
                                        <div class="col-md-7">

                                            <div class="row">
                                                <div class="col-sm-3 col">
                                                    <input type="text" class="form-control" id="sec_country_code" name="sec_country_code" value="+91" placeholder="+91" readonly>
                                                </div>
                                                <div class="col-sm-9 col">
                                                    <input type="text" name="devoteesecmobileno" class="form-control" placeholder="Enter Mobile Number" value="<?php echo set_value('devoteesecmobileno', substr($devotee_list['mobile1'], 2)); ?>" data-error=".devoteeErorr19">
                                                    <div class="input-field">
                                                        <div class="devoteeErorr19 error-msg"></div>
                                                        <?php echo form_error('devoteesecmobileno'); ?>
                                                    </div> 
                                                </div>
                                            </div>  

                                        </div>    
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Gender:</label>
                                        <div class="col-md-7">
                                            <select class="form-control custom-select" name="devoteegender" data-error=".devoteeErorr7">
                                                <option value="">Select Gender</option>
                                                <?php $selected_gender = set_value('devoteegender', $devotee_list['gender']); ?>
                                                <option value="Male" <?php echo ($selected_gender == "Male") ? 'selected' : ''; ?> >Male</option>
                                                <option value="Female" <?php echo ($selected_gender == "Female") ? 'selected' : ''; ?>>Female</option>
                                                <option value="Other" <?php echo ($selected_gender == "Other") ? 'selected' : ''; ?>>Other</option>
                                            </select>
                                            <div class="input-field">
                                                <div class="devoteeErorr7 error-msg"></div>
                                                <?php echo form_error('devoteegender'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">City:</label>
                                        <div class="col-md-7">
                                            <select class="form-control custom-select" name="devoteecity" id="devoteecity" data-error=".devoteeErorr8">
                                                <option value="">Select City</option>                                                
                                                <?php
                                                if (isset($city_list) && !empty($city_list)) {
                                                    foreach ($city_list as $city) {
                                                        $selected = '';
                                                        if (isset($devotee_list['city']) && $devotee_list['city'] == $city['name']) {
                                                            $selected = 'selected';
                                                        }
                                                        ?>
                                                        <option value="<?php echo $city['name']; ?>" <?php echo $selected; ?>><?php echo ucfirst($city['name']); ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <div class="input-field">
                                                <div class="devoteeErorr8 error-msg"></div>
                                                <?php echo form_error('devoteecity'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Address:</label>
                                        <div class="col-md-7">
                                            <textarea rows="3" name="devoteeaddress" class="form-control" placeholder="Enter address here" data-error=".devoteeErorr9"><?php echo set_value('devoteeaddress', $devotee_list['address']); ?></textarea>
                                            <div class="input-field">
                                                <div class="devoteeErorr9 error-msg"></div>
                                                <?php echo form_error('devoteeaddress'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--                                <div class="col-md-6">
                                                                <div class="form-group required row">
                                                                    <label class="control-label text-left col-md-4">Relative/Referred Name:</label>
                                                                    <div class="col-md-7">
                                                                        <input type="text" name="devoteerelativename" class="form-control form-control-danger" placeholder="Enter Relative Name" value="<?php echo set_value('devoteerelativename', $devotee_list['relative_name']); ?>" data-error=".devoteeErorr10">
                                                                        <div class="input-field">
                                                                            <div class="devoteeErorr10 error-msg"></div>
                            <?php echo form_error('devoteerelativename'); ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>-->
                            <!--/span-->

                            <!--/row-->
                            <!--         <div class="row">
                             

                                   <div class="col-md-6">
                                          <div class="form-group required row">

                                              <label class="control-label text-left col-md-4">Relative/Referred Number:</label>
                                              <div class="col-md-7">                                            
                                                  <div class="row">
                                                      <div class="col-sm-3 col">
                                                          <input type="text" class="form-control" id="sec_country_code" name="rel_country_code" value="+91" placeholder="+91" readonly>
                                                      </div>
                                                      <div class="col-sm-9 col">
                                                          <input type="text" name="devoteeralativeno" class="form-control" placeholder="Enter Relative Mobile Number" value="<?php echo set_value('devoteeralativeno', substr($devotee_list['relative_mobile'], 2)); ?>" data-error=".devoteeErorr11">

                                                          <div class="input-field">
                                                              <div class="devoteeErorr11 error-msg"></div>
                            <?php echo form_error('devoteeralativeno'); ?>
                                                          </div>
                                                      </div> 
                                                  </div>

                                              </div>

                                          </div>
                                      </div>
                                  </div>-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Pin Code:</label>
                                        <div class="col-md-7">
                                            <input type="text" name="devoteepincode" id="devoteepincode" class="form-control form-control-danger" placeholder="Enter PIN Code here" value="<?php echo set_value('devoteepincode', $devotee_list['pincode']); ?>" data-error=".devoteeErorr20">
                                            <div class="input-field">
                                                <div class="devoteeErorr20 error-msg"></div>
                                                <?php echo form_error('devoteepincode'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->

                                <!--/row-->


                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-left col-md-4">Email:</label>
                                        <div class="col-md-7">
                                            <input type="email" name="devoteemailid" class="form-control" placeholder="Enter Email" value="<?php echo set_value('devoteemailid', $devotee_list['email']); ?>" data-error=".devoteeErorr12">
                                            <div class="input-field">
                                                <div class="devoteeErorr12 error-msg"></div>
                                                <?php echo form_error('devoteemailid'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <!--                                <div class="col-md-6">
                                                                    <div class="form-group row">
                                                                        <label class="control-label text-left col-md-4">Sickness:</label>
                                                                        <div class="col-md-7">
                                                                            <input type="text" name="devoteesickness" class="form-control" placeholder="Enter Sickness" value="<?php echo set_value('devoteesickness', $devotee_list['sickness']); ?>" data-error=".devoteeErorr18">
                                                                            <div class="input-field">
                                                                                <div class="devoteeErorr18 error-msg"></div>
                                <?php echo form_error('devoteesickness'); ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>-->
<!--                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Education:</label>
                                        <div class="col-md-7">                                            
                                            <select class="form-control custom-select" name="devoteeeducation" id="devoteeeducation" data-error=".devoteeErorr16">
                                                <option value="">Select Education</option>
                                                <?php
                                                $selected_education = '';
                                                foreach ($education_list as $education) {
                                                    $selected_education = set_value('devoteeeducation', $devotee_list['education']);
                                                    $selected_education = ($selected_education == $education['id']) ? 'selected' : '';
                                                    ?>    
                                                    <option value="<?php echo $education['id'] ?>" <?php echo $selected_education ?> ><?php echo $education['education'] ?></option>    
                                                <?php } ?>
                                            </select>
                                            <div class="input-field">
                                                <div class="devoteeErorr16 error-msg"></div>
                                                <?php echo form_error('devoteeeducation'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Occupation:</label>
                                        <div class="col-md-7">
                                            <!--<input type="text" name="devoteeprofession" class="form-control" placeholder="Enter Profession" value="<?php set_value('devoteeprofession', $devotee_list['profession']); ?>" data-error=".devoteeErorr14">-->
                                            <select class="form-control custom-select" name="devoteeprofession" id="devoteeprofession" data-error=".devoteeErorr14">
                                                <option value="">Select Occupation</option>
                                                <?php
                                                $selected_prof = '';
                                                foreach ($occupation_list as $profession) {
                                                    $selected_prof = set_value('devoteeprofession', $devotee_list['profession']);
                                                    $selected_prof = ($selected_prof == $profession['id']) ? 'selected' : '';
                                                    ?>    
                                                    <option value="<?php echo $profession['id'] ?>" <?php echo $selected_prof ?> ><?php echo $profession['occupation'] ?></option>    
                                                <?php } ?>
                                            </select>    
                                            <div class="input-field">
                                                <div class="devoteeErorr14 error-msg"></div>
                                                <?php echo form_error('devoteeprofession'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>    
<!--                            <div class="row">                                
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Identity Proof:</label>
                                        <div class="col-md-7">                                            
                                            <select class="form-control custom-select" name="devoteeidproof" id="devoteeidproof" data-error=".devoteeErorr15">
                                                <option value="">Select ID Proof</option>
                                                <?php
                                                $selected_proof = '';
                                                foreach ($id_proof as $proof) {
                                                    $selected_proof = set_value('devoteeidproof', $devotee_list['identity_prof']);
                                                    $selected_proof = ($selected_proof == $proof['id']) ? 'selected' : '';
                                                    ?>    
                                                    <option value="<?php echo $proof['id'] ?>" <?php echo $selected_proof ?> ><?php echo $proof['document_type'] ?></option>    
                                                <?php } ?>
                                            </select>
                                            <div class="input-field">
                                                <div class="devoteeErorr15 error-msg"></div>
                                                <?php echo form_error('devoteeidproof'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                           
                            </div>
                            <div class="row">
                                <div class="col-md-6 aadharcarddiv">
                                    <div class="form-group row">
                                        <label class="control-label text-left col-md-4">Aadhar Card No :</label>
                                        <div class="col-md-7">
                                            <input type="text" name="aadharno" class="form-control aadharcard" placeholder="Enter Aadhar No." value="<?php echo set_value('aadharno', $devotee_list['adhaar_no']); ?>" data-error=".devoteeErorr17" maxlength="14" id="aadharno">
                                            <div class="input-field">
                                                <div class="devoteeErorr17 error-msg"></div>
                                                <?php echo form_error('aadharno'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>-->

                            <!--/row-->

                        </div>
                        <hr>   
                        <div class="form-actions float-left">
                            <a href="<?php echo base_url() . 'admin/devotee'; ?>"> <button type="button" class="btn btn-info add-button"><i class="fas fa-arrow-left"></i>&nbsp; Back to Devotee List</button></a>
                        </div>
                        <div class="form-actions float-right">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="mr-5">
                                            <!--<button type="reset" class="btn btn-success back-button" >Clear</button>--> 
                                            <button type="submit" class="btn btn-inverse update-button">Update</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6"> </div>
                            </div>
                        </div>
                        <!--</form>-->
                        <?php
                        form_close();
                    }
                } else {
                    ?>
                    <div class="form-body">
                        <hr class="m-t-0 m-b-40">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-center margin-top-10">Data not found</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<script>
    $(document).ready(function () {

        var selected_proof = $("#devoteeidproof option:selected").text();
        if (selected_proof == "Aadhaar card" || selected_proof == "aadhaar card")
        {
            $("#aadharno").prop('disabled', false)
            $(".aadharcarddiv").show();
            $(".aadharcarddiv").css("display", "block");
        } else {
            $("#aadharno").prop('disabled', true)
            $(".aadharcarddiv").hide();
            $(".aadharcarddiv").css("display", "none");
        }
        $("#devoteeidproof").change(function () {

            if ($(this).find("option:selected").text() == "Aadhaar card" || $(this).find("option:selected").text() == "aadhaar card")
            {
                $("#aadharno").prop('disabled', false)
                $(".aadharcarddiv").show();
                $(".aadharcarddiv").css("display", "block");
            } else {
                $(".aadharcarddiv").hide();
                $("#aadharno").prop('disabled', true)
                $(".aadharcarddiv").css("display", "none");
            }
        });
    });
</script>



<table id="devotee_list" class="display nowrap table table-hover table-striped table-bordered dataTable datatable" cellspacing="0" width="100%" role="grid" aria-describedby="example23_info">
    <thead>
        <tr>
            <th>DPN No</th>
            <th>Devotee Name </th>
            <th>Place </th>
            <th>Mobile No</th>
            <th>Status</th>
            <!--<th>Payment</th>-->
            <th>Actions</th></tr>
    </thead>
    <tbody>
        <?php
        if (isset($alldevotee_list) && !empty($alldevotee_list)) {
            foreach ($alldevotee_list as $devotee_list) {
                ?>
                <tr id="row_<?php echo $devotee_list['id'] ?>">
                    <td><?php echo ($devotee_list['dpn_no']) ? $devotee_list['dpn_no'] : ''; ?></td>
                    <td><?php echo ($devotee_list['firstname']) ? $devotee_list['firstname'] . " " . $devotee_list['middlename'] . " " . $devotee_list['lastname'] : ''; ?></td>
                    <td><?php echo ($devotee_list['address']) ? $devotee_list['address'] . ", " . $devotee_list['city'] : ''; ?></td>
                    <td><?php echo ($devotee_list['mobile']) ? $devotee_list['mobile'] : ''; ?></td>
                    <td><?php echo ($devotee_list['active']) ? 'Active' : 'Inactive'; ?></td>                                
                    <td>
                        <a href="<?php echo base_url() . 'admin/devotee/update/' . $devotee_list['id']; ?>" class="bg-button"><i class="fas fa-edit"></i>
                        </a>
                        <?php
                        $payment_status = '';
                        $onclick = '';
                        $confirm_pay = ($devotee_list['payment_status'] == 'success') ? '' : 'confirm_pay';

                        if ($devotee_list['payment_status'] != 'success') {
                            ?> 
                            <a href="#" class="bg-button <?php echo $confirm_pay ?> rupees-icons" data-toggle="modal" 
                               id="pay_<?php echo $devotee_list['id'] ?>"
                               data-devotee="<?php echo $devotee_list['id'] . "/" . $devotee_list['tbl_py_id']; ?>"
                               data-dtname="<?php echo $devotee_list['firstname'] . " " . $devotee_list['middlename'] . " " . $devotee_list['lastname'] ?>"
                               data-dtyear_id="<?php echo ($devotee_list['tbl_year_id']) ? $devotee_list['tbl_year_id'] : $curr_yr_id ?>"
                               data-dtyear="<?php echo ($devotee_list['tbl_year_year']) ? $devotee_list['tbl_year_year'] : date('Y') ?>"                                        
                               title="<?php echo ($devotee_list['payment_status'] == 'success') ? 'already paid' : 'Cash'; ?>">
                                <i class="fas fa-rupee-sign"></i></a>
                            <?php } ?>
                    </td>
                </tr>
                <?php
            }
        }
        ?>  
    </tbody>
</table>
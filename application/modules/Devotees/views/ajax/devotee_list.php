<table id="devotee_list" class="display table table-hover table-striped table-bordered dataTable datatable" cellspacing="0" width="100%" role="grid" aria-describedby="example23_info">
    <thead>
        <tr class="nowrap">
            <th style="width: 15%;">DPN No.</th>
            <th style="width: 20%;">Devotee Name </th>
            <th style="width: 30%;">Address</th>
            <th style="width: 10%">Mobile No.</th>
            <th style="width: 10%;">Status</th>
            <!--<th>Payment</th>-->
            <th style="width: 10%;">Action</th>
    </thead>
    <tbody>
        <?php
        $curr_yr_id = '';
        if (isset($year_list) && !empty($year_list)) {
            foreach ($year_list as $year) {
                $selected = '';
                $palkhiyear = ($this->session->userdata('palki_year')) ? $this->session->userdata('palki_year') : date('Y');
                if ($palkhiyear == $year['year']) {
                    $selected = "selected";
                    $curr_yr_id = $year['id'];
                }
            }
        }
        $this->session->unset_userdata('palki_year');
        if (isset($alldevotee_list) && !empty($alldevotee_list)) {
            foreach ($alldevotee_list as $devotee_list) {
                ?>
                <tr id="row_<?php echo $devotee_list['id'] ?>">
                    <td><?php echo ($devotee_list['dpn_no']) ? $devotee_list['dpn_no'] : ''; ?></td>
                    <td><?php echo ($devotee_list['firstname']) ? $devotee_list['firstname'] . " " . $devotee_list['middlename'] . " " . $devotee_list['lastname'] : ''; ?></td>
                    <td class="description-list" title="<?php echo $devotee_list['address'] . ", " . $devotee_list['city'] ?>"><?php echo ($devotee_list['address']) ? $devotee_list['address'] . ", " . $devotee_list['city'] : ''; ?></td>
                    <td><?php echo ($devotee_list['mobile']) ? substr($devotee_list['mobile'], 2) : ''; ?></td>
                    <td><?php echo ($devotee_list['active']) ? 'Active' : 'Inactive'; ?></td>                                
                    <td>                                                
                        <a href="<?php echo base_url() . 'admin/devotee/update/' . $devotee_list['id']; ?>" class="bg-button" title="Edit"><i class="fas fa-edit"></i>
                        </a>&nbsp;                                                
                       <!--  <a class="bg-button delete" data-devotee="<?php echo $devotee_list['id']; ?>" title="Delete"><i class="fas fa-trash"></i>
                        </a> -->
                        &nbsp;
                        <?php
                        $payment_status = '';
                        $onclick = '';
                        $age = '';
                        $image_path = '';
                        $confirm_pay = (strtolower($devotee_list['payment_status']) == 'success') ? '' : 'confirm_pay';
                        $elements_data = '';
                        $dbvehical_type = ($devotee_list['vehical_type']) ? $devotee_list['vehical_type'] : '';
                        $db_vehicle_details = ($devotee_list['vehicle_details']) ? $devotee_list['vehicle_details'] : '';
                        $db_seva_samiti_id = ($devotee_list['seva_samiti_id']) ? $devotee_list['seva_samiti_id'] : '';
                        $db_dindi_id = ($devotee_list['dindi_id']) ? $devotee_list['dindi_id'] : '26';


                        $elements_data = "id='pay_" . $devotee_list['id'] . "' data-devotee='" . $devotee_list['id'] . "/" . $devotee_list['tbl_py_id'] . "' data-dtname='" . $devotee_list['firstname'] . " " . $devotee_list['middlename'] . " " . $devotee_list['lastname'] . "' data-dindi_id='" . $db_dindi_id . "' data-vehical_type='" . $dbvehical_type . "' data-vehicle_details='" . $db_vehicle_details . "' data-seva_samiti_id='" . $db_seva_samiti_id . "' data-address='" . $devotee_list['address'] . ", " . $devotee_list['city'] . " - " . $devotee_list['pincode'] . "' data-dt-dpnno='" . $devotee_list['dpn_no'] . "'";
                        if (file_exists($devotee_list['user_image_path'])) {
                            $image_path = base_url() . $devotee_list['user_image_path'];
                        } else {
                            $image_path = base_url('assets/images/') . "default-profile.png";
                        }
                        $from = new DateTime($devotee_list['birth_date']);
                        $to = new DateTime('today');
                        $age = $from->diff($to)->y;
                        $amountinword = $this->numbertowords->convert_number($devotee_list['donation_amount']);

//                        if (strtolower($devotee_list['payment_status']) != 'success') {
                        ?> 
        <!--                            <a href="#" class="<?php echo $confirm_pay ?> rupees-icons" data-toggle="modal" <?php echo $elements_data ?>
                               data-dtyear_id="<?php echo ($devotee_list['tbl_year_id']) ? $devotee_list['tbl_year_id'] : $curr_yr_id; ?>"
                               data-dtyear="<?php echo ($devotee_list['tbl_year_year']) ? $devotee_list['tbl_year_year'] : date('Y'); ?>"                                                       
                               data-payment_amount="<?php echo ($devotee_list['payment_amount']) ? $devotee_list['payment_amount'] : '1500'; ?>" 
                               title="<?php echo 'Accept Payment'; ?>">
                                <i class="fas fa-rupee-sign"></i></a>&nbsp;&nbsp;&nbsp;                                                         -->
                        <?php
                        // } else

                        if (strtolower($devotee_list['payment_status']) != 'success' || strtolower($devotee_list['payment_status']) == 'success' && date("Y", strtotime($devotee_list['payment_date'])) != date("Y")) {
                            ?>
                            <a href="#" class="<?php echo 'confirm_pay' ?> rupees-icons" data-toggle="modal" <?php echo $elements_data ?>
                               data-dtyear_id="<?php echo $curr_yr_id ?>"
                               data-dtyear="<?php echo date('Y'); ?>"                                                       
                               data-payment_amount="1500" 
                               title="<?php echo 'Accept Payment'; ?>">
                                <i class="fas fa-rupee-sign"></i></a>&nbsp;&nbsp;&nbsp;                                                        

                            <?php
                        }
                        $donation_for = "";
                        if (isset($devotee_list['donation_for']) && $devotee_list['donation_for'] == 'vastunidhishirdi') {
                            $donation_for = "Vastunidhi - Shirdi";
                        } elseif (isset($devotee_list['donation_for']) && $devotee_list['donation_for'] == 'palkhi') {
                            $donation_for = "Palkhi Sohla";
                        }
                        $receipt_data = "data-pr-receiptno='" . $devotee_list['receipt_no'] . "' data-pr-dtname='" . $devotee_list['firstname'] . " " . $devotee_list['middlename'] . " " . $devotee_list['lastname'] . "' data-pr-address='" . $devotee_list['address'] . ", " . $devotee_list['city'] . " - " . $devotee_list['pincode'] . "' data-pr-amount='" . $devotee_list['donation_amount'] . "' data-pr-payment-date='" . date('d-m-Y', strtotime($devotee_list['donation_date'])) . "' data-pr-amountinword='" . $amountinword . "' data-pr-devotee='" . $devotee_list['id'] . "' data-pr-donationfor='" . $donation_for . "'";

                        if (strtolower($devotee_list['donation_status']) == 'success' && date("Y", strtotime($devotee_list['donation_date'])) == date("Y")) {
                            ?>
                            <i class="fas fa-donate cursor-pointer" title="Print Donation Receipt" onclick="printDonationReceipt(this, 'donationReceipt')" <?php echo $receipt_data ?>></i>
                        <?php } ?>
                    </td>
                </tr>
                <?php
            }
        }
        ?>   
    </tbody>
</table>

<script>
    $(".description-list").shorten({
        "showChars": 50,
        "moreText": "More",
        "lessText": "Less",
    });

</script>
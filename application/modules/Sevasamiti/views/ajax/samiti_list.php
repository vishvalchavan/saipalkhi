<table id="samiti_list" class="display nowrap table table-hover table-striped table-bordered datatable" cellspacing="0" width="100%" role="grid" aria-describedby="example23_info">
    <thead>
        <tr>
            <th>Seva Samiti</th>
            <th>Samiti Head</th>
            <th>Mobile No</th>
            <th>No.of Sevekari</th>
            <th>No.of Supplier</th>
            <th>Action </th></tr>
    </thead>
    <tbody>
        <?php
        if (isset($samiti_list) && !empty($samiti_list)) {
            foreach ($samiti_list as $samiti) {
                ?>
                <tr>
                    <td>
                        <?php if (isset($samiti['isactive']) && $samiti['isactive'] == 1) { ?>
                            <a href="<?php echo base_url() . 'admin/sevekari/' . $samiti['id'] ?>"><?php echo isset($samiti['name']) ? $samiti['name'] : ''; ?></a>
                        <?php } else { ?>
                            <a title="Please activate first"><?php echo isset($samiti['name']) ? $samiti['name'] : ''; ?></a>
                        <?php } ?>
                    <td><?php echo isset($samiti['samiti_head']) ? $samiti['samiti_head'] : ''; ?></td>
                    <td><?php echo isset($samiti['mobile']) ? $samiti['mobile'] : ''; ?></td>
                    <td><?php echo isset($samiti['sevekari_count']) ? $samiti['sevekari_count'] : ''; ?></td>
                    <td><?php echo isset($samiti['suplier_count']) ? $samiti['suplier_count'] : ''; ?></td>
                    <td>
                        <?php echo form_open("Supplier/generate_sevekari_report"); ?>  
                        <a href="<?php echo base_url() . 'admin/samiti/update/' . $samiti['id'] ?>" class="bg-button" title="Edit"><i class="fas fa-edit"></i></a>&nbsp;&nbsp;
                        <?php if (isset($samiti['isactive']) && $samiti['isactive'] == 1) { ?>
                            <a class="bg-button delete" data-samiti="<?php echo $samiti['id']; ?>" title="Delete"><i class="fas fa-trash"></i>
                            </a>&nbsp;
                        <?php } else { ?>
                            <a class="bg-button reactive" data-samiti="<?php echo $samiti['id']; ?>" title="Activate"><i class="fas fa-check-circle"></i>
                            </a>&nbsp;
                        <?php } ?>

                        <input type="hidden" name="samiti_id" value="<?php echo isset($samiti['id']) ? $samiti['id'] : ''; ?>">
                        <button type="submit" class="p-0 m-0 border-0 no-bg"><a class="report" data-target="tooltip" title="Report"><i class="fas fa-file"></i></a></button>
                                <?php echo form_close(); ?> 
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>
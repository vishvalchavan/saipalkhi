<div class="card min-height80">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <div class="row mx-0">
                    <div class="col-md-5 col-6 align-self-center p-0">
                        <h3 class="card-title">Add Seva Samiti</h3>
                    </div>
                    <div class="col-md-7 col-6 text-right p-0">
                        <a href="<?php echo base_url(); ?>admin/samiti"> <button type="button" class="btn btn-info add-button"><i class="fas fa-arrow-left"></i> &nbsp;&nbsp; Back to Samiti List</button></a>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <?php echo form_open("Sevasamiti/add_Sevasamiti", array('id' => 'add_sevasamiti', 'class' => 'form-horizontal p-t-20')); ?>  



                                 <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Samiti Name:</label>
                                        <div class="col-md-7">
                                                                 
                                    <input type="text" class="form-control" id="samiti_name" name="samiti_name" placeholder="Enter Samiti Name" data-error=".samitiErorr1">
                                    <div class="samitiErorr1 error-msg"></div>
                               
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group required row" aria-required="true">
                                        <label class="control-label text-left col-md-4">Description:</label>
                                        <div class="col-md-7">
                                          
                                    <textarea class="form-control" rows="3" id="description" name="description" placeholder="Enter Samiti Description" data-error=".samitiErorr2"></textarea>
                                    <div class="samitiErorr2 error-msg"></div>
                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                           <div class="row">
                        
                                <div class="col-md-6">
                                    <div class="form-group required row" aria-required="true">
                                        <label class="control-label text-left col-md-4">Enrollment Year:</label>
                                        <div class="col-md-7">
                                               
                                    <select class="form-control custom-select" name="enroll_year" id="enroll_year" data-error=".samitiErorr3">
                                        <option value="">Select Year</option>
                                        <?php
                                        if (isset($year_list) && !empty($year_list)) {
                                            foreach ($year_list as $year) {
                                                if ($year['year'] == date("Y")) {
                                                    ?>
                                                    <option value="<?php echo $year['id'] ?>"><?php echo $year['year'] ?></option>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </select>
                                    <div class="samitiErorr3 error-msg"></div>
                            
                                        </div>
                                    </div>
                                </div>
                            </div>
<hr class="my-4">
                           <div class="form-group row m-b-0 col">
                            <div class="offset-sm-11 col-sm-1 pl-0">
                                <input type="submit" name="submit" class="btn btn-success waves-effect waves-light update-button" value="Save">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

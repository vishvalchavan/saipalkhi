<div class="card min-height80">
    <div class="card-body">
        <div class="row">
            <div class="col-12">

                <div class="row mx-0">
                    <div class="col-md-5 col-7 align-self-center p-0">
                        <h3 class="card-title">Seva Samiti List</h3>
                    </div>
                    <div class="col-md-7 col-5 text-right p-0">
                        <a href="<?php echo base_url(); ?>admin/samiti/add">  <button type="button" class="btn btn-info add-button"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp; Add Samiti</button></a>
                    </div>
                </div>
                <hr>
                <div class="">
                    <div class="row mx-0">
                        <div class="button-table">

                            <form class="form-inline">
                                <label class="palkhi-select">Palkhi Sohala:</label>
                                <select class="form-control ml-2" id="year_list" name="year_list">
                                    <option value="">Select Year</option>
                                    <?php
                                    if (isset($year_list) && !empty($year_list)) {
                                        foreach ($year_list as $year) {
                                            $selected = '';
                                            if (date('Y') == $year['year']) {
                                                $selected = "selected";
                                            }
                                            ?>
                                            <option value="<?php echo $year['id'] ?>" <?php echo $selected; ?>><?php echo $year['year'] ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>

                                <label class="palkhi-select font-14 mr-1 ml-1">Category:</label>
                                <select class="form-control" id="search_by" name="search_by">
                                    <option value="">Search By</option>
                                    <option value="name" selected>Name</option>
                                    <option value="mobile">Mobile No.</option>
                                </select>

                                <div class="form-group has-search ml-2">
                                    <span class="fas fa-search form-control-feedback"></span>
                                    <input type="text" class="form-control" id="column_filter" placeholder="Search">
                                </div>
                            </form>


                        </div>
                    </div>
                    <div id="example23_wrapper" class="dataTables_wrapper dt-bootstrap4 table-responsive">
                        <table id="samiti_list" class="display nowrap table table-hover table-striped table-bordered datatable" cellspacing="0" width="100%" role="grid" aria-describedby="example23_info">
                            <thead>
                                <tr>
                                    <th>Seva Samiti</th>
                                    <th>Samiti Head</th>
                                    <th>Mobile No.</th>
                                    <th>No.of Sevekari</th>
                                    <th>No.of Contractor</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($samiti_list) && !empty($samiti_list)) {
                                    foreach ($samiti_list as $samiti) {
                                        ?>
                                        <tr>
                                            <td>
                                                <?php if (isset($samiti['isactive']) && $samiti['isactive'] == 1) { ?>
                                                    <a href="<?php echo base_url() . 'admin/sevekari/' . $samiti['id'] ?>"><?php echo isset($samiti['name']) ? $samiti['name'] : ''; ?></a>
                                                <?php } else { ?>
                                                    <a title="Please activate first"><?php echo isset($samiti['name']) ? $samiti['name'] : ''; ?></a>
                                                <?php } ?>
                                            </td>
                                            <td><?php echo isset($samiti['samiti_head']) ? $samiti['samiti_head'] : ''; ?></td>
                                            <td><?php echo isset($samiti['mobile']) ? $samiti['mobile'] : ''; ?></td>
                                            <td><?php echo isset($samiti['sevekari_count']) ? $samiti['sevekari_count'] : ''; ?></td>
                                            <td><?php echo isset($samiti['suplier_count']) ? $samiti['suplier_count'] : ''; ?></td>
                                            <td><?php echo isset($samiti['isactive']) ? $samiti['isactive'] == 1 ? 'Active' : 'In-Active' : ''; ?></td>
                                            <td>
                                                <?php echo form_open("Supplier/generate_sevekari_report"); ?>  
                                                <a href="<?php echo base_url() . 'admin/samiti/update/' . $samiti['id'] ?>" class="bg-button" title="Edit"><i class="fas fa-edit"></i></a>&nbsp;&nbsp;
                                                <?php if (isset($samiti['isactive']) && $samiti['isactive'] == 1) { ?>
                                                    <a class="bg-button delete" data-samiti="<?php echo $samiti['id']; ?>" title="Delete"><i class="fas fa-trash"></i>
                                                    </a>&nbsp;
                                                <?php } else { ?>
                                                    <a class="bg-button reactive" data-samiti="<?php echo $samiti['id']; ?>" title="Activate"><i class="fas fa-check-circle"></i>
                                                    </a>&nbsp;
                                                <?php } ?>

                                                <input type="hidden" name="samiti_id" value="<?php echo isset($samiti['id']) ? $samiti['id'] : ''; ?>">
                                                <button type="submit" class="p-0 m-0 border-0 no-bg"><a class="report" data-target="tooltip" title="Report"><i class="fas fa-file"></i></a></button>
                                                        <?php echo form_close(); ?> 
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function deleteSamiti(dId) {
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Sevasamiti/delete',
            data: {'samiti_id': dId},
            success: function (data) {
                if ($.trim(data) == 'success') {
                    location.reload();
                }
            }
        });
        return false;
    }
    function ActivateSamiti(dId) {
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Sevasamiti/activate',
            data: {'samiti_id': dId},
            success: function (data) {
                if ($.trim(data) == 'success') {
                    location.reload();
                }
            }
        });
        return false;
    }
    $('body').on('click', '.delete', function () {
        var id = $(this).attr('data-samiti');
        $('#confirm-delete').modal();
        $(".btn-ok").click(function () {
            deleteSamiti(id);
        });
    });
    $('body').on('click', '.reactive', function () {
        var id = $(this).attr('data-samiti');
        $('#confirm-active').modal();
        $(".btn-ok").click(function () {
            ActivateSamiti(id);
        });
    });
    $('body').on('change', '#year_list', function () {
        var year = $(this).val();
        if (year.length !== 0) {
            $(".loadding-page").css('display', 'block');
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>Sevasamiti/samiti_list',
                data: {'year': year}, success: function (data) {
                    if (data) {
                        $("#example23_wrapper").html(data);
                        $('#samiti_list').DataTable({
                            dom: 'Bfrtip',
                            buttons: [{
                                    extend: 'excel',
                                    exportOptions: {
                                        columns: [0, 1, 2, 3, 4, 5]
                                    }
                                }, {
                                    extend: 'pdf',
                                    exportOptions: {
                                        columns: [0, 1, 2, 3, 4, 5]
                                    }
                                }, {
                                    extend: 'print',
                                    exportOptions: {
                                        columns: [0, 1, 2, 3, 4, 5]
                                    }
                                }]
                        });
                        $(".loadding-page").css('display', 'none');
                    }
                }
            });
        }
    });
//    $('body').on('click', '.report', function () {
////        $(".loadding-page").css('display', 'block');
//        var samiti = $(this).attr('data-samiti');
//        if (samiti.length !== 0) {
//            $.ajax({
//                type: "POST",
//                url: '<?php echo base_url(); ?>Supplier/generate_report',
//                data: {'samiti_id': samiti}, success: function (data) {
//
//                }
//            });
//        }
//    });
    $(document).ready(function () {
        $("#search_by").change(function () {
            var search = $(this).val();
            if (search.length !== 0) {
                $("#column_filter").removeAttr('disabled');
                $("#column_filter").val("");
                if ($.trim(search) == 'name') {
                    filterColumn('3', "");
                } else if ($.trim(search) == 'mobile') {
                    filterColumn('0', "");
                }
            }
        });
        $('#column_filter').on('keyup click', function () {
            var search_txt = $(this).val();
            var search_by = $("#search_by").val();
            if (search_by.length !== 0) {
                if ($.trim(search_by) == 'name') {
                    filterColumn('0', search_txt);
                } else if ($.trim(search_by) == 'mobile') {
                    filterColumn('2', search_txt);
                }
            } else {
                filterColumn('0', search_txt);
            }
        });
    });
    function filterColumn(i, searchVal) {
        $('#samiti_list').DataTable().column(i).search(searchVal, false, true).draw();
    }
</script>

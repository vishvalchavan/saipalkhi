<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sevasamiti extends MY_Controller {

    public function __construct() {
        parent::__construct();
        
         if (!$this->ion_auth->logged_in()) {
            redirect('admin', 'refresh');
        }
        if (!$this->ion_auth->is_admin()) {
            redirect('admin', 'refresh');
        }
        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->model(array('Sevasamiti_model', 'Year', 'language'));
        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('Auth');
    }

    public function index() {
        // if (!$this->ion_auth->logged_in()) {
        //     redirect('admin', 'refresh');
        // }
        // if (!$this->ion_auth->is_admin()) {
        //     redirect('admin', 'refresh');
        // }
        $user_id = $this->session->userdata('user_id');
        $year = $this->Year->get_year_id(date('Y'));
        $data['samiti_list'] = $this->Sevasamiti_model->get_samiti_list($year);
        $data['year_list'] = $this->Year->get_year_list();
        $data['dataHeader']['title'] = 'Seva Samiti List';
        $this->template->set_master_template('template.php');
        $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
        $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
        $this->template->write_view('content', 'sevasamiti_list', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'snippets/footer', '', TRUE);
        $this->template->render();
    }

    public function samiti_list() {
        if ($this->input->post('year')) {
            $data['samiti_list'] = $this->Sevasamiti_model->get_samiti_list($this->input->post('year'));
            $view = $this->load->view('ajax/samiti_list', $data);
            echo $view;
        }
    }

    public function add_Sevasamiti() {
        // if (!$this->ion_auth->logged_in()) {
        //     redirect('admin', 'refresh');
        // }
        // if (!$this->ion_auth->is_admin()) {
        //     redirect('admin', 'refresh');
        // }
        $user_id = $this->session->userdata('user_id');
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->form_validation->set_rules('samiti_name', 'Samiti Name', 'required');
            $this->form_validation->set_rules('description', 'Samiti Description', 'required');
            $this->form_validation->set_rules('enroll_year', 'Enrollment year', 'required');
            if ($this->form_validation->run() == TRUE) {
                $samiti = $this->input->post('samiti_name');
                $enroll_year = $this->input->post('enroll_year');
                $check = $this->Sevasamiti_model->if_samiti_exists($samiti, $enroll_year);
                if (count($check) == 0) {
                    $samiti_details = array(
                        'name' => $samiti,
                        'year_id' => $enroll_year,
                        'created_by' => $user_id,
                        'created_date' => date('Y-m-d'),
                        'isactive' => 1
                    );
                    if ($this->input->post('description')) {
                        $samiti_details['description'] = $this->input->post('description');
                    }
                    $response = $this->Sevasamiti_model->insert($samiti_details);
                    if ($this->db->affected_rows() > 0) {
                        $this->Sevasamiti_model->update_samiticount(date("Y"));
                        $this->session->set_flashdata('success_msg', 'Seva samiti added Successfully.');
                        redirect('admin/samiti');
                    } else {
                        $this->session->set_flashdata('error_msg', 'Failed to add seva samiti');
                        redirect('admin/samiti');
                    }
                } else {
                    $this->session->set_flashdata('error_msg', 'Seva samiti already exists.');
                    redirect('admin/samiti');
                }
            }
        } else {
//            $data['dataHeader'] = $this->devotee->get_allData($user_id);
            $data['dataHeader']['title'] = 'Add Seva Samiti';
            $data['year_list'] = $this->Year->get_year_list();
            $this->template->set_master_template('template.php');
            $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
            $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
            $this->template->write_view('content', 'sevasamiti_add', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'snippets/footer', '', TRUE);
            $this->template->render();
        }
    }

    public function update_Sevasamiti($samiti_id) {
        // if (!$this->ion_auth->logged_in()) {
        //     redirect('admin', 'refresh');
        // }
        // if (!$this->ion_auth->is_admin()) {
        //     redirect('admin', 'refresh');
        // }
        $user_id = $this->session->userdata('user_id');
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->form_validation->set_rules('samiti_name', 'Samiti Name', 'required');
            $this->form_validation->set_rules('description', 'Samiti Description', 'required');
            $this->form_validation->set_rules('enroll_year', 'Enrollment year', 'required');
            if ($this->form_validation->run() == TRUE) {
                $samiti = $this->input->post('samiti_name');
                $enroll_year = $this->input->post('enroll_year');
                $check = $this->Sevasamiti_model->if_samiti_exists($samiti, $enroll_year);
                if ((count($check) == 1 && $check[0]->id == $samiti_id) || count($check) == 0) {
                    $samiti_details = array(
                        'name' => $samiti,
                        'year_id' => $enroll_year,
                        'modified_date' => date('Y-m-d')
                    );
                    if ($this->input->post('description')) {
                        $samiti_details['description'] = $this->input->post('description');
                    }
                    if ($this->Sevasamiti_model->update($samiti_id, $samiti_details)) {
                        $this->session->set_flashdata('success_msg', 'Seva samiti updated Successfully.');
                        redirect('admin/samiti');
                    } else {
                        $this->session->set_flashdata('error_msg', 'Failed to Update seva samiti');
                        redirect('admin/samiti');
                    }
                } else {
                    $this->session->set_flashdata('error_msg', 'Dindi already exists.');
                    redirect('admin/samiti');
                }
            }
        } else {
            $data['samiti_list'] = $this->Sevasamiti_model->get_allData($samiti_id);
            $data['year_list'] = $this->Year->get_year_list();
            $data['dataHeader']['title'] = 'Edit Seva Samiti';
            $this->template->set_master_template('template.php');
            $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
            $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
            $this->template->write_view('content', 'sevasamiti_edit', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'snippets/footer', '', TRUE);
            $this->template->render();
        }
    }

    public function delete() {
        if ($this->input->post('samiti_id')) {
            if ($this->Sevasamiti_model->delete_samiti($this->input->post('samiti_id'))) {
                $this->session->set_flashdata('success_msg', 'Seva Samiti deleted successfully.');
                echo 'success';
            } else {
                $this->session->set_flashdata('error_msg', 'Failed to delete seva samiti.');
                echo 'failed';
            }
        }
    }

    public function activate() {
        if ($this->input->post('samiti_id')) {
            if ($this->Sevasamiti_model->activate_samiti($this->input->post('samiti_id'))) {
                $this->session->set_flashdata('success_msg', 'Seva Samiti Activated successfully.');
                echo 'success';
            } else {
                $this->session->set_flashdata('error_msg', 'Failed to activate seva samiti.');
                echo 'failed';
            }
        }
    }
}
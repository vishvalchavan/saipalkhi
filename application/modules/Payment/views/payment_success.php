<div class="page-wrapper1">
    <div class="container-fluid mt-4">
        <div id="overviews" class="section ">
            <div class="bg-white p-4">
                <h2 class="text-left font-weight-bold thanks-page center">Enrollment for Palakhi <?php echo date('Y'); ?>.</h2>
                <?php
                if (isset($payment_mode)) {
                    if ($payment_mode == "online") {
                        if ($payment_status == 'Success') {
                            ?>
                            <h2 class="text-left">Registration complete.</h2>
                            <p class="text-left">Your DPN Number is <span class="text-primary font-weight-bold"><?php echo $dpn; ?></span></p>
                            <?php
                        } elseif ($payment_status == 'Aborted') {
                            ?>
                            <h2 class="text-left">Transaction Aborted.</h2>
                            <p class="text-left">Kindly try again or use alternate payment option.</p>
                            <?php
                        } elseif ($payment_status == 'Failure') {
                            ?>
                            <h2 class="text-left">Registration Failed.</h2>
                            <p class="text-left">Your transaction has been declined. Kindly try again or use alternate payment option.</p>
                            <?php
                        } else {
                            ?>
                            <h2 class="text-left">Registration Failed.</h2>
                            <p class="text-left">Security Error. Illegal access detected</p>
                            <?php
                        }
                    } elseif ($payment_mode == "palakhibhavan") {
                        ?>
                        <h2 class="text-left">Registration Status : <span class="text-danger font-weight-bold">Pending</span></h2>
                        <p class="text-left">Your DPN Number is <span class="text-primary font-weight-bold"><?php echo $dpn; ?></span> and submit cash to below address</p>
                        <p class="text-left"><?php echo $address; ?></p>
                        <?php
                    } elseif ($payment_mode == "bank") {
                        ?>
                        <h2 class="text-left">Registration Status : <span class="text-danger font-weight-bold">Pending</span></h2>
                        <p class="text-left">Your DPN Number is <span class=""><?php echo $dpn; ?></span> and deposit cash to any Bank of India Branch</p>
                        <p class="text-left">Bank Details :</p>
                        <p class="text-left">Bank Name :&nbsp;<span class=""><?php echo isset($bank_name) ? $bank_name : ''; ?></span></p>
                        <p class="text-left">Bank A/C No : &nbsp;<span class=""><?php echo isset($bank_ac) ? $bank_ac : ''; ?></p>
                        <p class="text-left">IFSC Code : &nbsp;<span class=""><?php echo isset($bank_ifsc) ? $bank_ifsc : ''; ?></p>
                        <p class="text-left">Branch Details : &nbsp;<span class=""><?php echo isset($bank_branch) ? $bank_branch : ''; ?></p>
                        <?php
                    }
                }
                ?>
                <div class="float-right mb-4">
                    <a href="<?php echo base_url('home'); ?>" class="btn btn-primary btn-lg float-right">Back to Home</a>
                </div>
                <br>
            </div>
        </div>
    </div>
</div>

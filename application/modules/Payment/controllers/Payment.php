<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation', 'numbertowords', 'm_pdf'));
        $this->load->library('encryption');
        $this->load->model('Payment_model');
        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('Auth');
        if ($this->ion_auth->is_admin()) {
            $this->ion_auth->logout();
            $this->session->unset_userdata('user_id');
        }
        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0', false);
        header('Pragma: no-cache');
        header("Access-Control-Allow-Origin: *");
    }

    public function ccavRequestHandler() {
        if ($_POST) {
            $payment_option = $this->input->post('param5');
            $this->session->set_userdata('payment_option', $payment_option);
            $this->session->set_userdata('dpn_no', $this->input->post('param4'));
            if ($payment_option == 'online') {
                $this->session->set_userdata('padyatridata', $_POST);
                require_once "ccav/ccavRequestHandler.php";
            } else {
                $devotee = $this->Payment_model->get_devotee_id($this->input->post('param4'));
                if (isset($devotee)) {
                    $devotee_phone_no = $devotee[0]['mobile'];
                    $year_id = $this->Payment_model->get_year_id(date('Y'));
                    //Update Devotee details
                    $devotee_info = array(
                        'relative_name' => $this->input->post('param6'),
                        'relative_mobile' => '91' . $this->input->post('param7'),
                        'sickness' => $this->input->post('param10'),
                        'identity_prof' => $this->input->post('param8'),
                        'adhaar_no' => $this->input->post('param9')
                    );
                    if (isset($devotee_info)) {
                        $this->Payment_model->update_devotee($devotee[0]['id'], $devotee_info);
                    }

                    $padyatri_details = $this->Payment_model->if_padyatri_exists($devotee[0]['id'], $year_id);
                    //IF Padyatri already exists
                    if (isset($padyatri_details) && !empty($padyatri_details)) {
                        if ($padyatri_details[0]['vehical_type'] != $this->input->post('param2')) {
                            if ($padyatri_details[0]['vehical_type'] == "Self" && $this->input->post('param2') == "Samiti") {
                                $truck_name = $this->Payment_model->get_vehicle_details(date("Y"));
                            } elseif ($padyatri_details[0]['vehical_type'] == "Samiti" && $this->input->post('param2') == "Self") {
                                $this->Payment_model->update_vehicle_details($year_id, $padyatri_details[0]['vehicle_details']);
                                $truck_name = "";
                            }
                        } else {
                            $truck_name = $padyatri_details[0]['vehicle_details'];
                        }
                        $padyatri_update = array(
                            // 'dpn_no' => $this->input->post('param4'),
                            'devotee_id' => $devotee[0]['id'],
                            'participation_year_id' => $year_id,
                            'vehical_type' => $this->input->post('param2'),
                            'vehicle_details' => $truck_name,
                            'dindi_id' => $this->input->post('param3'),
                            'seva_samiti_id' => $this->input->post('param1'),
                            'payment_date' => date('Y-m-d'),
                            'payment_mode' => $payment_option,
                            'payment_amount' => $this->input->post('amount'),
                            'payment_status' => 'pending',
                            'enrollment_via' => 'web'
                        );
                        //Insert New padyatri
                        if ($this->Payment_model->update_padyatri($padyatri_details[0]['id'], $padyatri_update)) {
//                            $this->Payment_model->update_padyatri_count(date("Y"), $padyatridata['amount']);
                            if ($payment_option == 'palakhibhavan') {
                                $paymode_Text = "while making payment at Bhavan";
                            } elseif ($payment_option == 'bank') {
                                $paymode_Text = "while making payment at Bhavan";
//                                $paymode_Text = "to us after payment";
                            }
                            $dpn = $this->input->post('param4');
                            $smstext = 'Om Sai Ram!\nYour registration is not confirmed yet\nYour DPN no is "' . $dpn . '"\nKindly mention this DPN no ' . $paymode_Text . '\nCall: 9860003639';
                            send_sms($devotee_phone_no, $smstext);

                            redirect('PaymentSuccess');
                        } else {
                            redirect('enrollment');
                        }
                    } else {
                        if ($this->input->post('param2') == 'Self') {
                            $truck_name = "";
                        } elseif ($this->input->post('param2') == 'Samiti') {
                            $truck_name = $this->Payment_model->get_vehicle_details(date("Y"));
                        }

                        $padyatri_insert = array(
                            // 'dpn_no' => $this->input->post('param4'),
                            'devotee_id' => $devotee[0]['id'],
                            'participation_year_id' => $year_id,
                            'vehical_type' => $this->input->post('param2'),
                            'vehicle_details' => $truck_name,
                            'dindi_id' => $this->input->post('param3'),
                            'seva_samiti_id' => $this->input->post('param1'),
                            'payment_date' => date('Y-m-d'),
                            'payment_mode' => $payment_option,
                            'payment_amount' => $this->input->post('amount'),
                            'payment_status' => 'pending',
                            'enrollment_via' => 'web'
                        );
                        //Insert New padyatri
                        if ($this->Payment_model->insert_padyatri($devotee[0]['id'], $padyatri_insert)) {
//                            $this->Payment_model->update_padyatri_count(date("Y"), $padyatridata['amount']);
                            if ($payment_option == 'palakhibhavan') {
                                $paymode_Text = "while making payment at Bhavan";
                            } elseif ($payment_option == 'bank') {
                                $paymode_Text = "while making payment at Bhavan";
//                                $paymode_Text = "to us after payment";
                            }
                            $dpn = $this->input->post('param4');
                            $smstext = 'Om Sai Ram!\nYour registration is not confirmed yet\nYour DPN no is "' . $dpn . '"\nKindly mention this DPN no ' . $paymode_Text . '\nCall: 9860003639';
//                            $smstext = "Om Sai Ram!\nYour registration is not confirmed yet\nYour DPN no is $dpn\nKindly mention this DPN no $paymode_Text\nCall: 9860003639";
                            send_sms($devotee_phone_no, $smstext);

                            redirect('PaymentSuccess');
                        } else {
                            redirect('enrollment');
                        }
                    }
                }
            }
        }
    }

    public function ccavResponseHandler() {
        require_once "ccav/ccavResponseHandler.php";
        $padyatridata = $this->session->userdata('padyatridata');
        if (isset($padyatridata) && !empty($padyatridata)) {
            if (isset($padyatridata['param4']) && !empty($padyatridata['param4'])) {
                $devotee = $this->Payment_model->get_devotee_id($padyatridata['param4']);
                if (isset($devotee)) {
                    $devotee_phone_no = $devotee[0]['mobile'];

                    $dpn = $padyatridata['param4'];
                    if ($order_status == "Success") {
                        $year_id = $this->Payment_model->get_year_id(date('Y'));
                        $devotee_info = array(
                            'relative_name' => $padyatridata['param6'],
                            'relative_mobile' => '91' . $padyatridata['param7'],
                            'sickness' => $padyatridata['param10'],
                            'identity_prof' => $padyatridata['param8'],
                            'adhaar_no' => $padyatridata['param9']
                        );
                        if (isset($devotee_info)) {
                            $this->Payment_model->update_devotee($devotee[0]['id'], $devotee_info);
                        }
                        if ($padyatridata['param2'] == 'Self') {
                            $truck_name = "";
                        } elseif ($padyatridata['param2'] == 'Samiti') {
                            $truck_name = $this->Payment_model->get_vehicle_details(date("Y"));
                        }
                        $padyatri_insert = array(
                            'devotee_id' => $devotee[0]['id'],
                            'participation_year_id' => $year_id,
                            'vehical_type' => $padyatridata['param2'],
                            'vehicle_details' => $truck_name,
                            'dindi_id' => $padyatridata['param3'],
                            'seva_samiti_id' => $padyatridata['param1'],
                            'payment_date' => date('Y-m-d'),
                            'payment_mode' => 'online',
                            'payment_amount' => $padyatridata['amount'],
                            'payment_status' => $order_status,
                            'transaction_id' => $tracking_id,
                            'enrollment_via' => 'web'
                        );
                        //Insert New padyatri
                        if ($this->Payment_model->insert_padyatri($devotee[0]['id'], $padyatri_insert)) {
                            $this->Payment_model->update_padyatri_count(date("Y"), $padyatridata['amount']);
                            $this->send_receipt($padyatridata['amount'], date('d-m-Y'), $devotee[0]['id']);
                            $this->session->unset_userdata('padyatridata');
                        }
                        $smstext = 'Om Sai Ram!!\nThanks for Sai Palkhi -' . date('Y') . ' registration\nYour DPN no is "' . $dpn . '"\nPlease collect your ID card from samiti members.\nSai Palkhi Samiti, Pune';
//                        $smstext = "Om Sai Ram!!\nThanks for Sai-Palakhi " . date('Y') . " registration\nYour DPN no is $dpn\n-Sai Palakhi Samiti,Pune\nCall: 9860003639 / 7057451648 / 8600258390";
                    } else if ($order_status === "Aborted") {
                        $smstext = "Om Sai Ram!!\nExtremely sorry.\nSomething went wrong.\nYour transation has been Aborted.\nKindly try again or use alternate payment option\nSai Palkhi Samiti";
                    } else if ($order_status === "Failure") {
                        $smstext = "Om Sai Ram!!\nExtremely sorry.\nSomething went wrong.\nYour transation has been declined.\nKindly try again or use alternate payment option\nSai Palkhi Samiti";
                    } else {
                        $smstext = "Om Sai Ram!!\nExtremely sorry.\nSecurity Error\nIllegal access detected\nSai Palkhi Samiti";
                    }
                    $sms_send_status = send_sms($devotee_phone_no, $smstext);
                    $data['dpn'] = $padyatridata['param4'];
                    $data['payment_mode'] = "online";
                    $data['payment_status'] = $order_status;
                    $data['dataHeader']['title'] = 'Thank you';
                    $this->template->set_master_template('frontend_template.php');
                    $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
                    $this->template->write_view('content', 'Payment/payment_success', (isset($this->data) ? $this->data : NULL), TRUE);
                    $this->template->write_view('footer', 'frontend/footer', '', TRUE);
                    $this->template->render();
                }
            }
        } else {
            redirect('AccountDetails');
        }
    }

    public function payment_success() {
        $data['payment_mode'] = $this->session->userdata('payment_option');
        $data['dpn'] = $this->session->userdata('dpn_no');
        $data['bank_name'] = 'Bank of India';
        $data['bank_ac'] = '050610100005667';
        $data['bank_ifsc'] = 'BKID0000506';
        $data['bank_branch'] = 'Pune City branch, Nr Dagdusheth ganpati temple,Pune - 411002';

        $data['address'] = "'Shri Saibaba Palakhi Bhavan',25,Kasaba Peth,Gangotri Building,<br>Above Hero-Honda Showroom,Fadake Houda Chowk,Pune-411011.<br>";
        $data['dataHeader']['title'] = 'Thank you';
        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'Payment/payment_success', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }

    public function send_receipt($amount, $paydate, $devoteeid) {
        error_reporting(E_ALL ^ E_WARNING);
        error_reporting(0);
        $devotee = $this->Payment_model->get_devotee($devoteeid);
        if (isset($devotee) && !empty($devotee)) {
            $email = $devotee['email'];
            if (isset($email) && !empty($email)) {
                $data['name'] = $devotee['firstname'] . " " . $devotee['middlename'] . " " . $devotee['lastname'];
                $data['amount'] = $amount;
                $data['pay_date'] = $paydate;
                $data['address'] = $devotee['address'] . ", " . $devotee['city'] . ", " . $devotee['pincode'];
                $data['dpn_no'] = $devotee['dpn_no'];
                $data['amount_in_word'] = $this->numbertowords->convert_number($amount) . " only";
                $data['payment_mode'] = "Online";

                $html = $this->load->view("Auth/email/email_receipt", $data, true);
                $pdfFilePath = 'RECEIPT_' . md5(date('Y-m-d H:i:s')) . '.PDF';
                $pdf = $this->m_pdf->load();
                $pdf->allow_charset_conversion = true;
                $pdf->charset_in = 'UTF-8';
                $pdf->SetDirectionality('rtl');
                $pdf->autoLangToFont = true;
                $pdf->AddPage('P');
                $pdf->WriteHTML($html);
                $pdf->Output($pdfFilePath, "F");
                $enquirer_name = "Shri Saibaba Palkhi Sohala Samiti, Pune";
                $enquirer_email = "saipalkhipune@gmail.com";

                $mail_body = "Om Sai Ram!!,<br><br>Please find the attached document of payment receipt.<br><br><br>Regards,<br>Shri Palkhi Samiti, Pune<br>Call: 9860003639 / 7057451648 / 8600258390";
                $mail = new PHPMailer();
                //$mail->IsSendmail(); // send via SMTP
                $mail->IsSMTP();
                $mail->SMTPDebug = 0;
                $mail->Debugoutput = 'html';
                $mail->SMTPAuth = true; // turn on SMTP authentication
                $mail->Username = "saipalkhipune@gmail.com"; // SMTP username
                $mail->Password = "Palkhi20!@"; // SMTP password
                //$mail->SMTPSecure = 'ssl';
                $mail->Port = "587";
                $mail->Host = 'smtp.gmail.com'; //hostname
                $mail->From = $enquirer_email;
                $mail->FromName = $enquirer_name;
                $mail->AddAddress($email);
                $mail->AddReplyTo($enquirer_email, $enquirer_name);
                $mail->AddAttachment($pdfFilePath);
                $mail->WordWrap = 50; // set word wrap
                $mail->IsHTML(false); // send as HTML
                $mail->Subject = "Sabhasad Nondani Pavti";
                $mail->MsgHTML($mail_body);
                $mail->AltBody = ""; //Text Body
                $mail->Send();
                unlink($pdfFilePath);
            }
        }
    }

}

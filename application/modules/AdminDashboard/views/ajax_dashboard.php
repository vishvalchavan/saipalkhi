<div class="admin_section">
    <div class="row">
        <div class="col-lg-4 bg-f5f5f5 Devotee-management mx-4">
            <div class="row width-500 mt-2">
                <div class="col-lg-5 mt-2 col">
                    <div class="dashboard-img">
                        <img src="<?php echo base_url() ?>assets/images/icon10.png" class="img-responsive Devotee-img">
                    </div>
                    <h3 class="Devotee-head mt-2">Devotee </h3>
                    <h5 class="dashboard-management">Management</h5>
                </div>
                <div class="col-lg-7 col">
                    <div class="row mt-2">
                        <div class="col-lg-12 col border-bottom">
                            <div class="float-left h65">
                                <h5 class="font-14">Total</h5>
                                <h3 class="center">#<span class="total-Devotee"><?php echo isset($sum_devotee) ? $sum_devotee : 0; ?></span></h3>
                                <a href="<?php echo base_url(); ?>admin/devotee" class="edit-icon float-right">
                                    <i aria-hidden="true" class="fas fa-pencil-alt"></i>
                                </a>
                            </div>
                            <input type="hidden" id="sumDevotee" value="<?php echo isset($sum_devotee) ? $sum_devotee : 0; ?>">
                        </div>
                        <div class="col-lg-12 m-top-5  text-left">
                            <h5 class="total font-14 mleft">This Year</h5>
                            <h3 class="center text-left">#<span class="total-Devotee"><?php echo isset($dashboard_year_data) && !empty($dashboard_year_data) ? $dashboard_year_data[0]->total_devotees : 0; ?></span></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="col-lg-1"></div>-->
        <?php
        $cash_sum = $app_sum = $totalcah = $online_sum = $sabhasad_amt = 0;
        if (isset($dashboard_year_data) && !empty($dashboard_year_data)) {
            if ($dashboard_year_data[0]->enroll_year == date('Y')) {
                $sabhasad_amt = 1500;
            } else {
                $sabhasad_amt = 1200;
            }
            $app_sum = $dashboard_year_data[0]->payment_via_app * $sabhasad_amt;
            $cash_sum = $dashboard_year_data[0]->cash_cheque * $sabhasad_amt;
            $totalcah = $app_sum + $cash_sum;
            $online_sum = $dashboard_year_data[0]->registered_via_web * $sabhasad_amt;
        }
        ?>
        <div class="col-lg-7 col bg-f5f5f5 Padyatri-management mx-4">
            <div class="row">
                <div class="col-lg-3 mt-3">
                    <div class="dashboard-img">
                        <img src="<?php echo base_url() ?>assets/images/icon16.png" class="img-responsive Devotee-img">
                    </div>
                    <h3 class="Devotee-head mt-2">Padyatri</h3>
                    <h5 class="dashboard-management">Management</h5>
                </div>
                <div class="col-lg-9 col">
                    <div class="row">
                        <div class="col-lg-12 col padding-28 border-bottom">
                            <div class="row h67">
                                <div class="col-lg-4 col-sm-6 text-center mt-4">
                                    <h5 class="total text-left font-14">Total :
                                    </h5>
                                    <h3 class="center margin-top">#<span class="total-Devotee"><?php echo isset($dashboard_year_data) && !empty($dashboard_year_data) ? $dashboard_year_data[0]->total_padyatri : 0; ?></span></h3>
                                </div>
                                <div class="col-lg-8 col text-center mt">
                                    <h1 class="total text-left font-14 mt-2">Amount :<h3 class="center margin-top14"><span class="total-Devotee"><i class="fas fa-rupee-sign amount"></i><?php echo isset($dashboard_year_data) && !empty($dashboard_year_data) ? $dashboard_year_data[0]->total_amt : 0; ?></span></h3> </h1>
                                    <a href="<?php echo base_url(); ?>admin/padyatri" class="edit-icon-amount"><i aria-hidden="true" class="fas fa-pencil-alt"></i></a>
                                    <span class="float-left total font-14">Cash :&nbsp;&nbsp;&nbsp;
                                        <strong><i class="fas fa-rupee-sign amount"></i>&nbsp;&nbsp;&nbsp;
                                            <?php echo isset($totalcah) ? $totalcah : 0; ?>
                                        </strong>
                                    </span>
                                    <span class="float-right total font-14">Online : &nbsp;&nbsp;&nbsp;
                                        <strong><i class="fas fa-rupee-sign amount"></i>&nbsp;&nbsp;&nbsp;
                                            <?php
                                            echo isset($online_sum) ? $online_sum : 0;
                                            ?>
                                        </strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-12 mt-3">
                            <div class="row">
                                <div class="col-lg-3 col text-center mt-3">
                                    <h6 class="total font-14">Mode of Payment:</h6>
                                </div>
                                <div class="col-lg-3 text-center">
                                    <div class="col-lg-12 ">
                                        <h5 class="total font-14">Tablet</h5>
                                        <h3 class="center">#<span class="total-Devotee"><?php echo isset($dashboard_year_data) && !empty($dashboard_year_data) ? $dashboard_year_data[0]->payment_via_app : 0; ?></span></h3>
                                    </div>
                                </div>
                                <div class="col-lg-3 text-center">
                                    <div class="col-lg-12">
                                        <h5 class="total font-14">Online</h5>
                                        <h3 class="center"> #<span class="total-Devotee"><?php echo isset($dashboard_year_data) && !empty($dashboard_year_data) ? $dashboard_year_data[0]->registered_via_web : 0; ?></span></h3>
                                    </div>
                                </div>
                                <div class="col-lg-3 text-center">
                                    <div class="col-lg-12">
                                        <h5 class="total font-14">Cash</h5>
                                        <h3 class="center">#<span class="total-Devotee"><?php echo isset($dashboard_year_data) && !empty($dashboard_year_data) ? $dashboard_year_data[0]->cash_cheque : 0; ?></span></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row 1 complete-->
    <!-- row 2 start-->
    <div class="clearfix"></div>
    <div class="row ">
        <div class="col-lg-7 bg-f5f5f5 Sevekari-management mx-4">
            <div class="row">
                <div class="col-lg-3 mt-2 mb-2">
                    <div class="dashboard-img">
                        <img src="<?php echo base_url() ?>assets/images/icon17.png" class="img-responsive Devotee-img">
                    </div>
                    <h3 class="Devotee-head mt-2">Sevekari</h3>
                    <h5 class="dashboard-management">Management</h5>
                </div>
                <div class="col-lg-2 col m-top-5">
                    <h5 class="total text-center font-14">No.of <br>Samiti</h5>
                    <h3 class="center">#<span class="total-Devotee text-center"><?php echo isset($dashboard_year_data) && !empty($dashboard_year_data) ? $dashboard_year_data[0]->total_seva_samiti : 0; ?></span></h3>
                </div>
                <div class="border-right d-none d-md-block"></div>
                <div class="col-lg-3 col m-top-5 col-12">
                    <h5 class="total text-center font-14">No.of <br>Sevekari</h5>
                    <h3 class="center">#<span class="total-Devotee text-center"><?php echo isset($dashboard_year_data) && !empty($dashboard_year_data) ? $dashboard_year_data[0]->total_sevekari : 0; ?></span></h3>
                </div>
                <div class="border-right d-none d-md-block"></div>
                <div class="col-lg-3 col m-top-5 ">
                    <a href="<?php echo base_url(); ?>admin/samiti" class="edit-icon-supplier"><i aria-hidden="true" class="fas fa-pencil-alt"></i></a>
                    <h5 class="total text-center font-14">No.of Supplier Resources</h5>
                    <h3 class="center">#<span class="total-Devotee text-center"><?php echo isset($dashboard_year_data) && !empty($dashboard_year_data) ? $dashboard_year_data[0]->total_resources : 0; ?></span></h3>
                </div>
            </div>
        </div>
        <!--<div class="col-lg-1"></div>-->
        <div class="col-lg-4 bg-f5f5f5 Dindi-management mx-4">
            <div class="row width-500">
                <div class="col-lg-6 col mt-2 mb-2">
                    <div class="dashboard-img left-panel">
                        <img src="<?php echo base_url() ?>assets/images/icon18.png" class="img-responsive Devotee-img">
                    </div>
                    <h3 class="Devotee-head mt-2">Dindi</h3>
                    <h5 class="dashboard-management">Management</h5>
                </div>
                <div class="col-lg-6 col mt-10">
                    <a href="<?php echo base_url(); ?>admin/dindi" class="edit-icon-supplier-dindi"><i aria-hidden="true" class="fas fa-pencil-alt"></i></a>
                    <h5 class="total font-14">Total</h5>
                    <h3 class="text-left center">#<span class="total-Devotee" ><?php echo isset($dashboard_year_data) && !empty($dashboard_year_data) ? $dashboard_year_data[0]->no_of_dindis : 0; ?></span></h3>
                </div>
            </div>
        </div>
    </div>
</div>

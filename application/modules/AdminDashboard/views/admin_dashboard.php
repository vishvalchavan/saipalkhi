<div class="dashboard-page">
    <div class="dashboard-wrapper">
        <div class="row">
            <!-- Column -->
            <div class="col-lg-2 col-12 col-sm-6 col-md-6 dashboard-widget">
                <div class="card card-radius admin">
                    <div class="card-body">
                        <div class="row">
                            <div class="dash-head">
                                <h1 class="m-b-01">#<?php echo isset($dashboard_data) ? $dashboard_data['sum_devotee'] : 0; ?></h1>
                                <h4 class="text-white">Total Devotee</h4>
                            </div>
                            <div>
                                <span>
                                    <img src="<?php echo base_url() ?>assets/images/icon10.png" class="widget-img img-responsive">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-lg-2 col-12 col-sm-6 col-md-6 dashboard-widget">
                <div class="card card-radius admin">
                    <div class="card-body">
                        <div class="row">
                            <div class="dash-head">
                                <h1 class="m-b-01">#<?php echo isset($dashboard_data) ? $dashboard_data['sum_padyatri'] : 0; ?></h1>
                                <h4 class="text-white ml-1">Total Padyatri</h4>
                            </div>
                            <div>
                                <span>
                                    <img src="<?php echo base_url() ?>assets/images/icon11.png" class="widget-img img-responsive">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-lg-2 col-12 col-sm-6 col-md-6 dashboard-widget">
                <div class="card card-radius admin">
                    <div class="card-body">
                        <div class="row">
                            <div class="dash-head">
                                <h1 class="m-b-01">#<?php echo isset($dashboard_data) ? $dashboard_data['sum_app'] : 0; ?></h1>
                                <h4 class="text-white ml-1">Tablet App</h4>
                            </div>
                            <div>
                                <span>
                                    <img src="<?php echo base_url() ?>assets/images/icon12.png" class="widget-img img-responsive">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-2 col-12 col-sm-6 col-md-6 dashboard-widget">
                <div class="card card-radius admin">
                    <div class="card-body">
                        <div class="row">
                            <div class="dash-head">
                                <h1 class="m-b-01">#<?php echo isset($dashboard_data) ? $dashboard_data['sum_web'] : 0; ?></h1>
                                <h4 class="text-white ml-1">Online</h4>
                            </div>
                            <div> <span>
                                    <img src="<?php echo base_url() ?>assets/images/icon13.png" class="widget-img img-responsive">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-2 col-12 col-sm-6 col-md-6 dashboard-widget">
                <div class="card card-radius admin">
                    <div class="card-body">
                        <div class="row">
                            <div class="dash-head">
                                <h1 class="m-b-01">#<?php echo isset($dashboard_data) ? $dashboard_data['sum_cash'] : 0; ?></h1>
                                <h4 class="text-white">Cash At Bhavan</h4>
                            </div>
                            <div>
                                <span>
                                    <img src="<?php echo base_url() ?>assets/images/icon14.png" class="widget-img img-responsive">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-12 col-sm-6 col-md-6 dashboard-widget">
                <div class="card card-radius admin">
                    <div class="card-body">
                        <div class="row">
                            <div class="float-right m-l-10">
                                <h1 class="m-b-01 ml-3"><i class="fas fa-rupee-sign dashboard icons"></i><?php echo isset($dashboard_data) ? $dashboard_data['sum_donation'] : 0; ?></h1>
                                <h4 class="text-white ml-3">No of Donor: #<?php echo isset($dashboard_data) ? $dashboard_data['sum_donor'] : 0; ?></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- row -->
        <div class="card border-radius20">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="dashboard-select">
                            <label class="palkhi-select ml-2">Palkhi Sohala&nbsp;:&nbsp;</label>
                            <select class="dashboard-select-option" id="year_list" name="year_list">
                                <?php
                                $enroll_year = isset($dashboard_year_data) ? $dashboard_year_data->enroll_year : date("Y");
                                if (isset($year_list) && !empty($year_list)) {
                                    foreach ($year_list as $year) {
                                        $selected = '';
                                        if ($enroll_year == $year['year']) {
                                            $selected = "selected";
                                        }
                                        ?>
                                        <option value="<?php echo $year['id'] ?>" <?php echo $selected; ?>><?php echo $year['year'] ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-9"></div>
                </div>
                <div class="clearfix"></div>
                <div id="dashboard_section">
                    <div class="row">
                        <div class="col-lg-4 bg-f5f5f5 Devotee-management mx-4">
                            <div class="row width-500 mt-2">
                                <div class="col-lg-5 mt-2 col">
                                    <div class="dashboard-img">
                                        <img src="<?php echo base_url() ?>assets/images/icon10.png" class="img-responsive Devotee-img">
                                    </div>
                                    <h3 class="Devotee-head mt-2">Devotee </h3>
                                    <h5 class="dashboard-management">Management</h5>
                                </div>
                                <div class="col-lg-7 col">
                                    <div class="row mt-2">
                                        <div class="col-lg-12 col border-bottom">
                                            <div class="float-left h65 ">
                                                <h5 class="font-14">Total</h5>
                                                <h3 class="center">#<span class="total-Devotee"><?php echo isset($dashboard_data) ? $dashboard_data['sum_devotee'] : 0; ?></span></h3>

                                                <a href="<?php echo base_url(); ?>admin/devotee" class="edit-icon float-right">
                                                    <i aria-hidden="true" class="fas fa-pencil-alt"></i>
                                                </a>
                                            </div>
                                            <input type="hidden" id="sumDevotee" value="<?php echo isset($dashboard_data) ? $dashboard_data['sum_devotee'] : 0; ?>">
                                        </div>
                                        <div class="col-lg-12 m-top-5 text-left">
                                            <h5 class="total font-14 mleft">This Year</h5>
                                            <h3 class="center text-left">#<span class="total-Devotee"><?php echo isset($dashboard_year_data) ? $dashboard_year_data->total_devotees : 0; ?></span></h3>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php
                        $cash_sum = $app_sum = $totalcah = $online_sum = 0;
                        if (isset($dashboard_year_data) && !empty($dashboard_year_data)) {
                            $app_sum = $dashboard_year_data->payment_via_app * 1500;
                            $cash_sum = $dashboard_year_data->cash_cheque * 1500;
                            $totalcah = $app_sum + $cash_sum;
                            $online_sum = $dashboard_year_data->registered_via_web * 1500;
                        }
                        ?>
                        <div class="col-lg-7 col bg-f5f5f5 Padyatri-management mx-4">
                            <div class="row">
                                <div class="col-lg-3 mt-3">
                                    <div class="dashboard-img">
                                        <img src="<?php echo base_url() ?>assets/images/icon16.png" class="img-responsive Devotee-img">
                                    </div>
                                    <h3 class="Devotee-head mt-2">Padyatri</h3>
                                    <h5 class="dashboard-management">Management</h5>
                                </div>
                                <div class="col-lg-9 col">
                                    <div class="row">
                                        <div class="col-lg-12 col padding-28 border-bottom">
                                            <div class="row h67">
                                                <div class="col-lg-4 col-sm-6 text-center mt-4">
                                                    <h5 class="total text-left font-14">Total :
                                                    </h5>
                                                    <h3 class="center margin-top">#<span class="total-Devotee"><?php echo isset($dashboard_year_data) ? $dashboard_year_data->total_padyatri : 0; ?></span></h3>
                                                </div>
                                                <div class="col-lg-8 col text-center mt">
                                                    <h1 class="total text-left font-14 mt-2">Amount :<h3 class="center margin-top14"><span class="total-Devotee"><i class="fas fa-rupee-sign amount"></i><?php echo isset($dashboard_year_data) ? $dashboard_year_data->total_amt : 0; ?></span></h3> </h1>
                                                    <a href="<?php echo base_url(); ?>admin/padyatri" class="edit-icon-amount"><i aria-hidden="true" class="fas fa-pencil-alt"></i></a>
                                                    <span class="float-left total font-14">Cash :&nbsp;&nbsp;&nbsp;
                                                        <strong><i class="fas fa-rupee-sign amount"></i>&nbsp;&nbsp;&nbsp;
                                                            <?php echo isset($totalcah) ? $totalcah : 0; ?>
                                                        </strong>
                                                    </span>
                                                    <span class="float-right total font-14">Online : &nbsp;&nbsp;&nbsp;
                                                        <strong><i class="fas fa-rupee-sign amount"></i>&nbsp;&nbsp;&nbsp;
                                                            <?php
                                                            echo isset($online_sum) ? $online_sum : 0;
                                                            ?>
                                                        </strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-12 mt-3">
                                            <div class="row">
                                                <div class="col-lg-3 col text-center mt-3">
                                                    <h6 class="total font-14">Mode of Payment:</h6>
                                                </div>
                                                <div class="col-lg-3 text-center">
                                                    <div class="col-lg-12 ">
                                                        <h5 class="total font-14">Tablet</h5>
                                                        <h3 class="center">#<span class="total-Devotee"><?php echo isset($dashboard_year_data) ? $dashboard_year_data->payment_via_app : 0; ?></span></h3>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 text-center">
                                                    <div class="col-lg-12">
                                                        <h5 class="total font-14">Online</h5>
                                                        <h3 class="center"> #<span class="total-Devotee"><?php echo isset($dashboard_year_data) ? $dashboard_year_data->registered_via_web : 0; ?></span></h3>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 text-center">
                                                    <div class="col-lg-12">
                                                        <h5 class="total font-14">Cash</h5>
                                                        <h3 class="center">#<span class="total-Devotee"><?php echo isset($dashboard_year_data) ? $dashboard_year_data->cash_cheque : 0; ?></span></h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- row 1 complete-->
                    <!-- row 2 start-->
                    <div class="clearfix"></div>
                    <div class="row ">
                        <div class="col-lg-7 bg-f5f5f5 Sevekari-management mx-4 ">
                            <div class="row">
                                <div class="col-lg-3 mt-2 mb-2">
                                    <div class="dashboard-img">
                                        <img src="<?php echo base_url() ?>assets/images/icon17.png" class="img-responsive Devotee-img">
                                    </div>
                                    <h3 class="Devotee-head mt-2">Sevekari</h3>
                                    <h5 class="dashboard-management">Management</h5>
                                </div>
                                <div class="col-lg-2 col m-top-5">
                                    <h5 class="total text-center font-14">No.of <br>Samiti</h5>
                                    <h3 class="center">#<span class="total-Devotee text-center"><?php echo isset($dashboard_year_data) ? $dashboard_year_data->total_seva_samiti : 0; ?></span></h3>
                                </div>
                                <div class="border-right d-lg-block d-none d-xl-none"></div>
                                <div class="col-lg-3 col m-top-5 col-12">
                                    <h5 class="total text-center font-14">No.of <br>Sevekari</h5>
                                    <h3 class="center">#<span class="total-Devotee text-center"><?php echo isset($dashboard_year_data) ? $dashboard_year_data->total_sevekari : 0; ?></span></h3>
                                </div>
                                <div class="border-right d-lg-block d-none d-xl-none"></div>
                                <div class="col-lg-3 col m-top-5">
                                    <a href="<?php echo base_url(); ?>admin/samiti" class="edit-icon-supplier"><i aria-hidden="true" class="fas fa-pencil-alt"></i></a>
                                    <h5 class="total text-center font-14">No.of Contractors<br> Employee</h5>
                                    <h3 class="center">#<span class="total-Devotee text-center"><?php echo isset($dashboard_year_data) ? $dashboard_year_data->total_resources : 0; ?></span></h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col bg-f5f5f5 Dindi-management mx-4">
                            <div class="row width-500">
                                <div class="col-lg-6 col mt-2 mb-2">
                                    <div class="dashboard-img left-panel">
                                        <img src="<?php echo base_url() ?>assets/images/icon18.png" class="img-responsive Devotee-img">
                                    </div>
                                    <h3 class="Devotee-head mt-2">Dindi</h3>
                                    <h5 class="dashboard-management">Management</h5>
                                </div>
                                <div class="col-lg-6 col mt-10">
                                    <a href="<?php echo base_url(); ?>admin/dindi" class="edit-icon-supplier-dindi"><i aria-hidden="true" class="fas fa-pencil-alt"></i></a>
                                    <h5 class="total font-14">Total</h5>
                                    <h3 class="text-left center">#<span class="total-Devotee" ><?php echo isset($dashboard_year_data) ? $dashboard_year_data->no_of_dindis : 0; ?></span></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- row2 complete-->
                <div class="clearfix"></div>
                <!-- bottom row start -->
                <div class="row bottom-card">
                    <div class="col-lg-24 col card-radius-bottom">
                        <a href="<?php echo base_url(); ?>admin/album">
                            <div class="row">                            
                                <div class="col-lg-6 col">
                                    <div class="m-t-18">                                    
                                        <h4 class="text-white text-center">Photogallery</h4>                                   
                                    </div>
                                </div>
                                <div class="center">
                                    <div class="col-lg-6 m-t-9 col-6 float-right">
                                        <img src="<?php echo base_url() ?>assets/images/1.png" class="mb-2">
                                    </div>
                                </div>
                            </div> 
                        </a>
                    </div>
                    <div class="col-lg-24  col card-radius-bottom">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="m-t-18">
                                    <h4 class="text-white text-center">CMS</h4>
                                </div>
                            </div>
                            <div class="">
                                <div class="col-lg-6 m-t-9 col-6 float-right">
                                    <img src="<?php echo base_url() ?>assets/images/cms.png" width="58" height="53" class="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-24 col card-radius-bottom">
                        <a href="<?php echo base_url(); ?>admin/reports">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="m-t-18">
                                        <h4 class="text-white text-center">Reports</h4>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="col-lg-6 m-t-9 col-6 float-right">
                                        <img src="<?php echo base_url() ?>assets/images/3.png" class="m-t-3 ">
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-24 col card-radius-bottom">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="m-t-18 ">
                                    <h4 class="text-white text-center">Config</h4>
                                </div>
                            </div>
                            <div class="">
                                <div class="col-lg-6 m-t-9 col-6 float-right">
                                    <img src="<?php echo base_url() ?>assets/images/4.png" width="58" height="53" class="mt-1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card -end -->
            <!-- Column -->
            <!-- Column -->
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('body').on('change', '#year_list', function () {
            $(".loadding-page").css('display', 'block');
            var year = $(this).val();
            var sum_devotee = $("#sumDevotee").val();
            if (year.length !== 0) {
                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url(); ?>AdminDashboard/load_dashboard',
                    data: {'year_id': year, 'devotees': sum_devotee},
                    success: function (data) {
                        if (data) {
                            $("#dashboard_section").html(data);
                            $(".loadding-page").css('display', 'none');
                        }
                    }
                });
            }
        });
    });
</script>

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminDashboard extends MY_Controller {

    public function __construct() {
        parent::__construct();

        if (!$this->ion_auth->logged_in()) {
            redirect('admin', 'refresh');
        }
        if (!$this->ion_auth->is_admin()) {
            redirect('admin', 'refresh');
        }
        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->model(array('dashboard', 'Year', 'language'));
        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('Auth');
    }

    public function index() {

        $user_id = $this->session->userdata('user_id');
        $data = $this->dashboard->get_dashboardInfo();
        $data['year_list'] = $this->Year->get_year_list();
        $data['dataHeader']['title'] = 'Home';
        $this->template->set_master_template('template_admindash.php');
        $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'AdminDashboard/admin_dashboard', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'snippets/footer', '', TRUE);
        $this->template->render();
    }

    public function load_dashboard() {
        if ($this->input->post('year_id')) {
            $year = $this->Year->get_year_by_id($this->input->post('year_id'));
            $data['sum_devotee'] = $this->input->post('devotees');
            $data['dashboard_year_data'] = $this->dashboard->get_dashboard_year_data($year);
            $view = $this->load->view('ajax_dashboard', $data, true);
            echo $view;
        }
    }

}

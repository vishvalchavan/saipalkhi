<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <!-- Slide One - Set the background image for this slide in the line below -->
        <div class="carousel-item active" style="background-image: url('<?php echo base_url(); ?>assets/images/bg-1.jpg')">
            <!--  <div class="carousel-caption d-none d-md-block">
                 <h2>श्री साईबाबा पालखी सोहळा समिती आयोजित श्री साई पालखी सोहळा</h2>
             </div> -->
        </div>
        <!-- Slide Two - Set the background image for this slide in the line below -->
        <div class="carousel-item" style="background-image: url('<?php echo base_url(); ?>assets/images/bg.jpg')">
            <!--    <div class="carousel-caption d-none d-md-block">
                   <h2>श्री साईबाबा पालखी सोहळा समिती आयोजित श्री साई पालखी सोहळा</h2>
               </div> -->
        </div>
        <!-- Slide Three - Set the background image for this slide in the line below -->
        <div class="carousel-item" style="background-image: url('<?php echo base_url(); ?>assets/images/bg3.jpg')">
            <!--   <div class="carousel-caption d-none d-md-block">
                  <h2>श्री साईबाबा पालखी सोहळा समिती आयोजित श्री साई पालखी सोहळा</h2>
              </div> -->
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<!-- Page Content -->

<div id="sec-about" data-vc-full-width="true" data-vc-full-width-init="true" class="about-page">
    <div class=" col-sm-12">
        <div class="container ">
            <div class="wrapper">
                <div class="">
                    <div class="rt-about-one">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="rtin-about-image"> <img  src="<?php echo base_url() . 'assets/' ?>images/home1.png" class="attachment-full size-full img-fluid img-rounded"></div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mx-0">
                                <div class="rtin-about-content-center mx-0">
                                    <div class="rtin-about-content p-0">
                                        <div class="w-100 mb-3">
                                            <img class="rounded-circle" width="130px" src="<?php echo base_url() . 'assets/' ?>images/om.png">
                                        </div>
                                        <h2>Shri SaiBaba Palkhi Sohla</h2>

                                        <div class="mt-4">With the blessing of Shri Sainath maharaj & with immense support from sai devotees the “Shri Saibaba Palkhi Sohla” is moving towards 30 year of completion. It started in the year 1989 with mare devotees going to Shirdi on foot & now this process has taken a form of cultural festival Maharashtra. This is our small effort to present history of this palkhi festival.
                                            <p></p>
                                            <p class="palakhi-heading">This is the first palkhi from pune which goes to Shirdi.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section id="cta2">
    <div class="container">
        <div class="text-center section-header">

            <div class="row">

                <div class="col-md-12">
                    <h2 class="text-white section-title text-center wow fadeInDown animated font-weight-bold">Appeal for Vastunidhi</h2>
                    <p class="wow fadeInUp animated" data-wow-duration="300ms" data-wow-delay="100ms">Sai Palkhi Samiti Trust has been group on successfully for all these years due to Donors, well-wishers and inspires. With the same donations, and faithtrust has planned to build a “Sai Palkhi Bhavan at Shirdi” and for this, we have managed to purchase land measuring 9924 sq.ft in Shirdi.</p>
                    <p class="wow fadeInUp animated" data-wow-duration="300ms" data-wow-delay="200ms">Sai palkhi samiti, appeals Sai devotees for contribution and involvement in this project so that trust can start construction work. Every single rupee that is donated is extremely important to the Samiti to complete this project.             
                    </p>
                    <a class="btn btn-primary btn-lg" href="<?php echo base_url(); ?>donation">Donate Now</a>
                </div>

            </div>

<!--             <img class="img-responsive wow fadeIn animated" src="<?php echo base_url() . 'assets/' ?>images/cta2-img.png" alt="" data-wow-duration="300ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 300ms; animation-delay: 300ms; animation-name: fadeIn;"> -->
        <!--     <img class="img-responsive wow fadeIn animated" src="" alt="" data-wow-duration="300ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 300ms; animation-delay: 300ms; animation-name: fadeIn;"> -->
        </div>
    </div>
</section>


<section id="what-we-do">
    <div class="container activities-page">
        <h2 class="section-title mb-2 h1">Our Mission</h2>
        <p class="text-center text-muted h5">Having and managing a correct marketing strategy is crucial in a fast moving market.</p>
        <div class="row mt-5">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                <div class="card">
                    <div class="card-block block-1">
                        <h3 class="card-title activities-title">A platform to meet
                        </h3>
                        <p class="album-description-userside">To provide Sai devotees world over, a platform to meet and exchange their experiences and beliefs.</p>
           <!--              <a href="javascript:void();" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a> -->
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                <div class="card">
                    <div class="card-block block-3">
                        <h3 class="card-title activities-title">Libraries and reading rooms
                        </h3>
                        <p class="album-description-userside">To run and maintain libraries and reading rooms.<br>&nbsp;

                        </p>
           <!--              <a href="javascript:void();" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a> -->
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                <div class="card">
                    <div class="card-block block-2">
                        <h3 class="card-title activities-title">Better service to humanity at large
                        </h3>
                        <p class="album-description-userside">To create a network among various Shirdi Sai institutions around the world and to bring them on to a common platform to render better service to humanity at large.</p>
           <!--              <a href="javascript:void();" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a> -->
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                <div class="card">
                    <div class="card-block block-4">
                        <h3 class="card-title activities-title">Medical check-up camps
                        </h3>
                        <p class="album-description-userside">To organize regular medical check-up camps for the benefit of the poor strata of the society with the help of qualified medical specialists.</p>
           <!--              <a href="javascript:void();" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a> -->
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                <div class="card">
                    <div class="card-block block-5">
                        <h3 class="card-title activities-title">Facilitate the rehabilitation
                        </h3>
                        <p class="album-description-userside">To assist areas affected by natural calamities such as cyclone, food, earthquake etc. and to facilitate the rehabilitation process nationally.</p>
           <!--              <a href="javascript:void();" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a> -->
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                <div class="card">
                    <div class="card-block block-6">
                        <h3 class="card-title activities-title">Grant-in-aid
                        </h3>
                        <p class="album-description-userside">To establish, maintain, run, develop, give grant-in-aid to the schools, colleges, institutions of fine arts, institutions imparting technical vocational and industrial education,poor houses, orphanages, old age homes and other like institutions with public service as the motive.</p>
           <!--              <a href="javascript:void();" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a> -->
                    </div>
                </div>
            </div>
        </div>
    </div>   
</section>
<section id="cta" class="wow fadeIn animated mt-4 account-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 mb-4">
                <h2 class="section-title mb-2 h1 text-left text-white">Create new account</h2>
                <p class="wow fadeInUp animated text-white" data-wow-duration="300ms" data-wow-delay="100ms">Shri Sai Baba taught a moral code of love, forgiveness, helping others, charity, contentment, inner peace, devotion to God and guru. His philosophy was Advaita Vedanta and his teachings consisted of elements both of this school as well as of bhakti and Islam.
                </p>
                <a class="btn btn-primary btn-lg" href="<?php echo base_url(); ?>signup">Register for Palakhi Sohala <?php echo date("Y"); ?></a>
            </div>
            <div class="col-sm-6 text-right">
                <!-- 16:9 aspect ratio -->
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="my_class_name" width="100%" height="480" src="https://www.youtube.com/embed/Ew190ZvXXsw" frameborder="0" allowfullscreen></iframe>

                </div>
            </div>
        </div>
</section>
<div class="sticky-container p-0">
    <ul class="sticky p-0">
        <!--        <li style="margin-left:-115px">
                    <a href="<?php echo base_url() . 'signup' ?>">
                        <img src="<?php echo base_url() . 'assets/' ?>images/register.png" width="32" height="32">
                        <p><br>New Registration</p>
                    </a>
                </li>-->
        <!--   <li >
              <a href="<?php echo base_url() . 'AccountDetails' ?>">
                  <img src="<?php echo base_url() . 'assets/' ?>images/padyatri-register.png" width="32" height="32">
                  <p>Padyatri<br>Registration 2019</p>
              </a>
          </li> -->
        <li style="margin-left:-50px">
            <a href="<?php echo base_url() . 'login' ?>">
                <img src="<?php echo base_url() . 'assets/' ?>images/login-new.png" width="32" height="32">
                <p><br>Login</p>
            </a>
        </li>


    </ul>
</div>
<script>
    baguetteBox.run('.compact-gallery', {animation: 'slideIn'});
</script>
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('url', 'language','html'));
        $this->lang->load('Auth');
        if ($this->ion_auth->is_admin()) {
            $this->ion_auth->logout();
            $this->session->unset_userdata('user_id');
        }

    }

    public function index() {
        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'dashboard', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }

}

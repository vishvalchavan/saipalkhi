<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sevekari extends MY_Controller {

    public function __construct() {
        parent::__construct();
        
         if (!$this->ion_auth->logged_in()) {
            redirect('admin', 'refresh');
        }
        if (!$this->ion_auth->is_admin()) {
            redirect('admin', 'refresh');
        }
        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->model(array('Sevekari_model', 'Sevasamiti_model', 'Year', 'language'));
        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('Auth');
    }

    public function sevekari_list($samiti_id) {
         if (!$this->ion_auth->logged_in()) {
             redirect('admin', 'refresh');
         }
         if (!$this->ion_auth->is_admin()) {
             redirect('admin', 'refresh');
         }
        $user_id = $this->session->userdata('user_id');
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $samiti_details = $this->Sevasamiti_model->get_allData($samiti_id);
            $sevekaris = $this->input->post('sevekari');
            if ($this->input->post('sevekari_old')) {
                $old_sevekaris = explode(',', $this->input->post('sevekari_old'));
            }
            $head = $this->input->post('head');
            $year = date('Y');
            if (isset($sevekaris, $head)) {
                $sevekari_array = array();
                $insertedCount = $updatedCount = 0;

                if (isset($old_sevekaris) && !empty($old_sevekaris)) {
                    $sevekari_diff = array_diff($old_sevekaris, $sevekaris);
                    if (isset($sevekari_diff) && !empty($sevekari_diff)) {
                        $this->Sevekari_model->delete_sevekari($sevekari_diff, $samiti_id);
                        $this->Sevekari_model->delete_sevekariCount(count($sevekari_diff), $year);
                    }
                }

                foreach ($sevekaris as $sevekari) {
                    $count = $this->Sevekari_model->if_sevekari_exists($samiti_details['id'], $sevekari);
                    $sevekariData = array(
                        'padyatri_no' => $sevekari,
                        'seva_samiti_id' => $samiti_details['id'],
                        'created_by' => $user_id,
                        'created_date' => date("Y-m-d"),
                        'isactive' => 1,
                        'ishead' => $sevekari == $head ? 1 : 0
                    );
                    if (count($count) > 0) {
                        $this->Sevekari_model->update($count[0]['id'], $sevekariData);
                        $updatedCount++;
                    } else {
                        $this->Sevekari_model->insert($sevekariData);
                        $insertedCount++;
                    }
                }

                if (($insertedCount > 0) || ($updatedCount > 0)) {
                    if ($insertedCount > 0) {
                        $this->Sevekari_model->update_sevekariCount($insertedCount, $year);
                    }
                    $this->session->set_flashdata('success_msg', 'Sevekari details added successfully.');
                    redirect('admin/sevekari/' . $samiti_id);
                } else {
                    $this->session->set_flashdata('error_msg', 'Failed to add Sevekari details.');
                    redirect('admin/sevekari/' . $samiti_id);
                }
            }
        } else {
            $data['sevekari_willing'] = $this->Sevekari_model->get_willing_sevekari($samiti_id);
            $data['sevekari_list'] = $this->Sevekari_model->get_sevekari_list($samiti_id);
            $data['samiti_details'] = $this->Sevasamiti_model->get_allData($samiti_id);

            if (isset($data['sevekari_list'])) {
                foreach ($data['sevekari_list'] as $sevekari) {
                    $data['sevekari_ids'][] = $sevekari['padyatri_no'];
                    if ($sevekari['ishead'] == 1) {
                        $data['head'] = $sevekari['padyatri_no'];
                    }
                }
            }

            $data['dataHeader']['title'] = 'Add Sevekari';
            $this->template->set_master_template('template.php');
            $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
            $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
            $this->template->write_view('content', 'sevekari_list', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'snippets/footer', '', TRUE);
            $this->template->render();
        }
    }
}
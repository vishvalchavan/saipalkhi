<div class="card min-height80">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <div class="row mx-0">
                    <div class="col-md-5 col-5 align-self-center p-0">
                        <h3 class="card-title">Add Sevekari</h3>
                    </div>
                    <div class="col-md-7 col-7 align-self-center text-right p-0">
                        <a href="<?php echo base_url() . 'admin/samiti' ?>"> <button type="button" class="btn btn-info back-button"><i class="fas fa-arrow-left"></i> &nbsp;&nbsp; Back to Samiti List</button></a>
                    </div>
                </div>
                <hr>
                <div class="row mx-0">
                    <div class="col-12">
                        <form class="form-horizontal p-t-20">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Samiti Name:</label>
                                        <div class="col-md-7">
                                                                     
                                                <input type="text" class="form-control" id="samitiName" placeholder="Samiti Name" value="<?php echo isset($samiti_details['name']) ? $samiti_details['name'] : '' ?>" readonly>
                                           
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group required row" aria-required="true">
                                        <label class="control-label text-left col-md-4">Description:</label>
                                        <div class="col-md-7">
                                           
                                                <textarea class="form-control" colspan="8" rows="3" id="comment" readonly><?php echo isset($samiti_details['description']) ? $samiti_details['description'] : '' ?></textarea>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- 
                                                        <div class="form-group required row">
                                                            <label for="exampleInputuname3" class="col-sm-2 control-label">Samiti Name</label>
                                                            <div class="col-sm-4">
                                                                <div class="input-group">                            
                                                                    <input type="text" class="form-control" id="samitiName" placeholder="Samiti Name" value="<?php echo isset($samiti_details['name']) ? $samiti_details['name'] : '' ?>" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group required row">
                                                            <label for="exampleInputEmail3" class="col-sm-2 control-label">Description</label>
                                                            <div class="col-sm-8">
                                                                <div class="input-group">
                                                                    <textarea class="form-control" colspan="8" rows="5" id="comment" readonly><?php echo isset($samiti_details['description']) ? $samiti_details['description'] : '' ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div> -->
                        </form>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-7 align-self-center">
                        <h3 class="card-title"></h3>
                    </div>

                    <div class="col-md-5 text-right col-12 p-0">

                        <div class="row col-12 p-0">
                            <div class="col-lg-7 m-0 col-6 p-0">
                                <?php echo form_open("Supplier/generate_sevekari_report"); ?>  
                                <input type="hidden" name="samiti_id" value="<?php echo isset($samiti_details['id']) ? $samiti_details['id'] : ''; ?>">
                                <button type="submit" class="btn btn-info back-button ml4 float-right ml-4"><i class="fas fa-plus-circle"></i> Generate Report</button>
                                <?php echo form_close(); ?> 
                            </div>
                            <div class="col-lg-5 col-6 p-0">
                                <a href="<?php echo base_url() . 'admin/supplier/' . $samiti_details['id'] ?>"> <button type="button" class="btn btn-info back-button"><i class="fas fa-plus-circle"></i> &nbsp;&nbsp;Manage Contractor</button></a>
                                </a>
                            </div>                                
                        </div>                   
                    </div>
                </div>
                <?php echo form_open("admin/sevekari/" . $samiti_details['id'], array('id' => 'addSevekari_form', 'class' => 'form-horizontal p-t-20')); ?>  
                <div class="table-responsive">
                    <table id="sevekari_list" class="display nowrap table table-hover table-striped table-bordered dataTable datatable" cellspacing="0" width="100%" role="grid" aria-describedby="example23_info" style="border: 1px solid #dee2e6;">
                        <thead>
                            <tr role="row">
                                <th class="text-center"><input type="checkbox" id="selectAll" />&nbsp;&nbsp;&nbsp;All</th>
                                <th >Sevekari Name</th>
                                <th>Mobile No.</th>
                                <th class="text-center">Select Samiti Head</th>
                            </tr>
                        </thead>                                        
                        <tbody>
                            <?php
                            if (isset($sevekari_willing) && !empty($sevekari_willing)) {
                                foreach ($sevekari_willing as $sevekari) {
                                    if (isset($sevekari_ids) && in_array($sevekari['padyatriid'], $sevekari_ids)) {
                                        $selected = "checked";
                                    } else {
                                        $selected = "";
                                    }
                                    if (isset($head) && $sevekari['padyatriid'] == $head) {
                                        $head_check = 'checked';
                                    } else {
                                        $head_check = "";
                                    }
                                    ?>
                                    <tr>
                                        <td class="text-center"><input type="checkbox" name="sevekari[]" value="<?php echo $sevekari['padyatriid']; ?>" data-rel="head<?php echo $sevekari['padyatriid']; ?>" class="check" <?php echo isset($selected) ? $selected : '' ?>/></td>
                                        <td><?php echo $sevekari['firstname'] . ' ' . $sevekari['middlename'] . ' ' . $sevekari['lastname']; ?></td>          
                                        <td><?php echo $sevekari['mobile']; ?></td>
                                        <td class="text-center"><input type="radio" class="radiobtn" id="head<?php echo $sevekari['padyatriid']; ?>" name="head" value="<?php echo $sevekari['padyatriid']; ?>" <?php echo isset($selected) && $selected == 'checked' ? '' : 'disabled'; ?> <?php echo isset($head_check) ? $head_check : '' ?>></td>   
                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr><td colspan="4" class="text-center">No records found.</td></tr>
                            <?php }
                            ?>
                        </tbody>
                    </table>
                </div>
                <input type="hidden" name="sevekari_old" value="<?php echo isset($sevekari_ids) ? implode(',', $sevekari_ids) : ''; ?>">
                <input type="hidden" name="head_old" value="<?php echo isset($head) ? $head : ''; ?>">
                <div class="clearfix"></div><br>
                <div class="pull-left error-msg" id="sevekari_error"></div>
                <div class="form-actions pull-right">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row m-r14">
                                <div class="">
                                    <button type="button" class="btn btn-inverse update-button" id="addSevekari">Save</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6"> </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#selectAll').click(function (e) {
            $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
            if ($(this).prop('checked') == true) {
                $(this).closest('table').find('td input:radio').prop('disabled', false);
            } else if ($(this).prop('checked') == false) {
                $(this).closest('table').find('td input:radio').prop('disabled', true);
                $(this).closest('table').find('td input:radio').prop('checked', false);
            }
        });
        $('.check').click(function () {
            var radio = $(this).attr('data-rel');
            if ($(this).prop('checked') == true) {
                $("#" + radio).prop('disabled', false);
            } else if ($(this).prop('checked') == false) {
                $("#" + radio).prop('disabled', true);
                $("#" + radio).prop('checked', false);
            }
            IsCheck_checkbox();
        });
        $("#addSevekari").click(function () {
            var selectCount = $('input[name="sevekari[]"]:checked').length;
            var headcount = $('input[name="head"]:checked').length;
            if (selectCount > 0) {
                if (headcount > 0) {
                    $("#addSevekari_form").submit();
                } else {
                    $("#sevekari_error").html('Please select one of sevekari as Samiti Head');
                }
            } else {
                $("#sevekari_error").html('Minimum 1 checkbox need to be selected');
            }
        });
        IsCheck_checkbox();
    });
    function IsCheck_checkbox(){
        var allcheckbox = $("input[class='check']").length;        
        var checked_box = $('input:checkbox.check:checked').length;        
        if(allcheckbox == checked_box){ $("#selectAll").prop('checked', true); }else{$("#selectAll").prop('checked', false);}
    }
   
</script>
<div class="all-title-box-upcoming-events-banner">
</div>
<div class="page-wrapper">
    <div class="container-fluid">
        <div id="overviews" class="">
            <div class="container">     
                <div class="heading section-header">
                    <h2 class="section-title text-center wow fadeInDown animated font-weight-bold">Upcoming Events</h2>                
                </div>
                <div class="row align-items-center">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="message-box">
                            <h2>Palakhi</h2>
                            <p>The Marathi word ‘Palkhi’ is not only been associated with pilgrimages, but to carry people as well. It has always represented a royal mode of transport. But here we are discussing about ‘Palkhi’ in which paduka (Silver Footwear) of Shri Saibaba are carried with honor from Pune to Shirdi. It signifies his spiritual presence in the journey. </p>
                            <p>The present day of Palkhi of Shri Saibaba is quite elaborate: The pole is covered with silver plating, the seat is inlaid with precious metals and the framework is finely carved (See figure). Its weight is considerable and eight to ten men are required to carry it on their shoulders. During the journey it is placed on a four wheeled tractor which is made of completely silver. This is known as rath. Whenever the palkhi reaches the any town or village it is taken down from the rath and carried on shoulders. This Silver rath with the palkhi is the center of attraction of the entire pilgrimage and has a magnetic presence pulling visitors, tourist , local village people to ‘see’ and ‘touch’ any part of the assembly as a way of taking darshan and getting blessed.</p>

                            <!--<a href="trustee.html" class="hover-btn-new orange"><span>View Commitee Members</span></a>-->
                        </div><!-- end messagebox -->
                    </div><!-- end col -->
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="post-media wow fadeIn">
                            <img src="<?php echo base_url(); ?>assets/images/whypalkhi2.png" alt="" class="img-fluid img-rounded">
                        </div><!-- end media -->
                    </div><!-- end col -->
                </div>
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="post-media wow fadeIn">
                            <img src="<?php echo base_url(); ?>assets/images/whypalkhi2.png" alt="" class="img-fluid img-rounded">
                        </div><!-- end media -->
                    </div><!-- end col -->
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="message-box">
                            <h2>Varkari</h2>
                            <p class="text-left wow fadeInDown animated">Maharashtra has very beautiful history of "Palkhi". Varkaris meanings - People who follow the wari, a fundamental ritual.Varkari is a religious movement (sampraday) within the bhakti spiritual tradition of Hinduism. 
                                In the Marathi language of Maharashtra, Varkari as Var + Kari was defined as-
                            </p>
                            <p class="text-left wow fadeInDown animated">
                                <b>‘Vari’:</b> Derives from 'Var' the 'regular occurenece' or 'coming &amp; going' of pilgrime to Shirdi;
                            </p>
                            <p class="text-left wow fadeInDown animated">
                                <b>'Kari':</b> is one who does this pilgrimage.
                            </p>
                            <p class="text-left wow fadeInDown animated">
                                Varkari is a person who although living in the midst of his family &amp; carrying on his profession or trade has pledged himself to reach Moksha (Salvation).
                            </p>
                                   
                            <!--<a href="trustee.html" class="hover-btn-new orange"><span>View Commitee Members</span></a>-->
                        </div><!-- end messagebox -->
                    </div><!-- end col -->

                     <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <p class="text-left wow fadeInDown animated mt-4">
                                Every year, Varkari walk hundreds of miles to the holy town of Shirdi, gathering there on GuruPournima (the 9th day) of the Hindu lunar calendar month of Aashaadha (which falls sometime in July). Varkaris worship Shri SaiBaba, the presiding deity of Shirdi, who is identified with Krishna, an avatar (incarnation) of Vishnu. Because of this association with Vishnu, Varkari is a branch of Vaishnavism. The teachers responsible for establishing and supporting the movement through its history include Dnyaneshwar, Namdev, Tukaram, Chokhamela and Eknath, who are accorded the Marathi title of Sant (Saint).Varkari making the pilgrimage to Shirdi carry the palkhis of the saints from Pune.
                            </p> 
                            </div>
                </div><!-- end row -->
                <div class="row align-items-center">
                    <div class="section-title row text-center">
                        <div class="col-md-12">
                            <h3>Movement to Shirdi</h3>
                            <p class="lead">Daily routine
                                The walk to Shirdi is characterized by the activities carried out by the pilgrims. The daily routine of the piligrimage is traditionally prest and followed meticulously. The daily routine of the palkhi is more or less fixed like a timetable. It can be briefly described as follows.
                            </p>
                        </div>
                    </div><!-- end title -->
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">    
                        <div class="message-box">
                            <ul class="float-left p-0">
                                <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;  Daily routine The walk to Shirdi is characterized by the activities carried out by the pilgrims. The daily routine of the piligrimage is traditionally prest and followed meticulously. The daily routine of the palkhi is more or less fixed like a timetable. It can be briefly described as follows.
                                </li>
                                <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;
                                    Early morning there is an elaborate puja (worship event) of the silver paduka(footwear) of Shri Saibaba and a collective prayer .This is followed  by the departure from the place of halt, normally as early as 6.00am. The twin horses12 begins the journey and lead the procession. The trucks carrying the food and belongings of the varkari normally leave ahead of the palkhi in order to prepare for the midday meals.
                                </li>
                                <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;  After the Palkhi is placed in the Rath , the varkaris start singing the bhajans (devotional songs).
                                </li>
                            </ul>
                        </div><!-- end messagebox -->
                    </div><!-- end col -->
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <ul class="float-right p-0">
                            <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp; After every 3 - 4 miles of walk, the procession halts for rest. The main halt during the day occurs at noon. It is normally in any big hall where all the warkari can accommodate. After a collective lunch and a brief rest, the pilgrims are refreshed and begin their onward journey.
                            </li>
                            <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;  The after noon journey is normally arduous due to scorching heat but may sometimes be relived with onset of rains. The collective singing is continued throughout the journey. After one or two intermediate halts, depending on the distance to be covered, the Palkhi reaches the town in the evening where it halts for the night.
                            </li>
                            <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp; The place of halt is normally in any school’s ground or any big hall. The main hall is already set up for the palkhi and the town welcomes it with decoration and banners. 
                            </li>
                        </ul>                   
                    </div><!-- end col -->
                </div><!-- end row -->
                <h3 class="text-center mt-4">
                    <a href="<?php echo base_url(); ?>assurance-to-his-devotee" class="hover-btn-new orange"><span>Shri Sadguru Sai Baba's Assurance to his devotees</span></a>
                </h3>
            </div><!-- end container -->
        </div>
    </div>
</div>
<div class="all-title-box-rules-regulations-banner">
</div>
<div class="page-wrapper">
    <div class="container-fluid mt-4">
        <div id="overviews" class="">
            <div class="container">
                <div class="heading section-header">
                    <h2 class="section-title text-center wow fadeInDown animated font-weight-bold">Guidelines For Padyatri
                    </h2>
                </div>

                <div class="row">

                    <div class="col-sm-12">
                        <!--    <div class="col-sm-8">
                                <div class="col-sm-6"> -->
                        <ul class="float-left font14">
                            <li class="bullet-none"> <small class="font-14-bold">◈</small>&nbsp; Your overall Shri Sai Palkhi experience will be enriched if go through 'Shri Sai Sacharitra' and available 'Shri Sai' literature. </li>
                            <li class="bullet-none"> <small class="font-14-bold">◈</small>&nbsp; Devotees should bring minimum luggage along (e.g. Bed roll, Raincoat, Battery, your meds. Etc.)
                            </li>
                            <li class="bullet-none"> <small class="font-14-bold">◈</small>&nbsp; Once form is filled at Shri Sai Palkhi Bhawan, devotees are free to start the Palkhi from any Palkhi location. </li>
                            <li class="bullet-none"> <small class="font-14-bold">◈</small>&nbsp; At all times Padyatri and volunteers must wear their badge. </li>
                            <li class="bullet-none"> <small class="font-14-bold">◈</small>&nbsp; If you plan to get your vehicle along, please ensure that the vehicle and the driver both are registered with Sai Palkhi Trust </li>
                            <li class="bullet-none"> <small class="font-14-bold">◈</small>&nbsp; Devotees are requested to avoid carrying expensive things and jewellery. If carried, it is your responsibility to take ownership of same.</li>

                            <li class="bullet-none"> <small class="font-14-bold">◈</small>&nbsp; Devotees will need to walk through 29 to 31 KM distance every day during the Palkhi procession. 
                                Hence kindly ensure you plan your participation at your own risk and doctor’s recommendation.
                                Incase of any medical complexities please share the details with accompanied friends/family in advance. </li>
                            <li class="bullet-none"> <small class="font-14-bold">◈</small>&nbsp; Do not pour water on your feet immediately when you halt at stopover,
                                it triggers boils on your feet due to the heat. Let your feet normalize the heat post which you can do so.  </li>
                            <li class="bullet-none"> <small class="font-14-bold">◈</small>&nbsp; We recommend you sit in 'Vajrasana' pose in order to relax quicker and normalise the body. </li>
                            <li class="bullet-none"> <small class="font-14-bold">◈</small>&nbsp; Care should be taken when Palkhi halts at stopover. Devotees must exhibit good behaviour and strictly adhere to all the rules laid 
                                w.r.t. the cleanliness and any addictions you may have. Devotees must not treat Palkhi procession as a trip. </li>

                            <li class="bullet-none"> <small class="font-14-bold">◈</small>&nbsp; Shirdi stay will be organised only for the devotees who walked the Palkhi and not for your relatives and friends if any..</li>
                            <li class="bullet-none"> <small class="font-14-bold">◈</small>&nbsp; Once reached in Shirdi, devotees should inform the organisers in case you plan to stay at your personal location.</li>
                            <li class="bullet-none"> <small class="font-14-bold">◈</small>&nbsp; The cost of return travel will be borne by yourself.
                            </li><li class="bullet-none"> <small class="font-14-bold">◈</small>&nbsp; During the Palkhi procession, organisers are not responsible for any of your personal matters/issues.
                                <!--
                                </div-->
                            </li>
                        </ul>



                    </div>
                </div>

                <h3 class="text-center mt-4">
                    <a href="<?php echo base_url(); ?>assurance-to-his-devotee" class="hover-btn-new orange"><span>Shri Sadguru Sai Baba's Assurance to his devotees</span></a>
                </h3>
            </div><!-- end container -->
        </div>
    </div>
</div>
<div class="page-wrapper">
    <div class="container mt-4">
        <div id="overviews" class="section">

            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-signin flex-row">
                        <div class="card-body">
              <div class="heading section-header">
               <h2 class="section-title text-center wow fadeInDown animated font-weight-bold">Refund and Cancellation Policy</h2>               
            </div>

                            <form class="form-signin">
                                <p>Shee Sai Baba Palakhi Solhala Samiti,Pune ("SSBPSS") takes the utmost care to process donations as per the instructions given by our donors, online and offline. However, in case of an unlikely event of an erroneous donation or if the donor would like to cancel his donation, SSBPSS will respond to the donor within 7 working days of receiving a valid request for refund from the donor. The timely refund of the donation will depend upon the type of credit card/banking instrument used during the transaction. The donor will have to send SSBPSS a written request for a refund within 2 days of making the donation to SSBPSS’s official address or email <a href=""><strong class="text-primary">info@saibabapalkhipune.org</strong></a>  along with-</p>
                                <p> 1. Proof of the deduction of the donation amount.</p>
                                <p>2. In cases where the donation receipt has already been issued to the donor, the donor will have to return the original receipt to us at our office addresses.</p>
                                <p>3. If the tax exemption certificate is already issued, then we are sorry but we will not be able to refund the donation. However, in case of a valid refund request due to any error on SSBPSS’s part, SSBPSS will refund the donation and bear the costs of the same.</p>
                                <p>4. International donations will need minimum 10 days for the refund process.</p>

                                <br>
                                <br>                         
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
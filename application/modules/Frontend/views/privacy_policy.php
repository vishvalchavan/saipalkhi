<div class="page-wrapper">
    <div class="container mt-4">
        <div id="overviews" class="section">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-signin flex-row">
                        <div class="card-body">
                    <div class="heading section-header">
               <h2 class="section-title text-center wow fadeInDown animated font-weight-bold">Privacy Policy</h2>               
            </div>
                            <form class="form-signin">
                                <p>1. Shri Sai Baba Palakhi Sohala Samiti,Pune ("SSBPSS") committed to the ethical collection, retention and use of information that you provide to us about yourself (“Personal Information”) on this site <a href="http://saibabapalkhipune.org"><strong class="text-danger">http://saibabapalkhipune.org</strong></a></p>
                                <p>1. Proof of the deduction of the donation amount.</p>
                                <p>2. Your Personal Information may comprise the following:<br>
                                    a) Your name<br>
                                    b) Your age <br>
                                    c) Your occupation<br>
                                    d) a user ID and password of your choice<br>
                                    e) Your email and mailing address<br>
                                    f) Your telephone number<br>
                                    g) Your payment processing details<br>
                                    h) limited personal details<br>
                                    i) Any other data SSBPSS may require.</p>
                                <p>3.The following Privacy Policy sets forth our understanding with you on the collection, use and protection of your Personal Information. Please read the entire Privacy Policy.</p>
                                <p>4. YOUR USE OF THE WEBSITE CONSTITUTES YOUR CONSENT TO THE ALL THE TERMS AND CONDITIONS CONTAINED IN THIS PRIVACY POLICY (AS AMENDED FROM TIME TO TIME) AND YOU SHALL BE BOUND BY THE SAME.</p>
                                <h5 class="text-success font-weight-bold">5. Collection of Information</h5>
                                <p>5.1: Site Browsing: You browse the Site anonymously. We do not require you to identify yourself or reveal any Personal Information while browsing through the Site. However, you may not be able to access certain sections of the Site or interact with us without supplying us with Personal Information. For instance, you would not be able to transact at the Site or make any donations at the Site, without providing the Site with Personal Information. If you desire to register yourself at the Site, you would be required to provide your Personal Information.</p>
                                <p>5.2: While you are browsing through the Site, the Site’s operating system may automatically record some general information (“General Information”) about your visit such as:</p>

                                <p>(i) the date and time you visit the Site, along with the address of the previous website your were visiting, if you linked to us from another website</p>
                                <p>(ii) the type of browser you are using (such as Internet Explorer version ‘X’)
                                </p>
                                <p>(iii) which ‘hit’ it is on the Site
                                </p>
                                <p>5.3: The General Information is not Personal Information. SSBPSS’s tracking system does not record personal information about individuals or link this information to any Personal Information collected from you.</p>
                                <p>5.4: The General Information is used by SSBPSS for statistical analysis, for tracking overall traffic patterns on the Site and to gauge the public interest in SSBPSS and the Site. Such General Information may be shared by SSBPSS with any person, at SSBPSS’s discretion.</p>
                                <p>5.5: Cookies: “Cookies” are small amounts of data that a website can send to a web browser on a visitor’s computer. The cookie is stored on a visitor’s computer. A cookie may enable the site holder to track how a visitor navigates through its site and the areas in which they show interest. This is similar to a traffic report: it tracks trends and behaviors, but does not identify individuals. Information gathered may include date and time of visits, pages viewed, time spent at the site, and the site visited just before and just after the SSBPSS site.</p>
                                <p>Cookies can be set to expire: on a specified date after a specific period of time when a transaction has been completed or when a user turns off his/her browser. Cookies can be set to expire: on a specified date after a specific period of time when a transaction has been completed or when a user turns off his/her browser. A cookie that is erased from memory when a visitor’s browser closes is called a “session” cookie. Cookies that expire based on a time set by the Web server are called “persistent” cookies. Persistent cookies may be used in some cases, for example, to recognize when a visitor returns to a website. The web browser will send the cookie information from the initial visit back to the website. This is useful to the visitor if he or she has established a password at a particular website and wants to be identified by that site to perform a function or transaction. To browse anonymously, you may set your browser to disable cookies or delete cookies. Please note that disabling cookies for some services will impact ability to use that service.</p>
                                <h5  class="text-success font-weight-bold">Refusing Cookies on SSBPSS’s Website</h5>
                                <p>Customers may choose not to accept cookies. Your web browser may alert and permit you to refuse cookies. When you receive an alert, you may choose at that time to refuse that cookie. If the use of cookies is a concern to you, then please make sure your browser has this capability, and that you set your browser to alert you accordingly. Newer browser versions allow you to be alerted or to automatically refuse cookies. You may need to download a more current version of your web browser from your service provider in order to obtain this option. Note: Some product features and services provided by SSBPSS require you to accept a cookie in order to be able to use the particular functionality or service. These cookies are used to establish a link between the user and the application server. The cookies contain unique session IDs, and no customer data is stored on the cookies.</p>
                                <h5  class="text-success font-weight-bold">Usage of Information</h5>
                                <p>6.1: Personal information will be used by SSBPSS for internal purposes including the following:</p>
                                <p>(i) sending you inter alia emails, features, promotional material, surveys, brochures, catalogues, SSBPSS Annual Report, SSBPSS-in -Action, reminders for donations, regular updates on the utilisation of donations by SSBPSS and other updates.</p>
                                <p>(ii) processing your donations to SSBPSS and purchases of SSBPSS products on the Site.</p>
                                <p>(iii) delivering the SSBPSS products you have purchased on the Site /Receipt for donations made by you to SSBPSS. </p>
                                <p>(iv) maintaining an internal confidential database of all the Personal Information collected from visitors to the Site</p>
                                <p>(v) evaluating and administering the Site and SSBPSS’s activities, responding to any problems that may arise and gauging visitor trends on the Site.</p>
                                <h5  class="text-success font-weight-bold">Disclosure of Personal Information by SSBPSS</h5>
                                <p>7.1: Within SSBPSS, access to Personal Information collected by SSBPSS will be given only to those persons who are authorised by SSBPSS and third parties hired by SSBPSS to perform administrative services. SSBPSS will provide access to third parties for inter alia entering and managing Personal Information in SSBPSS’s Database, processing your orders or donations preparing address labels, sending emails, which require such third parties to have access to your Personal Information. SSBPSS cannot guarantee that such parties will keep your Personal Information confidential and SSBPSS will not be liable in any manner for any loss of confidentiality attributable to such third parties.</p>
                                <p>7.2: SSBPSS may share Personal Information with any of persons who are associated with SSBPSS, including companies and non-governmental organisations affiliated with SSBPSS in any manner. SSBPSS will retain ownership rights over such information and will share only such portions of the Personal Information as it deems fit.</p>
                                <p>7.3: SSBPSS is not liable in any manner whatsoever for the loss, damage(whether direct, indirect, consequential or incidental) or harm caused to you by the misuse of your Personal Information by a third party who is not an employee of SSBPSS.</p>
                                <p>7.4: Notwithstanding anything contained herein or any other contract between you and SSBPSS, SSBPSS reserves the right to disclose any Personal Information about you without notice or consent as needed to satisfy any requirement of law, regulation, legal request or legal investigation, to conduct investigations of breaches of law, to protect the Site, to protect SSBPSS and it’s property, to fufill your requests, to protect our visitors and other persons and if required by the policy of SSBPSS.</p>
                                <h5  class="text-success font-weight-bold">Security</h5>
                                <p>8.1: SSBPSS endevours to use up-to-date security measures to protect your Personal Information.The payment gateway Site is a VERISIGN approved site. 8.2: SSBPSS however does not make any express or implied warranty with respect to the security measures that it may employ from time to time for the protection of the Personal Information.</p>
                                <h5>Links to other Websites</h5>
                                <p>The Site contains links to other websites for the benefit of it’s visitors. This Privacy Policy does not apply to such other websites. SSBPSS is not expressly or impliedly responsible for, or liable to any loss or damage caused to you by the collection, use and retention of Personal Information by such website in any manner whatsoever. It is important that you review the privacy policies of all websites you visit before you disclose any information to such websites</p>
                                <h5  class="text-success font-weight-bold">Variation of the Privacy Policy</h5>
                                <p>SSBPSS shall be absolutely entitled at its sole discretion from time to time add to, alter, delete or modify any of the terms and conditions contained herein. Such changes, additions, alterations, deletions or modifications shall be binding on you once you visit the Site after the Privacy Policy has been so amended.</p>
                                <h5  class="text-success font-weight-bold">SSBPSS Donation Refund Policy</h5>
                                <p>SSBPSS takes the utmost care to process donations as per the instructions given by our donors, online and offline. However, in case of an unlikely event of an erroneous donation or if the donor would like to cancel his donation, SSBPSS will respond to the donor within 7 working days of receiving a valid request for refund from the donor. The timely refund of the donation will depend upon the type of credit card/banking instrument used during the transaction. The donor will have to send SSBPSS a written request for a refund within 2 days of making the donation to SSBPSS’s official address or email info@saibabapalkhipune.org along with-</p>
                                <p>1. Proof of the deduction of the donation amount.</p>
                                <p>2. In cases where the donation receipt has already been issued to the donor, the donor will have to return the original receipt to us at our office addresses.
                                </p>
                                <p>3. If the tax exemption certificate is already issued, then we are sorry but we will not be able to refund the donation. However, in case of a valid refund request due to any error on SSBPSS’s part, SSBPSS will refund the donation and bear the costs of the same.
                                </p>
                                <p>4. International donations will need minimum 10 days for the refund process.
                                </p>
                                <br>
                                <br>                         
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
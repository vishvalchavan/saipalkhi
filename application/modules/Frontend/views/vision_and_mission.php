<div class="all-title-box-upcoming-events-banner">
</div>
<div class="page-wrapper">
    <div class="container-fluid mt-4">
        <div id="overviews" class="section">
            <div class="heading section-header">
                <div class="col-md-12">
                    <h2 class="section-title text-center wow fadeInDown animated font-weight-bold">Mission and Vision</h2>
                    <p class="lead text-center">The Marathi word ‘Palkhi’ is not only been associated with pilgrimages, but to carry people as well. It has always represented a royal mode of transport.
                    </p>
                </div>
            </div>
            <br>
            <br>
            <div class="container">
                <div class="row">
                <!--     <div class="col-sm-6">
                        <div class="post-media wow fadeIn">
                            <div class="col-sm-12 wow fadeInLeft mb-2 animated">
                                <img src="<?php echo base_url(); ?>assets/images/baba1.jpg" alt="" class="img-fluid img-rounded">
                            </div>
                        </div>


                    </div> -->
                    <div class="col-sm-6 col-12">
                        <ul class="pl-0">

                            <div class="media service-box wow fadeInRight mb-4 animated">
                                <li class="bullet-none"> <small class="font-35-bold">&#9672;</small>&nbsp; </li>
                                <div class="media-body mt-2">
                                    <h4 class="media-heading ml-4 font-weight-bold">A platform to meet </h4>
                                    <p class="ml-4">To provide Sai devotees world over, a platform to meet and exchange their experiences and beliefs. </p>
                                </div>

                            </div>

                            <div class="media service-box wow fadeInRight  mb-4 animated">
                                <li class="bullet-none"> <small class="font-35-bold">&#9672;</small>&nbsp; </li>
                                <div class="media-body mt-2">
                                    <h4 class="media-heading ml-4 font-weight-bold">Better service to humanity at large</h4>
                                    <p class="ml-4">To create a network among various Shirdi Sai institutions around the world and to bring them on to a common platform to render better service to humanity at large.</p>
                                </div>
                            </div>
                <div class="media service-box wow fadeInRight  mb-4 animated">
                                <li class="bullet-none"> <small class="font-35-bold">&#9672;</small>&nbsp; </li>
                                <div class="media-body mt-2">
                                    <h4 class="media-heading ml-4 font-weight-bold">Medical check-up camps</h4>
                                    <p class="ml-4">To organize regular medical check-up camps for the benefit of the poor strata of the society with the help of qualified medical specialists.</p>
                                </div>
                            </div>
                  
</div>

                    <div class="col-sm-6 col-12">
                        <ul class="pl-0">
                               <div class="media service-box wow fadeInRight  mb-5 animated">
                                <li class="bullet-none"> <small class="font-35-bold">&#9672;</small>&nbsp; </li>
                                <div class="media-body mt-2">
                                    <h4 class="media-heading ml-4 font-weight-bold">Libraries and reading rooms</h4>
                                    <p class="ml-4">To run and maintain libraries and reading rooms. </p>
                                </div>
                            </div>
</ul>

                            <div class="media service-box wow fadeInRight mb-5 animated">
                                <li class="bullet-none"> <small class="font-35-bold">&#9672;</small>&nbsp; </li>
                                <div class="media-body mt-2">
                                    <h4 class="media-heading ml-4 font-weight-bold">Facilitate the rehabilitation</h4>
                                    <p class="ml-4">To assist areas affected by natural calamities such as cyclone, food, earthquake etc. and to facilitate the rehabilitation process nationally.</p>
                                </div>
                            </div>

                            <div class="media service-box wow fadeInRight  mb-4  animated">
                                <li class="bullet-none"> <small class="font-35-bold">&#9672;</small>&nbsp; </li>
                                <div class="media-body mt-2">
                                    <h4 class="media-heading ml-4 font-weight-bold">Grant-in-aid</h4>
                                    <p class="ml-4">To establish, maintain, run, develop, give grant-in-aid to the schools, colleges, institutions of fine arts, institutions imparting technical, vocational and industrial education,poor houses, orphanages, old age homes and other like institutions with public service as the motive.</p>
                                </div>
                            </div>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
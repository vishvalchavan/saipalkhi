<div class="all-title-box-history">
</div>
<div class="page-wrapper">
    <div class="container-fluid mt-4">
        <div id="overviews" class="">
            <div class="container">
                <div class="heading section-header">
                    <h2 class="section-title text-center wow fadeInDown animated font-weight-bold">Newly planned Palkhi Bhavan - Shirdi</h2>
                    <p class="lead">
                        Sai Palkhi Samiti Trust has been group on successfully for all these years due to Donors, well wishers and inspires. With this inspiration we progress further taking all in good faith. With the same faith, effort and donations it is planned to build a “Sai Palkhi Bhavan at Shirdi” and for this, we have managed to purchase land measuring 9924 sq.ft in Shirdi. </p>
                </div>
                <div class="row align-items-center">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="message-box">
                            <h2>Appeals for Vastunidhi</h2>
                            <p>As we know Shirdi has become one of India’s most important pilgrimage town. 60 to 70,000 Saibhakts visits the town daily from across the world and this number go to 1 lakh during festival time. Many saibhakt from Pune goes to shirdi on weekly/ monthly to take the blessings from Shri Sai.</p>
                            <p>For many years, Sai palkhi samiti trust and many Saibhakt from Pune and around places has wished that, we should have our own place in holy land of Shirdi where all devotees can stay.</p>
                            <p>To accomplish that dream, with the help of Donors, well-wishers, Trust has recently purchased land measuring 9924 sq.ft in Shirdi.Newly built complex will be known as “Sai Palkhi Bhavan – Shirdi” which will be shelter for anyone, it will become a place for devotional gathering, mediation, Faith & unity with the Divine.</p>
                            <p>Sai palkhi samiti, appeals Sai devotees for contribution and involvement in this project so that trust can start construction work. Every single rupee that is donated is extremely important to the Samiti for its sustenance. </p>

                            <!--<a href="trustee.html" class="hover-btn-new orange"><span>View Commitee Members</span></a>-->
                        </div><!-- end messagebox -->
                    </div><!-- end col -->
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="post-media wow fadeIn">
                            <img src="<?php echo base_url(); ?>assets/images/shirdibhavan.jpg" alt="" class="img-fluid img-rounded">
                        </div><!-- end media -->
                    </div><!-- end col -->
                </div>
                <div class="row align-items-center">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="post-media wow fadeIn">
                            <img src="<?php echo base_url(); ?>assets/images/shirdibhavan1.jpg" alt="" class="img-fluid img-rounded">
                        </div><!-- end media -->
                    </div><!-- end col -->
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="message-box">
                            <p>So Far Samiti have seen overwhelming fund contribution by numerous donors and the same is expected in coming year to make our dream come true.</p>                  
                            <p>These donations are accepted in the form of cash (Indian as well as foreign currencies), Net Banking Services, all debit/credit cards through payment gateway, Crossed and A/c Payee Cheques or Demand Drafts.</p>                  
                            <p>We are vey happy to mention that, All Donation Funds are eligible for Income Tax deduction under section 80G (Govt. Of India)</p>                  

                            <!--<a href="trustee.html" class="hover-btn-new orange"><span>View Commitee Members</span></a>-->
                        </div><!-- end messagebox -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->

        </div>
    </div>

</div>
<section id="cta2">
    <div class="container">
        <div class="text-center section-header">

            <div class="row">

                <div class="col-md-12">
                    <h2 class="text-white section-title text-center wow fadeInDown animated font-weight-bold">Appeal for Vastunidhi</h2>
                    <p class="wow fadeInUp animated" data-wow-duration="300ms" data-wow-delay="100ms">Sai Palkhi Samiti Trust has been group on successfully for all these years due to Donors, well-wishers and inspires. With the same donations, and faithtrust has planned to build a “Sai Palkhi Bhavan at Shirdi” and for this, we have managed to purchase land measuring 9924 sq.ft in Shirdi.</p>
                    <p class="wow fadeInUp animated" data-wow-duration="300ms" data-wow-delay="200ms">Sai palkhi samiti, appeals Sai devotees for contribution and involvement in this project so that trust can start construction work. Every single rupee that is donated is extremely important to the Samiti to complete this project.              
                    </p>
                    <a class="btn btn-primary btn-lg" href="<?php echo base_url(); ?>donation">Donate Now</a>
                </div>

            </div>

<!--             <img class="img-responsive wow fadeIn animated" src="http://localhost/saipalkhi/assets/images/cta2-img.png" alt="" data-wow-duration="300ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 300ms; animation-delay: 300ms; animation-name: fadeIn;"> -->
        <!--     <img class="img-responsive wow fadeIn animated" src="" alt="" data-wow-duration="300ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 300ms; animation-delay: 300ms; animation-name: fadeIn;"> -->
        </div>
    </div>
</section>
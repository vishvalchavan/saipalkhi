<div class="all-title-box-saipalkhi-banner">
</div>
<section id="features">
   <div class="page-wrapper">
      <div id="overviews" class="">
         <div class="container-fluid mt-4">
            <div class="container">
               <div class="section-header mt-5">
                  <h2 class="section-title text-center wow fadeInDown animated font-weight-bold">Shri Sadguru Sai Baba's Assurance to his devotees</h2>
                 </div>
               <div class="row mt-10">
                  <div class="col-sm-8">
                     <ul class="p-0">
                        <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;No harm shall befall him, who steps on the soil of Shirdi. </li>
                        <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;He who cometh to my Samadhi, his sorrow and suffering shall cease.</li>
                        <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;Though I be no more in flesh and blood, I shall ever protect my devotees. </li>
                        <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;Trust in me and your prayer shall be answered.</li>
                        <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;Know that my spirit is immortal, know this for yourself.</li>
                        <li class="bullet-none" > <small class="font-14-bold">&#9672;</small>&nbsp;Show unto me he who has sought refuge and has been turned away.</li>
                        <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;In whatever faith men worship me, even so do I render to them.</li>
                        <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;Not in vain is my promise that I shall ever lighten your burden.</li>
                        <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;Knock, and the door shall open, ask and it shall be granted.</li>
                        <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;To him who surrenders unto me totally I shall be ever indebted.</li>
                        <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp; Blessed is he who has become one with me.</li>
                     </ul>
                  </div>
                  <div class="col-sm-4 wow fadeInLeft animated">
                     <img class="img-responsive img-fluid img-rounded" src="<?php echo base_url(); ?>assets/images/main-feature.png" alt="">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
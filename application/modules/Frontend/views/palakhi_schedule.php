<div class="page-wrapper">
    <div class="container-fluid mt-4">
        <div id="overviews" class="section">
            <div class="container">
                <div class="section-header">
                    <h2 class="section-title text-center wow fadeInDown animated font-weight-bold">Palakhi <?php echo date("Y"); ?> Schedule</h2>
                    <p>Shri Sai Baba taught a moral code of love, forgiveness, helping others, charity, contentment, inner peace, devotion to God and guru. His philosophy was Advaita Vedanta and his teachings consisted of elements both of this school as well as of bhakti and Islam.</p>
                    <p>
                        Shri Sai Baba remains a very popular saint and is worshipped by Indians all across the World.
                    </p>
                    <h2 class="text-center wow fadeInDown animated font-weight-bold">Teaching of Shri Sai Baba</h2>
                    <p>Sai Baba encouraged his devotees to pray, chant God's name and read holy scriptures - he told Muslims to study the Qur'an and Hindus texts like the Ramayana, Vishnu Sahasranam, Bhagavad Gita (and commentaries to it), Yoga Vasistha. He advised his devotees and followers to lead a moral life, help others, treat them with love and develop two important features of character: faith (Shraddha) and patience (Saburi). He also criticized atheism. In his teachings Sai Baba emphasised the importance of performing one's duties without attachment to earthly matters and being ever content regardless of the situation.
                    </p>
                    <p>
                        Sai encouraged charity and the importance of sharing with others. He said: "Unless there is some relationship or connection, nobody goes anywhere. If any men or creatures come to you, do not discourteously drive them away, but receive them well and treat them with due respect. Shri Hari (God) will be certainly pleased if you give water to the thirsty, bread to the hungry, clothes to the naked and your verandah to strangers for sitting and resting. If anybody wants any money from you and you are not inclined to give, do not give, but do not bark at him like a dog." Other favourite sayings of his were: "Why do you fear when I am here", "He has no beginning... He has no end." Sai Baba made eleven assurances to his devotees:
                    </p>
                    <ul>
                        <li>Whosoever puts their feet on Shirdi soil, their sufferings will come to an end. </li>
                        <li>The wretched and miserable will rise to joy and happiness as soon as they
                            climb the steps of My Samadhi. 
                        </li>
                        <li>I shall be ever active and vigorous even after leaving this earthly body. </li>
                        <li>My tomb shall bless and speak to the needs of my devotees. </li>
                        <li>I shall be active and vigorous even from my tomb. </li>
                        <li>My mortal remains will speak from My tomb. </li>
                        <li>I am ever living to help and guide all who come to Me, who surrender to Me        and who seek refuge in Me. </li>
                        <li>If you look at Me, I look at you. </li>
                        <li>If you cast your burden on Me, I shall surely bear it. </li>
                        <li>If you seek My advice and help, it shall be given to you at once. </li>
                        <li>There shall be no want in the house of My devotee.</li>
                    </ul>
                    <p>
                        The devotees of Shirdi Sai Baba have spread all over India. In India there is at least one Shri Sai Baba mandir in nearly every Indian city. His image is quite popular in India. Some ordinary non-religious publishing houses (such as Sterling Publishers) publish books about Shirdi Sai written by his devotees. Shirdi is among the major Hindu places of pilgrimage. 
                    </p>
                    <p >
                        Beyond India the Shri Sai movement has spread to other countries such as the U.S. or the Caribbean. Sai Baba mandirs and organisations of his devotees have been built in countries including Australia, Malaysia, Singapore and the USA. The Shri Sai Baba movement is one of the main Hindu religious movements in English speaking countries. According to estimates the Sai mandir in Shirdi is visited by around twenty thousand pilgrims a day and during religious festivals this number amounts to a hundred thousand.
                    </p>
                    <h3 class="text-center"><a href="assurance-to-his-devotees.html" class="btn btn-primary btn-lg">Shri Sadguru Sai Baba's Assurance to his devotees</a></h3>
                </div>
            </div>
        </div>
    </div>
</div>
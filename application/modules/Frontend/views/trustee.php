<div class="all-title-box-saipalkhi-banner">
</div>
<div class="page-wrapper">
    <div class="container mt-4">
        <div id="overviews" class="">
            <div class="heading section-header">
                <h2 class="section-title text-center wow fadeInDown animated font-weight-bold">Commitee Member List</h2>
            </div>
            <div class="row mt-5">
                <div class="col-lg-6">
                    <div class="media service-box wow fadeInRight mb-2 animated">
                          <h2 class="ml-4 mt-10"><small class="font-14-bold">&#9672;</small> &nbsp;Shri Sham Haribhau Veer </h2>
                  </div>
                    <div class="media service-box wow fadeInRight mb-2 animated">
                   
                     
                            <h2 class="ml-4 mt-10"><small class="font-14-bold">&#9672;</small>&nbsp;Shri Hariprasad Dagadu Pagde </h2>
                        </div>
                    
                    <div class="media service-box wow fadeInRight mb-2 animated">
                   
                     
                            <h2 class="ml-4 mt-10"><small class="font-14-bold">&#9672;</small>&nbsp;
                                Shri Mahavir Padmakar Kshirsagar
                            </h2>
                        </div>
             
                    <div class="media service-box wow fadeInRight mb-2   animated">
                   
                     
                            <h2 class="ml-4 mt-10"> <small class="font-14-bold">&#9672;</small>&nbsp;  Shri Vijay Dattatray Methe
                            </h2>
                     
                    </div>
                    <div class="media service-box wow fadeInRight mb-2   animated">
                   
                     
                            <h2 class="ml-4 mt-10"><small class="font-14-bold">&#9672;</small>&nbsp;Shri Mandar Prabhakar Shahane
                            </h2>
                    
                    </div>
                    <div class="media service-box wow fadeInRight mb-2  animated">
                   
                     
                            <h2 class="ml-4 mt-10"><small class="font-14-bold">&#9672;</small>&nbsp;Shri Gokul Dagadoba Rahurkar
                            </h2>
                      
                    </div>
                    <div class="media service-box wow fadeInRight mb-2  animated">
                   
                     
                            <h2 class="ml-4 mt-10"><small class="font-14-bold">&#9672;</small>&nbsp;  Shri Kiran Arun Kadam
                            </h2>
                        </div>
                
                    <div class="media service-box wow fadeInRight mb-2 animated">
                   
                     
                            <h2 class="ml-4 mt-10"><small class="font-14-bold">&#9672;</small>&nbsp; Shri Ramesh Maruti Bhosale
                            </h2>
                        </div>
            
                    <div class="media service-box wow fadeInRight mb-2 animated">
                   
                     
                            <h2 class="ml-4 mt-10"><small class="font-14-bold">&#9672;</small>&nbsp;Shri Sanjay Narhar Kharote
                            </h2>
                        </div>
                    <div class="media service-box wow fadeInRight mb-2 animated">
                   
                     
                            <h2 class="ml-4 mt-10"><small class="font-14-bold">&#9672;</small>&nbsp; Shri Devidas Kisanrao Phadatare
                            </h2>
                        </div>
                    </div>
             
                <div class="col-lg-6 pt-4">
                    <img src="<?php echo base_url(); ?>assets/images/about-2.png" class="img-responsive img-fluid img-rounded float-right">
                </div>
            </div>
        </div>
    </div>
</div>
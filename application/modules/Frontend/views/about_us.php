<div class="all-title-box">
    <div class="container text-center">        
    </div>
</div>
<div id="overviews" class="section">
    <div class="container">
        <div class="section-title row text-center">
            <div class="col-md-12">
                <div class="heading section-header">
               <h2 class="section-title text-center wow fadeInDown animated font-weight-bold">About Us</h2>
                <p class="lead">With the blessing of Shri Sainath maharaj & with immense support from sai devotees the “Shri Saibaba Palkhi Sohla” is moving towards 30 year of completion. It started in the year 1989 with mare devotees going to Shirdi on foot & now this process has taken a form of cultural festival Maharashtra. This is our small effort to present history of this palkhi festival.</p>
            </div>
        </div>
        </div><!-- end title -->
        <div class="row align-items-center">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                <div class="message-box">
                    <h2>Sai Aarti</h2>
                    <p>Pilgrimage is a significant ritual in almost all civilizations. It has a common underlying foundation of attaining a higher spiritual level through the act of journey to holy sites. Maharashtra has got a very beautiful history of pilgrimage festival. The Alandi to Pandharpur pilgrimage, colloquially known as the “Palkhi”.</p>

                    <p>It is one of the most important cultural event in the state of Maharashtra. The Alandi - Pandharpur palkhi sohla is a great festival for devotees. Chanting “Gyanba Tukaram” the devotees march from Alandi to Pandharpur via pune. Some of people from Pune city got inspiration from palkhi and they decided to start a palkhi from pune to Shirdi. </p>

                    <!--<a href="<?php echo base_url(); ?>trustee" class="hover-btn-new orange"><span>View Commitee Members</span></a>-->
                </div><!-- end messagebox -->
            </div><!-- end col -->
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 mt-7">
                <div class="post-media wow fadeIn">
                    <div class="embed-responsive embed-responsive-16by9"><iframe allowfullscreen="" frameborder="0" height="480" src="https://www.youtube.com/embed/6pXiwqk0rBw?rel=0" width="853"></iframe></div>                    </div><!-- end media -->
            </div><!-- end col -->
        </div>
        <div class="row align-items-center">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                <div class="post-media wow fadeIn">
                    <img src="<?php echo base_url(); ?>assets/images/about-1.png" alt="" class="img-fluid img-rounded">
                </div><!-- end media -->
            </div><!-- end col -->
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                <div class="message-box m-1">
                    <h2>Shirdi Maze Pandharpur | Sai baba Ramavatar</h2>
                    <p>Shri Sainath Maharaj is Guru of whole universe. Guru is the God, say the scriptures. Guru Purnima (“Purnima means full- moon”) is the one which disciples and devotees honor and felicitates their guru and seeks his or her special blessing. Hence some of sai devotees from pune decided to start palkhi sohla on a day of Guru Purnima. Pune via Nagar to Shirdi was the decided route for palkhi. On the first year of palkhi that is on 1989, only 5 devotees from pune come forward to take initiative.</p>

                    <p>They started their journey (Palkhi) to Shirdi with shri sainath’s photo and paduka (footwear) in hand and continue chanting of “shri sai nath”. These 5 people started from Pune towards Shirdi via Nagar. Sai devotees from Pune gave a Good response on the first year of palkhi as well. Just because of faith in shri sainath, the thing which was dream, come into reality.</p>

                    <!--<a href="<?php echo base_url(); ?>trustee" class="hover-btn-new orange"><span>View Commitee Members</span></a>-->
                </div><!-- end messagebox -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</div>
<!-- <section class="about-activities">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <p class="main_title"><span>Our Activities</span></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="box">
                    <div class="box-icon"> <span class="fa fa-2x fa-music"></span> </div>
                    <div class="info">
                        <h4 class="text-center">Activity 1</h4>
                        <p>The present day of Palkhi of Shri Saibaba is quite elaborate: The pole is covered with silver plating, the seat is inlaid with precious metals and the framework is finely carved. </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box">
                    <div class="box-icon"> <span class="fa fa-2x fa-magic"></span> </div>
                    <div class="info">
                        <h4 class="text-center">Activity 2</h4>
                        <p>The present day of Palkhi of Shri Saibaba is quite elaborate: The pole is covered with silver plating, the seat is inlaid with precious metals and the framework is finely carved. </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box">
                    <div class="box-icon"> <span class="fa fa-2x fa-book"></span> </div>
                    <div class="info">
                        <h4 class="text-center">Activity 3</h4>
                        <p>The present day of Palkhi of Shri Saibaba is quite elaborate: The pole is covered with silver plating, the seat is inlaid with precious metals and the framework is finely carved. </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="box">
                    <div class="box-icon"> <span class="fa fa-2x fa-music"></span> </div>
                    <div class="info">
                        <h4 class="text-center">Activity 4</h4>
                        <p>The present day of Palkhi of Shri Saibaba is quite elaborate: The pole is covered with silver plating, the seat is inlaid with precious metals and the framework is finely carved. </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box">
                    <div class="box-icon"> <span class="fa fa-2x fa-magic"></span> </div>
                    <div class="info">
                        <h4 class="text-center">Activity 5</h4>
                        <p>The present day of Palkhi of Shri Saibaba is quite elaborate: The pole is covered with silver plating, the seat is inlaid with precious metals and the framework is finely carved. </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box">
                    <div class="box-icon"> <span class="fa fa-2x fa-book"></span> </div>
                    <div class="info">
                        <h4 class="text-center">Activity 6</h4>
                        <p>The present day of Palkhi of Shri Saibaba is quite elaborate: The pole is covered with silver plating, the seat is inlaid with precious metals and the framework is finely carved. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->

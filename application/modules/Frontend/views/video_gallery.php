<div class="page-wrapper">
    <div class="container-fluid mt-4">
        <div id="overviews" class="">
            <div class="heading section-header mt-5">
                <h2 class="section-title text-center wow fadeInDown animated font-weight-bold">Video Gallery</h2>
            </div>
        <!--<section id="features">-->
        <div id="svList">
            <div class="row mb-4 ">
                <div class="col-sm-3 col-md-3 col-lg-3 mb-2">
                  
                    <div class="svThumb ytVideo" data-videoID="XCR8drZpRsE" frameborder="0" allowfullscreen">
</div>
                </div>
                <div class="col-sm-3 col-md-3 col-lg-3 mb-2">
                    <div class="svThumb ytVideo" data-videoID="XCR8drZpRsE" frameborder="0" allowfullscreen">Video Gallery</div>
                </div>
                <div class="col-sm-3 col-md-3 col-lg-3 mb-2">
                    <div class="svThumb ytVideo" data-videoID="zAZIcVzl9fo" frameborder="0" allowfullscreen">Video Gallery</div>
                </div>
                <div class="col-sm-3 col-md-3 col-lg-3 mb-2">
                    <div class="svThumb ytVideo" data-videoID="TlgN0ghYAwk"  frameborder="0" allowfullscreen">Video Gallery</div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-sm-3 col-md-3 col-lg-3 mb-2">
                    <div class="svThumb ytVideo" data-videoID="-dCfVI7vn_E" frameborder="0" allowfullscreen">Video Gallery</div>
                </div>
                <div class="col-sm-3 col-md-3 col-lg-3 mb-2">
                    <div class="svThumb ytVideo" data-videoID="xBDDoXiM53Q" frameborder="0" allowfullscreen">Video Gallery</div>
                </div>
                <div class="col-sm-3 col-md-3 col-lg-3 mb-2">
                    <div class="svThumb ytVideo" data-videoID="BiHtRQNnXV8" frameborder="0" allowfullscreen">Video Gallery</div>
                </div>
                <div class="col-sm-3 col-md-3 col-lg-3 mb-2">
                    <div class="svThumb ytVideo" data-videoID="ouwKwJj2wOU" frameborder="0" allowfullscreen">Video Gallery</div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-sm-3 col-md-3 col-lg-3 mb-2">
                    <div class="svThumb ytVideo" data-videoID="ySp7HHMzWtg" frameborder="0" allowfullscreen">Video Gallery</div>
                </div>
                <div class="col-sm-3 col-md-3 col-lg-3 mb-2">
                    <div class="svThumb ytVideo" data-videoID="tsgpXDn9Gjw" frameborder="0" allowfullscreen">Video Gallery</div>
                </div>
                <div class="col-sm-3 col-md-3 col-lg-3 mb-2">
                    <div class="svThumb ytVideo" data-videoID="Uq94-w0avCg" frameborder="0" allowfullscreen">Video Gallery</div>
                </div> 
                <div class="col-sm-3 col-md-3 col-lg-3 mb-2">
                    <div class="svThumb ytVideo" data-videoID="ABA9T4nB8RY" frameborder="0" allowfullscreen">Video Gallery</div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-sm-3 col-md-3 col-lg-3 mb-2">
                    <div class="svThumb ytVideo" data-videoID="JXBkzLPOReo" frameborder="0" allowfullscreen">Video Gallery</div>
                </div>
                <div class="col-sm-3 col-md-3 col-lg-3 mb-2">
                    <div class="svThumb ytVideo" data-videoID="OkT1GSAPLJ0" frameborder="0" allowfullscreen">Video Gallery</div>
                </div>
            </div>
        </div>
        <!--</div>-->
        <!--  <div class="container-fluid" id="svList">
                  <div class="row mb-2" >
                      <div class="col-sm-4 svThumb ytVideo">
                          <iframe  width="400" height="250" data-videoID="XCR8drZpRsE" frameborder="0" allowfullscreen></iframe>
                      </div>
                      <div class="col-sm-4 svThumb ytVideo">
                          <iframe id="video" width="400" height="250" data-videoID="https://www.youtube.com/embed/XCR8drZpRsE" frameborder="0" allowfullscreen
                                  ></iframe>
                      </div>
                      <div class="col-sm-4 svThumb ytVideo">
                          <iframe id="video" width="400" height="250" data-videoID="TlgN0ghYAwk" frameborder="0" allowfullscreen></iframe>
                      </div>
                  </div>
                  <div class="row mb-2">
                      <div class="col-sm-4 svThumb ytVideo">
                          <iframe id="video" width="400" height="250" data-videoID="-dCfVI7vn_E" frameborder="0" allowfullscreen></iframe>
                      </div>
                      <div class="col-sm-4 svThumb ytVideo">
                          <iframe id="video" width="400" height="250" data-videoID="xBDDoXiM53Q" frameborder="0" allowfullscreen></iframe>
                      </div>
                      <div class="col-sm-4 svThumb ytVideo">
                          <iframe id="video" width="400" height="250" data-videoID="BiHtRQNnXV8" frameborder="0" allowfullscreen></iframe>
                      </div>
                  </div>
                  <div class="row mb-2">
                      <div class="col-sm-4 svThumb ytVideo">
                          <iframe id="video" width="400" height="250" data-videoID="ouwKwJj2wOU" frameborder="0" allowfullscreen></iframe>
                      </div>
                      <div class="col-sm-4 svThumb ytVideo">
                          <iframe id="video" width="400" height="250" data-videoID="ySp7HHMzWtg" frameborder="0" allowfullscreen></iframe>
                      </div>
                      <div class="col-sm-4 svThumb ytVideo">
                          <iframe id="video" width="400" height="250" data-videoID="tsgpXDn9Gjw" frameborder="0" allowfullscreen></iframe>
                      </div>
                  </div>
                  <div class="row mb-2">
                      <div class="col-sm-4 svThumb ytVideo">
                          <iframe id="video" width="400" height="250" data-videoID="Uq94-w0avCg" frameborder="0" allowfullscreen></iframe>
                      </div>
                      <div class="col-sm-4 svThumb ytVideo">
                          <iframe id="video" width="400" height="250" data-videoID="ABA9T4nB8RY" frameborder="0" allowfullscreen></iframe>
                      </div>
                      <div class="col-sm-4 svThumb ytVideo">
                          <iframe id="video" width="400" height="250" data-videoID="JXBkzLPOReo" frameborder="0" allowfullscreen></iframe>
                      </div>
                  </div>
                  <div class="row mb-2">
                      <div class="col-sm-4 svThumb ytVideo">
                          <iframe id="video" width="400" height="250" data-videoID="OkT1GSAPLJ0" frameborder="0" allowfullscreen></iframe>
                      </div>
                  </div>
              </div> -->
        <!--</section>-->
    </div>
    </div>
</div>
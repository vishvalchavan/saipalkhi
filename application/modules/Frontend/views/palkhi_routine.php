<div class="all-title-box-saipalkhi-banner">
</div>
<div class="page-wrapper">
    <div class="container-fluid mt-4">
        <div id="overviews" class="section">
            <div class="container">
                <div class="section-title row text-center">
                          <div class="heading section-header">
                         <h2 class="section-title text-center wow fadeInDown animated font-weight-bold">Daily Routine in Palkhi</h2>
                        <p class="lead">Shri Sai Baba taught a moral code of love, forgiveness, helping others, charity, contentment, inner peace, devotion to God and guru. His philosophy was Advaita Vedanta and his teachings consisted of elements both of this school as well as of bhakti and Islam. Shri Sai Baba remains a very popular saint and is worshipped by Indians all across the World. </p>
                    </div>
                </div><!-- end title -->

                <div class="row align-items-center">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="message-box">
                            <h2>Teaching of Shri Sai Baba</h2>
                            <p>Sai Baba encouraged his devotees to pray, chant God's name and read holy scriptures - he told Muslims to study the Qur'an and Hindus texts like the Ramayana, Vishnu Sahasranam, Bhagavad Gita (and commentaries to it), Yoga Vasistha. He advised his devotees and followers to lead a moral life, help others, treat them with love and develop two important features of character: faith (Shraddha) and patience (Saburi). He also criticized atheism. In his teachings Sai Baba emphasised the importance of performing one's duties without attachment to earthly matters and being ever content regardless of the situation. </p>

                            <!--<a href="trustee.html" class="hover-btn-new orange"><span>View Commitee Members</span></a>-->
                        </div><!-- end messagebox -->
                    </div><!-- end col -->

                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="post-media wow fadeIn">
                            <img src="<?php echo base_url(); ?>assets/images/pandharpur.png" alt="" class="img-fluid img-rounded">
                        </div><!-- end media -->
                    </div><!-- end col -->
                </div>
                <div class="row align-items-center">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="post-media wow fadeIn">
                            <img src="<?php echo base_url(); ?>assets/images/whypalkhi2.png" alt="" class="img-fluid img-rounded">
                        </div><!-- end media -->
                    </div><!-- end col -->

                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="message-box">
                            <h2>Shirdi Maze Pandharpur | Sai baba Ramavatar</h2>
                            <p>Sai encouraged charity and the importance of sharing with others. He said: "Unless there is some relationship or connection, nobody goes anywhere. If any men or creatures come to you, do not discourteously drive them away, but receive them well and treat them with due respect. Shri Hari (God) will be certainly pleased if you give water to the thirsty, bread to the hungry, clothes to the naked and your verandah to strangers for sitting and resting. If anybody wants any money from you and you are not inclined to give, do not give, but do not bark at him like a dog." Other favourite sayings of his were: "Why do you fear when I am here", "He has no beginning... He has no end." Sai Baba made eleven assurances to his devotees: </p>                  

                            <!--<a href="trustee.html" class="hover-btn-new orange"><span>View Commitee Members</span></a>-->
                        </div><!-- end messagebox -->
                    </div><!-- end col -->

                </div><!-- end row -->

                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <ul class="float-left font14">
                                <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;Whosoever puts their feet on Shirdi soil, their sufferings will come to an end. </li>
                                <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;The wretched and miserable will rise to joy and happiness as soon as they
                                    climb the steps of My Samadhi. 
                                </li>
                                <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;I shall be ever active and vigorous even after leaving this earthly body. </li>
                                <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;My tomb shall bless and speak to the needs of my devotees. </li>
                                <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;I shall be active and vigorous even from my tomb. </li>
                                <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;My mortal remains will speak from My tomb. </li>
                            </ul>

                        </div>
                        <div class="col-sm-6 float-right">

                            <ul class="">

                                <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;I am ever living to help and guide all who come to Me, who surrender to Me        and who seek refuge in Me. </li>
                                <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;If you look at Me, I look at you. </li>
                                <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;If you cast your burden on Me, I shall surely bear it. </li>
                                <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;If you seek My advice and help, it shall be given to you at once. </li>
                                <li class="bullet-none"> <small class="font-14-bold">&#9672;</small>&nbsp;There shall be no want in the house of My devotee.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <p class="lead">
                    The devotees of Shirdi Sai Baba have spread all over India. In India there is at least one Shri Sai Baba mandir in nearly every Indian city. His image is quite popular in India. Some ordinary non-religious publishing houses (such as Sterling Publishers) publish books about Shirdi Sai written by his devotees. Shirdi is among the major Hindu places of pilgrimage. 
                </p>
                <p class="lead">
                    Beyond India the Shri Sai movement has spread to other countries such as the U.S. or the Caribbean. Sai Baba mandirs and organisations of his devotees have been built in countries including Australia, Malaysia, Singapore and the USA. The Shri Sai Baba movement is one of the main Hindu religious movements in English speaking countries. According to estimates the Sai mandir in Shirdi is visited by around twenty thousand pilgrims a day and during religious festivals this number amounts to a hundred thousand.
                </p>
                <h3 class="text-center mt-4">
                    <a href="<?php echo base_url(); ?>assurance-to-his-devotee" class="hover-btn-new orange"><span>Shri Sadguru Sai Baba's Assurance to his devotees</span></a>
                </h3>
            </div><!-- end container -->
        </div>
    </div>
</div>
<div class="all-title-box-contact">
</div>

<div id="contact" class="section wb">
    <div class="container">
        <div class="section-title text-center">
            <div class="heading section-header">

                <h2 class="section-title text-center wow fadeInDown animated font-weight-bold">Need Help? Sure we are Online!</h2>
            </div><!-- end title -->
            <?php if ($this->session->flashdata('error_msg1')) { ?>
                <br>
                <div class="error-msg col-md-10 font-16">
                    <?php echo $this->session->flashdata('error_msg1'); ?>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('success_msg1')) { ?>
                <br>
                <div class="success-msg col-md-10 font-16">
                    <?php echo $this->session->flashdata('success_msg1'); ?>
                </div>
            <?php } ?>
        </div>
        <div class="row">
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="contact_form">
                    <div id="message"></div>
                    <form id="contactform" class="" action="<?php echo base_url() . 'contact' ?>" name="contactform" method="post">
                        <div class="row row-fluid">
                            <div class="col-lg-6 col-md-6 col-sm-6 mb-2">
                                <input type="text" name="name" id="name" class="form-control" placeholder="Enter your name" data-error=".error1">
                                <div class="error1 error-msg"></div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 mb-2">
                                <input type="email" name="email" id="email" class="form-control" placeholder="Enter your enail" data-error=".error2">
                                <div class="error2 error-msg"></div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 mb-2">
                                <input type="text" name="subject" id="subject" class="form-control" placeholder="Enter Subject here" data-error=".error3">
                                <div class="error3 error-msg"></div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 mb-2">
                                <textarea class="form-control" name="comments" id="comments" rows="3" placeholder="Enter message here" data-error=".error4"></textarea>
                                <div class="error4 error-msg"></div>
                            </div>
                            <div class="text-center pd">
                                <button type="submit" value="SEND" id="submit" class="btn btn-light btn-radius btn-brd grd1 btn-block">Send Message</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- end col -->
            <div class="col-xl-6 col-md-12 col-sm-12">
              <div class="mapouter"><div class="gmap_canvas"><iframe width="600" height="425" id="gmap_canvas" src="https://maps.google.com/maps?q=Shri%20Saibaba%20Palakhi%20Bhavan%2C%2025%2CKasaba%20Peth%2CGangotri%20Building%20Fadake%20Houda%20Chowk%2CPune-411011&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div><style>.mapouter{position:relative;text-align:right;height:500px;width:600px;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:600px;}</style></div>
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
</div><!-- end section -->
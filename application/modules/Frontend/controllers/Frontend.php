<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends MY_Controller {

    public function __construct() {
        parent::__construct();
        if ($this->ion_auth->is_admin()) {
            $this->ion_auth->logout();
            $this->session->unset_userdata('user_id');
        }
        $this->load->database();
        $this->load->library(array('form_validation', 'session'));
        $this->load->helper(array('url', 'language'));
    }

    public function about_us() {
        $data['dataHeader']['title'] = 'About US';
        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'about_us', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }

    public function devotee_assurance() {
        $data['dataHeader']['title'] = 'Assurance to his devotees';
        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'devotee_assurance', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }

    public function contact_us() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('subject', 'Subject', 'trim|required');
            $this->form_validation->set_rules('comments', 'Message', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $name = $this->input->post('name');
                $email = $this->input->post('email');
                $subject = $this->input->post('subject');
                $message = $this->input->post('comments');
                $staus = send_mail($name, $email, $subject, $message);
                if ($staus == 'success') {
                    $this->session->set_flashdata('success_msg1', 'Thank you for contact us. As early as possible  we will contact you.');
                    redirect('contact');
                } else {
                    $this->session->set_flashdata('error_msg1', 'Failed to send message.');
                    redirect('contact');
                }
            } else {
                $data['dataHeader']['title'] = 'Contact';
                $this->template->set_master_template('frontend_template.php');
                $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
                $this->template->write_view('content', 'contact_us', (isset($this->data) ? $this->data : NULL), TRUE);
                $this->template->write_view('footer', 'frontend/footer', '', TRUE);
                $this->template->render();
            }
        } else {
            $data['dataHeader']['title'] = 'Contact';
            $this->template->set_master_template('frontend_template.php');
            $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
            $this->template->write_view('content', 'contact_us', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'frontend/footer', '', TRUE);
            $this->template->render();
        }
    }

    public function palkhi_routine() {
        $data['dataHeader']['title'] = 'Daily Routine in Palakhi';
        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'palkhi_routine', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }

    public function palakhi_history() {
        $data['dataHeader']['title'] = 'History Of Palkhi';
        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'palakhi_history', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }

    public function palakhi_schedule() {
        $data['dataHeader']['title'] = 'Palakhi Schedule';
        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'palakhi_schedule', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }

    public function privacy_policy() {
        $data['dataHeader']['title'] = 'Privacy Policy';
        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'privacy_policy', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }

    public function refund_policy() {
        $data['dataHeader']['title'] = 'Refund Policy';
        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'refund_policy', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }

    public function rules_regulations() {
        $data['dataHeader']['title'] = 'Rules & Regulations';
        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'rules_regulations', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }

    public function palkhi_bhavan_schedule() {
        $data['dataHeader']['title'] = 'Schedule of Pune Palkhi Bhavan';
        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'palkhi_bhavan_schedule', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }

    public function shirdi() {
        $data['dataHeader']['title'] = 'Shirdi';
        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'shirdi', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }

    public function trustee() {
        $data['dataHeader']['title'] = 'Trustee';
        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'trustee', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }

    public function upcoming_events() {
        $data['dataHeader']['title'] = 'Upcoming Events';
        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'upcoming_events', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }

    public function video_gallery() {
        $data['dataHeader']['title'] = 'Video Gallery';
        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'video_gallery', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }

    public function vision_mission() {
        $data['dataHeader']['title'] = 'Vision And Mission';
        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'vision_and_mission', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }

    public function why_palkhi() {
        $data['dataHeader']['title'] = 'Why Palkhi';
        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'why_palkhi', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }
}
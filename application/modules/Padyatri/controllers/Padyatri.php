<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Padyatri extends MY_Controller {

    public function __construct() {
        parent::__construct();

        if (!$this->ion_auth->logged_in()) {
            redirect('admin', 'refresh');
        }
        if (!$this->ion_auth->is_admin()) {
            redirect('admin', 'refresh');
        }
        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation', 'session', 'm_pdf', 'numbertowords'));
        $this->load->model(array('Sevasamiti_model', 'DindiModel', 'Padyatri_model', 'Year', 'Devotee', 'language'));
        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('Auth');
    }

    function print_receipt() {
        $this->load->view("Padyatri/print");
//         $this->load->view("common/CommonView", $data);
    }

    public function index() {

        $user_id = $this->session->userdata('user_id');
        $year = $this->Year->get_year_id(date('Y'));
//        $data['dindi_list'] = $this->DindiModel->get_dindi_list();
        $data['allpadyatri_list'] = $this->Padyatri_model->get_allpadyatri($year);
        $data['year_list'] = $this->Year->get_year_list();
        $data['dataHeader']['title'] = 'Padyatri List';
        $this->template->set_master_template('template.php');
        $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
        $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
        $this->template->write_view('content', 'Padyatri/padyatri_list', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'snippets/footer', '', TRUE);
        $this->template->render();
    }

    public function update_padyatri($padyatri_id) {

        $user_id = $this->session->userdata('user_id');
        $year = $this->Year->get_year_id(date('Y'));
        $padyatri_id = ($padyatri_id) ? $padyatri_id : $this->input->post('padyatri_id');
        $data['padyatri_listby_id'] = $this->Padyatri_model->get_padyatri_by_id($padyatri_id);

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $joining = (!empty($data['padyatri_listby_id'][0]['date_of_joining'])) ? date("Y", strtotime($data['padyatri_listby_id'][0]['date_of_joining'])) : '';

            $this->form_validation->set_rules('padyatrifirstname', 'Padyatri First Name', 'trim|required|alpha');
            $this->form_validation->set_rules('padyatrimiddlename', 'Padyatri Middle Name', 'trim|required|alpha');
            $this->form_validation->set_rules('padyatrilastname', 'Padyatri Last Name', 'trim|required|alpha');
            $this->form_validation->set_rules('padyatridob', 'Padyatri D.O.B', 'trim|required');
            $this->form_validation->set_rules('padyatrimobileno', 'Padyatri Mobile No.', 'trim|required|numeric|min_length[10]|max_length[10]');
//            $this->form_validation->set_rules('padyatrialtername_no', 'Padyatri Altername Mobile No.', 'required');
            $this->form_validation->set_rules('padyatrigender', 'Padyatri Gender', 'required');
//            $this->form_validation->set_rules('padyatripaidamount', 'Padyatri Paid Amount', 'required');
            $this->form_validation->set_rules('padyatriaddress', 'Padyatri Address', 'trim|required');
            $this->form_validation->set_rules('padyatripaydate', 'Padyatri pay Date', 'trim|required');
            $this->form_validation->set_rules('padyatrimodeofpayment', 'Padyatri Payment Mode', 'trim|required');
            $this->form_validation->set_rules('padyatriidproof', 'Padyatri Id Proof', 'trim|required');
            $this->form_validation->set_rules('padyatridindiname', 'Padyatri Dindi Name', 'trim|required');
            $this->form_validation->set_rules('padyatrimailid', 'Padyatri Email ID', 'trim');
            $this->form_validation->set_rules('padyatricity', 'Padyatri City', 'trim|required');
            $this->form_validation->set_rules('padyatriprofession', 'Padyatri Profession', 'trim|required');
//            $this->form_validation->set_rules('padyatrieducation', 'Padyatri Education', 'trim|required');
//            $this->form_validation->set_rules('padyatrisamithi', 'Padyatri Seva Samithi', 'trim|required');
            $this->form_validation->set_rules('padyatransport_samithi', 'Padyatri Transport Samithi', 'trim|required');
            $this->form_validation->set_rules('aadharno', 'Aadhaar Card No.', 'trim|min_length[14]|max_length[14]');
            $this->form_validation->set_rules('padyatrirelativename', 'Relative Name', 'trim|required');
            $this->form_validation->set_rules('padyatriralativeno', 'Relative No.', 'trim|required|numeric|min_length[10]|max_length[10]');
            $this->form_validation->set_rules('padyatripincode', 'Pin Code', 'trim|required|numeric|min_length[6]|max_length[6]');
            $yearid = $this->input->post('participation_year_id');
//            var_dump($_POST);die;
            if ($this->form_validation->run() == TRUE) {

                $first_name = $this->input->post('padyatrifirstname');
                $middle_name = $this->input->post('padyatrimiddlename');
                $last_name = $this->input->post('padyatrilastname');
                $dob = $this->input->post('padyatridob');
                $city = $this->input->post('padyatricity');
                $year = $joining;

                $dpn_no = $this->create_dpn_no($first_name, $middle_name, $last_name, $dob, $city, $year);
//                $dpnno = $firstChar_fname . "" . $firstChar_lname . "" . $dob . "" . $firsttwoChar_city . "" . $dt_of_joining;
                $padyatridata = array(
                    'id' => $this->input->post('padyatri_id'),
//                    'dpn_no' => ($this->input->post('dpn_no') ? $dpnno : $dpnno),
                    'devotee_id' => $this->input->post('dt_id'),
                    'vehical_type' => $this->input->post('padyatransport_samithi'),
                    'dindi_id' => $this->input->post('padyatridindiname'),
                    'seva_samiti_id' => $this->input->post('padyatrisamithi'),
                    'payment_date' => date("Y-m-d", strtotime($this->input->post('padyatripaydate'))),
                    'payment_mode' => $this->input->post('padyatrimodeofpayment'),
                    'payment_status' => 'success',
                    'enrollment_via' => $this->input->post('padyatrimodeofpayment')
//                    'receipt_no' => $this->input->post('1001')
                );
                $devoteedata = array('id' => $this->input->post('dt_id'),
                    'email' => $this->input->post('padyatrimailid'),
                    'dpn_no' => $dpn_no,
                    'firstname' => $this->input->post('padyatrifirstname'),
                    'middlename' => $this->input->post('padyatrimiddlename'),
                    'lastname' => $this->input->post('padyatrilastname'),
                    'birth_date' => date("Y-m-d", strtotime($this->input->post('padyatridob'))),
                    'mobile' => '91' . $this->input->post('padyatrimobileno'),
                    'mobile1' => '91' . $this->input->post('padyatrialtername_no'),
                    'address' => $this->input->post('padyatriaddress'),
                    'city' => $this->input->post('padyatricity'),
                    'profession' => $this->input->post('padyatriprofession'),
//                    'education' => $this->input->post('padyatrieducation'),
                    'gender' => $this->input->post('padyatrigender'),
                    'identity_prof' => $this->input->post('padyatriidproof'),
                    'adhaar_no' => ($this->input->post('aadharno')) ? $this->input->post('aadharno') : '',
                    'pincode' => $this->input->post('padyatripincode'),
                    'relative_name' => ($this->input->post('padyatrirelativename')) ? $this->input->post('padyatrirelativename') : '',
                    'relative_mobile' => ($this->input->post('padyatriralativeno')) ? '91' . $this->input->post('padyatriralativeno') : '',
                );

                $response = $this->Padyatri_model->update_divtotee_and_padyatri($padyatri_id, $padyatridata, $devoteedata, $this->input->post('truckid'), $yearid);
                if ($response) {
                    $this->session->set_flashdata('success_msg', 'Padyatri details Successfully updated.');
                    redirect('admin/padyatri');
                } else {
                    $this->session->set_flashdata('error_msg', 'Failed to Update Padyatri details');
                    redirect('admin/padyatri');
                }
            }
        } else {
            
        }
        $data['samiti_list'] = $this->Sevasamiti_model->get_samiti_list($year);
        $data['dindi_list'] = $this->DindiModel->get_dindi_list();
        $data['year_list'] = $this->Year->get_year_list();
        $data['id_proof'] = $this->Devotee->get_idproof_list();
        $data['occupation_list'] = $this->Devotee->get_profession_list();
//        $data['education_list'] = $this->Devotee->get_education_list();
        $data['city_list'] = $this->Devotee->get_city_list();
//            $data['dindi_details'] = $this->DindiModel->get_allData($padyatri_id);
        $data['dataHeader']['title'] = 'Edit Padyatri';
        $this->template->set_master_template('template.php');
        $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
        $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
        $this->template->write_view('content', 'Padyatri/padyatri_details', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'snippets/footer', '', TRUE);
        $this->template->render();
    }

    public function delete_padyatri() {
        if ($this->input->post('dindi_id')) {
            $this->DindiModel->delete_dindi($this->input->post('dindi_id'));
            if ($this->db->affected_rows() > 0) {
                $this->session->set_flashdata('success_msg', 'Dindi deleted successfully.');
                echo 'success';
            } else {
                $this->session->set_flashdata('error_msg', 'Failed to delete dindi.');
                echo 'failed';
            }
        }
    }

    public function padyatri_list() {
        if ($this->input->post('year')) {

            $data['padyatri_list'] = $this->Padyatri_model->get_allpadyatri($this->input->post('year'));
            $view = $this->load->view('ajax/padyatri_list', $data, true);
            echo $view;
        }
    }

    public function create_dpn_no($firstname, $middle_name, $lastname, $dob, $city, $year) {
        $firstChar_fname = strtoupper(substr($firstname, 0, 1));
        $firstChar_mname = strtoupper(substr($middle_name, 0, 1));
        $firstChar_lname = strtoupper(substr($lastname, 0, 1));
        $entiredob = date("dmY", strtotime($dob));
        $firsttwoChar_city = strtoupper(substr($city, 0, 2));
        $dpnno = $firstChar_fname . "" . $firstChar_mname . "" . $firstChar_lname . "" . $entiredob . "" . $firsttwoChar_city . "" . $year;
        return $dpnno;
    }

}

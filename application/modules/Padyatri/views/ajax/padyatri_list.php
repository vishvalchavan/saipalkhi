<table id="padyatri_list" class="display nowrap table table-hover table-striped table-bordered dataTable datatable" cellspacing="0" role="grid" aria-describedby="example23_info">
    <thead>
        <tr role="row">
            <th>DPN No.</th>
            <th>Padyatri Name</th>
            <th>Mobile No.</th>
            <th>Pay Date</th>
            <th>Payment Mode</th>
            <th>Dindi</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>  
        <?php
        // var_dump($padyatri_list);
        if (isset($padyatri_list) && !empty($padyatri_list)) {
            foreach ($padyatri_list as $padyatri_list) {
                if (file_exists($padyatri_list['user_image_path'])) {
                    $image_path = base_url() . $padyatri_list['user_image_path'];
                } else {
                    $image_path = base_url('assets/images/') . "default-profile.png";
                }
                $from = new DateTime($padyatri_list['birth_date']);
                $to = new DateTime('today');
                $age = $from->diff($to)->y;
                $amountinword = $this->numbertowords->convert_number($padyatri_list['payment_amount']);

                $print_payment = (strtolower($padyatri_list['payment_mode']) == 'online') ? 'Online' : 'Cash';
                $badget_data = "data-pr-age='" . $age . "' data-pr-image='" . $image_path . "' data-pr-dpn='" . $padyatri_list['dpn_no'] . "' data-pr-dtname='" . $padyatri_list['firstname'] . " " . $padyatri_list['middlename'] . " " . $padyatri_list['lastname'] . "' data-pr-address='" . $padyatri_list['address'] . ", " . $padyatri_list['city'] . " - " . $padyatri_list['pincode'] . "' data-pr-bldgrp='" . $padyatri_list['blood_group'] . "' data-pr-mobile='" . substr($padyatri_list['mobile'], 2) . "' data-pr-rel='" . $padyatri_list['relative_name'] . "' data-pr-relmobile='" . substr($padyatri_list['relative_mobile'], 2) . "' data-pr-devotee='" . $padyatri_list['id'] . "'";
                $receipt_data = "data-pr-dpn='" . $padyatri_list['dpn_no'] . "' data-pr-dtname='" . $padyatri_list['firstname'] . " " . $padyatri_list['middlename'] . " " . $padyatri_list['lastname'] . "' data-pr-address='" . $padyatri_list['address'] . ", " . $padyatri_list['city'] . " - " . $padyatri_list['pincode'] . "' data-pr-amount='" . $padyatri_list['payment_amount'] . "' data-pr-payment-date='" . date('d-m-Y', strtotime($padyatri_list['payment_date'])) . "' data-pr-amountinword='" . $amountinword . "' data-pr-devotee='" . $padyatri_list['id'] . "' data-pr-paymentmode='" . $print_payment . "'";
                ?>
                <tr>
                    <td><?php echo ($padyatri_list['dpn_no']) ? $padyatri_list['dpn_no'] : ''; ?></td>
                    <td><?php echo ($padyatri_list['firstname']) ? $padyatri_list['firstname'] . " " . $padyatri_list['middlename'] . " " . $padyatri_list['lastname'] : ''; ?></td>
                    <td><?php echo ($padyatri_list['mobile']) ? substr($padyatri_list['mobile'], 2) : ''; ?></td>
                    <td><?php echo ($padyatri_list['payment_date']) ? date("d F Y", strtotime($padyatri_list['payment_date'])) : ''; ?></td>
                    <td><?php echo isset($padyatri_list['payment_mode']) ? ucfirst($padyatri_list['payment_mode']) : ''; ?></td>
                    <td><?php echo ($padyatri_list['tbl_dl_name']) ? $padyatri_list['tbl_dl_name'] : ''; ?></td>
                    <td class="">
                        <a href="<?php
                        $tbl_py_id = ($padyatri_list['tbl_py_id']) ? $padyatri_list['tbl_py_id'] : '0';
                        echo base_url() . 'admin/padyatri/update/' . $tbl_py_id;
                        ?>" class="bg-button" title="Edit"><i class="fas fa-edit"></i></a>&nbsp;&nbsp;&nbsp; 
                        <!-- <a class="bg-button delete"><i class="fas fa-trash"></i></a> -->

                        <?php if (strtolower($padyatri_list['payment_status']) == 'success' && date("Y", strtotime($padyatri_list['payment_date'])) == date("Y")) { ?>
                            <i class="fas fa-id-card cursor-pointer <?php echo ($padyatri_list['badget_print'] == 1) ? 'fahover' : '' ?>" <?php echo ($padyatri_list['badget_print'] == 1) ? 'title="Badget Already Printed"' : 'title="Print Badget"' ?> onclick="printBadget(this, 'printMe')" <?php echo $badget_data; ?> ></i>&nbsp;&nbsp;&nbsp;
                            <i class="fas fa-file-alt cursor-pointer <?php echo ($padyatri_list['receipt_print'] == 1) ? 'fahover' : '' ?>" <?php echo ($padyatri_list['receipt_print'] == 1) ? 'title="Receipt Already Printed"' : 'title="Print Receipt"' ?> onclick="printReceipt(this, 'printReceipt')" <?php echo $receipt_data; ?> ></i>
                        <?php } ?> 
                    </td>
                </tr> 
                <?php
            }
        }
        ?>  
    </tbody>
</table>
<div class="card min-height80">
    <div class="card-body">
        <?php
        if (isset($padyatri_listby_id) && !empty($padyatri_listby_id)) {
            foreach ($padyatri_listby_id as $padyatri_list) {
                ?>
                <div class="row page-titles mx-0">
                    <div class="col-md-5 col-7 align-self-center p-0">
                        <h3 class="card-title">Padyatri Details</h3>

                    </div>
                    <div class="col-md-7 col-5 text-right p-0">
                        <h5><?php echo ($padyatri_list['dpn_no']) ? 'DPN: ' . $padyatri_list['dpn_no'] : ''; ?> </h5>
                    </div>

                </div>

                <div class="row">
                    <div class="col-12">
                        <?php
                        //var_dump($padyatri_list);
                        echo form_open('admin/padyatri/update/' . $padyatri_list['tbl_py_id'], array('id' => 'update_padyatri', 'class' => 'form_horizontal'));
                        echo form_hidden('padyatri_id', $padyatri_list['tbl_py_id']);
                        echo form_hidden('dpn_no', $padyatri_list['dpn_no']);
                        echo form_hidden('dt_id', $padyatri_list['devotee_id']);
                        echo form_hidden('truckid', $padyatri_list['vehicle_details']);
                        echo form_hidden('participation_year_id', $padyatri_list['participation_year_id']);
                        ?>

                        <div class="form-body">
                            <hr class="m-t-0 m-b-40">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Full Name:</label>
                                        <div class="col-md-7 row">
                                            <div class="col-md-4 col-md-15">
                                                <input type="text" name="padyatrifirstname" value="<?php echo set_value('padyatrifirstname', $padyatri_list['firstname']); ?>" data-error=".padyatriErorr1"  class="form-control1" placeholder="First Name">
                                                <div class="input-field">
                                                    <div class="padyatriErorr1 error-msg"></div>
                                                    <?php echo form_error('padyatrifirstname'); ?>
                                                </div> 
                                            </div>                                                        
                                            <div class="col-md-4 col-md-15">
                                                <input type="text" name="padyatrimiddlename" class="form-control1" value="<?php echo set_value('padyatrimiddlename', $padyatri_list['middlename']); ?>" data-error=".padyatriErorr2" placeholder="Middle Name">
                                                <div class="input-field">
                                                    <div class="padyatriErorr2 error-msg"></div>
                                                    <?php echo form_error('padyatrimiddlename'); ?>
                                                </div> 
                                            </div>
                                            <div class="col-md-4 col-md-15">
                                                <input type="text" name="padyatrilastname" class="form-control1" value="<?php echo set_value('padyatrilastname', $padyatri_list['lastname']); ?>" data-error=".padyatriErorr3" placeholder="Last Name">
                                                <div class="input-field">
                                                    <div class="padyatriErorr3 error-msg"></div>
                                                    <?php echo form_error('padyatrilastname'); ?>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Date of Birth:</label>
                                        <div class="col-md-7">
                                            <?php
                                            $dob = '';
                                            if (!empty($padyatri_list['birth_date']) && $padyatri_list['birth_date'] != "0000-00-00") {
                                                $dob = date("d F Y", strtotime($padyatri_list['birth_date']));
                                            }
                                            ?>
                                            <input name="padyatridob" type="text" class="form-control datepicker" value="<?php echo set_value('padyatridob', $dob); ?>" data-error=".padyatriErorr4" placeholder="dd-mm-yyyy">
                                            <div class="input-field">
                                                <div class="padyatriErorr4 error-msg"></div>
                                                <?php echo form_error('padyatridob'); ?>
                                            </div> 
                                        </div>
                                    </div>
                                </div>

                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Mobile No:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="row">
                                                    <div class="col-sm-3 col">
                                                        <input type="text" class="form-control mobile-width" id="country_code" name="country_code" value="+91" placeholder="+91" readonly>
                                                    </div>
                                                    <div class="col-sm-9 col mobile-width">
                                                        <input type="text" name="padyatrimobileno" class="form-control mobile-width" value="<?php echo set_value('padyatrimobileno', substr($padyatri_list['mobile'], 2)); ?>" data-error=".padyatriErorr5" placeholder="Enter Mobile Number">
                                                        <div class="input-field">
                                                            <div class="padyatriErorr5 error-msg"></div>
                                                            <?php echo form_error('padyatrimobileno'); ?>
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Transport:</label>
                                        <div class="col-md-7">
                                            <select class="form-control custom-select" name="padyatransport_samithi" data-error=".padyatriErorr6">
                                                <?php $selected_trnsport = set_value('padyatransport_samithi', $padyatri_list['vehical_type']); ?>
                                                <option value="">Select Transport</option>
                                                <option value="Self" <?php echo ($selected_trnsport == "Self") ? 'selected' : ''; ?>>Self</option>
                                                <option value="Samiti" <?php echo ($selected_trnsport == "Samiti") ? 'selected' : ''; ?>>Samiti Transport</option>
                                            </select>                                            
                                            <div class="input-field">
                                                <div class="padyatriErorr6 error-msg"></div>
                                                <?php echo form_error('padyatransport_samithi'); ?>
                                            </div> 
                                        </div>
                                    </div>
                                </div>

                                <!--/span-->
                            </div>
                            <!--/row-->


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-left col-md-4">Sec Mobile:</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <div class="row">
                                                    <div class="col-sm-3 col">
                                                        <input type="text" class="form-control mobile-width" id="alt_country_code" name="alt_country_code" value="+91" placeholder="+91" readonly>
                                                    </div>
                                                    <div class="col-sm-9 col mobile-width">
                                                        <input type="text" name="padyatrialtername_no" class="form-control mobile-width" value="<?php echo set_value('padyatrialtername_no', substr($padyatri_list['mobile1'], 2)); ?>" data-error=".padyatriErorr7" placeholder="Enter Mobile No.">
                                                        <div class="input-field">
                                                            <div class="padyatriErorr7 error-msg"></div>
                                                            <?php echo form_error('padyatrialtername_no'); ?>
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Gender:</label>
                                        <div class="col-md-7">
                                            <select class="form-control custom-select" name="padyatrigender" value="" data-error=".padyatriErorr18">
                                                <option value="">Select Gender</option>
                                                <?php $selected_gender = set_value('padyatrigender', $padyatri_list['gender']); ?>
                                                <option value="Male" <?php echo ($selected_gender == "Male") ? 'selected' : ''; ?> >Male</option>
                                                <option value="Female" <?php echo ($selected_gender == "Female") ? 'selected' : ''; ?>>Female</option>
                                                <option value="Other" <?php echo ($selected_gender == "Other") ? 'selected' : ''; ?>>Other</option>
                                            </select>
                                            <div class="input-field">
                                                <div class="padyatriErorr18 error-msg"></div>
                                                <?php echo form_error('padyatrigender'); ?>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Address:</label>
                                        <div class="col-md-7">
                                            <textarea rows="3" name="padyatriaddress" class="form-control" data-error=".padyatriErorr8" placeholder="Enter Address here"><?php echo set_value('padyatriaddress', $padyatri_list['address']); ?></textarea>
                                            <div class="input-field">
                                                <div class="padyatriErorr8 error-msg"></div>
                                                <?php echo form_error('padyatriaddress'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-left col-md-4">Email:</label>
                                        <div class="col-md-7">
                                            <input type="email" name="padyatrimailid" class="form-control" value="<?php echo set_value('padyatrimailid', $padyatri_list['email']); ?>" data-error=".padyatriErorr9" placeholder="Enter Email">
                                            <div class="input-field">
                                                <div class="padyatriErorr9 error-msg"></div>
                                                <?php echo form_error('padyatrimailid'); ?>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Relative/Referred Name:</label>
                                        <div class="col-md-7">

                                            <input type="text" name="padyatrirelativename" class="form-control form-control-danger" placeholder="Enter Relative Name" value="<?php echo set_value('devoteerelativename', $padyatri_list['relative_name']);    ?>" data-error=".devoteeErorr10">
                                            <div class="input-field">
                                                <div class="devoteeErorr10 error-msg"></div>
                                                <?php echo form_error('padyatrirelativename'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group required row">

                                        <label class="control-label text-left col-md-4">Relative/Referred Number:</label>
                                        <div class="col-md-7">                                            
                                            <div class="row">
                                                <div class="col-sm-3 col">
                                                    <input type="text" class="form-control" id="sec_country_code" name="rel_country_code" value="+91" placeholder="+91" readonly>
                                                </div>
                                                <div class="col-sm-9 col">

                                                    <input type="text" name="padyatriralativeno" class="form-control" placeholder="Enter Relative Mobile Number" value="<?php echo set_value('devoteeralativeno', substr($padyatri_list['relative_mobile'], 2));    ?>" data-error=".devoteeErorr11">

                                                    <div class="input-field">
                                                        <div class="devoteeErorr11 error-msg"></div>
                                                        <?php echo form_error('padyatriralativeno'); ?>
                                                    </div>
                                                </div> 
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">City:</label>
                                        <div class="col-md-7">
                                            <select class="form-control custom-select" name="padyatricity" id="padyatricity" data-error=".padyatriErorr10">
                                                <option value="">Select City</option>   
                                                <?php
                                                if (isset($city_list) && !empty($city_list)) {
                                                    foreach ($city_list as $city) {
                                                        $selected = '';
                                                        if (isset($padyatri_list['city']) && $padyatri_list['city'] == $city['name']) {
                                                            $selected = 'selected';
                                                        }
                                                        ?>
                                                        <option value="<?php echo $city['name']; ?>" <?php echo $selected; ?>><?php echo ucfirst($city['name']); ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <div class="input-field">
                                                <div class="padyatriErorr10 error-msg"></div>
                                                <?php echo form_error('padyatricity'); ?>
                                            </div> 
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Pay Date:</label>
                                        <div class="col-md-7">                                             
                                            <input type="text" name="padyatripaydate" class="form-control" value="<?php echo set_value('padyatripaydate', date('d F Y', strtotime($padyatri_list['payment_date']))); ?>" data-error=".padyatriErorr11" placeholder="dd-mm-yyyy" readonly>
                                            <div class="input-field">
                                                <div class="padyatriErorr11 error-msg"></div>
                                                <?php echo form_error('padyatripaydate'); ?>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Pin Code:</label>
                                        <div class="col-md-7">
                                            <input type="text" name="padyatripincode" id="padyatripincode" class="form-control form-control-danger" placeholder="Enter PIN Code here" value="<?php echo set_value('padyatripincode', $padyatri_list['pincode']); ?>" data-error=".padyatriErorr19">
                                            <div class="input-field">
                                                <div class="padyatriErorr19 error-msg"></div>
                                                <?php echo form_error('padyatripincode'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Profession:</label>
                                        <div class="col-md-7">
                                            <select class="form-control custom-select" name="padyatriprofession" id="devoteeprofession" data-error=".padyatriErorr12">
                                                <option value="">Select Occupation</option>
                                                <?php
                                                $selected_prof = '';
                                                foreach ($occupation_list as $profession) {
                                                    $selected_prof = set_value('padyatriprofession', $padyatri_list['profession']);
                                                    $selected_prof = ($selected_prof == $profession['id']) ? 'selected' : '';
                                                    ?>    
                                                    <option value="<?php echo $profession['id'] ?>" <?php echo $selected_prof ?> ><?php echo $profession['occupation'] ?></option>    
                                                <?php } ?>
                                            </select> 
                                            <div class="input-field">
                                                <div class="padyatriErorr12 error-msg"></div>
                                                <?php echo form_error('padyatriprofession'); ?>
                                            </div> 
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Mode of Payment:</label>
                                        <div class="col-md-7">
                                            <?php
                                            if (strtolower($padyatri_list['payment_status']) == 'success') {
                                                $disabled = 'pointer-events:none;';
                                                $readonly = 'readonly';
                                            }
                                            ?>
                                            <select class="form-control" id="padyatrimodeofpayment" name="padyatrimodeofpayment" data-error=".padyatriErorr13" <?php echo isset($readonly) ? $readonly : ''; ?> style="<?php echo isset($disabled) ? $disabled : ''; ?>">
                                                <option value="">Select Payment Mode</option> 
                                                <option value="online" <?php echo isset($padyatri_list['payment_mode']) && $padyatri_list['payment_mode'] == "online" ? 'selected' : ''; ?>>Online</option>
                                                <option value="palakhibhavan" <?php echo isset($padyatri_list['payment_mode']) && $padyatri_list['payment_mode'] == "palakhibhavan" ? 'selected' : ''; ?>>At Sai Palkhi Bhavan</option>
                                                <option value="bank" <?php echo isset($padyatri_list['payment_mode']) && $padyatri_list['payment_mode'] == "bank" ? 'selected' : ''; ?>>In Bank Account</option>
                                            </select>
                                        <!--<input type="text" name="padyatrimodeofpayment" class="form-control" value="<?php echo set_value('padyatrimodeofpayment', $padyatri_list['payment_mode']); ?>" data-error=".padyatriErorr13" placeholder="Mode Of Payment">-->
                                            <div class="input-field">
                                                <div class="padyatriErorr13 error-msg"></div>
                                                <?php echo form_error('padyatrimodeofpayment'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-left col-md-4">Sickness:</label>
                                        <div class="col-md-7">
                                            <input type="text" name="devoteesickness" class="form-control" placeholder="Enter Sickness" value="<?php //echo set_value('devoteesickness', $devotee_list['sickness']);     ?>" data-error=".devoteeErorr18">
                                            <div class="input-field">
                                                <div class="devoteeErorr18 error-msg"></div>
                                                <?php echo form_error('devoteesickness'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
<!--                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Education:</label>
                                        <div class="col-md-7">
                                            <select class="form-control custom-select" name="padyatrieducation" id="padyatrieducation" data-error=".padyatriErorr14">
                                                <option value="">Select Education</option>
                                                <?php
                                                $selected_education = '';
                                                foreach ($education_list as $education) {
                                                    $selected_education = set_value('padyatrieducation', $padyatri_list['education']);
                                                    $selected_education = ($selected_education == $education['id']) ? 'selected' : '';
                                                    ?>    
                                                    <option value="<?php echo $education['id'] ?>" <?php echo $selected_education ?> ><?php echo $education['education'] ?></option>    
                                                <?php } ?>
                                            </select>
                                            <div class="input-field">
                                                <div class="padyatriErorr14 error-msg"></div>
                                                <?php echo form_error('padyatrieducation'); ?>
                                            </div> 
                                        </div>
                                    </div>
                                </div>-->
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Dindi:</label>
                                        <div class="col-md-7"> 
                                            <select class="form-control custom-select" name="padyatridindiname" data-error=".padyatriErorr15">
                                                <option value="">Select Dindi</option>                                                                        
                                                <?php
                                                if (isset($dindi_list) && !empty($dindi_list)) {
                                                    foreach ($dindi_list as $dindi) {
                                                        $selected = '';
                                                        $selectedval = set_value('padyatridindiname', $padyatri_list['dindi_id']);
                                                        //                                               
                                                        if (!empty($selectedval) && $selectedval == $dindi['id']) {
                                                            $selected = "selected";
                                                        }
                                                        ?>
                                                        <option value="<?php echo $dindi['id'] ?>" <?php echo $selected; ?>><?php echo $dindi['name']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <div class="input-field">
                                                <div class="padyatriErorr15 error-msg"></div>
                                                <?php echo form_error('padyatridindiname'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                                               <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-left col-md-4">Seva Samiti:</label>
                                        <div class="col-md-7">                                            
                                            <select class="form-control custom-select" name="padyatrisamithi" value="<?php echo set_value('padyatrisamithi'); ?>" data-error=".padyatriErorr17">
                                                <option value="">Select Seva Samiti</option>
                                                <?php
                                                if (isset($samiti_list) && !empty($samiti_list)) {
                                                    foreach ($samiti_list as $samiti) {
                                                        $selected = '';
                                                        $selected_samithi = set_value('padyatrisamithi', $padyatri_list['tbl_ssm_id']);
                                                        if ($selected_samithi && $selected_samithi == $samiti['id']) {
                                                            $selected = "selected";
                                                        }
                                                        ?>
                                                        <option value="<?php echo $samiti['id'] ?>" <?php echo $selected; ?>><?php echo $samiti['name']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <div class="input-field">
                                                <div class="padyatriErorr17 error-msg"></div>
                                                <?php echo form_error('padyatrisamithi'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="row">                                
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">ID Proof:</label>
                                        <div class="col-md-7">
                                            <select class="form-control custom-select" name="padyatriidproof" id="padyatriidproof" data-error=".padyatriErorr16">
                                                <option value="">Select ID Proof</option>
                                                <?php
                                                $selected_proof = '';
                                                foreach ($id_proof as $proof) {
                                                    $selected_proof = set_value('devoteeidproof', $padyatri_list['identity_prof']);
                                                    $selected_proof = ($selected_proof == $proof['id']) ? 'selected' : '';
                                                    ?>    
                                                    <option value="<?php echo $proof['id'] ?>" <?php echo $selected_proof ?> ><?php echo $proof['document_type'] ?></option>    
                                                <?php } ?>
                                            </select>
                                            <div class="input-field">
                                                <div class="padyatriErorr16 error-msg"></div>
                                                <?php echo form_error('padyatriidproof'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>   

                                <div class="col-md-6 aadharcarddiv">
                                    <div class="form-group row">
                                        <label class="control-label text-left col-md-4">Aadhar Card No :</label>
                                        <div class="col-md-7">
                                            <input type="text" name="aadharno" class="form-control aadharcard" placeholder="Enter Aadhar No." value="<?php echo set_value('aadharno', $padyatri_list['adhaar_no']); ?>" data-error=".devoteeErorr17" maxlength="14" id="aadharno">
                                            <div class="input-field">
                                                <div class="devoteeErorr17 error-msg"></div>
                                                <?php echo form_error('aadharno'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>    
                            <!--/row-->
                        </div>
                        <hr>
                        <div class="form-actions pull-left">
                            <a href="<?php echo base_url() . 'admin/padyatri'; ?>"> <button type="button" class="btn btn-info"><i class="fas fa-arrow-left"></i> &nbsp;&nbsp; Back to Padyatri List</button></a>
                        </div>
                        <div class="form-actions pull-right">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="mr-5">

                                            <button type="submit" class="btn btn-inverse update-button">Update</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6"> </div>
                            </div>
                        </div>
                        <!--</form>-->
                        <?php
                        form_close();
                    }
                } else {
                    ?>
                    <div class="row page-titles">
                        <div class="col-md-5 align-self-center">
                            <h3 class="card-title">Padyatri Details</h3>
                        </div>
                    </div>
                    <div class="form-body">
                        <hr class="m-t-0 m-b-40">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-center margin-top-10">Data not found</div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

        var selected_proof = $("#padyatriidproof option:selected").text();
        if (selected_proof == "Aadhaar card" || selected_proof == "aadhaar card")
        {
            $("#aadharno").prop('disabled', false)
            $(".aadharcarddiv").show();
            $(".aadharcarddiv").css("display", "block");
        } else {
            $("#aadharno").prop('disabled', true)
            $(".aadharcarddiv").hide();
            $(".aadharcarddiv").css("display", "none");
        }
        $("#padyatriidproof").change(function () {

            if ($(this).find("option:selected").text() == "Aadhaar card" || $(this).find("option:selected").text() == "aadhaar card")
            {
                $("#aadharno").prop('disabled', false)
                $(".aadharcarddiv").show();
                $(".aadharcarddiv").css("display", "block");
            } else {
                $(".aadharcarddiv").hide();
                $("#aadharno").prop('disabled', true)
                $(".aadharcarddiv").css("display", "none");
            }
        });
    });
</script>
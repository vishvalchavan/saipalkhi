<div class="row">
    <div class="col-12">
        <div class="card min-height80">
            <div class="card-body">
                <h3 class="card-title">Padyatri List</h3>
                <hr>
                <div class="">
                    <div class="row mx-0">
                        <div class="button-table">
                            <form class="form-inline">
                                <label class="palkhi-select">Palkhi Sohala:</label>
                                <select class="form-control ml-2" id="year_list" name="year_list">
                                    <option value="">Select Year</option>
                                    <option value="all">All</option>
                                    <?php
                                    if (isset($year_list) && !empty($year_list)) {
                                        foreach ($year_list as $year) {
                                            $selected = '';
                                            if (date('Y') == $year['year']) {
                                                $selected = "selected";
                                            }
                                            ?>
                                            <option value="<?php echo $year['id'] ?>" <?php echo $selected; ?>><?php echo $year['year'] ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <label class="palkhi-select font-14 mr-1 ml-1">Category:</label>
                                <select class="form-control" id="search_by" name="search_by">
                                    <option value="">Search By</option>
                                    <option value="name" selected>Name</option>
                                    <option value="mobile">Mobile No.</option>
                                    <option value="dpn">DPN No.</option>
                                </select>
                                <div class="form-group has-search ml-2">
                                    <span class="fas fa-search form-control-feedback"></span>
                                    <input type="text" class="form-control" id="column_filter" placeholder="Search">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div id="example23_wrapper" class="dataTables_wrapper dt-bootstrap4 table-responsive">
                        <table id="padyatri_list" class="display nowrap table table-hover table-striped table-bordered dataTable datatable" cellspacing="0" role="grid" aria-describedby="example23_info">
                            <thead>
                                <tr role="row">
                                    <th>DPN No.</th>
                                    <th>Padyatri Name</th>
                                    <th>Mobile No.</th>
                                    <th>Pay Date</th>
                                    <th>Payment Mode</th>
                                    <th>Dindi</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                // var_dump($allpadyatri_list);
                                if (isset($allpadyatri_list) && !empty($allpadyatri_list)) {
                                    foreach ($allpadyatri_list as $padyatri_list) {
                                        if (file_exists($padyatri_list['user_image_path'])) {
                                            $image_path = base_url() . $padyatri_list['user_image_path'];
                                        } else {
                                            $image_path = base_url('assets/images/') . "default-profile.png";
                                        }
                                        $from = new DateTime($padyatri_list['birth_date']);
                                        $to = new DateTime('today');
                                        $age = $from->diff($to)->y;
                                        $amountinword = $this->numbertowords->convert_number($padyatri_list['payment_amount']);

                                        $print_payment = (strtolower($padyatri_list['payment_mode']) == 'online') ? 'Online' : 'Cash';
                                        $badget_data = "data-pr-age='" . $age . "' data-pr-image='" . $image_path . "' data-pr-dpn='" . $padyatri_list['dpn_no'] . "' data-pr-dtname='" . $padyatri_list['firstname'] . " " . $padyatri_list['middlename'] . " " . $padyatri_list['lastname'] . "' data-pr-address='" . $padyatri_list['address'] . ", " . $padyatri_list['city'] . " - " . $padyatri_list['pincode'] . "' data-pr-bldgrp='" . $padyatri_list['blood_group'] . "' data-pr-mobile='" . substr($padyatri_list['mobile'], 2) . "' data-pr-rel='" . $padyatri_list['relative_name'] . "' data-pr-relmobile='" . substr($padyatri_list['relative_mobile'], 2) . "' data-pr-devotee='" . $padyatri_list['id'] . "'";
                                        $receipt_data = "data-pr-dpn='" . $padyatri_list['dpn_no'] . "' data-pr-dtname='" . $padyatri_list['firstname'] . " " . $padyatri_list['middlename'] . " " . $padyatri_list['lastname'] . "' data-pr-address='" . $padyatri_list['address'] . ", " . $padyatri_list['city'] . " - " . $padyatri_list['pincode'] . "' data-pr-amount='" . $padyatri_list['payment_amount'] . "' data-pr-payment-date='" . date('d-m-Y', strtotime($padyatri_list['payment_date'])) . "' data-pr-amountinword='" . $amountinword . "' data-pr-devotee='" . $padyatri_list['id'] . "' data-pr-paymentmode='" . $print_payment . "'";
                                        ?>
                                        <tr>
                                            <td><?php echo ($padyatri_list['dpn_no']) ? $padyatri_list['dpn_no'] : ''; ?></td>
                                            <td><?php echo ($padyatri_list['firstname']) ? $padyatri_list['firstname'] . " " . $padyatri_list['middlename'] . " " . $padyatri_list['lastname'] : ''; ?></td>
                                            <td><?php echo ($padyatri_list['mobile']) ? substr($padyatri_list['mobile'], 2) : ''; ?></td>
                                            <td><?php echo ($padyatri_list['payment_date']) ? date("d F Y", strtotime($padyatri_list['payment_date'])) : ''; ?></td>
                                            <td><?php echo isset($padyatri_list['payment_mode']) ? ucfirst($padyatri_list['payment_mode']) : ''; ?></td>
                                            <td><?php echo ($padyatri_list['tbl_dl_name']) ? $padyatri_list['tbl_dl_name'] : ''; ?></td>
                                            <td class="">
                                                <a href="<?php
                                                $tbl_py_id = ($padyatri_list['tbl_py_id']) ? $padyatri_list['tbl_py_id'] : '0';
                                                echo base_url() . 'admin/padyatri/update/' . $tbl_py_id;
                                                ?>" class="bg-button" title="Edit"><i class="fas fa-edit"></i></a>&nbsp;&nbsp;&nbsp; 
                                                <!-- <a class="bg-button delete"><i class="fas fa-trash"></i></a> -->

                                                <?php if (strtolower($padyatri_list['payment_status']) == 'success' && date("Y", strtotime($padyatri_list['payment_date'])) == date("Y")) { ?>
                                                    <i class="fas fa-id-card cursor-pointer <?php echo ($padyatri_list['badget_print'] == 1) ? 'fahover' : '' ?>" <?php echo ($padyatri_list['badget_print'] == 1) ? 'title="Badget Already Printed"' : 'title="Print Badget"' ?> onclick="printBadget(this, 'printMe')" <?php echo $badget_data; ?> ></i>&nbsp;&nbsp;&nbsp;
                                                    <i class="fas fa-file-alt cursor-pointer <?php echo ($padyatri_list['receipt_print'] == 1) ? 'fahover' : '' ?>" <?php echo ($padyatri_list['receipt_print'] == 1) ? 'title="Receipt Already Printed"' : 'title="Print Receipt"' ?> onclick="printReceipt(this, 'printReceipt')" <?php echo $receipt_data; ?> ></i>
                                                <?php } ?> 
                                            </td>
                                        </tr> 
                                        <?php
                                    }
                                }
                                ?>    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('print_form.php'); ?>
<?php include('print.php'); ?>
<!----------------------- slip print ------------------------------------------------>
<script>
    function printBadget(click_Eve, divName) {
        $("#image_div").attr('src', $(click_Eve).attr('data-pr-image'));
        $("#dpnno_div").html($(click_Eve).attr('data-pr-dpn'));
        $("#fullname_div").html($(click_Eve).attr('data-pr-dtname'));
        $("#age_div").html($(click_Eve).attr('data-pr-age'));
        $("#bldgrp_div").html($(click_Eve).attr('data-pr-bldgrp'));
        $("#address_div").html($(click_Eve).attr('data-pr-address'));
        $("#mobile_div").html($(click_Eve).attr('data-pr-mobile'));
        $("#relative_div").html($(click_Eve).attr('data-pr-rel'));
        $("#relmobile_div").html($(click_Eve).attr('data-pr-relmobile'));
        $("#dpn2_div").html($(click_Eve).attr('data-pr-dpn'));
        $("#" + divName).css("display", 'block');
        var devotee = $(click_Eve).attr('data-pr-devotee');
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        setTimeout(function () {
            window.print();
            document.body.innerHTML = originalContents;
            $("#" + divName).css("display", 'none');
            updateBadget(devotee);
        }, 200);
    }
    function printReceipt(click_Eve, divName) {
        $("#" + divName).css("display", 'block');
        $("#rdpnno_div").html($(click_Eve).attr('data-pr-dpn'));
        $("#rfullname_div").html($(click_Eve).attr('data-pr-dtname'));
        $("#rpaydate_div").html($(click_Eve).attr('data-pr-payment-date'));
        $("#ramount_div").html($(click_Eve).attr('data-pr-amount'));
        $("#ramount_inword").html($(click_Eve).attr('data-pr-amountinword') + ' only');
        $("#raddress_div").html($(click_Eve).attr('data-pr-address'));
        $("#rpaymentmode_div").html($(click_Eve).attr('data-pr-paymentmode'));
        var devotee = $(click_Eve).attr('data-pr-devotee');
        var receiptContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = receiptContents;
        window.print();
        document.body.innerHTML = originalContents;
        $("#" + divName).css("display", 'none');
        updateReceipt(devotee);
    }
    function updateBadget(devotee) {
        window.onafterprint = function () {
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>Devotees/update_badget',
                data: {'devotee_id': devotee},
                success: function (data) {
//                    location.reload();
                }
            });
        }
    }
    function updateReceipt(devotee) {
        window.onafterprint = function () {
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>Devotees/update_receipt',
                data: {'devotee_id': devotee},
                success: function (data) {
//                    location.reload();
                }
            });
        }
    }

</script>
<script>
    function deletePadyatri(dId) {
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Padyatri/delete',
            data: {'padyatri_id': dId},
            success: function (data) {
                if ($.trim(data) == 'success') {
                    location.reload();
                }
            }
        });
        return false;
    }
    $('body').on('click', '.delete', function () {
        var id = $(this).attr('data-devotee');
        $('#confirm-delete').modal();
        $(".btn-ok").click(function () {
            deletePadyatri(id);
        });
    });
    $('body').on('change', '#year_list', function () {

        var year = $(this).val();
        if (year.length !== 0) {
            $(".loadding-page").css('display', 'block');
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>Padyatri/padyatri_list',
                data: {'year': year}, success: function (data) {
                    if (data) {
                        $("#example23_wrapper").html(data);
                        $('#padyatri_list').DataTable({
                            dom: 'Bfrtip',
                            buttons: [{
                                    extend: 'excel',
                                    exportOptions: {
                                        columns: [0, 1, 2, 3, 4]
                                    }
                                }, {
                                    extend: 'pdf',
                                    exportOptions: {
                                        columns: [0, 1, 2, 3, 4]
                                    }
                                }, {
                                    extend: 'print',
                                    exportOptions: {
                                        columns: [0, 1, 2, 3, 4]
                                    }
                                }]
                        });
                        $(".loadding-page").css('display', 'none');
                    }
                }
            });
        }
    });
    $(document).ready(function () {
        $("#search_by").change(function () {
            var search = $(this).val();
            if (search.length !== 0) {
                $("#column_filter").removeAttr('disabled');
                $("#column_filter").val("");
                if ($.trim(search) == 'name') {
                    filterColumn('2', "");
                } else if ($.trim(search) == 'mobile') {
                    filterColumn('0', "");
                } else if ($.trim(search) == 'dpn') {
                    filterColumn('2', "");
                }
            }
        });
        $('#column_filter').on('keyup click', function () {
            var search_txt = $(this).val();
            var search_by = $("#search_by").val();
            if (search_by.length !== 0) {
                if ($.trim(search_by) == 'name') {
                    filterColumn('1', search_txt);
                } else if ($.trim(search_by) == 'mobile') {
                    filterColumn('2', search_txt);
                } else if ($.trim(search_by) == 'dpn') {
                    filterColumn('0', search_txt);
                }
            } else {
                filterColumn('1', search_txt);
            }
        });
    });
    function filterColumn(i, searchVal) {
        $('#padyatri_list').DataTable().column(i).search(searchVal, false, true).draw();
    }
</script>
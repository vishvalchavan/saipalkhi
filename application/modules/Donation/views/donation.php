<div class="all-title-box-contact" style="height:150px !important;">
</div>
<div class="page-wrapper">
    <div class="container-fluid mt-4">
        <div id="" class="">
            <div class="section-title row text-center">
                <div class="col-md-12">
                    <div class="heading section-header">
                        <h2 class="section-title text-center wow fadeInDown animated font-weight-bold">Donation</h2>


                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5 mobile-p0">
                    <div class="col-lg-12">
                        <div class="card card-signin flex-row bg-none">
                            <div class="card-body p-2">                             
                                <?php echo form_open('donation', array('id' => 'donation_form', 'method' => "POST", 'class' => 'form-horizontal')); ?>
                                <input type="hidden" name="tid" id="tid" />
                                <input type="hidden" name="merchant_id" value="134572"/>
                                <input type="hidden" name="billing_country" value="India"/>
                                <input type="hidden" name="currency" value="INR"/>
                                <input type="hidden" name="redirect_url" value="<?php echo base_url() . 'donation-response' ?>"/>
                                <input type="hidden" name="cancel_url" value="<?php echo base_url() . 'donation-response' ?>"/>
                                <input type="hidden" name="language" value="EN"/>
                                <input type="hidden" name="order_id" value="<?php echo date("d-m-Y") . mt_rand(); ?>"/>
                                <input type="hidden" name="devotee" value="<?php echo $this->session->userdata('user_id'); ?>"/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group required row">
                                            <label class="control-label col-md-4">Donation For:</label>
                                            <div class="col-md-8">
                                                <!--<input type="text" class="form-control" placeholder="PAN">-->
                                                <select class="form-control custom-select" name="donationfor" id="donationfor" data-error=".error1">
                                                    <option>Select Donation Type</option>    
                                                    <option value="vastunidhishirdi">Vastunidhi - Shirdi</option>
                                                    <option value="palkhi">Palkhi Sohla</option>                                        
                                                </select>
                                                <div class="input-field">
                                                    <div class="error1 error-msg"></div>
                                                    <?php echo form_error('donationfor'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="control-label col-md-4">Amount:</label>
                                            <div class="col-md-8">
                                                <input type="text" name="amount" id="amount" class="form-control bg-white" value="<?php echo set_value('amount'); ?>" placeholder="Amount" data-error=".error4">
                                                <div class="input-field">
                                                    <div class="error4 error-msg"></div>
                                                    <?php echo form_error('amount'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>   
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group required row">
                                            <label class="control-label col-md-4 mb-0 mt-2">Messege:</label>
                                            <div class="col-md-8 ">
                                                <textarea name="comment" id="comment" class="form-control bg-white" rows="7" placeholder="Message / comment" data-error=".error8"></textarea>
                                                <div class="input-field">
                                                    <div class="error8 error-msg"></div>
                                                    <?php echo form_error('comment'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <label class="control-label col-md-4"></label>
                                            <div class="col-md-8">
                                                <button type="submit" class="btn btn-info border-0">Next</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <ul class="list-ul">
                        <li class="bullet-none bullet-none wow fadeInRight animated mb-2" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold">&#9672;</small>&nbsp;Shri Sainath himself explained the importance of Charity in the 14th chapter of Shri Sai Satcharitra. The meaning of explanation stated in the chapter is that, <i class="font-weight-bold">“Charity accepted by Shri Saibaba means devotees mind purity and ruin their sin’’.</i></li>
                        <li class="bullet-none bullet-none wow fadeInRight animated mb-2" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold">&#9672;</small>&nbsp;One of the primary supports for the Sai Palkhi Samiti is obviously financial support. Every single rupee that is donated is extremely important to the Samiti for its sustenance. There has been overwhelming contribution to the Sai Palkhi Samiti fund by numerous donors so far this year. The Sai Palkhi Samiti would like to extend heartfelt thanks and blessings from Sai Baba to the donors for being part of the devotional experience of Sai Baba, each individual in their own unique way.</li>
                        <li class="bullet-none bullet-none wow fadeInRight animated mb-2" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold">&#9672;</small>&nbsp;Every one's contribution and involvement is very much appreciated how much ever big or small. </li>
                        <li class="bullet-none bullet-none wow fadeInRight animated mb-2" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold">&#9672;</small>&nbsp;These donations are accepted in the form of cash (Indian as well as foreign currencies), Net Banking Services, all debit/credit cards through payment gateway, Crossed and A/c Payee Cheques or Demand Drafts. </li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-12 offset-md-4">
                        <p class="bullet-none bullet-none wow fadeInRight animated text-center font-weight-bold"> &nbsp;Please make note that – All Donation Funds are eligible for Income Tax deduction under section 80G (Govt. Of India)</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
































<!-- <div class="all-title-box-contact">
</div>
<div class="page-wrapper-bg-white">
    <section id="features">
        <div id="overviews" class="">
            <div class="container">
                <div class="section-header">
                 
                    <div class="col-md-12">
                        <h2 class="section-title text-center wow fadeInDown animated font-weight-bold" style="visibility: visible; animation-name: fadeInDown;">Donation</h2>
                        <hr>
                    </div>
                 

                    <p></p>
                    <p>
                         </p>
                    <p>
                    </p>
                    <p></p>
                    <p align="Center"><b></b></p>
                    <p></p>

<?php echo form_open('donation', array('id' => 'donation_form', 'method' => "POST", 'class' => 'form-horizontal')); ?>
                    <input type="hidden" name="tid" id="tid" />
                    <input type="hidden" name="merchant_id" value="134572"/>
                    <input type="hidden" name="billing_country" value="India"/>
                    <input type="hidden" name="currency" value="INR"/>
                    <input type="hidden" name="redirect_url" value="<?php echo base_url() . 'donation-response' ?>"/>
                    <input type="hidden" name="cancel_url" value="<?php echo base_url() . 'donation-response' ?>"/>
                    <input type="hidden" name="language" value="EN"/>
                    <input type="hidden" name="order_id" value="<?php echo date("d-m-Y") . mt_rand(); ?>"/>
                    <input type="hidden" name="devotee" value="<?php echo $this->session->userdata('user_id'); ?>"/>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <input type="text" name="billing_name" id="billing_name" class="form-control  bg-white" value="<?php echo set_value('billing_name'); ?>" placeholder="Name" data-error=".error1">
                                <div class="input-field">
                                    <div class="error1 error-msg"></div>
<?php echo form_error('billing_name'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1 col-sm-2 pr-0 col-3">
                            <input type="text" class="form-control" id="country_code" name="country_code" value="+91" placeholder="+91" readonly>
                        </div>
                        <div class="col-md-5 col-sm-4 col-9">
                            <div class="form-group">
                                <input type="text" name="billing_tel" id="billing_tel" class="form-control bg-white" value="<?php echo set_value('billing_tel'); ?>"  placeholder="Mobile" data-error=".error2">
                                <div class="input-field">
                                    <div class="error2 error-msg mr-4"></div>
<?php echo form_error('billing_tel'); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                   <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <input type="email" name="billing_email" id="billing_email" class="form-control bg-white" value="<?php echo set_value('billing_email'); ?>" placeholder="Email" data-error=".error3">
                                <div class="input-field">
                                    <div class="error3 error-msg"></div>
<?php echo form_error('billing_email'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <input type="text" name="amount" id="amount" class="form-control bg-white" value="<?php echo set_value('amount'); ?>" placeholder="Amount" data-error=".error4">
                                <div class="input-field">
                                    <div class="error4 error-msg"></div>
<?php echo form_error('amount'); ?>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <select name="billing_country" class="form-control bg-white" id="billing_country" name="country" data-error=".error5">
                                    <option value="">Select Country</option>
<?php
if (isset($country_list) && !empty($country_list)) {
    foreach ($country_list as $country) {
        echo "<option value='" . $country['id'] . "'>" . $country['name'] . "</option>";
    }
}
?>
                                </select>
                                <div class="input-field">
                                    <div class="error5 error-msg"></div>
<?php echo form_error('billing_country'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <textarea name="comment" id="comment" class="form-control bg-white" rows="7" placeholder="Message / comment" data-error=".error8"></textarea>
                                <div class="input-field">
                                    <div class="error8 error-msg"></div>
<?php echo form_error('comment'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 mt-7">
                            <div class="form-group">
                                <select name="billing_state" class="form-control bg-white" id="billing_state" data-error=".error6">
                                    <option value="">Please Select State first</option>
                                </select>
                                <div class="input-field">
                                    <div class="error6 error-msg"></div>
<?php echo form_error('billing_state'); ?>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-6 mt-one">
                            <div class="form-group">
                                <select name="billing_city" class="form-control bg-white" id="billing_city" data-error=".error7">
                                    <option value="">Please Select City first</option>
                                </select>
                                <div class="input-field">
                                    <div class="error7 error-msg"></div>
<?php echo form_error('billing_city'); ?>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="float-right">
                        <input type="submit" class="btn btn-primary" name="Donate" value="Donate">
<?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>
<script>     window.onload = function () {
        var d = new Date().getTime();
        document.getElementById("tid").value = d;
    };
    $('document').ready(function () {
        $("#billing_country").change(function () {
            var country = $(this).val();
            if (country.length !== 0) {
                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url(); ?>Donation/get_state',
                    data: {'country_id': country},
                    success: function (data) {
                        if (data) {
                            $("#billing_state").html(data);
                        }
                    }
                });
            }
        });
        $("#billing_state").change(function () {
            var state = $(this).val();
            if (state.length !== 0) {
                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url(); ?>Donation/get_city',
                    data: {'state_id': state},
                    success: function (data) {
                        if (data) {
                            $("#billing_city").html(data);
                        }
                    }
                });
            }
        });
    });
</script> -->
<!DOCTYPE html>
<html lang="en">
    <head> 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Donation Receipt</title>
        <link rel="stylesheet" href="<?php base_url(); ?>assets/css/paper.css"  type="text/css" media="print">
        <style>
            body{
                width:100%;
            }
            .bg-background{
                background: url(<?php echo base_url('assets/images/donationpavti.jpg'); ?>) no-repeat 4px 0px;
                height: 600px;
                background-size: contain;
                position: relative;
                max-width: 100%;
                width: 100%;
            }
            @page {

                margin-top:0mm; 
                margin-left: 0mm; margin-right: 1mm;
            }


            @media print {
                .extradiv {
                    margin-bottom: 97px;
                    margin-left:200px;
                }
                .printdiv{
                    margin-top:0mm; 
                    margin-left: 0mm; margin-right: 0mm;

                }

            }
        </style>
    <body class="A4">
        <?php $nbsp = "&nbsp;"; ?>
           <div class="bg-background" style='page-break-after:avoid;'>
            <div class="extradiv"><br><br><br>

            </div>

            <div class="printdiv" style="position:absolute;top:188px;margin-left: 180px;font-size: 12px;">                        
                <div style="text-align:left;margin-left: 30px;margin-top:18px;">
                    <span style="font-weight:bold;color:#7f0907;">पावती क्र. : </span>
                    <span style=""><?php echo ($receipt_no) ? $nbsp . $receipt_no : ''; ?> </span>
                    <div style="margin-left: 280px;margin-top:-16px;">
                        <span style="font-weight:bold;color:#7f0907;">दिनांक :</span>
                        <span style=""><?php echo ($pay_date) ? $nbsp . date("d-m-Y", strtotime($pay_date)) : ''; ?></span>
                    </div>
                </div>
                <div class="" style="position:absolute;top:8px;"></div>

                <div style="width:100%">
                    <div style="text-align:left;margin-left:30px;margin-top:7px;">
                        <span style="font-weight:bold;color:#7f0907;">श्री. / सौ. / श्रीमती. / मेसर्स  : </span>
                        <span id="rfullname_div" style=" padding-bottom: 10px;height:15px;">
                            <?php echo ($name) ? $nbsp . $name : ''; ?></span>
                    </div>
                </div>

                <div style="width:100%">
                    <div style="text-align:left;margin-top:7px;margin-left:30px;">
                        <span style="font-weight:bold;color:#7f0907;">पत्ता  : </span>
                        <span id="rfullname_div" style=" padding-bottom: 10px;height:15px;">
                            <?php echo ($address) ? $nbsp . wordwrap($address, 65, "<br/>") : '';
                            
                            ?></span>
                    </div>
                </div>
                <div style="width:100%">
                    <div style="position: absolute;text-align:left;margin-top:15px;margin-left:30px;">
                        <span style="font-weight:bold;color:#7f0907;">अक्षरी रुपये : </span>
                        <span id="rfullname_div" style=" padding-bottom: 10px;height:15px;">
                            <?php echo ($amount_in_word) ? $nbsp . $amount_in_word : ''; ?></span>
                    </div>
                </div>

                <div style="width:100%">
                    <div style="text-align:left;margin-top:7px;margin-left:30px;">
                        <span style="font-weight:bold;color:#7f0907;">देणगी : </span>
                        <span id="rfullname_div" style=" padding-bottom: 10px;height:15px;">
                           <?php echo ($donation_for) ? $nbsp . $donation_for : ''; ?></span>
                        <span style="font-weight:bold;color:#7f0907;"> साठी </span>
                    </div>
                </div>                
                <div style="width:100%">
                    <div style="text-align:left;margin-top:0px;margin-left:330px;">                      
                        <!--<span style="border-left:1px solid #7f0907;"></span>-->
                        <p id="" style="margin-top:-10px;padding:1px 3px; border:1px solid #7f0907;margin-right: 80px;color:#7f0907;">रु.
                           <?php echo ($amount) ? $amount . " /-" : ''; ?></p>
                    </div>
                </div>      


            </div>
        </div>
    </body>
</html>
<div class="row">
    <div class="col-12">
        <div class="card min-height80">
            <div class="card-body">
                <div class="row mx-0">
                    <div class="col-md-5 col-5 align-self-center p-0">
                        <h3 class="card-title">Donation List</h3>
                    </div>
                </div>
                <hr>
                <div class="">
                    <div class="row mx-0">
                        <div class="float-left"></div>
                        <div class="button-table">
                            <form class="form-inline">
                                <label class="palkhi-select">Palkhi Sohala:</label>
                                <select class="form-control ml-2" id="year_list" name="year_list">
                                    <option value="">Select Year</option>
                                    <?php
                                    if (isset($year_list) && !empty($year_list)) {
                                        foreach ($year_list as $year) {
                                            $selected = '';
                                            if (date('Y') == $year['year']) {
                                                $selected = "selected";
                                            }
                                            ?>
                                            <option value="<?php echo $year['id'] ?>" <?php echo $selected; ?>><?php echo $year['year'] ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <label class="palkhi-select font-14 mr-1 ml-1">Category:</label>
                                <select class="form-control" id="search_by" name="search_by">
                                    <option value="">Search By</option>
                                    <option value="name" selected>Name</option>
                                    <option value="mobile">Mobile No.</option>
                                </select>

                                <div class="form-group has-search ml-2">
                                    <span class="fas fa-search form-control-feedback"></span>
                                    <input type="text" class="form-control" id="column_filter" placeholder="Search">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div id="example23_wrapper" class="dataTables_wrapper dt-bootstrap4 table-responsive">
                        <table id="donation_list" class="display nowrap table table-hover table-striped table-bordered dataTable datatable" cellspacing="0" width="100%" role="grid" aria-describedby="example23_info">
                            <thead>
                                <tr role="row">
                                    <!--<th>DPN No.</th>-->
                                    <th>Donor Name</th>
                                    <th>Mobile No.</th>
                                    <th>Email</th>
                                    <th>Donation For</th>
                                    <th>Comment</th>
                                    <th>Amount</th>
                                    <th>Date</th>
                                    <th>Receipt No.</th>
                                    <th>Transaction Id</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($donor_list) && !empty($donor_list)) {
                                    foreach ($donor_list as $donor) {
                                        ?>
                                        <tr>
                                            <!--<td><?php echo isset($donor['dpn_no']) ? $donor['dpn_no'] : ''; ?></td>-->
                                            <td><?php echo $donor['firstname'] . " " . $donor['middlename'] . " " . $donor['lastname']; ?></td>
                                            <td><?php echo isset($donor['mobile']) ? $donor['mobile'] : ''; ?></td>
                                            <td><?php echo isset($donor['email']) ? $donor['email'] : ''; ?></td>
                                            <?php
                                            if (isset($donor['donation_for']) && $donor['donation_for'] == 'vastunidhishirdi') {
                                                $donation_for = "Vastunidhi - Shirdi";
                                            } elseif (isset($donor['donation_for']) && $donor['donation_for'] == 'palkhi') {
                                                $donation_for = "Palkhi Sohla";
                                            }
                                            ?>
                                            <td><?php echo isset($donation_for) ? $donation_for : ''; ?></td>
                                            <td><?php echo isset($donor['note']) ? $donor['note'] : ''; ?></td>
                                            <td><?php echo isset($donor['donation_amount']) ? $donor['donation_amount'] : ''; ?></td>
                                            <td><?php echo isset($donor['donation_date']) ? date('d F Y', strtotime($donor['donation_date'])) : ''; ?></td>
                                            <td><?php echo isset($donor['receipt_no']) ? $donor['receipt_no'] : ''; ?></td>
                                            <td><?php echo isset($donor['transaction_id']) ? $donor['transaction_id'] : ''; ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('body').on('change', '#year_list', function () {
            var year = $(this).val();
            if (year.length !== 0) {
                $(".loadding-page").css('display', 'block');
                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url(); ?>Donation/Donor_list/donor_list',
                    data: {'year': year}, success: function (data) {
                        if (data) {
                            $("#example23_wrapper").html(data);
                            $('#donation_list').DataTable({
                                dom: 'Bfrtip',
                                buttons: [{
                                        extend: 'excel',
                                        exportOptions: {
                                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                                        }
                                    }, {
                                        extend: 'pdf',
                                        exportOptions: {
                                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                                        }
                                    }, {
                                        extend: 'print',
                                        exportOptions: {
                                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                                        }
                                    }]
                            });
                            $(".loadding-page").css('display', 'none');
                        }
                    }
                });
            }
        });
        $("#search_by").change(function () {
            var search = $(this).val();
            if (search.length !== 0) {
                $("#column_filter").removeAttr('disabled');
                $("#column_filter").val("");
                if ($.trim(search) == 'name') {
                    filterColumn('2', "");
                } else if ($.trim(search) == 'mobile') {
                    filterColumn('1', "");
                }
            }
        });
        $('#column_filter').on('keyup click', function () {
            var search_txt = $(this).val();
            var search_by = $("#search_by").val();
            if (search_by.length !== 0) {
                if ($.trim(search_by) == 'name') {
                    filterColumn('1', search_txt);
                } else if ($.trim(search_by) == 'mobile') {
                    filterColumn('2', search_txt);
                }
            } else {
                filterColumn('0', search_txt);
            }
        });
    });
    function filterColumn(i, searchVal) {
        $('#donation_list').DataTable().column(i).search(searchVal, false, true).draw();
    }
</script>

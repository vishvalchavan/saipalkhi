<div class="page-wrapper">
    <div class="container-fluid mt-4">
        <div id="overviews" class="section">
            <div class="wp-caption alignnone">
                <h2 class="text-left font-weight-bold thanks-page">Donation</h2>
                <?php
                if (isset($payment_status)) {
                    if ($payment_status == "Success") {
                        ?>
                        <h2 class="text-left">Payment Status : <span class="text-danger font-weight-bold"> Success</span></h2>
                        <p class="text-left">Thank you for your donation. <br>Regards, Sai Baba Palakhi Sohala Samiti,Pune</p>
                        <?php
                    } elseif ($payment_status == "Aborted") {
                        ?>
                        <h2 class="text-left">Transaction Aborted.</h2>
                        <p class="text-left">Sorry. Something went wrong. Your transaction has been Aborted. Kindly try again or use alternate payment option.</p>
                        <?php
                    } elseif ($payment_status == "Failure") {
                        ?>
                        <h2 class="text-left">Transaction Failed.</h2>
                        <p class="text-left">Sorry. Something went wrong. Your transaction has been declined. Kindly try again or use alternate payment option.</p>
                        <?php
                    } else {
                        ?>
                        <h2 class="text-left">Transaction Failed.</h2>
                        <p class="text-left">Security Error. Illegal access detected.</p>
                        <?php
                    }
                }
                ?>
                <div class="float-right  mb-4">
                    <a href="<?php echo base_url('home'); ?>" class="btn btn-primary btn-lg float-right">Back to Home</a>
                </div>
                <br>
            </div>
        </div>
    </div>
</div>
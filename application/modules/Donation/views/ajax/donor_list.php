<table id="donation_list" class="display nowrap table table-hover table-striped table-bordered dataTable datatable" cellspacing="0" width="100%" role="grid" aria-describedby="example23_info">
    <thead>
        <tr role="row">
            <!--<th>DPN No.</th>-->
            <th>Donor Name</th>
            <th>Mobile No.</th>
            <th>Email</th>
            <th>Donation For</th>
            <th>Comment</th>
            <th>Amount</th>
            <th>Date</th>
            <th>Receipt No.</th>
            <th>Transaction Id</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (isset($donor_list) && !empty($donor_list)) {
            foreach ($donor_list as $donor) {
                ?>
                <tr>
                    <!--<td><?php echo isset($donor['dpn_no']) ? $donor['dpn_no'] : ''; ?></td>-->
                    <td><?php echo $donor['firstname'] . " " . $donor['middlename'] . " " . $donor['lastname']; ?></td>
                    <td><?php echo isset($donor['mobile']) ? $donor['mobile'] : ''; ?></td>
                    <td><?php echo isset($donor['email']) ? $donor['email'] : ''; ?></td>
                    <?php
                    if (isset($donor['donation_for']) && $donor['donation_for'] == 'vastunidhishirdi') {
                        $donation_for = "Vastunidhi - Shirdi";
                    } elseif (isset($donor['donation_for']) && $donor['donation_for'] == 'palkhi') {
                        $donation_for = "Palkhi Sohla";
                    }
                    ?>
                    <td><?php echo isset($donation_for) ? $donation_for : ''; ?></td>
                    <td><?php echo isset($donor['note']) ? $donor['note'] : ''; ?></td>
                    <td><?php echo isset($donor['donation_amount']) ? $donor['donation_amount'] : ''; ?></td>
                    <td><?php echo isset($donor['donation_date']) ? date('d M Y', strtotime($donor['donation_date'])) : ''; ?></td>
                    <td><?php echo isset($donor['receipt_no']) ? $donor['receipt_no'] : ''; ?></td>
                    <td><?php echo isset($donor['transaction_id']) ? $donor['transaction_id'] : ''; ?></td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Donation extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation', 'session', 'm_pdf', 'numbertowords'));
        $this->load->library('encryption');
        $this->load->model('Donation_model');
        $this->load->helper(array('url', 'language'));
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->lang->load('Auth');
//        if ($this->ion_auth->is_admin()) {
//            $this->ion_auth->logout();
//            $this->session->unset_userdata('user_id');
//        }
//        if (!$this->ion_auth->logged_in()) {
//            redirect('login', 'refresh');
//        }
    }

    public function index() {
        $data['dataHeader']['title'] = 'Donation';
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->form_validation->set_rules('donationfor', 'Donation', 'trim|required');
            $this->form_validation->set_rules('amount', 'Amount', 'trim|numeric|required');
            $this->form_validation->set_rules('comment', 'Comment', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->session->set_userdata('donationinfo', $_POST);
                require_once "ccav/ccavRequestHandler.php";
            } else {
                $this->template->set_master_template('frontend_template.php');
                $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
                $this->template->write_view('content', 'Donation/donation', (isset($this->data) ? $this->data : NULL), TRUE);
                $this->template->write_view('footer', 'frontend/footer', '', TRUE);
                $this->template->render();
            }
        } else {
            $data['country_list'] = $this->Donation_model->country_list();

            $this->template->set_master_template('frontend_template.php');
            $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
            $this->template->write_view('content', 'Donation/donation', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'frontend/footer', '', TRUE);
            $this->template->render();
        }
    }

    public function ccavResponseHandler() {
        require_once "ccav/ccavResponseHandler.php";
        $donation_info = $this->session->userdata('donationinfo');
        if (isset($donation_info) && !empty($donation_info)) {
            $donor_info = $this->Donation_model->getDevotee($donation_info['devotee']);
            if (isset($donor_info) && !empty($donor_info)) {
                $donor_phone_no = $donor_info[0]['mobile'];
                if ($order_status == "Success") {
                    $year = $this->Donation_model->get_year_id(date("Y"));
                    $receipt_no = ($this->Donation_model->get_receipt_no() + 1);
                    $donation_details = array(
                        'devotee_id' => $donation_info['devotee'],
                        'donation_for' => $donation_info['donationfor'],
                        'donation_amount' => $donation_info['amount'],
                        'note' => $donation_info['comment'],
                        'donation_status' => $order_status,
                        'donation_date' => date('Y-m-d'),
                        'donated_year' => $year,
                        'receipt_no' => $receipt_no,
                        'created_date' => date("Y-m-d"),
                        'transaction_id' => $tracking_id
                    );
                    $donor = $this->Donation_model->add_donation($donation_details);
                    if (isset($donor)) {
                        //send receipt
                        $donation_for = '';
                        if (isset($donor_info[0]['email']) && !empty($donor_info[0]['email'])) {
                            if (isset($donor_info[0]['donation_for']) && $donor_info[0]['donation_for'] == 'vastunidhishirdi') {
                                $donation_for = "Vastunidhi - Shirdi";
                            } elseif (isset($donor_info[0]['donation_for']) && $donor_info[0]['donation_for'] == 'palkhi') {
                                $donation_for = "Palkhi Sohla";
                            }

                            $receipt_data = array(
                                'email' => $donor_info[0]['email'],
                                'pay_date' => date("Y-m-d"),
                                'receipt_no' => $receipt_no,
                                'donation_for' => $donation_for,
                                'name' => $donor_info[0]['firstname'] . " " . $donor_info[0]['middlename'] . " " . $donor_info[0]['lastname'],
                                'amount' => $donation_info['amount'],
                                'address' => $donor_info[0]['address'] . ", " . $donor_info[0]['city'] . " - " . $donor_info[0]['pincode'],
                                'amount_in_word' => $this->numbertowords->convert_number($donation_info['amount']) . " only"
                            );
                            $this->SendDonationReceipt($receipt_data);
                        }
                        $this->session->unset_userdata('donation_info');
                    }
                    $smstext = "Om Sai Ram !!\nSai Palkhi Samiti would like to extend heartfelt thanks and blessings from Sai Baba to the donors.\n- Sai Palkhi Samiti, Pune";
                } else if ($order_status === "Aborted") {
                    $smstext = "Om Sai Ram !!\nExtremely sorry.\nSomething went wrong.\nYour transation has been Aborted.\nKindly try again or use alternate payment option\n- Sai Palkhi Samiti";
                } else if ($order_status === "Failure") {
                    $smstext = "Om Sai Ram !!\nExtremely sorry.\nSomething went wrong.\nYour transation has been declined.\nKindly try again or use alternate payment option\n- Sai Palkhi Samiti";
                } else {
                    $smstext = "Om Sai Ram !!\nExtremely sorry.\nSecurity Error\nIllegal access detected\n- Sai Palkhi Samiti";
                }
                $sms_send_status = send_sms($donor_phone_no, $smstext);
                $data['payment_status'] = $order_status;
                $data['dataHeader']['title'] = 'Thank you';
                $this->template->set_master_template('frontend_template.php');
                $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
                $this->template->write_view('content', 'Donation/donation_success', (isset($this->data) ? $this->data : NULL), TRUE);
                $this->template->write_view('footer', 'frontend/footer', '', TRUE);
                $this->template->render();
            }
        } else {
            redirect('donation');
        }
    }

    public function donation_success() {
        $data['payment_status'] = $this->session->userdata('donation');
        $data['dataHeader']['title'] = 'Thank you';
        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'Donation/donation_success', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }

    public function get_state() {
        if ($this->input->post('country_id')) {
            $state_list = $this->Donation_model->get_state($this->input->post('country_id'));
            $options = "<option value=''>Select State</option>";
            if (isset($state_list) && !empty($state_list)) {
                foreach ($state_list as $state) {
                    $options.="<option value='" . $state['id'] . "'>" . $state['name'] . "</option>";
                }
            }
            echo $options;
        }
    }

    public function get_city() {
        if ($this->input->post('state_id')) {
            $city_list = $this->Donation_model->get_city($this->input->post('state_id'));
            $options = "<option value=''>Select City</option>";
            if (isset($city_list) && !empty($city_list)) {
                foreach ($city_list as $city) {
                    $options.="<option value='" . $city['id'] . "'>" . $city['name'] . "</option>";
                }
            }
            echo $options;
        }
    }

    public function SendDonationReceipt($data = NULL) {
        
        error_reporting(E_ALL ^ E_WARNING);
        error_reporting(0);
        $pdffile_name = '';

        $webpage = $this->load->view("Donation/donation_receipt", $data, true);
        $pdffile_name = md5(date('Y-m-d H:i:s')) . '.PDF';
        $pdf = $this->m_pdf->load();
        $pdf->allow_charset_conversion = true;
        $pdf->charset_in = 'UTF-8';
        $pdf->SetDirectionality('rtl');
        $pdf->autoLangToFont = true;
        $pdf->SetAutoFont(AUTOFONT_INDIC);
        $pdf->AddPage('P');
        $pdf->WriteHTML($webpage);
//        $pdfdoc = $pdf->Output($pdffile_name, "D");DIE;exit();
        $pdf->Output($pdffile_name, "F");
        $enquirer_name = "Shri Saibaba Palkhi Sohala Samiti, Pune";
        $enquirer_email = "saipalkhipune@gmail.com";
        $subject_title = "Donation Receipt";
        $mail_body = "Om Sai Ram !!,<br><br>Please find the attached document of donation receipt.<br><br><br>Regards,<br>Shri Palkhi Samiti, Pune<br>Call: 9860003639 / 7057451648 / 8600258390";
        $mail = new PHPMailer();
        //$mail->IsSendmail(); // send via SMTP
        $mail->IsSMTP();
        $mail->SMTPDebug = 0;
        //Ask for HTML-friendly debug output
        $mail->Debugoutput = 'html';
        //Set the hostname of the mail server
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->Username = "saipalkhipune@gmail.com"; // SMTP username
        $mail->Password = "Palkhi20!@"; // SMTP password
        $webmaster_email = "saipalkhipune@gmail.com"; //Reply to this email ID
        //$mail->SMTPSecure = 'ssl';
        $mail->Port = "587";
        $mail->Host = 'smtp.gmail.com'; //hostname
//        $receiver_email = $data['email']; #'padamasing.m@mindworx.in';
        $email = $data['email']; // Recipients email ID //inquiry@mindworx.in

        $mail->From = $enquirer_email;
        $mail->FromName = $enquirer_name;
        $mail->AddAddress($email);
        $mail->AddReplyTo($enquirer_email, $enquirer_name);
//        $mail->AddAttachment(RECEIPT_PATH . $pdffile_name);
        $mail->AddAttachment($pdffile_name);
        $mail->WordWrap = 50; // set word wrap
        $mail->IsHTML(false); // send as HTML
        $mail->Subject = $subject_title;
        $mail->MsgHTML($mail_body);
        $mail->AltBody = "This is the body when user views in plain text format"; //Text Body
        if (!$mail->Send()) {
            // echo "Mailer Error: " . $mail->ErrorInfo;
            $a = 0;
        } else {
            if (file_exists($pdffile_name)) {
                unlink($pdffile_name);
            }
            $a = 1;
        }
    }

}

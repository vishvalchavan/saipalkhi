<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Donor_list extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();

        $this->load->library(array('ion_auth', 'form_validation', 'session'));
        $this->load->model(array('Year', 'Donation_model', 'language', 'Ion_auth_model'));
        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('Auth');
    }

    public function index() {
        if (!$this->ion_auth->logged_in()) {
            redirect('admin', 'refresh');
        }
        if (!$this->ion_auth->is_admin()) {
            redirect('admin', 'refresh');
        }
        $user_id = $this->session->userdata('user_id');
        $data['year_list'] = $this->Year->get_year_list();
        $data['donor_list'] = $this->Donation_model->donor_list();
        $data['dataHeader']['title'] = 'Donor List';
        $this->template->set_master_template('template.php');
        $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
        $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
        $this->template->write_view('content', 'donation_list', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'snippets/footer', '', TRUE);
        $this->template->render();
    }

    public function donor_list() {
        if ($this->input->post('year')) {
            $data['donor_list'] = $this->Donation_model->donor_list($this->input->post('year'));
            $view = $this->load->view('ajax/donor_list', $data);
            echo $view;
        }
    }

}

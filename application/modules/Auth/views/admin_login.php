<div class="bg-image d-flex justify-content-center align-items-center container-fluid mb-0">
    <div class="container">
        <div class="row">
            <div class="mt-6">
                <div class="card card-signin my-5">
                    <div class="card-body">

                        <h5 class="card-title text-center"><img src="<?php echo base_url(LOGOIMAGE) ?>" class="img-responsive"></h5>
                        <?php echo form_open("admin", array('id' => 'admin_login_form', 'class' => 'form-signin')); ?>  

                        <input type='hidden' name='usertype' value='admin'>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <?php if ($this->session->flashdata('error_msg')) { ?>
                                <div class="error-msg col-md-10">
                                    <?php echo $this->session->flashdata('error_msg'); ?>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('success_msg')) { ?>
                                <div class="success-msg col-md-10">
                                    <?php echo $this->session->flashdata('success_msg'); ?>
                                </div>
                            <?php } ?>
                        </div>                       
                        <div class="form-label-group">
                            <?php echo form_input($identity); ?>
                            <div class="lgnErorr1 error-msg"></div>
                        </div>

                        <div class="form-label-group">
                            <?php echo form_input($password); ?>
                            <div class="lgnErorr2 error-msg"></div>
                        </div>

                        <div class="custom-control custom-checkbox mb-3">
                            <input type="checkbox" class="custom-control-input" id="remember" name="remember">
                            <label class="custom-control-label font-weight-500 checkmrgin" for="remember">Remember password</label>
                            <label class="float-right font-weight-500"> <a href="<?php echo base_url() ?>admin/forgot_password">Forgot password ?</a></label>
                        </div>
                        <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Login</button>
                        <div class="my-4"></div>
                        <?php echo form_close(); ?>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
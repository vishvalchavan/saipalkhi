<div class="conatiner-fluid">
    <div class="bg-image d-flex justify-content-center align-items-center container-fluid mb-0">
        <div class="login-form">
            <h1 class="text-center mb-2">Reset Password</h1>

            <!--<div id="infoMessage"><?php echo isset($message) ? $message : ''; ?></div>-->
            <?php echo form_open("forgot_password", array('id' => 'user_forgot_form', 'class' => 'user_forgot_form', 'name' => 'user_forgot_form', 'autocomplete' => "off")); ?>  
            <span id="reauth-email" class="reauth-email"></span>
            <?php if ($this->session->flashdata('error_msg1')) { ?>
                <div class="error-msg">
                    <?php echo $this->session->flashdata('error_msg1'); ?>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('success_msg1')) { ?>
                <div class="success-msg">
                    <?php echo $this->session->flashdata('success_msg1'); ?>
                </div>
            <?php } ?>
            <div class="form-group">
                <input type="text" id="identity" name="identity" class="form-control" placeholder="Mobile Number" data-error='.lgnErorr1' required autofocus value="<?php echo set_value('identity'); ?>" >
                <div class="input-field">
                    <div class="lgnErorr1 error-msg"></div>
                    <?php echo form_error('identity'); ?>
                </div> 
            </div>
            <div class="form-group">
                <input type="text" id="dob" name="dob" class="form-control datepicker" placeholder="Date of Birth" data-error='.lgnErorr2' required autofocus value="<?php echo set_value('dob') ?>" >

                <div class="input-field">
                    <div class="lgnErorr2 error-msg"></div>
                    <?php echo form_error('dob'); ?>
                </div>

            </div>
            <div class="form-group">
                <input type="password" id="password" name="password" class="form-control" placeholder="Password" data-error='.lgnErorr3' required value="<?php echo set_value('password') ?>" >
                <div class="input-field">
                    <div class="lgnErorr3 error-msg"></div>            
                    <?php echo form_error('password'); ?>
                </div>
            </div>
            <div class="form-group">
                <input type="password" id="confirmpassword" name="confirmpassword" class="form-control" placeholder="Confirm Password" data-error='.lgnErorr4' value="<?php echo set_value('confirmpassword') ?>" required >

                <div class="input-field">
                    <div class="lgnErorr4 error-msg"></div>
                    <?php echo form_error('confirmpassword'); ?>
                </div> 
            </div>
            <button class="btn btn-lg btn-primary btn-block btn-signin" name="submit_form" type="submit">Submit</button>
            <br>
            <a href="<?php echo base_url() ?>login" class="float-right login-link">
                Click here to login
            </a> 
            <?php echo form_close(); ?>

        </div><!-- /card-container -->
    </div><!-- /container -->
</div>
</div>    
<script>
    $(document).ready(function () {
        $(".alert").fadeOut(3000);

    });

</script>

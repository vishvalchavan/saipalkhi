  <div class="bg-image">
      <div class="container">
         <div class="row ml-10">
            <div class="mt-10">
               <div class="card card-signin my-5">
                  <div class="card-body">
                      <h5 class="card-title text-center"><img src="<?php echo base_url('assets/images/logo1.png') ?>" class="img-responsive"></h5>
                      <h1><?php echo lang('change_password_heading');?></h1>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("auth/change_password",array('id' => 'changepassword_form', 'class' => 'form-signin'));?>

      <p>
            <?php echo lang('change_password_old_password_label', 'old_password');?> <br />
            <?php echo form_input($old_password);?>
      </p>

      <p>
            <label for="new_password"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label> <br />
            <?php echo form_input($new_password);?>
      </p>

      <p>
            <?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?> <br />
            <?php echo form_input($new_password_confirm);?>
      </p>

      <?php echo form_input($user_id);?>
      <p><?php echo form_submit('submit', lang('change_password_submit_btn'));?></p>

<?php echo form_close();?>
</div>
               </div>
            </div>
         </div>
      </div>
   </div>


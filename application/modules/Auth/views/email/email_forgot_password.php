<!DOCTYPE html>
<html lang="en">
   <head>
   
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Forget-Password</title>
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">     
      
      </head>
     <body>
      <style type="text/css">
    .recover-submit{
          background: #55afba;
          border: #55afba;
              font-size: 80%;
    border-radius: 5rem;
    letter-spacing: .1rem;
    font-weight: bold;
    padding: 1rem;
    transition: all 0.2s;
        }
    </style>
     <center>
      <div id="formWrapper">
      <div id="form-forget">
      <div class="row">
             <div class="logo1">
               <!--<img src="http://saibabapalkhipune.org/beta/assets/images/logo-admin.png" class="img-responsive">-->
               <img src="<?php echo base_url(LOGOIMAGE);?>" class="img-responsive">
            </div>
            <div class="form-gap"></div>
   <div class="container-fluid">
   <div class="row">
      <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="text-center">
                  <h3><i class="fa fa-lock fa-4x"></i></h3>
                  <h2 class="text-center">Forgot Password?</h2>
                  <p>You can reset your password here.</p>
                  <div class="panel-body">    
                    <form id="register-form" role="form" autocomplete="off" class="form" method="post">                          
                      <div class="form-group">
                        <b>Hi '<?php echo $username ?>' </b><br>You recently requested to reset your password for Shri Sai Baba Palakhi account.<br>Click the button to reset it
                                Forgot Password <br><a target="blank" title="open in Google Chrome" href="<?php echo $password_link;?>">
                                    <input type="button" class="btn btn-lg btn-primary btn-block " value="Reset your password" style="padding: 10px;
    background: #f58734;
    color: #fff;
    border: 1px solid #f58734;
    font-weight: bold;"></a><br>
                     If you did not request a password reset please ignore this email or reply to let us know.<br>
                     This password reset link is only valid for the next 10 minutes.<br>
                     Thanks.<br>
                     Sai Baba Palakhi Team  
                     <br>
                     <hr>
                     If you are having trouble clicking the password reset the button,copy and paste the URL below into your web browser
                     <br>                       
                     <p title="open in Google Chrome"><?php echo $password_link;?></p>
                      </div>
                    </form>
    
                  </div>
                </div>
              </div>
            </div>
          </div>
   </div>
</div>
</div>
</div>
</div></center>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
    <head> 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Payment Receipt</title>
        <link rel="stylesheet" href="<?php base_url(); ?>assets/css/paper.css"  type="text/css" media="print">
        <style>
            body{
                width:100%;
            }
            .bg-background{
                background: url(<?php echo base_url(BGIMAGE); ?>) no-repeat 4px 0px;
                height: 600px;
                background-size: contain;
                position: relative;
                max-width: 100%;
                width: 100%;
            }
            @page {

                margin-top:0mm; 
                margin-left: 0mm; margin-right: 1mm;
            }


            @media print {
                .extradiv {
                    margin-bottom: 97px;
                    margin-left:200px;
                }
                .printdiv{
                    margin-top:0mm; 
                    margin-left: 0mm; margin-right: 0mm;

                }

            }
        </style>
    <body class="A4">

  <!--<section class="sheet padding-10mm">-->
        <!-- Write HTML just like a web page -->
        <!--<article>-->
        <?php $nbsp = "&nbsp;"; ?>

        <div class="bg-background" style='page-break-after:avoid;'>
            <div class="extradiv"><br><br><br>

            </div>

            <div class="printdiv" style="position:absolute;top:188px;margin-left: 250px;font-size: 12px;">                        
                <div style="text-align:left;margin-left: 30px;margin-top:23px;">
                    <span style=""><?php echo ($dpn_no) ? ": " . $nbsp . $dpn_no : ':'; ?> </span>
                    <div style="margin-left: 270px;margin-top:-16px;"> <span style=""><?php echo ($pay_date) ? ": " . $nbsp . date("d-m-Y", strtotime($pay_date)) : ':'; ?></span>
                    </div>
                </div>
                <div class="" style="position:absolute;top:8px;"></div>

                <div style="width:100%">
                    <div style="text-align:left;margin-left:65px;margin-top:23px;">

                        <span id="rfullname_div" style=" padding-bottom: 10px;height:15px;">
                            <?php echo ($name) ? ": " . $nbsp . $name : ':'; ?></span>
                    </div>
                </div>

                <div style="width:100%">
                    <div style="text-align:left;margin-top:7px;margin-left:0px;">

                        <span id="rfullname_div" style=" padding-bottom: 10px;height:15px;">
                            <?php echo ($address) ? ": " . $nbsp . wordwrap($address, 65, "<br/>") : ':';
                            if(strlen($address) < 65){ echo "<br><br>";}
                            ?></span>
                    </div>
                </div>
                <div style="width:100%">
                    <div style="position: absolute;text-align:left;margin-top:15px;margin-left:30px;">

                        <span id="rfullname_div" style=" padding-bottom: 10px;height:15px;">
                            <?php echo ($amount_in_word) ? ": " . $nbsp . $amount_in_word : ':'; ?></span>
                    </div>
                </div>

                <div style="width:100%">
                    <div style="text-align:left;margin-top:5px;margin-left:50px;">

                        <span id="rfullname_div" style=" padding-bottom: 10px;height:15px;">
                            <?php echo ($payment_mode) ? ": " . $nbsp . $payment_mode : ':'; ?></span>
                    </div>
                </div>                


                <div style="width:100%">
                    <div style="text-align:left;margin-top:-13px;margin-left:274px;">

                        <span id="rfullname_div" style=" padding-bottom: 10px;height:15px;">
                            <?php echo ($amount) ? $amount . " /-" : ''; ?></span>
                    </div>
                </div>      


            </div>
        </div>

        <!--</article>-->

        <!--</section>-->

    </body>

</html>
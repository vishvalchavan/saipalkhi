<div class="bg-image">
    <div class="container-fluid">
        <div class="row">
            <div class="mt-10">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center"><img src="<?php echo base_url('assets/images/logo1.png') ?>" class="img-responsive"></h5>
                        <?php echo form_open("auth/login", array('id' => 'login_form', 'class' => 'form-signin')); ?>  
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="error-msg col-md-10">
                                <?php echo $this->session->flashdata('error_msg'); ?>
                            </div>
                        </div>
                        <div class="form-label-group">
                            <?php echo form_input($identity); ?>
                            <div class="lgnErorr1 error-msg"></div>
                        </div>

                        <div class="form-label-group">
                            <?php echo form_input($password); ?>
                            <div class="lgnErorr2 error-msg"></div>
                        </div>

                        <div class="custom-control custom-checkbox mb-3">
                            <input type="checkbox" class="custom-control-input" id="remember" name="remember">
                            <label class="custom-control-label font-weight-500" for="remember">Remember password</label>
                            <a href="forget-password.html"><label class="float-right font-weight-500">Forget password ?</label></a>
                        </div>
                        <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Login</button>
                        <div class="my-4"></div>
                        <?php echo form_close(); ?>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

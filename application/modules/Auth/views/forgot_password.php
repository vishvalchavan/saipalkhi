<div class="bg-image">
    <div class="container-fluid">
        <div class="row">
            <div class="mt-10">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center"><img src="<?php echo base_url(LOGOIMAGE) ?>" class="img-responsive"></h5>
                        <div class="text-center">
                            <h4><i class="fa fa-lock fa-2x"></i></h4>
                            <h3>Forgot Password?</h3>
                            <p>You can reset your password here.</p>
                        </div>
                        <h5><?php lang('forgot_password_heading'); ?></h5>
                        <p><?php sprintf(lang('forgot_password_subheading'), $identity_label); ?></p>

                        <div id="infoMessage"><?php echo $message; ?></div>

                        <?php echo form_open("admin/forgot_password", array('id' => 'login_form', 'class' => 'form-signin')); ?>                          
                        <div class="form-label-group">                            
                            <?php echo form_input($identity); ?>
                        </div>


                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="error-msg col-md-10">
                                <?php echo $this->session->flashdata('error_msg'); ?>
                            </div>
                        </div>
                        <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">submit</button>
                        <?php // echo form_submit('submit', lang('forgot_password_submit_btn'));?>
                        <div class="row">
                            <div class="col-md-1"></div>                                                       
                            <label class="font-weight-500"> <a href="<?php echo base_url() ?>admin" cla
                                                               ss="login-link">Click here to login</a></label>
                        </div>

                        <div class="my-4"></div>
                        <?php echo form_close(); ?> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

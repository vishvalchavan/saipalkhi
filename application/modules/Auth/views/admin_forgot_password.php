<div class="bg-image">
    <div class="container">
        <div class="row">
            <div class="mt-10">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center"><img src="<?php echo base_url(LOGOIMAGE);?>" class="img-responsive"></h5>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <?php if ($this->session->flashdata('error_msg')) { ?>
                                <div class="error-msg col-md-10">
                                    <?php echo $this->session->flashdata('error_msg'); ?>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('success_msg')) { ?>
                                <div class="success-msg col-md-10">
                                    <?php echo $this->session->flashdata('success_msg'); ?>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('note_msg')) { ?>
                                <div class="note-msg col-md-10">
                                    <?php echo $this->session->flashdata('note_msg'); ?>
                                </div>
                            <?php } ?>
                        </div>
                        <!--                        <div class="row">
                                                    <div class="col-md-1"></div>
                                                    <div class="alert alert-danger fade in alert-dismissible show" id="error_message" style="display: none;">
                                                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                                                        <strong>Error !</strong> <?php
                        if ($this->session->flashdata('error_msg')) {
                            echo $this->session->flashdata('error_msg');
                        }
                        ?>
                                                    </div>
                        
                                                    <div class="alert alert-success fade in alert-dismissible show" id="success_message" style="display: none;">
                                                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                                                        <strong>Success!</strong> <?php
                        if ($this->session->flashdata('success_msg')) {
                            echo $this->session->flashdata('success_msg');
                        }
                        ?>
                                                    </div>
                                                    <div class="alert alert-info fade in alert-dismissible show" id="note_message" style="display: none;">
                                                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                                                        <strong>Note!</strong> <?php
                        if ($this->session->flashdata('note_msg')) {
                            echo $this->session->flashdata('note_msg');
                        }
                        ?>
                                                    </div>
                                                    <div class="error-msg col-md-10">
                        <?php // echo $this->session->flashdata('error_message'); ?>
                                                    </div>
                                                </div>-->
                        <div class="text-center">
                            <h4><i class="fa fa-lock fa-2x"></i></h4>
                            <h3>Forgot Password?</h3>
                            <p>You can reset your password here.</p>
                        </div>
                        <!--<form class="form-signin">-->
                        <?php echo form_open("admin/forgot_password", array('id' => 'forgot_form', 'class' => 'form-signin')); ?> 
                        <div class="form-label-group">
                            <input type="text" id="inputEmail" name="inputEmail" class="form-control1" placeholder="Email ID" required autofocus data-error=".error1">
                            <div class="error1 error-msg"></div>
                        </div>
                        <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Submit</button>
                        <hr class="my-4">
                        <label class="float-right"><a href="<?php echo base_url() ?>admin" class="login-link">Click here to login</a></label> 
                        <?php echo form_close(); ?>
                        <!--</form>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

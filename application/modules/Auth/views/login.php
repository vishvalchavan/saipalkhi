<style>
    .rgba-gradient {
        background: -webkit-linear-gradient(45deg, rgba(47, 42, 42, 0.7), rgba(0, 0, 255, 0.1) 100%);
        background: -webkit-gradient(linear, 45deg, from(rgba(53, 34, 31), #34211f 100%));
        background: linear-gradient(to 45deg, rgba(53, 34, 31), #34211f 100%);
        height: 100vh;
    }
    .card {
        background-color:#75666a;
        border-radius: 10px;        
    }

    .md-form label {
        color: #ffffff;
    }

    h6 {
        line-height: 1.7;
    }
</style>
<div class="view">
    <!-- Mask & flexbox options-->       
    <div class="mask rgba-gradient align-items-center">
        <div id="overviews" class="section">
            <!-- Content -->
            <div class="container">
                <!--Grid row-->
                <div class="row mt-5">
                    <!--Grid column-->
                    <div class="col-md-4 col-xl-4 mb-4 mt-5">
                        <!--Form-->
                        <div class="card wow fadeInRight" data-wow-delay="0.3s">
                            <div class="card-body">
                                <!--Header-->
                                <div class="col-lg-10 col-xl-9">
                                    <?php echo $this->session->flashdata('signupmsg'); ?>
                                </div>
                                <div class="text-center">
                                    <!--<h3 class="white-text"></h3>-->
                                    <h1 class="text-center mb-2 text-white">Login</h1>
                                    <hr class="hr-light">
                                    <?php echo form_open("login", array('id' => 'login_form', 'autocomplete' => "off")); ?>  
                                    <span id="reauth-email" class="reauth-email"></span>
                                    <?php if ($this->session->flashdata('error_msg1')) { ?>
                                        <div class="error-msg col-md-10">
                                            <?php echo $this->session->flashdata('error_msg1'); ?>
                                        </div>
                                    <?php } ?>
                                    <?php if ($this->session->flashdata('success_msg1')) { ?>
                                        <div class="success-msg col-md-10">
                                            <?php echo $this->session->flashdata('success_msg1'); ?>
                                        </div>
                                    <?php } ?>
                                    <input type='hidden' name='usertype' value='user'>
                                    <!--</div>-->
                                    <!--Body-->
                                    <div class="form-group">
                                        <input type="text" id="identity" name="identity" class="form-control" placeholder="Mobile Number" data-error='.lgnErorr1' required autofocus <?php echo isset($mobile_no) ? 'value="' . $mobile_no . '" readonly' : ''; ?>>
                                        <div class="lgnErorr1 error-msg"></div>     
                                    </div>
                                    <div class="form-group">
                                        <input type="text" id="dob" name="dob" class="dp-color dobreadonly form-control  <?php echo isset($birth_date) ? '' : 'datepicker' ?>" placeholder="Date of Birth" data-error='.lgnErorr2' required autofocus <?php echo isset($birth_date) ? 'value="' . date("d-m-Y", strtotime($birth_date)) . '" readonly' : ''; ?> readonly>
                                        <div class="lgnErorr2 error-msg"></div>
                                    </div>
                                    <div class=form-group">
                                        <input type="password" id="password" name="password" class="form-control" placeholder="Password" data-error='.lgnErorr3' required>
                                        <div class="lgnErorr3 error-msg"></div>
                                    </div>
                                    <br>
                                    <div id="remember" class="checkbox float-left mb-2 text-white float-left">
                                        <label>
                                            <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"'); ?><label for="remember">Remember Me</label>
                                        </label>
                                    </div>
                                    <a href="<?php echo base_url() ?>forgot_password" class="forgot-password float-right">
                                        Forgot password?
                                    </a>

                                    <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
                                    <?php echo form_close(); ?>
                                    <br>
                                    <div class="justify-content-center links">
                                        <span class="font-size-10 text-white">If you are not registered user &nbsp;</span><a href="<?php echo base_url() . 'signup'; ?>" class="forgot-password float-right">Sign up Here?</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-xl-1 mt-5"></div>
                    <div class="col-md-7 col-xl-7 mb-4 mobile-margin-top mt-5">
                        <h2 class="card-title text-left mb-0" style="color:#f58834">जुन्या पदयात्रींसाठी विशेष सूचना  </h2>
                        <div class="mt-2 text-white">
                            सन २०१६/१७/१८ मध्ये श्रीसाईपालखीसोहळ्यात सहभागी झालेल्या पदयात्रीने स्वतःचा मोबाईलनंबर, जन्मदिनांक व sairam पासवर्ड भरून अकाऊंट चालू करावे. 
                        </div>
                        <ul class="pl-0 mangal">
                            <li class="bullet-none wow fadeInRight animated mt-4 text-white" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold" style="font-size:20px;">&#9672;</small>&nbsp;अकाउंट मध्ये प्रवेश केल्यावर, आपली राहिलेली/अर्धवट (उदा. पत्ता, पिनकोड)  माहिती पूर्ण भरून घ्यावी. </li>
                            <li class="bullet-none wow fadeInRight animated text-white" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold" style="font-size:20px;">&#9672;</small>&nbsp;अकाउंट मध्ये स्वतःचा प्रोफाइल फोटो सुद्धा अपलोड करू शकता.</li>
                            <li class="bullet-none wow fadeInRight animated text-white" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold" style="font-size:20px;">&#9672;</small>&nbsp; साईपालखी मध्ये सहभागी होण्यासाठी "Enrolment for Palkhi 2019" वर क्लिक करा. </li>
                            <li class="bullet-none wow fadeInRight animated text-white" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold" style="font-size:20px;">&#9672;</small>&nbsp;"Payment Option" मध्ये "Online" निवडून, नाव नोंदणी फी भरावी. </li>
                            <li class="bullet-none wow fadeInRight animated text-white" style="visibility: visible; animation-name: fadeInRight;"> <small class="font-14-bold" style="font-size:20px;">&#9672;</small>&nbsp;आपला व्यवहार यशस्वी झाल्यानंतर, आपणास समितीकडून एक मेसेज मिळेल. </li>
                            <li class="bullet-none wow fadeInRight animated mb-2 text-white" style="visibility: visible; animation-name: fadeInRight;"><small class="font-14-bold" style="font-size:20px;">&#9672;</small>&nbsp;५ जुलै पासून सुरु होणाऱ्या पालखी सोहळ्यात आपण नाव सांगून आपले आय-कार्ड घ्यावे.</li>
                            <li class="bullet-none wow fadeInRight animated mb-2 text-white" style="visibility: visible; animation-name: fadeInRight;"> 
                                <div class="tip" style="background: #5a4637;">
                                    टीप - श्री साईबाबा पालखी समितीने ऑनलाईनपेमेंटसाठी "CC Avenue" या प्रसिद्ध कंपनी सोबत करारकेला आहे, ज्याणें करून आपल्याला घरबसल्या, कोणत्याही शहरातून, साईपालखी साठी नाव नोंदणी अथवा ऑनलाईन देणगीचे व्यवहार अतिशय सुरक्षित प्रमाणे करण्यात येईल.
                                </div>
                            </li>
                            <li class="bullet-none wow fadeInRight animated text-white" style="visibility: visible; animation-name: fadeInRight;">पदयात्री नाव नोंदणी करताना कोणतीही अडचण आल्यास, कृपया पुढे दिलेल्या सेवेकऱ्यांना फोन करा.<br><b>रश्मी झपके – ९८६०००३६३९ </b>|<b> अमेय कालेकर - ९९२२७५०९४० </b>|<b> केदार मेथे – ८६००२५८३९०</b> | <b>अक्षय बाजारे - ९७६६८५१५८६</b></li>
                        </ul>
                    </div>
                    <!--Grid column-->
                    <!--Grid column-->

                    <!--/.Form-->
                </div>
                <!--Grid column-->
            </div>
            <!--Grid row-->
        </div>
        <!-- Content -->
    </div>
    <!-- Mask & flexbox options-->
</div>
<!-- Full Page Intro -->
<!-- Main navigation -->

<!-- <div class="bg-image" style="height: auto !important; "> -->
<!-- <div class="mobile-wrapper">
    <div class="container mt-5" >
        <div id="overviews" class="section">
            <div class="row bg-white ">
                <div class="col-lg-5">
                    <div class="col-lg-10 col-xl-9">
<?php echo $this->session->flashdata('signupmsg'); ?>
                    </div>
                     <div class="">
                       <div class="card card-signin flex-row  ">
                       <div class="card-body">
                       <div class="userlogin">
            <h1 class="text-center mb-2">Login</h1>
<?php echo form_open("login", array('id' => 'login_form', 'autocomplete' => "off")); ?>  
            <span id="reauth-email" class="reauth-email"></span>
<?php if ($this->session->flashdata('error_msg1')) { ?>
                                                                        <div class="error-msg col-md-10">
    <?php echo $this->session->flashdata('error_msg1'); ?>
                                                                        </div>
<?php } ?>
<?php if ($this->session->flashdata('success_msg1')) { ?>
                                                                        <div class="success-msg col-md-10">
    <?php echo $this->session->flashdata('success_msg1'); ?>
                                                                        </div>
<?php } ?>
            <input type='hidden' name='usertype' value='user'>
            <div class="form-group">
                <input type="text" id="identity" name="identity" class="form-control" placeholder="Mobile Number" data-error='.lgnErorr1' required autofocus <?php echo isset($mobile_no) ? 'value="' . $mobile_no . '" readonly' : ''; ?>>
                <div class="lgnErorr1 error-msg"></div>
            </div>
            <div class="form-group">
                <input type="text" id="dob" name="dob" class="dp-color dobreadonly form-control  <?php echo isset($birth_date) ? '' : 'datepicker' ?>" placeholder="Date of Birth" data-error='.lgnErorr2' required autofocus <?php echo isset($birth_date) ? 'value="' . date("d-m-Y", strtotime($birth_date)) . '" readonly' : ''; ?> readonly>
                <div class="lgnErorr2 error-msg"></div>
            </div>
            <div class="form-group">
                <input type="password" id="password" name="password" class="form-control" placeholder="Password" data-error='.lgnErorr3' required>
                <div class="lgnErorr3 error-msg"></div>
            </div>
            <div id="remember" class="checkbox float-left mb-2">
                <label>
<?php echo form_checkbox('remember', '1', FALSE, 'id="remember"'); ?><label for="remember">Remember Me</label>
                </label>
            </div>
            <a href="<?php echo base_url() ?>forgot_password" class="forgot-password float-right">
                Forgot password?
            </a>

            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
<?php echo form_close(); ?>

            <div class="d-flex justify-content-center links">
                <span class="font-size-10">If you are not registered user &nbsp;</span><a href="<?php echo base_url() . 'signup'; ?>" class="forgot-password">Sign up Here?</a>
=======
                </div>

>>>>>>> 1dcbc023c70947845f7be789ea9c841b787f80c8
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".alert").fadeOut(3000);

    });

</script><?php
$this->session->set_flashdata('error_msg', "");
$this->session->set_flashdata('error_msg1', "");
$this->session->set_flashdata('message', "");
$this->session->set_flashdata('success_message', "");
$this->session->set_flashdata('error_message', "");
$this->session->set_flashdata('success_msg1', "");
?>
<div class="conatiner-fluid">
    <div class="bg-image d-flex justify-content-center align-items-center container-fluid mb-0">

        <div class="login-form userlogin">
            <h1 class="text-center mb-2">Login</h1>
<?php echo form_open("login", array('id' => 'login_form', 'autocomplete' => "off")); ?>  
            <span id="reauth-email" class="reauth-email"></span>
<?php if ($this->session->flashdata('error_msg1')) { ?>
                                                                        <div class="error-msg col-md-10">
    <?php echo $this->session->flashdata('error_msg1'); ?>
                                                                        </div>
<?php } ?>
<?php if ($this->session->flashdata('success_msg1')) { ?>
                                                                        <div class="success-msg col-md-10">
    <?php echo $this->session->flashdata('success_msg1'); ?>
                                                                        </div>
<?php } ?>
            <input type='hidden' name='usertype' value='user'>
            <div class="form-group">
                <input type="text" id="identity" name="identity" class="form-control" placeholder="Mobile Number" data-error='.lgnErorr1' required autofocus <?php echo isset($mobile_no) ? 'value="' . $mobile_no . '" readonly' : ''; ?>>
                <div class="lgnErorr1 error-msg"></div>
            </div>
            <div class="form-group">
                <input type="text" id="dob" name="dob" class="dp-color dobreadonly form-control  <?php echo isset($birth_date) ? '' : 'datepicker' ?>" placeholder="Date of Birth" data-error='.lgnErorr2' required autofocus <?php echo isset($birth_date) ? 'value="' . date("d-m-Y", strtotime($birth_date)) . '" readonly' : ''; ?> readonly>
                <div class="lgnErorr2 error-msg"></div>
            </div>
            <div class="form-group">
                <input type="password" id="password" name="password" class="form-control" placeholder="Password" data-error='.lgnErorr3' required>
                <div class="lgnErorr3 error-msg"></div>
            </div>
            <div id="remember" class="checkbox float-left mb-2">
                <label>
<?php echo form_checkbox('remember', '1', FALSE, 'id="remember"'); ?><label for="remember">Remember Me</label>
                </label>
            </div>
            <a href="<?php echo base_url() ?>forgot_password" class="forgot-password float-right">
                Forgot password?
            </a>

            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
<?php echo form_close(); ?>

            <div class="d-flex justify-content-center links">
                <span class="font-size-10">If you are not registered user &nbsp;</span><a href="<?php echo base_url() . 'signup'; ?>" class="forgot-password">Sign up Here?</a>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".alert").fadeOut(3000);

    });

</script><?php
$this->session->set_flashdata('error_msg', "");
$this->session->set_flashdata('error_msg1', "");
$this->session->set_flashdata('message', "");
$this->session->set_flashdata('success_message', "");
$this->session->set_flashdata('error_message', "");
$this->session->set_flashdata('success_msg1', "");
?> -->
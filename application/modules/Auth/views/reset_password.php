<div class="bg-image">
    <div class="container-fluid">
        <div class="row">
            <div class="mt-10">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center"><img src="<?php echo base_url(LOGOIMAGE) ?>" class="img-responsive"></h5>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="error-msg col-md-10">
                                <?php echo $this->session->flashdata('error_msg'); ?>
                            </div>
                        </div>
                        <h3 class="text-center mb-4"><?php echo lang('reset_password_heading'); ?></h3>

                        <div id="infoMessage"><?php echo $message; ?></div>

                        <?php echo form_open('admin/reset_password/' . $code, array('id' => 'reset_password_form', 'name' => 'reset_password_form', 'class' => 'form-signin')); ?>

                        <div class="form-label-group mb-4">
                        <!-- 	<label for="new_password" class="p-0"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length); ?></label> <br /> -->
                            <?php echo form_input($new_password); ?>
                            <div class="error1 error-msg"></div>
                        </div>

                        <div class="form-label-group mb-4">
                        <!-- 		<?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm'); ?> <br />
                            -->		<?php echo form_input($new_password_confirm); ?>
                            <div class="error2 error-msg"></div>
                        </div>

                        <?php echo form_input($user_id); ?>
                        <?php echo form_hidden($csrf); ?>

                        <div class="text-center button-reset">
                            <!-- // <?php form_submit('submit', lang('reset_password_submit_btn')); ?> -->
                            <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Change</button>
                        </div>

                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

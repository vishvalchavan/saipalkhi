<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

    public function __construct() {
        parent::__construct();


        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation', 'PHPMailer', 'SMTP'));
        $this->load->model(array('users', 'devotee', 'language'));
        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('Auth');
    }

    // redirect if needed, otherwise display the user list
    public function index() {

        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('login', 'refresh');
        } elseif (!$this->ion_auth->is_admin()) { // remove this elseif if you want to enable this for non-admins
            // redirect them to the home page because they must be an administrator to view this
            return show_error('You must be an administrator to view this page.');
        } else {
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('success_msg');

            //list the users
//            $this->data['users'] = $this->ion_auth->users()->result();
            //$this->data['users'] = $this->users->get_users();

            $user_id = $this->session->userdata('user_id');
            $data['dataHeader'] = $this->devotee->get_allData($user_id);

            $this->template->set_master_template('template.php');
            $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
            $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
            $this->template->write_view('content', 'index', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'snippets/footer', '', TRUE);
            $this->template->render();
        }
    }

    // log the user in
    public function login() {
        if ($this->ion_auth->logged_in()) {
            if ($this->ion_auth->is_admin()) {
                $this->ion_auth->logout();
            } else {
                redirect('home', 'refresh');
            }
        }
        $this->data['title'] = $this->lang->line('login_heading');

        //validate form input
        $this->form_validation->set_rules('dob', 'Date Of Birth', 'required');
        $this->form_validation->set_rules('identity', 'Mobile No', 'trim|required|numeric|min_length[10]|max_length[10]');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == true) {
            // check to see if the user is logging in
            // check for "remember me"
            $remember = (bool) $this->input->post('remember');
            if ($this->input->post('dob')) {
                $dob = date('Y-m-d', strtotime($this->input->post('dob')));
                $this->session->set_userdata('user_type', 'user');
            }
            if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $dob, $remember)) {
                //if the login is successful
                //redirect them back to the home page
                $current_user_id = $this->ion_auth->get_user_id();

                $data['users'] = $this->ion_auth->user($current_user_id)->result();
                $data['group'] = $this->devotee->get_group_name($current_user_id);

                if (isset($data['group']) && !empty($data['group'])) {
                    $group_name = $data['group'][0]['name'];

                    if ($this->ion_auth->in_group($group_name) && $data['users'][0]->active == '1') {
                        $this->session->set_userdata('user', 'user');
                        redirect('AccountDetails', 'refresh');
                    } else {
                        $redirect = "login";
                        $this->ion_auth->logout();
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                        redirect($redirect, 'refresh');
                    }
                } else {
                    redirect('login', 'refresh');
                }
            } else {
                $this->session->set_flashdata('error_msg1', $this->ion_auth->errors());
                redirect('login', 'refresh');
            }
        } else {
            // the user is not logging in so display the login page
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $this->data['identity'] = array('name' => 'identity',
                'id' => 'identity',
                'type' => 'text',
                'class' => 'form-control1 form-details',
                'data-error' => '.lgnErorr1',
                'autofocus' => 'autofocus',
                'placeholder' => 'Mobile number',
                'value' => $this->form_validation->set_value('identity'),
            );
//            }
            $this->data['password'] = array('name' => 'password',
                'id' => 'password',
                'type' => 'password',
                'class' => 'form-control1',
                'placeholder' => 'Password',
                'data-error' => '.lgnErorr2',
            );
            $data['dataHeader']['title'] = "Login";
            $this->template->set_master_template('frontend_template.php');
            $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
            $this->template->write_view('content', 'login', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'frontend/footer', '', TRUE);
            $this->template->render();
        }
    }

    public function admin_login() {
        if ($this->ion_auth->logged_in()) {
            if ($this->ion_auth->is_admin()) {
                redirect('admin/dashboard', 'refresh');
            } else {
                $this->ion_auth->logout();
            }
        }
        $this->data['title'] = $this->lang->line('login_heading');
        //validate form input
        $this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'required');
        $this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('login_password_label')), 'required');
        if ($this->form_validation->run() == true) {
            // check to see if the user is logging in
            // check for "remember me"
            $remember = (bool) $this->input->post('remember');
            $dob = "";
            $this->session->set_userdata('user_type', 'admin');
            if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $dob, $remember)) {
                //if the login is successful
                //redirect them back to the home page
                $current_user_id = $this->ion_auth->get_user_id();

                $data['users'] = $this->ion_auth->user($current_user_id)->result();
                $data['group'] = $this->devotee->get_group_name($current_user_id);

                if (isset($data['group']) && !empty($data['group'])) {
                    $group_name = $data['group'][0]['name'];

                    if ($this->ion_auth->in_group($group_name) && $data['users'][0]->active == '1') {
                        if ($group_name == 'admin') {
                            $this->session->set_userdata('user', 'admin');
                            redirect('admin/dashboard', 'refresh');
                        }
                    } else {
                        $redirect = "admin";
                        $this->ion_auth->logout();
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                        redirect($redirect, 'refresh');
                    }
                } else {
                    redirect('admin', 'refresh');
                }
            } else {
                $this->session->set_flashdata('error_msg', $this->ion_auth->errors());
                redirect('admin', 'refresh');
            }
        } else {
            // the user is not logging in so display the login page
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $this->data['identity'] = array('name' => 'identity',
                'id' => 'identity',
                'type' => 'email',
                'class' => 'form-control1',
                'data-error' => '.lgnErorr1',
                'autofocus' => 'autofocus',
                'required' => 'required',
                'placeholder' => 'Username',
                'value' => $this->form_validation->set_value('identity'),
            );
            $this->data['password'] = array('name' => 'password',
                'id' => 'password',
                'type' => 'password',
                'class' => 'form-control1',
                'placeholder' => 'Password',
                'data-error' => '.lgnErorr2',
            );
            $data['dataHeader']['title'] = "Login";
            $this->template->set_master_template('login_template.php');
            $this->template->write_view('content', 'admin_login', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'snippets/footer', '', TRUE);
            $this->template->render();
        }
    }

    // log the user out
    public function logout() {
        $this->data['title'] = "Logout";
        $user = $this->session->userdata('user');
        // log the user out
        $logout = $this->ion_auth->logout();
        $this->session->unset_userdata('user_id');
        // redirect them to the login page
        $this->session->set_flashdata('success_msg', $this->ion_auth->messages());
        if ($user == 'admin') {
            redirect('admin', 'refresh');
        } else {
            redirect('login', 'refresh');
        }
    }

    // change password
    public function change_password() {
        $this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
        $this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
        $this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

        if ($this->form_validation->run() == false) {
            // display the form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
            $this->data['old_password'] = array(
                'name' => 'old',
                'id' => 'old',
                'type' => 'password',
            );
            $this->data['new_password'] = array(
                'name' => 'new',
                'id' => 'new',
                'type' => 'password',
                'class' => 'form-control1',
                'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
            );
            $this->data['new_password_confirm'] = array(
                'name' => 'new_confirm',
                'id' => 'new_confirm',
                'type' => 'password',
                'class' => 'form-control1',
                'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
            );
            $this->data['user_id'] = array(
                'name' => 'user_id',
                'id' => 'user_id',
                'type' => 'hidden',
                'value' => $user->id,
            );

            // render
            $this->template->set_master_template('login_template.php');
            $this->template->write_view('content', 'Auth/change_password', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'snippets/footer', '', TRUE);
            $this->template->render();
        } else {
            $identity = $this->session->userdata('identity');

            $change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

            if ($change) {
                //if the password was successfully changed
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                $this->logout();
            } else {
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect('admin/change_password', 'refresh');
            }
        }
    }

    // forgot password
    public function forgot_password() {
        if ($this->config->item('admin_identity', 'ion_auth') != 'email') {
            $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_identity_label'), 'required');
        } else {
            $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
        }
        if ($this->form_validation->run() == false) {
            $this->data['type'] = $this->config->item('admin_identity', 'ion_auth');
            // setup the input
            $this->data['identity'] = array('name' => 'identity',
                'id' => 'identity',
                'placeholder' => 'Enter your email Id',
                'class' => 'form-control browser-default',
                'data-error' => '.errorEmailfield',
            );
            if ($this->config->item('admin_identity', 'ion_auth') != 'email') {
                $this->data['identity_label'] = $this->lang->line('forgot_password_identity_label');
            } else {
                $this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
            }
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $this->template->set_master_template('login_template.php');
            $this->template->write_view('content', 'Auth/forgot_password', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'snippets/footer', '', TRUE);
            $this->template->render();
        } else {
            $identity_column = $this->config->item('admin_identity', 'ion_auth');

            $identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();
            if (empty($identity)) {
                if ($this->config->item('admin_identity', 'ion_auth') != 'email') {
                    $this->ion_auth->set_error('forgot_password_identity_not_found');
                } else {
                    $this->ion_auth->set_error('forgot_password_email_not_found');
                }
                $this->session->set_flashdata('error_message', $this->ion_auth->errors());
                redirect("admin/forgot_password", 'refresh');
            }
            // run the forgotten password method to email an activation code to the user
            $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('admin_identity', 'ion_auth')});
            if ($forgotten) {
                $this->session->set_flashdata('success_message', $this->ion_auth->messages());
                redirect("admin", 'refresh'); //we should display a confirmation page here instead of the login page
            } else {
                $this->session->set_flashdata('error_message', $this->ion_auth->errors());
                redirect("admin/forgot_password", 'refresh');
            }
        }
    }

//-------------- user forgot password----------------------------------------------------------------------------
    public function user_forgot_password() {

        if ($this->config->item('identity', 'ion_auth') != 'email') {
            $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_identity_label'), 'trim|required|numeric|min_length[10]|max_length[10]');
        } else {
            $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
        }
        $this->form_validation->set_rules('dob', 'Date of Birth', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]|max_length[20]');
        $this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'trim|required|matches[password]|min_length[8]|max_length[20]');

        $this->template->set_master_template('frontend_template.php');
        $this->template->write_view('header', 'frontend/header', (isset($data) ? $data : NULL));
        $this->template->write_view('content', 'user_forgot_password', (isset($this->data) ? $this->data : NULL), TRUE);
        if ($this->form_validation->run() == false) {
            $this->data['type'] = $this->config->item('identity', 'ion_auth');

            if ($this->config->item('identity', 'ion_auth') != 'email') {
                $this->data['identity_label'] = $this->lang->line('forgot_password_identity_label');
            } else {
                $this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
            }

            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $this->template->write_view('content', 'user_forgot_password', (isset($this->data) ? $this->data : NULL), TRUE);
        } else {
            $identity_column = $this->config->item('identity', 'ion_auth');
            $identity = $this->ion_auth->email_where($identity_column, $this->input->post('identity'), $_POST['dob'])->users()->row();
            if (empty($identity)) {
                if ($this->config->item('identity', 'ion_auth') != 'email') {
                    $this->ion_auth->set_error('forgot_password_identity_not_found');
                } else {
                    $this->ion_auth->set_error('forgot_password_email_not_found');
                }
                $this->session->set_flashdata('error_msg1', "No record matched with given mobile no and DOB");
                $this->template->write_view('content', 'user_forgot_password', (isset($this->data) ? $this->data : NULL), TRUE);
            } else {
                // run the forgotten password method to email an activation code to the user
                $change = $this->ion_auth->reset_password($identity->mobile, $_POST['confirmpassword'], $identity->password, $identity->id);

                if ($change) {
                    $this->session->set_flashdata('success_msg1', $this->ion_auth->messages());
                    redirect("login", 'refresh'); //we should display a confirmation page here instead of the login page
                } else {
                    $this->session->set_flashdata('error_msg1', $this->ion_auth->errors());
                    redirect("forgot_password", 'refresh');
                }
            }
        }
        $this->template->write_view('footer', 'frontend/footer', '', TRUE);
        $this->template->render();
    }

//--------------end user forgot password----------------------------------------------------------------------------
    // reset password - final step for forgotten password
    public function reset_password($code = NULL) {
        if (!$code) {
            show_404();
        }

        $user = $this->ion_auth->forgotten_password_check($code);
        if (isset($user) && !empty($user)) {

            $passDbDate = date("Y-m-d", strtotime($user->forgotten_password_time));
            $date1 = strtotime($user->forgotten_password_time);
            $currenttime = strtotime(date("Y-m-d H:i:s"));
            $currentdate = date("Y-m-d");
            $delta_T = ($currenttime - $date1);
            $minutes = round(((($delta_T % 604800) % 86400) % 3600) / 60);
            if ($minutes <= 10 && $passDbDate == $currentdate) {
                // if the code is valid then display the password reset form

                $this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'trim|required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]|max_length[20]');
                $this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'trim|required|matches[new]|min_length[8]|max_length[20]');

                if ($this->form_validation->run() == false) {
                    // display the form
                    // set the flash data error message if there is one
                    $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

                    $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
                    $this->data['new_password'] = array(
                        'name' => 'new',
                        'id' => 'new',
                        'type' => 'password',
                        'class' => 'form-control1 mb-2',
                        'placeholder' => 'New Password',
                        'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
                        'data-error' => '.error1'
                    );
                    $this->data['new_password_confirm'] = array(
                        'name' => 'new_confirm',
                        'id' => 'new_confirm',
                        'type' => 'password',
                        'class' => 'form-control1 mb-2',
                        'placeholder' => 'Confirm Password',
                        'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
                        'data-error' => '.error2'
                    );
                    $this->data['user_id'] = array(
                        'name' => 'user_id',
                        'id' => 'user_id',
                        'type' => 'hidden',
                        'value' => $user->id,
                    );
                    $this->data['csrf'] = $this->_get_csrf_nonce();
                    $this->data['code'] = $code;

                    // render
                    $this->template->set_master_template('login_template.php');
                    $this->template->write_view('content', 'Auth/reset_password', (isset($this->data) ? $this->data : NULL), TRUE);
                    $this->template->write_view('footer', 'snippets/footer', '', TRUE);
                    $this->template->render();
                } else {
                    // do we have a valid request?
                    if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id')) {

                        // something fishy might be up
                        $this->ion_auth->clear_forgotten_password_code($code);

                        show_error($this->lang->line('error_csrf'));
                    } else {
                        // finally change the password

                        $identity = $user->{$this->config->item('identity', 'ion_auth')};

                        $change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

                        if ($change) {
                            // if the password was successfully changed
                            $this->session->set_flashdata('success_msg', $this->ion_auth->messages());
                            redirect("admin", 'refresh');
                        } else {
                            $this->session->set_flashdata('error_msg', $this->ion_auth->errors());
                            redirect('admin/reset_password/' . $code, 'refresh');
                        }
                    }
                }
            } else {
                // if the code is invalid then send them back to the forgot password page
                $minute_msg = nl2br("you have cross reset password time limit,\n please try again");
                $this->session->set_flashdata('note_msg', $minute_msg);
                redirect("admin/forgot_password", 'refresh');
            }
        } else {
            // if the code is invalid then send them back to the forgot password page
            $minute_msg = nl2br("invlid request");
            $this->session->set_flashdata('note_msg', $minute_msg);
            redirect("admin/forgot_password", 'refresh');
        }
    }

    // activate the user
    public function activate($id, $code = false) {
        if ($code !== false) {
            $activation = $this->ion_auth->activate($id, $code);
        } else if ($this->ion_auth->is_admin()) {
            $activation = $this->ion_auth->activate($id);
        }

        if ($activation) {
            // redirect them to the auth page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect('admin', 'refresh');
        } else {
            // redirect them to the forgot password page
            $this->session->set_flashdata('message', $this->ion_auth->errors());
            redirect("admin/forgot_password", 'refresh');
        }
    }

    // deactivate the user
    public function deactivate($id = NULL) {
        // if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
        //     // redirect them to the home page because they must be an administrator to view this
        //     return show_error('You must be an administrator to view this page.');
        // }

        $id = (int) $id;

        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirm', $this->lang->line('deactivate_validation_confirm_label'), 'required');
        $this->form_validation->set_rules('id', $this->lang->line('deactivate_validation_user_id_label'), 'required|alpha_numeric');

        if ($this->form_validation->run() == FALSE) {
            // insert csrf check
            $this->data['csrf'] = $this->_get_csrf_nonce();
            $this->data['user'] = $this->ion_auth->user($id)->row();

            $this->_render_page('Auth/deactivate_user', $this->data);
        } else {
            // do we really want to deactivate?
            if ($this->input->post('confirm') == 'yes') {
                // do we have a valid request?
                if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id')) {
                    show_error($this->lang->line('error_csrf'));
                }

                // do we have the right userlevel?
                if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
                    $this->ion_auth->deactivate($id);
                }
            }

            // redirect them back to the auth page
            redirect('admin', 'refresh');
        }
    }

    // create a new user
    public function create_user() {

        $user_id = $this->session->userdata('user_id');
        $data['dataHeader'] = $this->users->get_allData($user_id);
        $this->data['title'] = $this->lang->line('create_user_heading');

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('admin', 'refresh');
        }

        $tables = $this->config->item('tables', 'ion_auth');
        $identity_column = $this->config->item('identity', 'ion_auth');
        $this->data['identity_column'] = $identity_column;

        // validate form input
        $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_firstname_label'), 'required');
        $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_surname_label'), 'required');
        $this->form_validation->set_rules('sa_id_no', $this->lang->line('create_user_validation_saIDNO_label'), 'required|is_unique[' . $tables['users'] . '.sa_id_number]');
        $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        $this->form_validation->set_rules('mobile_no', $this->lang->line('create_user_validation_phone_label'), 'trim|required');
        $this->form_validation->set_rules('bith_date', $this->lang->line('create_user_validation_birthdate_label'), 'required');
        $this->form_validation->set_rules('language', $this->lang->line('create_user_validation_language_label'), 'required');

        if ($this->form_validation->run() == true) {

            $email = strtolower($this->input->post('email'));
            $identity = $email;
            $password = 'password';

            /* Upload profile picture */
            if (isset($_FILES['profile_image']['name']) && $_FILES['profile_image']['name'] != '') {
                $targetDir = "uploads/";
                $fileName = $_FILES['profile_image']['name'];
                $targetFile = $targetDir . $fileName;

                $slug = $this->input->post('name');

                $fileExt = pathinfo($_FILES['profile_image']['name']);
                $dataDocumentDetail['type'] = $fileExt['extension'];


                $uploded_file_path = $this->handleUploadUser($slug);
                if ($uploded_file_path != '')
                    $data['profileimg'] = $slug . '/profile/' . $uploded_file_path;
            }
            $interest_ids = implode(',', $this->input->post('interests'));

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'sa_id_number' => $this->input->post('sa_id_no'),
                'language_id' => $this->input->post('language'),
                'phone' => $this->input->post('mobile_no'),
                'interest_id' => $interest_ids,
                'birth_date' => date('y-m-d', strtotime($this->input->post('bith_date'))),
            );
        }
        if ($this->form_validation->run() == true && $this->ion_auth->register($identity, $password, $email, $additional_data)) {
            // check to see if we are creating the user
            // redirect them back to the admin page
            $response = $this->send_mail($email);
            $message = $this->ion_auth->messages();
            if ($response == 'success') {
                $message.="<br>Email has been sent to: $email";
            } else {
                $message.="<br>Failed to send email to: $email";
            }
            $this->session->set_flashdata('success_msg', $message);
            redirect('admin', 'refresh');
        } else {
            // display the create user form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['first_name'] = array(
                'name' => 'first_name',
                'id' => 'first_name',
                'class' => 'form-control',
                'data-error' => '.regErorr8',
                'autofocus' => 'autofocus',
                'type' => 'text',
                'value' => $this->form_validation->set_value('first_name'),
            );
            $this->data['last_name'] = array(
                'name' => 'last_name',
                'id' => 'last_name',
                'class' => 'form-control',
                'data-error' => '.regErorr1',
                'autofocus' => 'autofocus',
                'type' => 'text',
                'value' => $this->form_validation->set_value('last_name'),
            );
            $this->data['sa_id_no'] = array(
                'name' => 'sa_id_no',
                'id' => 'sa_id_no',
                'class' => 'form-control',
                'data-error' => '.regErorr2',
                'type' => 'text',
                'value' => $this->form_validation->set_value('sa_id_no'),
            );
            $this->data['mobile_no'] = array(
                'name' => 'mobile_no',
                'id' => 'mobile_no',
                'class' => 'form-control',
                'data-error' => '.regErorr3',
                'type' => 'text',
                'value' => $this->form_validation->set_value('mobile_no'),
            );
            $this->data['email'] = array(
                'name' => 'email',
                'id' => 'email',
                'type' => 'text',
                'class' => 'form-control',
                'data-error' => '.regErorr4',
                'value' => $this->form_validation->set_value('email'),
            );
            $this->data['bith_date'] = array(
                'name' => 'bith_date',
                'id' => 'bith_date',
                'class' => 'form-control datepicker',
                'data-error' => '.regErorr5',
                'value' => $this->form_validation->set_value('bith_date'),
            );

            $data['language_list'] = $this->language->get_allLanguage();
            $data['interests_list'] = $this->interests->get_allInterests();

            $this->template->set_master_template('template.php');
            $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
            $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
            $this->template->write_view('content', 'create_user', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'snippets/footer', '', TRUE);
            $this->template->render();
        }
    }

    // edit a user
    public function edit_user($id) {
        $user_id = $this->session->userdata('user_id');
        $data['dataHeader'] = $this->users->get_allData($user_id);
        $this->data['title'] = $this->lang->line('edit_user_heading');

        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id))) {
            redirect('admin', 'refresh');
        }
        $tables = $this->config->item('tables', 'ion_auth');
        $identity_column = $this->config->item('identity', 'ion_auth');
        $this->data['identity_column'] = $identity_column;

//        $user = $this->ion_auth->user($id)->row();
        $user = $this->users->get_user_by_id($id);

        $groups = $this->ion_auth->groups()->result_array();
        $currentGroups = $this->ion_auth->get_users_groups($id)->result();

        // validate form input
        $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_firstname_label'), 'required');
        $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_surname_label'), 'required');
        $this->form_validation->set_rules('sa_id_no', $this->lang->line('create_user_validation_saIDNO_label'), 'required');
        $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
        $this->form_validation->set_rules('mobile_no', $this->lang->line('create_user_validation_phone_label'), 'trim|required');
        $this->form_validation->set_rules('bith_date', $this->lang->line('create_user_validation_birthdate_label'), 'required');
        $this->form_validation->set_rules('language', $this->lang->line('create_user_validation_language_label'), 'required');

        if (isset($_POST) && !empty($_POST)) {
            // do we have a valid request?
            if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id')) {
                show_error($this->lang->line('error_csrf'));
            }

            // update the password if it was posted
            if ($this->input->post('password')) {
                $this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
                $this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
            }

            if ($this->form_validation->run() === TRUE) {
                $interest_ids = implode(',', $this->input->post('interests'));
                $data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'sa_id_number' => $this->input->post('sa_id_no'),
                    'phone' => $this->input->post('mobile_no'),
                    'email' => $this->input->post('email'),
                    'birth_date' => date('y-m-d', strtotime($this->input->post('bith_date'))),
                    'language_id' => $this->input->post('language'),
                    'interest_id' => $interest_ids,
                );

                // update the password if it was posted
                if ($this->input->post('password')) {
                    $data['password'] = $this->input->post('password');
                }

                // Only allow updating groups if user is admin
                if ($this->ion_auth->is_admin()) {
                    //Update the groups user belongs to
                    $groupData = $this->input->post('groups');

                    if (isset($groupData) && !empty($groupData)) {

                        $this->ion_auth->remove_from_group('', $id);

                        foreach ($groupData as $grp) {
                            $this->ion_auth->add_to_group($grp, $id);
                        }
                    }
                }

                // check to see if we are updating the user
                if ($this->ion_auth->update($user['id'], $data)) {

                    // redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('success_msg', $this->ion_auth->messages());
                    if ($this->ion_auth->is_admin()) {
                        redirect('admin', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }
                } else {

                    // redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('error_msg', $this->ion_auth->errors());
                    if ($this->ion_auth->is_admin()) {
                        redirect('admin', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }
                }
            }
        }

        // display the edit user form
        $this->data['csrf'] = $this->_get_csrf_nonce();

        // set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

        // pass the user to the view
        $this->data['user'] = $user;
        $this->data['groups'] = $groups;
        $this->data['currentGroups'] = $currentGroups;

        $this->data['first_name'] = array(
            'name' => 'first_name',
            'id' => 'first_name',
            'class' => 'form-control',
            'data-error' => '.regErorr8',
            'autofocus' => 'autofocus',
            'type' => 'text',
            'value' => $this->form_validation->set_value('first_name', $user['first_name']),
        );
        $this->data['last_name'] = array(
            'name' => 'last_name',
            'id' => 'last_name',
            'type' => 'text',
            'class' => 'form-control',
            'data-error' => '.regErorr1',
            'value' => $this->form_validation->set_value('last_name', $user['last_name']),
        );
        $this->data['sa_id_no'] = array(
            'name' => 'sa_id_no',
            'id' => 'sa_id_no',
            'type' => 'text',
            'class' => 'form-control',
            'data-error' => '.regErorr2',
            'value' => $this->form_validation->set_value('sa_id_no', $user['sa_id_number']),
        );
        $this->data['email'] = array(
            'name' => 'email',
            'id' => 'email',
            'type' => 'text',
            'class' => 'form-control',
            'data-error' => '.regErorr3',
            'value' => $this->form_validation->set_value('email', $user['email']),
        );
        $this->data['mobile_no'] = array(
            'name' => 'mobile_no',
            'id' => 'mobile_no',
            'type' => 'text',
            'class' => 'form-control',
            'data-error' => '.regErorr4',
            'value' => $this->form_validation->set_value('mobile_no', $user['phone']),
        );
        $this->data['bith_date'] = array(
            'name' => 'bith_date',
            'id' => 'bith_date',
            'type' => 'text',
            'class' => 'form-control datepicker',
            'data-error' => '.regErorr5',
            'value' => $this->form_validation->set_value('bith_date', $user['birth_date']),
        );
        $data['language_list'] = $this->language->get_allLanguage();
        $data['interests_list'] = $this->interests->get_allInterests();

        $this->template->set_master_template('template.php');
        $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
        $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
        $this->template->write_view('content', 'edit_user', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'snippets/footer', '', TRUE);
        $this->template->render();
//        $this->_render_page('auth/edit_user', $this->data);
    }

    public function delete_user() {
        if ($this->input->post('delete_id')) {
            $id = $this->input->post('delete_id');
            $data = array(
                'active' => 0
            );
            $this->ion_auth->update($id, $data);
//            if ($this->ion_auth->update($id, $data)) {
//
//                // redirect them back to the admin page if admin, or to the base url if non admin
//                $this->session->set_flashdata('message', 'User Deleted Successfully');
//                if ($this->ion_auth->is_admin()) {
//                    redirect('admin', 'refresh');
//                } else {
//                    redirect('/', 'refresh');
//                }
//            } else {
//
//                // redirect them back to the admin page if admin, or to the base url if non admin
//                $this->session->set_flashdata('message', 'Failed to delete user');
//                if ($this->ion_auth->is_admin()) {
//                    redirect('admin', 'refresh');
//                } else {
//                    redirect('/', 'refresh');
//                }
//            }
        }
    }

    // create a new group
    public function create_group() {
        $this->data['title'] = $this->lang->line('create_group_title');

        // if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
        //     redirect('admin', 'refresh');
        // }
        // validate form input
        $this->form_validation->set_rules('group_name', $this->lang->line('create_group_validation_name_label'), 'required|alpha_dash');

        if ($this->form_validation->run() == TRUE) {
            $new_group_id = $this->ion_auth->create_group($this->input->post('group_name'), $this->input->post('description'));
            if ($new_group_id) {
                // check to see if we are creating the group
                // redirect them back to the admin page
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect('admin', 'refresh');
            }
        } else {
            // display the create group form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['group_name'] = array(
                'name' => 'group_name',
                'id' => 'group_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('group_name'),
            );
            $this->data['description'] = array(
                'name' => 'description',
                'id' => 'description',
                'type' => 'text',
                'value' => $this->form_validation->set_value('description'),
            );

            $this->_render_page('Auth/create_group', $this->data);
        }
    }

    // edit a group
    public function edit_group($id) {
        // bail if no group id given
        if (!$id || empty($id)) {
            redirect('admin', 'refresh');
        }

        $this->data['title'] = $this->lang->line('edit_group_title');

        // if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
        //     redirect('admin', 'refresh');
        // }

        $group = $this->ion_auth->group($id)->row();

        // validate form input
        $this->form_validation->set_rules('group_name', $this->lang->line('edit_group_validation_name_label'), 'required|alpha_dash');

        if (isset($_POST) && !empty($_POST)) {
            if ($this->form_validation->run() === TRUE) {
                $group_update = $this->ion_auth->update_group($id, $_POST['group_name'], $_POST['group_description']);

                if ($group_update) {
                    $this->session->set_flashdata('message', $this->lang->line('edit_group_saved'));
                } else {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
                }
                redirect('admin', 'refresh');
            }
        }

        // set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

        // pass the user to the view
        $this->data['group'] = $group;

        $readonly = $this->config->item('admin_group', 'ion_auth') === $group->name ? 'readonly' : '';

        $this->data['group_name'] = array(
            'name' => 'group_name',
            'id' => 'group_name',
            'type' => 'text',
            'value' => $this->form_validation->set_value('group_name', $group->name),
            $readonly => $readonly,
        );
        $this->data['group_description'] = array(
            'name' => 'group_description',
            'id' => 'group_description',
            'type' => 'text',
            'value' => $this->form_validation->set_value('group_description', $group->description),
        );

        $this->_render_page('Auth/edit_group', $this->data);
    }

    public function _get_csrf_nonce() {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    public function _valid_csrf_nonce() {
        $csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
        if ($csrfkey && $csrfkey == $this->session->flashdata('csrfvalue')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function _render_page($view, $data = null, $returnhtml = false) {//I think this makes more sense
        $this->viewdata = (empty($data)) ? $this->data : $data;

        $view_html = $this->load->view($view, $this->viewdata, $returnhtml);

        if ($returnhtml)
            return $view_html; //This will return html on 3rd argument being true
    }

    function handleUploadCommon($slug) {
        $fileTypes = array('jpeg', 'png', 'jpg'); // File extensions
        $fileParts = pathinfo($_FILES['profile_image']['name']);

        if (!in_array(strtolower($fileParts['extension']), $fileTypes)) {
            $this->session->set_flashdata('msg', 'File type not supported.');
//            $data['flashdata'] = array('type' => 'error', 'msg' => 'File type not supported.');
            return false;
        }

        $user_slug = $slug;
        $ext = pathinfo($_FILES['profile_image']['name'], PATHINFO_EXTENSION);
        $targetURL = '/assets/uploads/clients' . $user_slug . '/profile'; // Relative to the root
        $targetPath = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'clients' . DIRECTORY_SEPARATOR . $user_slug . '/profile';

        if (!file_exists($targetPath)) {
            mkdir($targetPath, 0777, true);
        }

        $tempFile = $_FILES['profile_image']['tmp_name'];
        $fileName = $_FILES['profile_image']['name'];
        $fileName = $slug . '-profile-' . $fileName;
        $targetPath .= DIRECTORY_SEPARATOR . $fileName;
        $upload_status = move_uploaded_file($tempFile, $targetPath);
        $dataDocumentDetail['type'] = $fileParts['extension'];
        if (isset($upload_status))
            return $fileName;
    }

    // if ($_SERVER['REQUEST_METHOD'] == 'POST' && $action = 'edit' && $modal_id) {


    function handleUploadUser($slug) {
        $fileTypes = array('jpeg', 'png', 'jpg'); // File extensions
        $fileParts = pathinfo($_FILES['profile_image']['name']);

        if (!in_array(strtolower($fileParts['extension']), $fileTypes)) {
            $this->session->set_flashdata('msg', 'File type not supported.');
//            $data['flashdata'] = array('type' => 'error', 'msg' => 'File type not supported.');
            return false;
        }

        $user_slug = $slug;
        $ext = pathinfo($_FILES['profile_image']['name'], PATHINFO_EXTENSION);
        $targetURL = '/assets/uploads/users' . $user_slug . '/profile'; // Relative to the root
        $targetPath = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'users' . DIRECTORY_SEPARATOR . $user_slug . DIRECTORY_SEPARATOR . 'profile';

        if (!file_exists($targetPath)) {
            mkdir($targetPath, 0777, true);
        }

        $tempFile = $_FILES['profile_image']['tmp_name'];
        $fileName = $_FILES['profile_image']['name'];
        $fileName = $slug . '-profile-' . $fileName;
        $targetPath .= DIRECTORY_SEPARATOR . $fileName;
        $upload_status = move_uploaded_file($tempFile, $targetPath);
        $dataDocumentDetail['type'] = $fileParts['extension'];
        if (isset($upload_status))
            return $fileName;
    }

    public function admin_forgot_pass() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->form_validation->set_rules('inputEmail', 'Email ID', 'trim|required');
//            die;forgot
            $name = '';
            if ($this->form_validation->run() == TRUE) {
                $this->input->post('inputEmail');
                $identity_column = $this->config->item('admin_identity', 'ion_auth');

                $identity = $this->ion_auth->email_where($identity_column, $this->input->post('inputEmail'))->users()->row();
                //  var_dump($identity);die;
//              die;
                if (empty($identity)) {
                    if ($this->config->item('admin_identity', 'ion_auth') != 'email') {
                        $this->ion_auth->set_error('forgot_password_identity_not_found');
                    } else {
                        $this->ion_auth->set_error('forgot_password_email_not_found');
                    }
                    $this->session->set_flashdata('error_msg', ucfirst($this->ion_auth->errors()));
                    redirect("admin/forgot_password", 'refresh');
                } else {

//-----------------send password link-----------------------------------------                
                    if ($identity->email) {

                        $name = $identity->firstname . " " . $identity->lastname;
                        $dpn_no = $identity->dpn_no;
                        $date = date("Y-m-d H:i:s");
                        $currenttime = strtotime(date("Y-m-d H:i:s"));

                        $mail_sent = $this->send_passwordlink($identity->id, $name, $identity->mobile, $identity->email, $dpn_no, $currenttime);
//                    die;
                        if ($mail_sent == 1) {
                            $this->ion_auth->set_forgot_pass($identity->id, $dpn_no, $date, $currenttime);
                            $msgs = nl2br("Reset password link has been sent to your email");
                            $this->session->set_flashdata('success_msg', $msgs);
                            redirect("admin/forgot_password", 'refresh'); //we should display a confirmation page here instead of the logon '.$identity->email.' this mail id');in page    
                        } else {
                            $this->session->set_flashdata('error_msg', ucfirst($this->ion_auth->errors()));
                            redirect("admin/forgot_password", 'refresh');
                        }
                    } else {
                        
                    }
//          
//            $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('admin_identity', 'ion_auth')});
//            
//            if ($forgotten) {
//                $this->session->set_flashdata('success_msg', $this->ion_auth->messages());
//                #redirect("admin", 'refresh'); //we should display a confirmation page here instead of the login page
//            } else {
//                $this->session->set_flashdata('error_msg', $this->ion_auth->errors());
//                #redirect("admin/forgot_password", 'refresh');
//            }
                }
            }
        }
        $this->template->set_master_template('login_template.php');
        $this->template->write_view('content', 'Auth/admin_forgot_password', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'snippets/footer', '', TRUE);
        $this->template->render();
    }

    function send_passwordlink($id, $uname, $mobile, $emailid, $dpn_no, $currenttime) {
        $form_name = 'Sai Baba Palkhi Team';
        $enquirer_name = "Sai Baba Palkhi Team";
        $company_name = "Sai Baba Palkhi Team";
        $retype = ucfirst(strtolower("SSS"));
        $enquirer_email = "saipalkhipune@gmail.com";
        $country = "india";
        $contact = "123698";
        $subject_title = "Email from Sai Palkhi credentials ";
        $data['username'] = $uname;
        $data['password_link'] = base_url() . 'admin/reset_password/' . $dpn_no . $currenttime;
        $mail_body = $this->load->view("email/email_forgot_password", $data, true);

        $mail = new PHPMailer();
        //$mail->IsSendmail(); // send via SMTP
        $mail->IsSMTP();
        $mail->SMTPDebug = 0;
        //Ask for HTML-friendly debug output
        $mail->Debugoutput = 'html';
        //Set the hostname of the mail server
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $webmaster_email = "saipalkhipune@gmail.com"; //Reply to this email ID
        //$mail->SMTPSecure = 'ssl';
        $mail->Username = "saipalkhipune@gmail.com"; // SMTP username
        $mail->Password = "Palkhi20!@"; // SMTP password
        $mail->Port = "587";
        $mail->Host = 'smtp.gmail.com'; //hostname

        $receiver_email = $emailid;
        $email = $receiver_email; // Recipients email ID //inquiry@mindworx.in

        $name = "Sai Palkhi"; // Recipient's name

        $mail->From = $enquirer_email;
        $mail->FromName = $enquirer_name;
        $mail->AddAddress($email, $name);
        $mail->AddReplyTo($enquirer_email, $enquirer_name);
//        $mail->AddAttachment(DOCUMENT_STORE_PATH.$file_name);
        $mail->WordWrap = 50; // set word wrap
        $mail->IsHTML(false); // send as HTML
        $mail->Subject = $subject_title;
        $mail->MsgHTML($mail_body);
        $mail->AltBody = "This is the body when user views in plain text format"; //Text Body
        if (!$mail->Send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
            die;
            return $a = 0;
        } else {
            return $a = 1;
        }
    }

}

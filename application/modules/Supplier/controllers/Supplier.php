<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends MY_Controller {

    public function __construct() {
        parent::__construct();

        if (!$this->ion_auth->logged_in()) {
            redirect('admin', 'refresh');
        }
        if (!$this->ion_auth->is_admin()) {
            redirect('admin', 'refresh');
        }
        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation', 'm_pdf'));
        $this->load->model(array('Sevasamiti_model', 'Sevekari_model', 'Supplier_model', 'Year', 'language'));
        $this->load->helper(array('url', 'language'));
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->lang->load('Auth');
        error_reporting(E_ALL ^ E_WARNING);
        error_reporting(0);
    }

    public function resource_list($samiti_id) {
        $user_id = $this->session->userdata('user_id');
        $data['resource_list'] = $this->Supplier_model->get_resource_list($samiti_id);
        $data['samiti_details'] = $this->Sevasamiti_model->get_allData($samiti_id);
        $data['dataHeader']['title'] = 'Manage Supplier';
        $this->template->set_master_template('template.php');
        $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
        $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
        $this->template->write_view('content', 'resource_list', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'snippets/footer', '', TRUE);
        $this->template->render();
    }

    public function add_resource($samiti_id) {
        $user_id = $this->session->userdata('user_id');
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->form_validation->set_rules('supplier_name', 'Supplier Name', 'required');
            $this->form_validation->set_rules('resource_name', 'Resource Name', 'required');
            $this->form_validation->set_rules('resource_mobile', 'Resource Mobile Number', 'required');
            $this->form_validation->set_rules('bloodgroup', 'Blood group', 'required');
            if ($this->form_validation->run() == TRUE) {
                $check = $this->Supplier_model->if_resource_exists($this->input->post('resource_name'), $this->input->post('supplier_name'));
                if (count($check) == 0) {
                    $resource_details = array(
                        'supplier_name' => $this->input->post('supplier_name'),
                        'resource_name' => $this->input->post('resource_name'),
                        'resource_mobile' => '91' . $this->input->post('resource_mobile'),
                        'blood_group' => $this->input->post('bloodgroup'),
                        'seva_samiti_id' => $this->input->post('samiti'),
                        'created_by' => $user_id,
                        'created_date' => date('Y-m-d'),
                        'isactive' => 1
                    );
                    if ($this->Supplier_model->insert($resource_details)) {
                        $this->Supplier_model->update_supplierCount(date("Y"));
                        $this->session->set_flashdata('success_msg', 'Employee added Successfully.');
                        redirect('admin/supplier/' . $samiti_id);
                    } else {
                        $this->session->set_flashdata('error_msg', 'Failed to add employee');
                        redirect('admin/supplier/' . $samiti_id);
                    }
                } else {
                    $this->session->set_flashdata('error_msg', 'Employee already exists.');
                    redirect('admin/supplier/' . $samiti_id);
                }
            }
        } else {
            $data['samiti_id'] = $samiti_id;
            $data['samiti_details'] = $this->Sevasamiti_model->get_allData($samiti_id);
            $data['dataHeader']['title'] = 'Add Resource';
            $this->template->set_master_template('template.php');
            $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
            $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
            $this->template->write_view('content', 'resource_add', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'snippets/footer', '', TRUE);
            $this->template->render();
        }
    }

    public function update_resource($samiti_id, $resource_id) {
        $user_id = $this->session->userdata('user_id');
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->form_validation->set_rules('supplier_name', 'Supplier Name', 'required');
            $this->form_validation->set_rules('resource_name', 'Resource Name', 'required');
            $this->form_validation->set_rules('resource_mobile', 'Resource Mobile Number', 'required');
            $this->form_validation->set_rules('bloodgroup', 'Blood group', 'required');
            if ($this->form_validation->run() == TRUE) {
                $check = $this->Supplier_model->if_resource_exists($this->input->post('resource_name'), $this->input->post('supplier_name'));
                if ((count($check) == 1 && $check[0]->id == $resource_id) || count($check) == 0) {
                    $resource_details = array(
                        'supplier_name' => $this->input->post('supplier_name'),
                        'resource_name' => $this->input->post('resource_name'),
                        'resource_mobile' => '91' . $this->input->post('resource_mobile'),
                        'blood_group' => $this->input->post('bloodgroup'),
                        'modified_date' => date('Y-m-d'),
                    );
                    if ($this->Supplier_model->update($resource_id, $resource_details)) {
                        $this->session->set_flashdata('success_msg', 'Employee updated successfully.');
                        redirect('admin/supplier/' . $samiti_id);
                    } else {
                        $this->session->set_flashdata('error_msg', 'Failed to update employee');
                        redirect('admin/supplier/' . $samiti_id);
                    }
                } else {
                    $this->session->set_flashdata('error_msg', 'Employee already exists.');
                    redirect('admin/supplier/' . $samiti_id);
                }
            }
        } else {
            $data['samiti_id'] = $samiti_id;
            $data['resource'] = $this->Supplier_model->get_allData($resource_id);
            $data['samiti_details'] = $this->Sevasamiti_model->get_allData($samiti_id);
            $data['dataHeader']['title'] = 'Update Resource';
            $this->template->set_master_template('template.php');
            $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
            $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
            $this->template->write_view('content', 'resource_edit', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'snippets/footer', '', TRUE);
            $this->template->render();
        }
    }

    public function delete() {
        if ($this->input->post('resource_id')) {
            if ($this->Supplier_model->delete($this->input->post('resource_id'))) {
                $this->Supplier_model->delete_supplierCount(date("Y"));
                $this->session->set_flashdata('success_msg', 'Employee deleted successfully.');
                echo 'success';
            } else {
                $this->session->set_flashdata('error_msg', 'Failed to delete employee.');
                echo 'failed';
            }
        }
    }

    public function generate_sevekari_report() {
        if ($this->input->post('samiti_id')) {
            $samiti_id = $this->input->post('samiti_id');
            $data['sevekariDetails'] = $this->Sevekari_model->sevekari_details($samiti_id);
            $data['supplierDetails'] = $this->Supplier_model->get_resource_list($samiti_id);
            $data['samiti_info'] = $this->Sevasamiti_model->get_allData($samiti_id);

            $html = $this->load->view("reports/sevekari_supplier_list", $data, true);

            $pdfFilePath = str_replace(" ", "_", $data['samiti_info']['name']) . '.PDF';

            $pdf = $this->m_pdf->load();
            $pdf->WriteHTML($html);

            $pdf->Output($pdfFilePath, "D");
            exit();
        }
    }

    public function generate_report() {
        if ($this->input->post('samiti_id')) {
            $samiti_id = $this->input->post('samiti_id');
            $data['sevekariDetails'] = $this->Sevekari_model->sevekari_details($samiti_id);
            $data['supplierDetails'] = $this->Supplier_model->get_resource_list($samiti_id);
            $data['samiti_info'] = $this->Sevasamiti_model->get_allData($samiti_id);

            $html = $this->load->view("reports/sevekari_supplier_list", $data, true);

            $pdfFilePath = str_replace(" ", "_", $data['samiti_info']['name']) . '.PDF';

            $pdf = $this->m_pdf->load();
            $pdf->WriteHTML($html);

            echo $pdf->Output($pdfFilePath, "D");
//            exit();
        }
    }

}

<div class="card min-height80">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-md-5 align-self-center">
                        <h3 class="card-title">Edit Employee</h3>
                    </div>
                    <div class="col-md-7 align-self-center text-right d-none d-md-block">
                        <a href="<?php echo base_url() . 'admin/supplier/' . $samiti_id ?>"> <button type="button" class="btn btn-info add-button"><i class="fas fa-arrow-left"></i>  Back to Contractors List</button></a>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <?php echo form_open("admin/supplier/update/" . $samiti_id . "/" . $resource['id'], array('id' => 'edit_resource', 'class' => 'form-horizontal p-t-20')); ?>
                        <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Samiti Name:</label>
                                        <div class="col-md-7">
                                                                   
                                    <input type="text" class="form-control" id="samitiName" placeholder="Samiti Name" value="<?php echo isset($samiti_details['name']) ? $samiti_details['name'] : '' ?>" readonly>
                               
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="col-md-6">
                                    <div class="form-group required row" aria-required="true">
                                        <label class="control-label text-left col-md-4">Contractor Name:</label>
                                        <div class="col-md-7">
                                                                            
                                    <input type="text" class="form-control" id="supplier_name" name="supplier_name" placeholder="Enter name" data-error=".editresourceErorr1" value="<?php echo isset($resource['supplier_name']) ? $resource['supplier_name'] : '' ?>">
                                    <div class="editresourceErorr1 error-msg"></div>
                                    <?php echo form_error('supplier_name'); ?> 
                               
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Employee Name:</label>
                                        <div class="col-md-7">
                                            
                                    <input type="text" class="form-control" id="resource_name" name="resource_name" placeholder="Enter name" data-error=".editresourceErorr2" value="<?php echo isset($resource['resource_name']) ? $resource['resource_name'] : '' ?>">
                                    <div class="editresourceErorr2 error-msg"></div>
                                    <?php echo form_error('resource_name'); ?> 
                              
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group required row" aria-required="true">
                                        <label class="control-label text-left col-md-4">Employee Mobile:</label>
                                        <div class="col-md-7">
                                             
                                    <div class="row">
                                        <div class="col-sm-3 col">
                                            <input type="text" class="form-control mobile-width" id="country_code" name="country_code" value="+91" readonly>
                                        </div>
                                        <div class="col-sm-9 col">
                                            <input type="text" class="form-control mobile-width" id="resource_mobile" name="resource_mobile" placeholder="Enter mobile no" data-error=".editresourceErorr3" value="<?php echo isset($resource['resource_mobile']) ? substr($resource['resource_mobile'], 2) : '' ?>">
                                        </div>
                                    </div>                                            
                                           <div class="editresourceErorr3 error-msg"></div>
                                           <?php echo form_error('resource_mobile'); ?>                                        
                              
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Blood Group:</label>
                                        <div class="col-md-7">
                                                    
                                            <select class="form-control custom-select" name="bloodgroup" id="bloodgroup" data-error=".editresourceErorr4">
                                                <option value="">Select Blood Group</option>
                                                <?php $selected_bldgrp = set_value('bloodgroup', $resource['blood_group']); ?>
                                                <option value="A+" <?php echo ($selected_bldgrp == "A+") ? 'selected' : ''; ?> >A+</option>
                                                <option value="A-" <?php echo ($selected_bldgrp == "A-") ? 'selected' : ''; ?> >A-</option>
                                                <option value="B+" <?php echo ($selected_bldgrp == "B+") ? 'selected' : ''; ?> >B+</option>
                                                <option value="B-" <?php echo ($selected_bldgrp == "B-") ? 'selected' : ''; ?> >B-</option>
                                                <option value="O+" <?php echo ($selected_bldgrp == "O+") ? 'selected' : ''; ?> >O+</option>
                                                <option value="O-" <?php echo ($selected_bldgrp == "O-") ? 'selected' : ''; ?> >O-</option>
                                                <option value="AB+" <?php echo ($selected_bldgrp == "AB+") ? 'selected' : ''; ?> >AB+</option>
                                                <option value="AB-" <?php echo ($selected_bldgrp == "AB-") ? 'selected' : ''; ?> >AB-</option>
                                                <option value="unknown" <?php echo ($selected_bldgrp == "unknown") ? 'selected' : ''; ?> >Unknown</option>
                                            </select>
                                            <div class="editresourceErorr4 error-msg"></div>
                                            <?php echo form_error('bloodgroup'); ?> 
                                        
                                        </div>
                                    </div>
                                </div>
                      
                            </div>
                            <hr class="my-4">
                        <div class="form-group row m-b-0 col">
                            <div class="offset-sm-11 col-sm-1 p-0">
                                <input type="submit" class="btn btn-success waves-effect waves-light update-button" value="Update">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>

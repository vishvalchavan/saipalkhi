<div class="card min-height80">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <div class="row mx-0">
                    <div class="col-md-5 col-5 align-self-center p-0">
                        <h3 class="card-title">Add Employee</h3>
                    </div>
                    <div class="col-md-7 col-7 align-self-center text-right p-0">
                        <a href="<?php echo base_url() . 'admin/supplier/' . $samiti_id ?>"> <button type="button" class="btn btn-info add-button"><i class="fas fa-arrow-left"></i>  Back to Contractors List</button></a>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <?php echo form_open("Supplier/add_resource/" . $samiti_id, array('id' => 'add_resource', 'class' => 'form-horizontal p-t-20')); ?>  
                        <input type="hidden" name="samiti" value="<?php echo isset($samiti_id) ? $samiti_id : ''; ?>">


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group required row">
                                    <label class="control-label text-left col-md-4">Samiti Name:</label>
                                    <div class="col-md-7">
                                        <div class="input-group">                            
                                            <input type="text" class="form-control" id="samitiName" placeholder="Samiti Name" value="<?php echo isset($samiti_details['name']) ? $samiti_details['name'] : '' ?>" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group required row">
                                    <label class="control-label text-left col-md-4">Contractor Name:</label>
                                    <div class="col-md-7">
                                        <div class="input-group">                            
                                            <input type="text" class="form-control" id="supplier_name" name="supplier_name" placeholder="Enter name" data-error=".resourceErorr1">
                                            <div class="resourceErorr1 error-msg"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group required row">
                                    <label class="control-label text-left col-md-4">Employee Name:</label>
                                    <div class="col-md-7">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="resource_name" name="resource_name" placeholder="Enter name" data-error=".resourceErorr2">
                                            <div class="resourceErorr2 error-msg"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group required row">
                                    <label class="control-label text-left col-md-4">Employee Mobile.:</label>
                                    <div class="col-md-7">
                                        
                                            <div class="row">
                                                <div class="col-sm-3 col">
                                                    <input type="text" class="form-control mobile-width" id="country_code" name="country_code" readonly="" value="+91">
                                                </div>
                                                <div class="col-sm-9 col">
                                                    <input type="text" class="form-control mobile mobile-width" id="resource_mobile" name="resource_mobile" placeholder="Enter mobile no" data-error=".resourceErorr3">
                                                </div>
                                            </div>
                                            <div class="resourceErorr3 error-msg"></div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group required row">
                                    <label class="control-label text-left col-md-4">Blood Group:</label>
                                    <div class="col-md-7">
                                           
                                            <select class="form-control custom-select" name="bloodgroup" id="bloodgroup" data-error=".resourceErorr4">
                                                <option value="">Select Blood Group</option>
                                                <option value="A+">A+</option>
                                                <option value="A-">A-</option>
                                                <option value="B+">B+</option>
                                                <option value="B-">B-</option>
                                                <option value="O+">O+</option>
                                                <option value="O-">O-</option>
                                                <option value="AB+">AB+</option>
                                                <option value="AB-">AB-</option>
                                                <option value="unknown">Unknown</option>
                                            </select>
                                            <div class="resourceErorr4 error-msg"></div>
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
<hr class="my-4">
                        <div class="form-group row m-b-0 col">
                            <div class="offset-sm-11 col-sm-1 pl-0">
                                <input type="submit" class="btn btn-success waves-effect waves-light update-button" value="Save">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>

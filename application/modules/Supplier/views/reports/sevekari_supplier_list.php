<html>
    <head>
        <style>
            .row{
                width:100%;
            }
            .col-lg-4{
                width:20%;
            }
            .col-lg-8{
                width:80%
            }
            .clearfix{
                margin-bottom: 15px;
            }
            .table {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            .table td, .table th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            .table tr:nth-child(even){background-color: #f2f2f2;}

            .table tr:hover {background-color: #ddd;}

            .table th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #55AFBA;
                color: white;
            }
            .float-left{
                float:left;
            }
            .float-right{
                float:right;
            }
            .pagebreak{
                page-break-before: always
            }
        </style>
    </head>
    <body>
        <div class="row">
            <div class="col-lg-4 float-left">Samiti Name :</div>
            <div class="col-lg-8 float-right"><?php echo isset($samiti_info) ? $samiti_info['name'] : ''; ?></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-lg-4 float-left">Description :</div>
            <div class="col-lg-8 float-right"><?php echo isset($samiti_info) ? $samiti_info['description'] : ''; ?></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <h3>List of Sevekari</h3>
        </div>
        <div class="row">
            <table class="table">
                <thead>
                    <tr>
                        <th>Sr.No.</th>
                        <th>Sevekari Name</th>
                        <th>Mobile No.</th>
                        <th>Sevekari Role</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (isset($sevekariDetails) && !empty($sevekariDetails)) {
                        $index = 1;
                        foreach ($sevekariDetails as $sevekari) {
                            ?>
                            <tr>
                                <td><?php echo $index++; ?></td>
                                <td><?php echo $sevekari['firstname'] . " " . $sevekari['middlename'] . " " . $sevekari['lastname']; ?></td>
                                <td><?php echo $sevekari['mobile']; ?></td>
                                <td><?php echo $sevekari['ishead'] == 1 ? 'Head' : ''; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
        <div class="pagebreak"></div>
        <div class="row">
            <h3>List of Supplier</h3>
        </div>
        <div class="row">
            <table class="table">
                <thead>
                    <tr>
                        <th>Sr No.</th>
                        <th>Contractor Name</th>
                        <th>Employee Name</th>
                        <th>Employee Mobile</th>       
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (isset($supplierDetails) && !empty($supplierDetails)) {
                        $index = 1;
                        foreach ($supplierDetails as $resource) {
                            ?>
                            <tr>
                                <td><?php echo $index++; ?></td>
                                <td><?php echo isset($resource['supplier_name']) ? $resource['supplier_name'] : ''; ?></td>
                                <td><?php echo isset($resource['resource_name']) ? $resource['resource_name'] : ''; ?></td>
                                <td><?php echo isset($resource['resource_mobile']) ? $resource['resource_mobile'] : ''; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </body>
</html>
<div class="card min-height80">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <div class="row mx-0">
                    <div class="col-md-5 align-self-center p-0 col-5">
                        <h3 class="card-title">Manage Contractor</h3>
                    </div>
                    <div class="col-md-7 text-right p-0 col-7">
                        <a href="<?php echo base_url() . 'admin/sevekari/' . $samiti_details['id'] ?>"> <button type="button" class="btn btn-info back-button"><i class="fas fa-arrow-left"></i> Back to Sevekari list</button></a>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <form class="form-horizontal p-t-20">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Samiti Name:</label>
                                        <div class="col-md-7">
                                                                       
                                                <input type="text" class="form-control" id="samitiName" placeholder="Samiti Name" value="<?php echo isset($samiti_details['name']) ? $samiti_details['name'] : '' ?>" readonly>
                                          
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group required row">
                                        <label class="control-label text-left col-md-4">Description:</label>
                                        <div class="col-md-7">
                                         
                                                <textarea class="form-control" colspan="8" rows="3" id="description" readonly><?php echo isset($samiti_details['description']) ? $samiti_details['description'] : '' ?></textarea>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-8 align-self-center">
                        <h3 class="card-title"></h3>
                    </div>

                    <div class="col-md-4 align-self-center text-right">
                        <div class="row">
                            <div class="col-lg-12 p-0 float-right">
                                <a href="<?php echo base_url() . 'admin/supplier/add/' . $samiti_details['id'] ?>">
                                    <button type="button" class="btn btn-info add-button mr-4"><i class="fas fa-plus-circle"></i> Add Employee</button>
                                </a>
                            </div>                                
                        </div>                    
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="resource_list1" class="display nowrap table table-hover table-striped table-bordered dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="example23_info">
                        <thead>
                            <tr role="row">
                                <th>Sr. No.</th>
                                <th>Contractor Name</th>
                                <th>Employee Name</th>
                                <th>Employee Mobile No.</th>       
                                <th>Blood Group</th>       
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (isset($resource_list) && !empty($resource_list)) {
                                $index = 1;
                                foreach ($resource_list as $resource) {
                                    ?>
                                    <tr>
                                        <td><?php echo $index++; ?></td>
                                        <td><?php echo isset($resource['supplier_name']) ? $resource['supplier_name'] : ''; ?></td>
                                        <td><?php echo isset($resource['resource_name']) ? $resource['resource_name'] : ''; ?></td>
                                        <td><?php echo isset($resource['resource_mobile']) ? $resource['resource_mobile'] : ''; ?></td>
                                        <td><?php echo isset($resource['blood_group']) ? $resource['blood_group'] == 'unknown' ? '-' : $resource['blood_group'] : ''; ?></td>
                                        <td>
                                            <a href="<?php echo base_url() . 'admin/supplier/update/' . $samiti_details['id'] . '/' . $resource['id'] ?>" class="bg-button" title="Edit"><i class="fas fa-edit"></i></a>&nbsp;&nbsp;
                                            <a class="bg-button delete" title="Delete" data-resource="<?php echo $resource['id'] ?>"><i class="fas fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div><br>
            </div>
        </div>
    </div>
</div>
<script>
    function deleteResource(dId) {
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Supplier/delete',
            data: {'resource_id': dId},
            success: function (data) {
                if ($.trim(data) == 'success') {
                    location.reload();
                }
            }
        });
        return false;
    }
    $('body').on('click', '.delete', function () {
        var id = $(this).attr('data-resource');
        $('#confirm-delete').modal();
        $(".btn-ok").click(function () {
            deleteResource(id);
        });
    });
//    $("#generateReport").click(function () {
//        var samiti = $(this).attr('data-samiti');
//        if (samiti.length !== 0) {
//            $.ajax({
//                type: "POST",
//                url: '<?php echo base_url(); ?>Supplier/generate_sevekari_report',
//                data: {'samiti_id': samiti},
//                success: function () {
//                }
//            });
//        }
//    });
</script>

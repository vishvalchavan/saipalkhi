<table id="vehicle_list" class="display nowrap table table-hover table-striped table-bordered dataTable datatable" cellspacing="0 " width="100% " role="grid " aria-describedby="example23_info ">
    <thead>
        <tr>
            <th>Truck name</th>
            <th>Truck Number</th>
            <th>Driver Name</th>
            <th>Mobile No.</th>
            <th>Luggage Capacity</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (isset($vehicle_list) && !empty($vehicle_list)) {
            foreach ($vehicle_list as $vehicle) {
                ?>
                <tr>
                    <td><?php echo isset($vehicle['truck_name']) ? $vehicle['truck_name'] : ''; ?></td>
                    <td><?php echo isset($vehicle['truck_number']) ? $vehicle['truck_number'] : ''; ?></td>
                    <td><?php echo isset($vehicle['driver_name']) ? $vehicle['driver_name'] : ''; ?></td>                                                            
                    <td><?php echo isset($vehicle['driver_mobile']) ? $vehicle['driver_mobile'] : ''; ?></td>
                    <td><?php echo isset($vehicle['truck_capacity']) ? $vehicle['truck_capacity'] : ''; ?></td>
                    <td>
                        <a href="<?php echo base_url() . 'admin/vehicle/update/' . $vehicle['id'] ?>" class="bg-button " data-toggle="tooltip " data-placement="top " title="Edit "><i class="fas fa-edit "></i>
                        </a>
                        <a data-vehicle="<?php echo $vehicle['id']; ?>" class="bg-button delete" title="Delete"><i class="fas fa-trash "></i>
                        </a>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>
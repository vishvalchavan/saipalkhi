<div class="row">
    <div class="col-12">
        <div class="card min-height80">
            <div class="card-body">
                <div class="row mx-0">
                    <div class="col-md-5 col-5 align-self-center p-0">
                        <h3 class="card-title">Vehicle List</h3>
                    </div>
                    <div class="col-md-7 col-7 text-right p-0">
                        <a href="<?php echo base_url(); ?>admin/vehicle/add"><button type="button " class="btn btn-info add-button "><i class="fa fa-plus-circle "></i> Add Vehicle</button></a>
                    </div>
                </div>
                <hr>
                <div class="">
                    <div class="row mx-0">
                        <div class="button-table">
                            <form class="form-inline">
                                <label class="palkhi-select">Palkhi Sohala:</label>
                                <select class="form-control ml-2" id="year_list" name="year_list">
                                    <option value="">Select Year</option>
                                    <?php
                                    if (isset($year_list) && !empty($year_list)) {
                                        foreach ($year_list as $year) {
                                            $selected = '';
                                            if (date('Y') == $year['year']) {
                                                $selected = "selected";
                                            }
                                            ?>
                                            <option value="<?php echo $year['id'] ?>" <?php echo $selected; ?>><?php echo $year['year'] ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>

                                <label class="palkhi-select font-14 mr-1 ml-1">Category:</label>
                                <select class="form-control" id="search_by" name="search_by">
                                    <option value="">Search By</option>
                                    <option value="name" selected>Name</option>
                                    <option value="mobile">Mobile No</option>
                                </select>

                                <div class="form-group has-search ml-2">
                                    <span class="fas fa-search form-control-feedback"></span>
                                    <input type="text" class="form-control" id="column_filter" placeholder="Search">
                                </div>

                            </form>


                        </div>
                    </div>

                    <div id="example23_wrapper" class="dataTables_wrapper dt-bootstrap4 table-responsive">
                        <table id="vehicle_list" class="display nowrap table table-hover table-striped table-bordered dataTable datatable" cellspacing="0 " width="100% " role="grid " aria-describedby="example23_info ">
                            <thead>
                                <tr>
                                    <th>Truck name</th>
                                    <th>Truck Number</th>
                                    <th>Driver Name</th>
                                    <th>Mobile No.</th>
                                    <th>Luggage Capacity</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($vehicle_list) && !empty($vehicle_list)) {
                                    foreach ($vehicle_list as $vehicle) {
                                        ?>
                                        <tr>
                                            <td><?php echo isset($vehicle['truck_name']) ? $vehicle['truck_name'] : ''; ?></td>
                                            <td><?php echo isset($vehicle['truck_number']) ? $vehicle['truck_number'] : ''; ?></td>
                                            <td><?php echo isset($vehicle['driver_name']) ? $vehicle['driver_name'] : ''; ?></td>                                                            
                                            <td><?php echo isset($vehicle['driver_mobile']) ? $vehicle['driver_mobile'] : ''; ?></td>
                                            <td><?php echo isset($vehicle['truck_capacity']) ? $vehicle['truck_capacity'] : ''; ?></td>
                                            <td><?php echo isset($vehicle['isactive']) ? $vehicle['isactive'] == 1 ? 'Active' : 'In-Active' : ''; ?></td>
                                            <td>
                                                <a href="<?php echo base_url() . 'admin/vehicle/update/' . $vehicle['id'] ?>" class="bg-button " data-toggle="tooltip " data-placement="top " title="Edit "><i class="fas fa-edit "></i>
                                                </a>&nbsp;
                                                <?php if (isset($vehicle['isactive']) && $vehicle['isactive'] == 1) { ?>
                                                    <a data-vehicle="<?php echo $vehicle['id']; ?>" class="bg-button delete" title="Delete"><i class="fas fa-trash "></i>
                                                    </a>&nbsp;
                                                <?php } else { ?>
                                                    <a class="bg-button reactive" data-vehicle="<?php echo $vehicle['id']; ?>" title="Activate"><i class="fas fa-check-circle"></i>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function deleteVehicle(dId) {
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Vehicle/delete',
            data: {'vehicle_id': dId},
            success: function (data) {
                if ($.trim(data) == 'success') {
                    location.reload();
                }
            }
        });
        return false;
    }
    function ActivateVehicle(dId) {
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Vehicle/activate',
            data: {'vehicle_id': dId},
            success: function (data) {
                if ($.trim(data) == 'success') {
                    location.reload();
                }
            }
        });
        return false;
    }
    $('body').on('click', '.delete', function () {
        var id = $(this).attr('data-vehicle');
        $('#confirm-delete').modal();
        $(".btn-ok").click(function () {
            deleteVehicle(id);
        });
    });
    $('body').on('click', '.reactive', function () {
        var id = $(this).attr('data-vehicle');
        $('#confirm-active').modal();
        $(".btn-ok").click(function () {
            ActivateVehicle(id);
        });
    });
    $('body').on('change', '#year_list', function () {
        $(".loadding-page").css('display', 'block');
        var year = $(this).val();
        if (year.length !== 0) {
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>Vehicle/vehicle_list',
                data: {'year': year},
                success: function (data) {
                    if (data) {
                        $("#example23_wrapper").html(data);
                        $('#vehicle_list').DataTable({
                            dom: 'Bfrtip',
                            buttons: [{
                                    extend: 'excel',
                                    exportOptions: {
                                        columns: [0, 1, 2, 3, 4, 5]
                                    }
                                }, {
                                    extend: 'pdf',
                                    exportOptions: {
                                        columns: [0, 1, 2, 3, 4, 5]
                                    }
                                }, {
                                    extend: 'print',
                                    exportOptions: {
                                        columns: [0, 1, 2, 3, 4, 5]
                                    }
                                }]
                        });
                        $(".loadding-page").css('display', 'none');
                    }
                }
            });
        }
    });
    $(document).ready(function () {
        $("#search_by").change(function () {
            var search = $(this).val();
            if (search.length !== 0) {
                $("#column_filter").removeAttr('disabled');
                $("#column_filter").val("");
                if ($.trim(search) == 'name') {
                    filterColumn('3', "");
                } else if ($.trim(search) == 'mobile') {
                    filterColumn('0', "");
                }
            }
        });
        $('#column_filter').on('keyup click', function () {
            var search_txt = $(this).val();
            var search_by = $("#search_by").val();
            if (search_by.length !== 0) {
                if ($.trim(search_by) == 'name') {
                    filterColumn('2', search_txt);
                } else if ($.trim(search_by) == 'mobile') {
                    filterColumn('3', search_txt);
                }
            } else {
                filterColumn('2', search_txt);
            }
        });
    });
    function filterColumn(i, searchVal) {
        $('#vehicle_list').DataTable().column(i).search(searchVal, false, true).draw();
    }

</script>

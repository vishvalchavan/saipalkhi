<div class="card min-height80">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <div class="row mx-0">
                    <div class="col-md-5 col-5 align-self-center p-0">
                        <h3 class="card-title">Edit Vehicle</h3>
                    </div>
                    <div class="col-md-7 col-7 text-right p-0">
                        <a href="<?php echo base_url() . 'admin/vehicle'; ?>">
                            <button type="button" class="btn btn-info add-button"><i class="fas fa-arrow-left"></i> Back to vehicle List</button>
                        </a>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <?php echo form_open("Vehicle/update_vehicle/" . $vehicle_list['id'], array('id' => 'add_vehicle', 'class' => 'form-horizontal p-t-20')); ?>  
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="control-label text-left col-md-4">Truck Name:</label>
                                    <div class="col-md-7">

                                        <input type="text" class="form-control" id="truck_name" name="truck_name" placeholder="Enter Truck Name" data-error=".vehicleErorr1" value="<?php echo isset($vehicle_list['truck_name']) ? $vehicle_list['truck_name'] : ''; ?>" readonly>
                                        <div class="vehicleErorr1 error-msg"></div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row" aria-required="true">
                                    <label class="control-label text-left col-md-4">Truck Number:</label>
                                    <div class="col-md-7">

                                        <input type="text" class="form-control" id="trucknumber" name="trucknumber" placeholder="MH-14 FP 1234" data-error=".vehicleErorr2" value="<?php echo isset($vehicle_list['truck_number']) ? $vehicle_list['truck_number'] : ''; ?>">
                                        <div class="vehicleErorr2 error-msg"></div>

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="control-label text-left col-md-4">Driver Name:</label>
                                    <div class="col-md-7">

                                        <input type="text" class="form-control" id="driver_name" name="driver_name" placeholder="Enter Driver Name" data-error=".vehicleErorr3" value="<?php echo isset($vehicle_list['driver_name']) ? $vehicle_list['driver_name'] : ''; ?>">
                                        <div class="vehicleErorr3 error-msg"></div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row" aria-required="true">
                                    <label class="control-label text-left col-md-4">Mobile No:</label>
                                    <div class="col-md-7">

                                        <div class="row">
                                            <div class="col-sm-3 col">
                                                <input type="text" class="form-control mobile-width" id="country_code" name="country_code" value="+91" readonly>
                                            </div>
                                            <div class="col-sm-9 col">
                                                <input type="text" class="form-control mobile-width" id="mobile_no" name="mobile_no" placeholder="Enter Mobile No" data-error=".vehicleErorr4" value="<?php echo isset($vehicle_list['driver_mobile']) ? substr($vehicle_list['driver_mobile'], 2) : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="vehicleErorr4 error-msg"></div>

                                    </div>
                                </div>
                            </div>



                        </div>


                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="control-label text-left col-md-4">Capacity:</label>
                                    <div class="col-md-7">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="capacity" name="capacity" placeholder="Capacity in number" data-error=".vehicleErorr5" value="<?php echo isset($vehicle_list['truck_capacity']) ? $vehicle_list['truck_capacity'] : ''; ?>">
                                            <div class="vehicleErorr5 error-msg"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group required row" aria-required="true">
                                    <label class="control-label text-left col-md-4">Enrollment Year:</label>
                                    <div class="col-md-7">
                                        <div class="input-group">
                                            <select class="form-control custom-select" name="enroll_year" id="enroll_year" data-error=".vehicleErorr6">
                                                <option value="">Select Year</option>
                                                <?php
                                                if (isset($year_list) && !empty($year_list)) {
                                                    foreach ($year_list as $year) {
                                                        if ($year['year'] >= date("Y")) {
                                                            $selected = "";
                                                            if ($year['id'] == $vehicle_list['participation_year_id']) {
                                                                $selected = "selected";
                                                            }
                                                            ?>
                                                            <option value="<?php echo $year['id'] ?>" <?php echo $selected; ?>><?php echo $year['year'] ?></option>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <div class="vehicleErorr6 error-msg"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- 
                        
                                                <div class="form-group required row">
                                                    <label for="truck_name" class="col-sm-2 control-label">Truck Name</label>
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="truck_name" name="truck_name" placeholder="Enter Truck Name" data-error=".vehicleErorr1" value="<?php echo isset($vehicle_list['truck_name']) ? $vehicle_list['truck_name'] : ''; ?>" readonly>
                                                            <div class="vehicleErorr1 error-msg"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="web" class="col-sm-2  control-label">Truck Number</label>
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="trucknumber" name="trucknumber" placeholder="MH-14 FP 1234" data-error=".vehicleErorr2" value="<?php echo isset($vehicle_list['truck_number']) ? $vehicle_list['truck_number'] : ''; ?>">
                                                            <div class="vehicleErorr2 error-msg"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="driver_name" class="col-sm-2 control-label">Driver Name</label>
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="driver_name" name="driver_name" placeholder="Enter Driver Name" data-error=".vehicleErorr3" value="<?php echo isset($vehicle_list['driver_name']) ? $vehicle_list['driver_name'] : ''; ?>">
                                                            <div class="vehicleErorr3 error-msg"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="mobile_no" class="col-sm-2  control-label">Mobile No.</label>
                                                    <div class="col-sm-4 col">
                                                        <div class="input-group">
                                                            <div class="row">
                                                                <div class="col-sm-3 col">
                                                                    <input type="text" class="form-control mobile-width" id="country_code" name="country_code" value="+91" readonly>
                                                                </div>
                                                                <div class="col-sm-9 col">
                                                                    <input type="text" class="form-control mobile-width" id="mobile_no" name="mobile_no" placeholder="Enter Mobile No" data-error=".vehicleErorr4" value="<?php echo isset($vehicle_list['driver_mobile']) ? substr($vehicle_list['driver_mobile'], 2) : ''; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="vehicleErorr4 error-msg"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group required row">
                                                    <label for="capacity" class="col-sm-2  control-label">Capacity</label>
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="capacity" name="capacity" placeholder="Capacity in number" data-error=".vehicleErorr5" value="<?php echo isset($vehicle_list['truck_capacity']) ? $vehicle_list['truck_capacity'] : ''; ?>">
                                                            <div class="vehicleErorr5 error-msg"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group required row">
                                                    <label for="enroll_year" class="col-sm-2 control-label">Enrollment Year</label>
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                            <select class="form-control custom-select" name="enroll_year" id="enroll_year" data-error=".vehicleErorr6">
                                                                <option value="">Select Year</option>
                        <?php
                        if (isset($year_list) && !empty($year_list)) {
                            foreach ($year_list as $year) {
                                if ($year['year'] >= date("Y")) {
                                    $selected = "";
                                    if ($year['id'] == $vehicle_list['participation_year_id']) {
                                        $selected = "selected";
                                    }
                                    ?>
                                                                                                                <option value="<?php echo $year['id'] ?>" <?php echo $selected; ?>><?php echo $year['year'] ?></option>
                                    <?php
                                }
                            }
                        }
                        ?>
                                                            </select>
                                                            <div class="vehicleErorr6 error-msg"></div>
                                                        </div>
                                                    </div>
                                                </div> -->
                        <hr class="my-4">
                        <div class="form-group row m-b-0 col">
                            <div class="offset-sm-11 col-sm-1 mr-5">
                                <button type="submit" class="btn btn-inverse update-button">Update</button>
                                <!--<button type="submit" class="btn btn-success waves-effect waves-light update-button">Cancel</button>-->
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

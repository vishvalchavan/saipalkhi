<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicle extends MY_Controller {

    public function __construct() {
        parent::__construct();
        
         if (!$this->ion_auth->logged_in()) {
            redirect('admin', 'refresh');
        }
        if (!$this->ion_auth->is_admin()) {
            redirect('admin', 'refresh');
        }
        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->model(array('Vehiclemodel', 'Year', 'language'));
        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('Auth');
    }

    public function index() {
        // if (!$this->ion_auth->logged_in()) {
        //     redirect('admin', 'refresh');
        // }
        // if (!$this->ion_auth->is_admin()) {
        //     redirect('admin', 'refresh');
        // }
        $user_id = $this->session->userdata('user_id');
        $year = $this->Year->get_year_id(date('Y'));
        $data['vehicle_list'] = $this->Vehiclemodel->get_vehicle_list($year);
        $data['year_list'] = $this->Year->get_year_list();
        $data['dataHeader']['title'] = 'Vehicle List';
        $this->template->set_master_template('template.php');
        $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
        $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
        $this->template->write_view('content', 'vehicle_list', (isset($this->data) ? $this->data : NULL), TRUE);
        $this->template->write_view('footer', 'snippets/footer', '', TRUE);
        $this->template->render();
    }

    public function vehicle_list() {
        if ($this->input->post('year')) {
            $data['vehicle_list'] = $this->Vehiclemodel->get_vehicle_list($this->input->post('year'));
            $view = $this->load->view('ajax/vehicle_list', $data);
            echo $view;
        }
    }

    public function add_vehicle() {
        // if (!$this->ion_auth->logged_in()) {
        //     redirect('admin', 'refresh');
        // }
        // if (!$this->ion_auth->is_admin()) {
        //     redirect('admin', 'refresh');
        // }
        $user_id = $this->session->userdata('user_id');
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->form_validation->set_rules('truck_name', 'Truck Name', 'required');
//            $this->form_validation->set_rules('trucknumber', 'Truck Number', 'required');
//            $this->form_validation->set_rules('driver_name', 'Driver Name', 'required');
//            $this->form_validation->set_rules('mobile_no', 'Driver Mobile no.', 'required');
            $this->form_validation->set_rules('capacity', 'Capacity', 'required');
            $this->form_validation->set_rules('enroll_year', 'Enrollment Year', 'required');
            if ($this->form_validation->run() == TRUE) {
                $truck_name = $this->input->post('truck_name');
//                $truck_number = $this->input->post('trucknumber');
                $truck_year = $this->input->post('enroll_year');
//                $check = $this->Vehiclemodel->if_vehicle_exists($truck_name, $truck_number, $truck_year);
//                if (count($check) == 0) {
                $vehicle_details = array(
                    'truck_name' => $this->input->post('truck_name'),
                    'truck_number' => $this->input->post('trucknumber'),
                    'driver_name' => $this->input->post('driver_name'),
                    'participation_year_id' => $truck_year,
                    'truck_capacity' => $this->input->post('capacity'),
                    'created_by' => $user_id,
                    'created_date' => date('Y-m-d'),
                    'isactive' => 1
                );
                if ($this->input->post('mobile_no')) {
                    $vehicle_details['driver_mobile'] = '91' . $this->input->post('mobile_no');
                }
                $response = $this->Vehiclemodel->insert($vehicle_details);
                if ($this->db->affected_rows() > 0) {
                    $this->session->set_flashdata('success_msg', 'Vehicle Details added successfully.');
                    redirect('admin/vehicle');
                } else {
                    $this->session->set_flashdata('error_msg', 'Failed to add Vehicle');
                    redirect('admin/vehicle');
                }
//                } else {
//                    $this->session->set_flashdata('error_msg', 'Vehicle details already exists.');
//                    redirect('admin/vehicle');
//                }
            }
        } else {
            $data['dataHeader']['title'] = 'Add Vehicle';
            $data['year_list'] = $this->Year->get_year_list();
            $year = $this->Year->get_year_id(date('Y'));
            $data['truck'] = $this->Vehiclemodel->get_vehicle($year);
            $this->template->set_master_template('template.php');
            $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
            $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
            $this->template->write_view('content', 'vehicle_add', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'snippets/footer', '', TRUE);
            $this->template->render();
        }
    }

    public function update_vehicle($vehicle_id) {
        // if (!$this->ion_auth->logged_in()) {
        //     redirect('admin', 'refresh');
        // }
        // if (!$this->ion_auth->is_admin()) {
        //     redirect('admin', 'refresh');
        // }
        $user_id = $this->session->userdata('user_id');
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->form_validation->set_rules('truck_name', 'Truck Name', 'required');
//            $this->form_validation->set_rules('trucknumber', 'Truck Number', 'required');
//            $this->form_validation->set_rules('driver_name', 'Driver Name', 'required');
//            $this->form_validation->set_rules('mobile_no', 'Driver Mobile no.', 'required');
            $this->form_validation->set_rules('capacity', 'Capacity', 'required');
            $this->form_validation->set_rules('enroll_year', 'Enrollment Year', 'required');
            if ($this->form_validation->run() == TRUE) {
                $truck_name = $this->input->post('truck_name');
                $truck_number = $this->input->post('trucknumber');
                $truck_year = $this->input->post('enroll_year');
                $check = $this->Vehiclemodel->if_vehicle_exists($truck_name, $truck_number, $truck_year);
                if ((count($check) == 1 && $check[0]->id == $vehicle_id) || count($check) == 0) {
                    $vehicle_details = array(
                        'truck_name' => $this->input->post('truck_name'),
                        'truck_number' => $this->input->post('trucknumber'),
                        'driver_name' => $this->input->post('driver_name'),
                        'participation_year_id' => $truck_year,
                        'truck_capacity' => $this->input->post('capacity'),
                        'modified_date' => date('Y-m-d')
                    );
                    if ($this->input->post('mobile_no')) {
                        $vehicle_details['driver_mobile'] = '91' . $this->input->post('mobile_no');
                    }
                    if ($this->Vehiclemodel->update($vehicle_id, $vehicle_details)) {
                        $this->session->set_flashdata('success_msg', 'Vehicle Details updated successfully.');
                        redirect('admin/vehicle');
                    } else {
                        $this->session->set_flashdata('error_msg', 'Failed to update Vehicle details');
                        redirect('admin/vehicle');
                    }
                } else {
                    $this->session->set_flashdata('error_msg', 'Vehicle details already exists.');
                    redirect('admin/vehicle');
                }
            }
        } else {
            $data['vehicle_list'] = $this->Vehiclemodel->get_allData($vehicle_id);
            $data['year_list'] = $this->Year->get_year_list();
            $data['dataHeader']['title'] = 'Edit Vehicle';
            $this->template->set_master_template('template.php');
            $this->template->write_view('header', 'snippets/header', (isset($data) ? $data : NULL));
            $this->template->write_view('sidebar', 'snippets/sidebar', (isset($this->data) ? $this->data : NULL));
            $this->template->write_view('content', 'vehicle_edit', (isset($this->data) ? $this->data : NULL), TRUE);
            $this->template->write_view('footer', 'snippets/footer', '', TRUE);
            $this->template->render();
        }
    }

    public function delete() {
        if ($this->input->post('vehicle_id')) {
            $vehicleid = $this->Vehiclemodel->delete_vehicle($this->input->post('vehicle_id'));
            if ($vehicleid) {
                $this->session->set_flashdata('success_msg', 'Vehicle deleted successfully.');
                echo 'success';
            } else {
                $this->session->set_flashdata('error_msg', 'Failed to delete Vehicle.');
                echo 'failed';
            }
        }
    }

    public function activate() {
        if ($this->input->post('vehicle_id')) {
            $activeid = $this->Vehiclemodel->activate_vehicle($this->input->post('vehicle_id'));
            if ($activeid) {
                $this->session->set_flashdata('success_msg', 'Vehicle activated successfully.');
                echo 'success';
            } else {
                $this->session->set_flashdata('error_msg', 'Failed to activate Vehicle.');
                echo 'failed';
            }
        }
    }
}

(function($, window, document, undefined) {
    var hiddencount=1;    
    window.addEventListener("dragover", function(e) {
        e = e || event;
        e.preventDefault();
    }, false);
    window.addEventListener("drop", function(e) {
        e = e || event;
        e.preventDefault();
    }, false);

    const compareMimeType = (mimeTypes, fileType, formatFile) => {        
        if (mimeTypes.length < 2 && mimeTypes[0] === "*") {
            return true;
        }        
        for (let index = 1; index < mimeTypes.length; index += 3) {
            if (mimeTypes[index + 1] === "*" && fileType.search(new RegExp(mimeTypes[index])) != -1) {
                return true;
            } else if (mimeTypes[index + 1] && mimeTypes[index + 1] != "*" && fileType.search(new RegExp("\\*" + mimeTypes[index + 1] + "\\*")) != -1) {
                return true;
            } else if (mimeTypes[index + 1] && mimeTypes[index + 1] != "*" && fileType.search(new RegExp(mimeTypes[index + 1])) != -1) {
                return true;
            } else if (mimeTypes[index + 1] === "" && (fileType.search(new RegExp(mimeTypes[index])) != -1 || formatFile.search(new RegExp(mimeTypes[index])) != -1)) {
                return true;
            }
        }
        return false;
    }
    $.fn.imageuploadify = function(opts) {
        const settings = $.extend({}, $.fn.imageuploadify.defaults, opts);        
        this.each(function() {
            const self = this;
            if (!$(self).attr("multiple")) {
                return;
            }
            let accept = $(self).attr("accept") ? $(self).attr("accept").replace(/\s/g, "").split(",") : null;
            let result = [];
            accept.forEach((item) => {
                let regexp;
                if (item.search(/\//) != -1) {
                    regexp = new RegExp("([A-Za-z-.]*)\/([A-Za-z-*.]*)", "g");
                } else {
                    regexp = new RegExp("\.([A-Za-z-]*)()", "g");
                }
                const r = regexp.exec(item);
                result = result.concat(r);
            });
            let totalFiles = [];
            let counter = 0;
            let dragbox = $(` <div class="showImgMsg error-msg"></div>
                               <div class="clickable-btn text-center imagediv">
                 <i class="fas fa-cloud-upload-alt" onclick="openExplorer()"></i><br>
                  <span class='imageuploadify-message' onclick="openExplorer()">Drag & Drop Your Photos Here To Upload</span>
                 </div>
                 <div class="imageuploadify well">                
                <div class="imageuploadify-overlay">
                <i class="fa fa-picture-o"></i>
                </div>
                <div class="imageuploadify-images-list text-center">
               
                 </div></div>`);
            let overlay = dragbox.find(".imageuploadify-overlay");
            let uploadIcon = dragbox.find(".imageuploadify-overlay i");
            let imagesList = dragbox.find(".imageuploadify-images-list");
            let addIcon = dragbox.find(".imageuploadify-images-list i");
            let addMsg = dragbox.find(".imageuploadify-images-list span");
            let button = dragbox.find(".imageuploadify-images-list button");
            const retrieveFiles = (files) => {                 
                for (let index = 0; index < files.length; ++index) {                    
                    if (!accept || compareMimeType(result, files[index].type, /(?:\.([^.]+))?$/.exec(files[index].name)[1])) {
                        const id = Math.random().toString(36).substr(2, 9);
                        readingFile(id, files[index]);
                       
                        totalFiles.push({
                            id: id, 
                            file: files[index]
                        });                    
                    }
                }
            }
            const readingFile = (id, file) => {
                const fReader = new FileReader();
                const width = dragbox.width();
                const boxesNb = Math.floor(width / 100);
                const marginSize = Math.floor((width - (boxesNb * 100)) / (boxesNb + 1));                
                let container = $(`<div class='imageuploadify-container' style='width:24%;' id='imgMainDiv_`+hiddencount+`'>
                    <button type='button' class='btn btn-danger' >X</button>
                    <div class='imageuploadify-details'>
                    <span class='img_name' id="img_name_`+hiddencount+`" name='${file.name}'>${file.name}</span>                        
                    <span>${file.type}</span>
                    <span>${file.size}</span>
                    </div>

<input type='text' id="title_`+hiddencount+`" name="title_`+hiddencount+`" class="imgname" value='' onchange="checkMaxlength(this,`+hiddencount+`)" placeholder='Enter photo title' style='background: gainsboro;position: absolute;    z-index: 99999;
    width: 91%;margin-top: 110px; margin-left: 0%; float: left;left: 5%;padding: 3px;border: navajowhite;border-radius: 5px;' data-error=".albumErorr_`+hiddencount+`" /><div class="albumErorr_`+hiddencount+` error-msg"></div>
                      <label class="check-select">
                    <input type='radio' id="chk_`+hiddencount+`" name="cover_photo" value="chk_`+hiddencount+`"  title="add to album title" value='1' style='position: absolute;
    z-index: 999999;float: left;margin-top: 3%;margin-left: -41%;' data-toggle="tooltip" data-placement="top" data-error=".showImgMsg" />
          <span class="checkmark"></span></label>

                    </div>`);
                var textid='title_'+hiddencount;
                
                $( ".addnew-div" ).insertAfter(container);

                 let hidden = $("<input type='hidden'>");
                 hidden.attr("name","hiddeninput_"+hiddencount);
                let details = container.find(".imageuploadify-details");
                let deleteBtn = container.find("button");
                        $("#lastcount").val(hiddencount);
                container.css("margin-left", marginSize + "px");
                details.hover(function() {
                    $(this).css("opacity", "1");
                }).mouseleave(function() {
                    $(this).css("opacity", "0");
                });

                if (file.type && file.type.search(/image/) != -1) { 
                    fReader.onloadend = function(e) {
                        let image = $("<img>");
                        image.attr("src", e.target.result);
                        image.attr("class", 'upload-img');
                        image.attr("id", 'upload_imgid');
                        container.append(image);
                        
                        // ------------------------------------------------------------    
                        var img = new Image;
                        img.src=e.target.result;
                        img.onload = function() {
                            var base64String = resizeImg(img, 700, 700, 0); //HERE IS WHERE THE FUNCTION RESIZE IS CALLED!!!!                            
                            hidden.attr("value",base64String);
                          }
                          // ------------------------------------------------------------ 
//                        hidden.attr("value",e.target.result);

                        container.append(hidden);
                        imagesList.append(container);
                        imagesList.find(".imageuploadify-container:nth-child(" + boxesNb + "n+4)").css("margin-left", marginSize + "px");
                        imagesList.find(".imageuploadify-container:nth-child(" + boxesNb + "n+3)").css("margin-right", marginSize + "px");
                    };
                    var imglength;
                    if($(".upload-img").length == 0)
                    { imglength=$(".upload-img").length+1;}else{imglength=$(".upload-img").length;}
                    $("#imglength").val(imglength); 
               } else if (file.type) {
                    let type = "<i class='fa fa-file'></i>";
                    if (file.type.search(/audio/) != -1) {
                        type = "<i class='fa fa-file-audio-o'></i>";
                    } else if (file.type.search(/video/) != -1) {
                        type = "<i class='fa fa-file-video-o'></i>";
                    }
                    fReader.onloadend = function(e) {
                        let span = $("<span>" + type + "</span>");
                        span.css("font-size", "5em");
                        container.append(span);
                        imagesList.append(container);
                        imagesList.find(".imageuploadify-container:nth-child(" + boxesNb + "n+4)").css("margin-left", marginSize + "px");
                        imagesList.find(".imageuploadify-container:nth-child(" + boxesNb + "n+3)").css("margin-right", marginSize + "px");
                    };
                }
                deleteBtn.on("click", function() {
//                    console.log(totalFiles);
                    $(this.parentElement).remove();
                    for (let index = 0; totalFiles.length > index; ++index) {
                        if (totalFiles[index].id === id) { 
                            console.log(totalFiles.splice(index, 1));
                            totalFiles.splice(index, 1);
                            $("#imglength").val($(".upload-img").length);
                            $("#abcd").val('');
                            break;
                        }
                    }
                     
                });
                fReader.readAsDataURL(file);
                hiddencount++;
            };
            const disableMouseEvents = () => {
                overlay.css("display", "flex");
                dragbox.css("border-color", "#3AA0FF");
                button.css("pointer-events", "none");
                addMsg.css("pointer-events", "none");
                addIcon.css("pointer-events", "none");
                imagesList.css("pointer-events", "none");
            }
            const enableMouseEvents = () => {
                overlay.css("display", "none");
                dragbox.css("border-color", "rgb(210, 210, 210)");
                button.css("pointer-events", "initial");
                addMsg.css("pointer-events", "initial");
                addIcon.css("pointer-events", "initial");
                imagesList.css("pointer-events", "initial");
            }
            button.mouseenter(function onMouseEnter(event) {
                button.css("background", "#3db7ba").css("color", "#3db7ba");
            }).mouseleave(function onMouseLeave() {
                button.css("background", "#3db7ba").css("color", "#3db7ba");
            });
            button.on("click", function onClick(event) {
                event.stopPropagation();
                event.preventDefault();
                $(self).click();
            });
            dragbox.on("dragenter", function onDragenter(event) {
                event.stopPropagation();
                event.preventDefault();
                counter++;
                disableMouseEvents();
            });
            dragbox.on("dragleave", function onDragLeave(event) {
                event.stopPropagation();
                event.preventDefault();
                counter--;
                if (counter === 0) {
                    enableMouseEvents();
                }
            });
            dragbox.on("drop", function onDrop(event) {
                event.stopPropagation();
                event.preventDefault();
                enableMouseEvents();
                const files = event.originalEvent.dataTransfer.files;
                retrieveFiles(files);
            });
            $(window).bind("resize", function(e) {
                window.resizeEvt;
                $(window).resize(function() {
                    clearTimeout(window.resizeEvt);
                    window.resizeEvt = setTimeout(function() {
                        const width = dragbox.width();
                        const boxesNb = Math.floor(width / 100);
                        const marginSize = Math.floor((width - (boxesNb * 100)) / (boxesNb + 1));
                        let containers = imagesList.find(".imageuploadify-container");
                        for (let index = 0; index < containers.length; ++index) {
                            $(containers[index]).css("margin-right", "0px");
                            $(containers[index]).css("margin-left", marginSize + "px");
                        }
                        imagesList.find(".imageuploadify-container:nth-child(" + boxesNb + "n+4)").css("margin-left", marginSize + "px");
                        imagesList.find(".imageuploadify-container:nth-child(" + boxesNb + "n+3)").css("margin-right", marginSize + "px");
                    }, 500);
                });
            })
            $(self).on("change", function onChange() {                
                $(".messenger-message").remove();
                const files = this.files;
                const inputfile=files[0].name;                
                const uploaded = $(".img_name");
                var flag='';
                var img_name=document.getElementsByName("img_name");
                const inputs =files[0].name;
                uploaded.each(function(){                    
                     if($(this).attr('name') == inputfile){
                        flag='true';
                        return false;
                    }else{
                      flag='false';  
                    }                 
                });                
                    if(flag == 'true'){                        
                        showErrorMessage("Image already added");
                    } else{
                    retrieveFiles(files);
                    }
//                retrieveFiles(files);
            });
            // $(self).closest("form").on("submit", function(event) {
            //     event.stopPropagation();
            //     event.preventDefault(event);
            //     const inputs = this.querySelectorAll("input, textarea, select, button");
            //     const formData = new FormData();
            //     for (let index = 0; index < inputs.length; ++index) {
            //         if (inputs[index].tagName === "SELECT" && inputs[index].hasAttribute("multiple")) {
            //             const options = inputs[index].options;
            //             for (let i = 0; options.length > i; ++i) {
            //                 if (options[i].selected) {
            //                     formData.append(inputs[index].getAttribute("name"), options[i].value);
            //                 }
            //             }
            //         } else if (!inputs[index].getAttribute("type") || ((inputs[index].getAttribute("type").toLowerCase()) !== "checkbox" && (inputs[index].getAttribute("type").toLowerCase()) !== "radio") || inputs[index].checked) {
            //             formData.append(inputs[index].name, inputs[index].value);
            //         }
            //          else if ($(inputs[index]).getAttribute("type") != "file") {
            //             formData.append(inputs[index].name, inputs[index].value);
            //         }
            //     }
            //     for (var i = 0; i < totalFiles.length; i++) {
            //         formData.append(self.name, totalFiles[i].file);
            //     }
            //     var xhr = new XMLHttpRequest();
            //     xhr.onreadystatechange = function(e) {
            //         if (xhr.status == 200 && xhr.readyState === XMLHttpRequest.DONE) {
            //             window.location.replace(xhr.responseURL);
            //         }
            //     }
            //     xhr.open("POST", $(this).attr("action"), true);
            //     xhr.send(formData);
            //     return false;
            // });
            $(self).hide();
            dragbox.insertAfter(this);
        });
        return this;
    };
    $.fn.imageuploadify.defaults = {};
}(jQuery, window, document));


/*  */
// add/remove checked class
$(".image-checkbox").each(function(){
    if($(this).find('input[type="checkbox"]').first().attr("checked")){
        $(this).addClass('image-checkbox-checked');
    }else{
        $(this).removeClass('image-checkbox-checked');
    }
});

// sync the input state
$(".image-checkbox").on("click", function(e){
    $(this).toggleClass('image-checkbox-checked');
    var $checkbox = $(this).find('input[type="checkbox"]');
    $checkbox.prop("checked",!$checkbox.prop("checked"));  
    e.preventDefault();
});

/* Selected Image*/
$(document).ready(function($) {
  $('#close').click(function() {    
   $(this).remove('#Image').hide();
  });
});
/* Pop-Over Image*/
   $('body').append('<div class="product-image-overlay"><span class="product-image-overlay-close">x</span><img src="" /></div>');
    var productImage = $('img');
    var productOverlay = $('.product-image-overlay');
    var productOverlayImage = $('.product-image-overlay img');

    productImage.click(function () {
    var productImageSource = $(this).attr('src');

    productOverlayImage.attr('src', productImageSource);
    productOverlay.fadeIn(100);
    $('body').css('overflow', 'hidden');
    $('.product-image-overlay-close').click(function () {
    productOverlay.fadeOut(100);
    $('body').css('overflow', 'auto');
    });
});
   /*
-------------------------------
-------RESIZING FUNCTION-------
-------------------------------
*/


function resizeImg(img, maxWidth, maxHeight, degrees) {
  var imgWidth = img.width,
    imgHeight = img.height;

  var ratio = 1,
    ratio1 = 1,
    ratio2 = 1;
  ratio1 = maxWidth / imgWidth;
  ratio2 = maxHeight / imgHeight;

  // Use the smallest ratio that the image best fit into the maxWidth x maxHeight box.
  if (ratio1 < ratio2) {
    ratio = ratio1;
  } else {
    ratio = ratio2;
  }
  var canvas = document.createElement("canvas");
  var canvasContext = canvas.getContext("2d");
  var canvasCopy = document.createElement("canvas");
  var copyContext = canvasCopy.getContext("2d");
  var canvasCopy2 = document.createElement("canvas");
  var copyContext2 = canvasCopy2.getContext("2d");
  canvasCopy.width = imgWidth;
  canvasCopy.height = imgHeight;
  copyContext.drawImage(img, 0, 0);

  // init
  canvasCopy2.width = imgWidth;
  canvasCopy2.height = imgHeight;
  copyContext2.drawImage(canvasCopy, 0, 0, canvasCopy.width, canvasCopy.height, 0, 0, canvasCopy2.width, canvasCopy2.height);


  var rounds = 1;
  var roundRatio = ratio * rounds;
  for (var i = 1; i <= rounds; i++) {


    // tmp
    canvasCopy.width = imgWidth * roundRatio / i;
    canvasCopy.height = imgHeight * roundRatio / i;

    copyContext.drawImage(canvasCopy2, 0, 0, canvasCopy2.width, canvasCopy2.height, 0, 0, canvasCopy.width, canvasCopy.height);

    // copy back
    canvasCopy2.width = imgWidth * roundRatio / i;
    canvasCopy2.height = imgHeight * roundRatio / i;
    copyContext2.drawImage(canvasCopy, 0, 0, canvasCopy.width, canvasCopy.height, 0, 0, canvasCopy2.width, canvasCopy2.height);

  } // end for

  canvas.width = imgWidth * roundRatio / rounds;
  canvas.height = imgHeight * roundRatio / rounds;
  canvasContext.drawImage(canvasCopy2, 0, 0, canvasCopy2.width, canvasCopy2.height, 0, 0, canvas.width, canvas.height);


  if (degrees == 90 || degrees == 270) {
    canvas.width = canvasCopy2.height;
    canvas.height = canvasCopy2.width;
  } else {
    canvas.width = canvasCopy2.width;
    canvas.height = canvasCopy2.height;
  }

  canvasContext.clearRect(0, 0, canvas.width, canvas.height);
  if (degrees == 90 || degrees == 270) {
    canvasContext.translate(canvasCopy2.height / 2, canvasCopy2.width / 2);
  } else {
    canvasContext.translate(canvasCopy2.width / 2, canvasCopy2.height / 2);
  }
  canvasContext.rotate(degrees * Math.PI / 180);
  canvasContext.drawImage(canvasCopy2, -canvasCopy2.width / 2, -canvasCopy2.height / 2);


  var dataURL = canvas.toDataURL();
  return dataURL;
}
function checkMaxlength(input,inputNo)
{
    var inputTxt=input.name;    
     $("input[name="+inputTxt+"]").each(function( ){
             if(this.value.length > 50){
//                    $(".albumErorr_"+inputNo).html("Maximum 50 digits are allowed");
                    $(".title_"+inputNo+"-error").css("display",'none');
                    $(this).attr("required",true);                    
                    $(this).attr('maxlength','50');
                }else{
                    $(".albumErorr3").html('');        
                    $(this).attr("required",false);
                }
     })
}


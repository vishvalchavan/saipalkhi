$('#devotee_list').DataTable({
    responsive: true,
    dom: 'Bfrtip',
        buttons: [{
        extend: 'excel',
        exportOptions: {
            columns: [0,1,2,3,4]
        }
    },{
        extend: 'pdf',
        exportOptions: {
            columns: [0,1,2,3,4]
        }
    },{
        extend: 'print',
        exportOptions: {
            columns: [0,1,2,3,4]
        }
    }]
});
$('#padyatri_list').DataTable({
    responsive: true,   
    dom: 'Bfrtip',    
    buttons: [{
        extend: 'excel',
        exportOptions: {
            columns: [0, 1, 2, 3,4]
        }
    },{
        extend: 'pdf',
        exportOptions: {
            columns: [0, 1, 2, 3,4]
        }
    },{
        extend: 'print',
        exportOptions: {
            columns: [0, 1, 2, 3,4]
        }
    }]
});
$('#samiti_list').DataTable({
    dom: 'Bfrtip',
    responsive: true,
    buttons: [{
        extend: 'excel',
        exportOptions: {
            columns: [0, 1, 2, 3,4,5]
        }
    },{
        extend: 'pdf',
        exportOptions: {
            columns: [0, 1, 2, 3,4,5]
        }
    },{
        extend: 'print',
        exportOptions: {
            columns: [0, 1, 2, 3,4,5]
        }
    }]
});
$('#dindi_list').DataTable({
    dom: 'Bfrtip',
    responsive: true,
    buttons: [{
        extend: 'excel',
        exportOptions: {
            columns: [0, 1, 2, 3,4,5,6]
        }
    },{
        extend: 'pdf',
        exportOptions: {
            columns: [0, 1, 2, 3,4,5,6]
        }
    },{
        extend: 'print',
        exportOptions: {
            columns: [0, 1, 2, 3,4,5,6]
        }
    }]
});
$('#vehicle_list').DataTable({
    dom: 'Bfrtip',
    responsive: true,
    buttons: [{
        extend: 'excel',
        exportOptions: {
            columns: [0, 1, 2, 3,4,5]
        }
    },{
        extend: 'pdf',
        exportOptions: {
            columns: [0, 1, 2, 3,4,5]
        }
    },{
        extend: 'print',
        exportOptions: {
            columns: [0, 1, 2, 3,4,5]
        }
    }]
});
$('#donation_list').DataTable({
    dom: 'Bfrtip',
    responsive: true,
    buttons: [{
        extend: 'excel',
        exportOptions: {
            columns: [0, 1, 2, 3,4,5,6,7,8]
        }
    },{
        extend: 'pdf',
        exportOptions: {
            columns: [0, 1, 2, 3,4,5,6,7,8]
        }
    },{
        extend: 'print',
        exportOptions: {
            columns: [0, 1, 2, 3,4,5,6,7,8]
        }
    }]
});
$('#dindi_report').DataTable({
    dom: 'Bfrtip',
    buttons: ['excel', 'pdf', 'print']
});
$('#devotee_enrollment').DataTable({
    dom: 'Bfrtip',
    buttons: ['excel', 'pdf', 'print']
});
